package com.fma.wb.integration.reportsmodule.utils.io;

import com.fma.wb.integration.common.utils.Utils;
import com.fma.wb.integration.reportsmodule.dto.trust.TrustBalancingOutputData;
import io.vertx.core.json.Json;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class TrustExcelFileGenerator {
    private List<TrustBalancingOutputData> data =  new ArrayList<>();
    private Workbook workbook = new XSSFWorkbook();
    private static String[] columns = {"INFINITY DATE", "DEBTOR", "FIRST NAME",
            "LAST NAME","CLIENT ACCT","BUS STREAM","CC","UNKNOWN","MANUAL","NSF","INFINITY"," ","RP DATE","ACH","ICL","RP SOLUTIONS","MATCH"};
    // This holds the infinity grand total for the whole entire month in double format
    double infinityBigTotal = 0.00;


    public TrustExcelFileGenerator() {
    }

    public TrustExcelFileGenerator(List<TrustBalancingOutputData> data) {
        this.data = data;
    }


    public String getBase64GeneratedFile(){

        Sheet sheet = workbook.createSheet("Trust"+System.currentTimeMillis());

        AtomicInteger r= new AtomicInteger();

        Map<String, List<TrustBalancingOutputData>> datasGrouped = this.data.stream().collect(sortedGroupingBy(TrustBalancingOutputData::getInfDate));

        //Style for infinity grand total
        CellStyle styleHeaderI = workbook.createCellStyle();
        Font fontI = workbook.createFont();
        fontI.setItalic(true);
        fontI.setFontHeightInPoints((short) 13);
        fontI.setBold(true);

        fontI.setColor(IndexedColors.WHITE.getIndex());
        styleHeaderI.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
        styleHeaderI.setFillBackgroundColor(IndexedColors.DARK_BLUE.getIndex());
        styleHeaderI.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleHeaderI.setFont(fontI);
        styleHeaderI.setAlignment(HorizontalAlignment.CENTER);
        //end style for infinity grand total

        DataFormat customFormat=workbook.createDataFormat();

        short csFormat=customFormat.getFormat("$###,##0.00");

        //System.out.println("MP:\n"+Json.encodePrettily(datasGrouped));

        datasGrouped.forEach((s, trustBalancingOutputDataList) -> {

            // Create a Font for styling header cells
            Font headerFont = workbook.createFont();
            Font blueFont=workbook.createFont();
            //headerFont.setBold(true);
            //blueFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 12);
            headerFont.setItalic(true);
            blueFont.setColor(IndexedColors.BLUE.getIndex());
            blueFont.setFontHeightInPoints((short) 12);
            blueFont.setItalic(true);

            // Create a CellStyle with the font
            CellStyle headerCellStyle = workbook.createCellStyle();
            CellStyle blankStyle = workbook.createCellStyle();
            CellStyle blueStyle = workbook.createCellStyle();

            headerCellStyle.setFont(headerFont);
            blankStyle.setFont(headerFont);
            blueStyle.setFont(blueFont);

            blankStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            blankStyle.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            blankStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            blueStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            blueStyle.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            blueStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            headerCellStyle.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            // Create a Row
            Row headerRow = sheet.createRow(r.getAndIncrement());

            // Create cells
            for(int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);


                if(columns[i].trim().isEmpty()){
                    cell.setBlank();
                    cell.setCellStyle(blankStyle);
                }else if(i==11){
                    cell.setCellValue(columns[i]);
                    cell.setCellStyle(blueStyle);
                }
                else{
                    cell.setCellValue(columns[i]);
                    cell.setCellStyle(headerCellStyle);
                }

            }

            Font font = workbook.createFont();
            font.setFontHeightInPoints((short) 10);
            font.setItalic(true);

            Font font2 = workbook.createFont();
            font2.setFontHeightInPoints((short) 10);
            font2.setColor(IndexedColors.BLUE.getIndex());

            Font font3 = workbook.createFont();
            font3.setFontHeightInPoints((short) 10);
            font3.setColor(IndexedColors.GREEN.getIndex());
            font3.setItalic(true);

            CellStyle style = workbook.createCellStyle();
            style.setFont(font);

            //Style used for all currency amount
            CellStyle styleCurrency = workbook.createCellStyle();
            styleCurrency.setFont(font);
            styleCurrency.setDataFormat(csFormat);


            CellStyle style2 = workbook.createCellStyle();
            style2.setFont(font2);
            style2.setDataFormat(csFormat);

            CellStyle style3 = workbook.createCellStyle();
            style3.setFont(font3);
            style3.setDataFormat(csFormat);


            Font fontF0 = workbook.createFont();
            //fontF0.setItalic(true);
            //fontF0.setFontHeightInPoints((short) 13);
            fontF0.setBold(true);

            CellStyle styleHeaderR = workbook.createCellStyle();
            styleHeaderR.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            styleHeaderR.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            styleHeaderR.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleHeaderR.setFont(fontF0);



            CellStyle styleHeaderR0 = workbook.createCellStyle();
            styleHeaderR0.setFont(fontF0);
            styleHeaderR0.setBorderTop(BorderStyle.THIN);



            CellStyle styleHeaderR1 = workbook.createCellStyle();
            styleHeaderR1.setFont(fontF0);


            CellStyle styleHeaderF = workbook.createCellStyle();
            Font fontF = workbook.createFont();
            fontF.setItalic(true);
            fontF.setFontHeightInPoints((short) 13);
            fontF.setBold(true);

            fontF.setColor(IndexedColors.WHITE.getIndex());
            styleHeaderF.setFillForegroundColor(IndexedColors.GREEN.getIndex());
            styleHeaderF.setFillBackgroundColor(IndexedColors.GREEN.getIndex());
            styleHeaderF.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleHeaderF.setFont(fontF);
            styleHeaderF.setAlignment(HorizontalAlignment.CENTER);

            CellStyle styleValR = workbook.createCellStyle();
            styleValR.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
            styleValR.setFillBackgroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
            styleValR.setFillPattern(FillPatternType.SOLID_FOREGROUND);


            blankStyle.setFont(font);
            blankStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            blankStyle.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            blankStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);


            ArrayList<String> identifiers= new ArrayList<>();
            ArrayList<String> identifiers2= new ArrayList<>();

            trustBalancingOutputDataList.stream().filter(trustBalancingOutputData1 -> !trustBalancingOutputData1.getListTrust().get("clientIdentifier").isEmpty()).forEach(dt -> {

                if(!Utils.isNullOrEmpty(dt.getListTrust().get("cc")))
                    identifiers.add(dt.getListTrust().get("clientIdentifier")+","+dt.getListTrust().get("cc"));
                else if(!Utils.isNullOrEmpty(dt.getListTrust().get("unknown")))
                    identifiers.add(dt.getListTrust().get("clientIdentifier")+","+dt.getListTrust().get("unknown"));
                else if(!Utils.isNullOrEmpty(dt.getListTrust().get("manual")))
                    identifiers.add(dt.getListTrust().get("clientIdentifier")+","+dt.getListTrust().get("manual"));
                if(!Utils.isNullOrEmpty(dt.getListTrust().get("nsf")))
                    identifiers2.add(dt.getListTrust().get("clientIdentifier")+","+dt.getListTrust().get("nsf"));
            });


            //Enhancement begin
            ArrayList<String> identifiersEnhancement = new ArrayList<>();
            ArrayList<String> identifiers2Enhancement = new ArrayList<>();

            trustBalancingOutputDataList.stream().filter(trustBalancingOutputData1 -> !trustBalancingOutputData1.getListTrust().get("clientId").isEmpty()).forEach(dt -> {

                if(!Utils.isNullOrEmpty(dt.getListTrust().get("cc")))
                    identifiersEnhancement.add(dt.getListTrust().get("clientId")+","+dt.getListTrust().get("cc"));
                else if(!Utils.isNullOrEmpty(dt.getListTrust().get("unknown")))
                    identifiersEnhancement.add(dt.getListTrust().get("clientId")+","+dt.getListTrust().get("unknown"));
                else if(!Utils.isNullOrEmpty(dt.getListTrust().get("manual")))
                    identifiersEnhancement.add(dt.getListTrust().get("clientId")+","+dt.getListTrust().get("manual"));
                if(!Utils.isNullOrEmpty(dt.getListTrust().get("nsf")))
                    identifiers2Enhancement.add(dt.getListTrust().get("clientId")+","+dt.getListTrust().get("nsf"));
            });
            //Enhancement end


            List<String> identifiersOwn = new ArrayList<>();
            List<String> identifiersOwn2 =  new ArrayList<>();
            //Enhancement begin
            List<String> identifiersOwnEnhancement = new ArrayList<>();
            List<String> identifiersOwn2Enhancement = new ArrayList<>();
            //Enhancement end
            identifiers.forEach(s13 -> identifiersOwn.add(s13.split(",")[0]));
            identifiers2.forEach(s13 -> identifiersOwn2.add(s13.split(",")[0]));
            //Enhancement begin
            identifiersEnhancement.forEach(s13 -> identifiersOwnEnhancement.add(s13.split(",")[0]));
            identifiers2Enhancement.forEach(s13 -> identifiersOwn2Enhancement.add(s13.split(",")[0]));
            //Enhancement end

            Map<String, Long> result = identifiersOwn.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

            Map<String, Long> result2 = identifiersOwn2.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

            //Enhancement begin
            Map<String, Long> resultEnhancement = identifiersOwnEnhancement.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            Map<String, Long> result2Enhancement = identifiersOwn2Enhancement.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            //Enhancement end

            Map<String, Double> resultfinal=new HashMap<>();
            Map<String, Double> resultfinal2=new HashMap<>();

            //Enhancement begin
            Map<String, Double> resultfinalEnhancement = new HashMap<>();
            Map<String, Double> resultfinal2Enhancement = new HashMap<>();
            //Enhancement end

            result.forEach((s1, aLong) -> {

                final double[] somme = {0.00};
                identifiers.forEach((s2) -> {
                    if(s2.split(",")[0].equals(s1))
                        somme[0] +=Double.valueOf(s2.split(",")[1]);
                });

                resultfinal.put(s1, somme[0]);
            });


            result2.forEach((s1, aLong) -> {
                final double[] somme2 = {0.00};
                identifiers2.forEach((s2) -> {
                    if(s2.split(",")[0].equals(s1))
                        somme2[0] +=Double.valueOf(s2.split(",")[1]);
                });

                resultfinal2.put(s1, somme2[0]);
            });

            System.out.println(identifiers.size());
            System.out.println("RECOVER ID: \n"+ Json.encodePrettily(resultfinal));
            System.out.println("RECOVER NSF: \n"+ Json.encodePrettily(resultfinal2));

            //Enhancement begin
            resultEnhancement.forEach((s1, aLong) -> {

                final double[] sommeEnhancement = {0.00};
                identifiersEnhancement.forEach((s2) -> {
                    if(s2.split(",")[0].equals(s1))
                        sommeEnhancement[0] +=Double.valueOf(s2.split(",")[1]);
                });

                resultfinalEnhancement.put(s1, sommeEnhancement[0]);
            });


            result2Enhancement.forEach((s1, aLong) -> {
                final double[] somme2Enhancement = {0.00};
                identifiers2Enhancement.forEach((s2) -> {
                    if(s2.split(",")[0].equals(s1))
                        somme2Enhancement[0] +=Double.valueOf(s2.split(",")[1]);
                });

                resultfinal2Enhancement.put(s1, somme2Enhancement[0]);
            });

            System.out.println(identifiersEnhancement.size());
            System.out.println("CLIENT ID: \n"+ Json.encodePrettily(resultfinalEnhancement));
            System.out.println("CLIENT NSF: \n"+ Json.encodePrettily(resultfinal2Enhancement));
            //Enhancement end

            //int rw=1;
            for (TrustBalancingOutputData trustBalancingOutputData : trustBalancingOutputDataList) {

                Row row = sheet.createRow(r.getAndIncrement());
                Cell cell0=row.createCell(0);
                cell0.setCellStyle(style);
                cell0.setCellValue(trustBalancingOutputData.getListTrust().get("infinityDate"));

                Cell cell1=row.createCell(1);
                cell1.setCellStyle(style);
                cell1.setCellValue(trustBalancingOutputData.getListTrust().get("debtorId"));

                Cell cell2=row.createCell(2);
                cell2.setCellStyle(style);
                cell2.setCellValue(trustBalancingOutputData.getListTrust().get("firstName"));

                Cell cell3=row.createCell(3);
                cell3.setCellStyle(style);
                cell3.setCellValue(trustBalancingOutputData.getListTrust().get("lastName"));

                Cell cell4=row.createCell(4);
                cell4.setCellStyle(style);
                cell4.setCellValue(trustBalancingOutputData.getListTrust().get("clientAccountNumber"));

                Cell cell5=row.createCell(5);
                cell5.setCellStyle(style);
                cell5.setCellValue(trustBalancingOutputData.getListTrust().get("busStream"));

                Cell cell6=row.createCell(6);
                cell6.setCellStyle(styleCurrency);

                if (!Utils.isNullOrEmpty(trustBalancingOutputData.getListTrust().get("cc")))
                    cell6.setCellValue(Double.valueOf(trustBalancingOutputData.getListTrust().get("cc")));

                Cell cell7=row.createCell(7);
                cell7.setCellStyle(styleCurrency);
                if (!Utils.isNullOrEmpty(trustBalancingOutputData.getListTrust().get("unknown")))
                    cell7.setCellValue(Double.valueOf(trustBalancingOutputData.getListTrust().get("unknown")));


                Cell cell8=row.createCell(8);
                cell8.setCellStyle(styleCurrency);
                if (!Utils.isNullOrEmpty(trustBalancingOutputData.getListTrust().get("manual")))
                    cell8.setCellValue(Double.valueOf(trustBalancingOutputData.getListTrust().get("manual")));


                Cell cell9=row.createCell(9);
                cell9.setCellStyle(styleCurrency);
                if (!Utils.isNullOrEmpty(trustBalancingOutputData.getListTrust().get("nsf")))
                    cell9.setCellValue(Double.valueOf(trustBalancingOutputData.getListTrust().get("nsf")));

                Cell infCell = row.createCell(10);
                infCell.setCellStyle(style2);

                if (!Utils.isNullOrEmpty(trustBalancingOutputData.getListTrust().get("infinity")))
                    infCell.setCellValue(Double.valueOf(trustBalancingOutputData.getListTrust().get("infinity")));


                Cell emptyCell = row.createCell(11);
                emptyCell.setCellStyle(blankStyle);
                emptyCell.setBlank();

                Cell cell12=row.createCell(12);
                cell12.setCellStyle(style);
                cell12.setCellValue(trustBalancingOutputData.getListTrust().get("rpDate"));

                Cell cell13=row.createCell(13);
                cell13.setCellStyle(styleCurrency);

                if (!Utils.isNullOrEmpty(trustBalancingOutputData.getListTrust().get("ach")))
                    cell13.setCellValue(Double.valueOf(trustBalancingOutputData.getListTrust().get("ach")));

                Cell cell14=row.createCell(14);
                cell14.setCellStyle(styleCurrency);

                if (!Utils.isNullOrEmpty(trustBalancingOutputData.getListTrust().get("icl")))
                    cell14.setCellValue(Double.valueOf(trustBalancingOutputData.getListTrust().get("icl")));

                Cell cellRpS = row.createCell(15);
                cellRpS.setCellStyle(style3);

                if (!Utils.isNullOrEmpty(trustBalancingOutputData.getListTrust().get("rpSolutions")))
                    cellRpS.setCellValue(Double.valueOf(trustBalancingOutputData.getListTrust().get("rpSolutions")));

                Cell cell16=row.createCell(16);
                cell16.setCellStyle(style);
                cell16.setCellValue((trustBalancingOutputData.getListTrust().get("matchFound")==null) ? "NO" : "YES");

            }



            Row rowDate = sheet.createRow(r.getAndIncrement());
            Cell totalDate=rowDate.createCell(0);

            totalDate.setCellValue(trustBalancingOutputDataList.get(0).getInfDate());

            Cell total1=rowDate.createCell(6);
            styleHeaderR1.setDataFormat(csFormat);
            total1.setCellStyle(styleHeaderR1);

            double ccTotal;
            ccTotal=trustBalancingOutputDataList.stream()
                    .mapToDouble(x -> Double.valueOf((Utils.isNullOrEmpty(x.getListTrust().get("cc")))?"0.00":x.getListTrust().get("cc")))
                    .sum();
            total1.setCellValue(ccTotal);



            Cell total2=rowDate.createCell(7);
            total2.setCellStyle(styleHeaderR1);
            double unknownTotal;
            unknownTotal=trustBalancingOutputDataList.stream()
                    .mapToDouble(x -> Double.valueOf((Utils.isNullOrEmpty(x.getListTrust().get("unknown")))?"0.00":x.getListTrust().get("unknown")))
                    .sum();
            total2.setCellValue(unknownTotal);

            Cell total3=rowDate.createCell(8);
            total3.setCellStyle(styleHeaderR1);
            double manualTotal;
            manualTotal=trustBalancingOutputDataList.stream()
                    .mapToDouble(x -> Double.valueOf((Utils.isNullOrEmpty(x.getListTrust().get("manual")))?"0.00":x.getListTrust().get("manual")))
                    .sum();
            total3.setCellValue(manualTotal);



            Cell total4=rowDate.createCell(9);
            total4.setCellStyle(styleHeaderR1);
            double nsfTotal;
            nsfTotal=trustBalancingOutputDataList.stream()
                    .mapToDouble(x -> Double.valueOf((Utils.isNullOrEmpty(x.getListTrust().get("nsf")))?"0.00":x.getListTrust().get("nsf")))
                    .sum();
            total4.setCellValue(nsfTotal);

            Cell total5=rowDate.createCell(10);
            total5.setCellStyle(styleHeaderR1);
            double infinityTotal;
            infinityTotal=trustBalancingOutputDataList.stream()
                    .mapToDouble(x -> Double.valueOf((Utils.isNullOrEmpty(x.getListTrust().get("infinity")))?"0.00":x.getListTrust().get("infinity")))
                    .sum();
            infinityBigTotal = infinityBigTotal + infinityTotal;
            total5.setCellValue( infinityTotal);

            Cell total6=rowDate.createCell(13);
            total6.setCellStyle(styleHeaderR1);
            double achTotal ;
            achTotal=trustBalancingOutputDataList.stream()
                    .mapToDouble(x -> Double.valueOf((Utils.isNullOrEmpty(x.getListTrust().get("ach")))?"0.00":x.getListTrust().get("ach")))
                    .sum();
            total6.setCellValue(achTotal);


            Cell total7=rowDate.createCell(14);
            total7.setCellStyle(styleHeaderR1);
            double iclTotal;
            iclTotal=trustBalancingOutputDataList.stream()
                    .mapToDouble(x -> Double.valueOf((Utils.isNullOrEmpty(x.getListTrust().get("icl")))?"0.00":x.getListTrust().get("icl")))
                    .sum();
            total7.setCellValue(iclTotal);

            Cell total8=rowDate.createCell(15);
            total8.setCellStyle(styleHeaderR1);
            double rpSolutionsTotal;
            rpSolutionsTotal=trustBalancingOutputDataList.stream()
                    .mapToDouble(x -> Double.valueOf((Utils.isNullOrEmpty(x.getListTrust().get("rpSolutions")))?"0.00":x.getListTrust().get("rpSolutions")))
                    .sum();
            total8.setCellValue(rpSolutionsTotal);


            Row rowDatTtls = sheet.createRow(r.getAndIncrement());
            Cell tot1=rowDatTtls.createCell(12);
            tot1.setCellStyle(styleHeaderR0);
            tot1.setCellValue("Actual Rp Ttls");

            Cell tot2=rowDatTtls.createCell(13);
            tot2.setCellStyle(styleHeaderR0);
            tot2.setCellValue(trustBalancingOutputDataList.get(0).getAchTotal());

            Cell tot3=rowDatTtls.createCell(14);
            tot3.setCellStyle(styleHeaderR0);
            tot3.setCellValue(trustBalancingOutputDataList.get(0).getIclTotal());

            Cell tot4=rowDatTtls.createCell(15);
            tot4.setCellStyle(styleHeaderR0);
            tot4.setCellValue(trustBalancingOutputDataList.get(0).getRpSolutionsTotal());

            for (int i = 0; i < 2; i++) {

                Row vide=sheet.createRow(r.getAndIncrement());

                for (int j = 0; j < 17; j++) {
                    Cell cellule=vide.createCell(i);
                    cellule.setBlank();

                }
            }

            Set<String> ident = mergeSet(result.keySet(),result2.keySet());
            System.out.println("OOOOO: "+Json.encodePrettily(ident));

            //Enhancement begin
            Set<String> identEnhancement = mergeSet(resultEnhancement.keySet(),result2Enhancement.keySet());
            System.out.println("OOOOO ENHANCEMENT: "+Json.encodePrettily(identEnhancement));
            //Enhancement end

            Row rowRec1=sheet.createRow(r.getAndIncrement());



            //Display recoverer data by recoverer code
            if (!ident.isEmpty()) {
                Cell recTitre1 = rowRec1.createCell(4);
                recTitre1.setCellStyle(styleHeaderR);
                recTitre1.setCellValue("Recoverer code");

                Cell recTitre2 = rowRec1.createCell(5);
                recTitre2.setCellStyle(styleHeaderR);
                recTitre2.setCellValue("Collected");

                Cell recTitre3 = rowRec1.createCell(6);
                recTitre3.setCellStyle(styleHeaderR);
                recTitre3.setBlank();

                Cell recTitre4 = rowRec1.createCell(7);
                recTitre4.setCellStyle(styleHeaderR);
                recTitre4.setCellValue("Nsf");

                Cell recTitre5 = rowRec1.createCell(8);
                recTitre5.setCellStyle(styleHeaderR);
                recTitre5.setCellValue("Remit");
            } else if (!identEnhancement.isEmpty()) {
                //Enhancement else if added section: Display recoverer data by client Id
                Cell recTitre1 = rowRec1.createCell(4);
                recTitre1.setCellStyle(styleHeaderR);
                recTitre1.setCellValue("Client Id");

                Cell recTitre2 = rowRec1.createCell(5);
                recTitre2.setCellStyle(styleHeaderR);
                recTitre2.setCellValue("Collected");

                Cell recTitre3 = rowRec1.createCell(6);
                recTitre3.setCellStyle(styleHeaderR);
                recTitre3.setBlank();

                Cell recTitre4 = rowRec1.createCell(7);
                recTitre4.setCellStyle(styleHeaderR);
                recTitre4.setCellValue("Nsf");

                Cell recTitre5 = rowRec1.createCell(8);
                recTitre5.setCellStyle(styleHeaderR);
                recTitre5.setCellValue("Remit");
            }

            //FORMULA

                /*for (int i = 9; i < 11; i++) {
                    Cell cellule=rowRec1.createCell(i);
                    cellule.setBlank();
                }*/


            Cell formula=rowRec1.createCell(11);
            formula.setCellStyle(styleHeaderF);
            formula.setCellValue("FORMULA");

            Cell formulaB=rowRec1.createCell(12);
            formulaB.setCellStyle(styleHeaderF);
            formulaB.setBlank();



            ArrayList<Row> rows = new ArrayList<>();
            if (!ident.isEmpty()) {

                double totalCollected=0.00;
                double totalNsf=0.00;
                double totalRemit=0.00;
                for (int i=0;i<ident.size();i++) {

                    Row rowRec=sheet.createRow(r.getAndIncrement());
                    rows.add(i,rowRec);


                    /*for (int j = 0; j < 4; j++) {
                        Cell cellule=rowRec.createCell(j);
                        cellule.setBlank();

                    }*/
                    Cell recVal1=rowRec.createCell(4);
                    styleValR.setDataFormat(csFormat);
                    recVal1.setCellStyle(styleValR);
                    recVal1.setCellValue((ident.toArray()[i]!=null)?ident.toArray()[i].toString():"");
                    Cell recVal2=rowRec.createCell(5);
                    try{
                        Double collected=Double.valueOf((ident.toArray()[i]==null)?"":(resultfinal.get(ident.toArray()[i].toString())==null)?"":""+resultfinal.get(ident.toArray()[i].toString()));
                        totalCollected+=collected;
                        recVal2.setCellValue(collected);

                    }catch (NumberFormatException e){
                        recVal2.setCellValue("");
                    }
                    recVal2.setCellStyle(styleValR);

                    Cell recVal3=rowRec.createCell(6);
                    recVal3.setCellStyle(styleValR);
                    recVal3.setBlank();

                    Cell recVal4=rowRec.createCell(7);

                    try{
                        Double nsf=Double.valueOf((ident.toArray()[i]==null)?"":(resultfinal2.get(ident.toArray()[i].toString())==null)?"":""+resultfinal2.get(ident.toArray()[i].toString()));
                        totalNsf+=nsf;
                        recVal4.setCellValue(nsf);
                    }catch (NumberFormatException e){
                        recVal4.setCellValue("");
                    }

                    recVal4.setCellStyle(styleValR);

                    Cell recVal5=rowRec.createCell(8);

                    if(ident.toArray()[i]==null)
                        recVal5.setCellValue("");
                    else{
                        double a= ((resultfinal.get(ident.toArray()[i].toString())==null)?0.00: resultfinal.get(ident.toArray()[i].toString()));
                        double b=((resultfinal2.get(ident.toArray()[i].toString())==null)?0.00: resultfinal2.get(ident.toArray()[i].toString()));

                        try{
                            Double remit=(a-b);
                            totalRemit+=remit;
                            recVal5.setCellValue(remit);
                        }catch (NumberFormatException e){
                            recVal5.setCellValue("");
                        }
                    }

                    recVal5.setCellStyle(styleValR);

                }


                Row rowRecTot=sheet.createRow(r.getAndIncrement());

                /*for (int j = 0; j < 3; j++) {
                    Cell cellule=rowRec1.createCell(j);
                    cellule.setBlank();

                }
*/
                Cell recTot1=rowRecTot.createCell(4);
                recTot1.setCellStyle(styleHeaderR);
                recTot1.setCellValue("Total");


                Cell recTot3=rowRecTot.createCell(5);
                styleHeaderR.setDataFormat(csFormat);
                recTot3.setCellStyle(styleHeaderR);
                recTot3.setCellValue(totalCollected);

                Cell recTot4=rowRecTot.createCell(6);
                recTot4.setCellStyle(styleHeaderR);
                recTot4.setBlank();

                Cell recTot5=rowRecTot.createCell(7);
                recTot5.setCellStyle(styleHeaderR);
                recTot5.setCellValue(totalNsf);


                Cell recTot6=rowRecTot.createCell(8);
                recTot6.setCellStyle(styleHeaderR);
                recTot6.setCellValue(totalRemit);

            } else if (!identEnhancement.isEmpty()) {
                //Enhancement: Here we calculate data that we display within the recoverer section by using client id instead of using clientidentifier
                double totalCollected = 0.00;
                double totalNsf = 0.00;
                double totalRemit = 0.00;

                for (int i=0;i<identEnhancement.size();i++) {

                    Row rowRec = sheet.createRow(r.getAndIncrement());
                    rows.add(i,rowRec);

                    Cell recVal1 = rowRec.createCell(4);
                    styleValR.setDataFormat(csFormat);
                    recVal1.setCellStyle(styleValR);
                    recVal1.setCellValue((identEnhancement.toArray()[i]!=null)?identEnhancement.toArray()[i].toString():"");
                    Cell recVal2 = rowRec.createCell(5);
                    try {
                        Double collected = Double.valueOf((identEnhancement.toArray()[i]==null)?"":(resultfinalEnhancement.get(identEnhancement.toArray()[i].toString())==null)?"":""+resultfinalEnhancement.get(identEnhancement.toArray()[i].toString()));
                        totalCollected+=collected;
                        recVal2.setCellValue(collected);

                    } catch (NumberFormatException e){
                        recVal2.setCellValue("");
                    }
                    recVal2.setCellStyle(styleValR);

                    Cell recVal3 = rowRec.createCell(6);
                    recVal3.setCellStyle(styleValR);
                    recVal3.setBlank();

                    Cell recVal4 = rowRec.createCell(7);

                    try {
                        Double nsf = Double.valueOf((identEnhancement.toArray()[i]==null)?"":(resultfinal2Enhancement.get(identEnhancement.toArray()[i].toString())==null)?"":""+resultfinal2Enhancement.get(identEnhancement.toArray()[i].toString()));
                        totalNsf+=nsf;
                        recVal4.setCellValue(nsf);
                    } catch (NumberFormatException e){
                        recVal4.setCellValue("");
                    }

                    recVal4.setCellStyle(styleValR);

                    Cell recVal5 = rowRec.createCell(8);

                    if (identEnhancement.toArray()[i] == null) {
                        recVal5.setCellValue("");
                    } else {
                        double a = ((resultfinalEnhancement.get(identEnhancement.toArray()[i].toString())==null)?0.00: resultfinalEnhancement.get(identEnhancement.toArray()[i].toString()));
                        double b =((resultfinal2Enhancement.get(identEnhancement.toArray()[i].toString())==null)?0.00: resultfinal2Enhancement.get(identEnhancement.toArray()[i].toString()));

                        try {
                            Double remit = (a-b);
                            totalRemit+=remit;
                            recVal5.setCellValue(remit);
                        } catch (NumberFormatException e){
                            recVal5.setCellValue("");
                        }
                    }

                    recVal5.setCellStyle(styleValR);

                }


                Row rowRecTot=sheet.createRow(r.getAndIncrement());

                /*for (int j = 0; j < 3; j++) {
                    Cell cellule=rowRec1.createCell(j);
                    cellule.setBlank();

                }
*/
                Cell recTot1=rowRecTot.createCell(4);
                recTot1.setCellStyle(styleHeaderR);
                recTot1.setCellValue("Total");


                Cell recTot3=rowRecTot.createCell(5);
                styleHeaderR.setDataFormat(csFormat);
                recTot3.setCellStyle(styleHeaderR);
                recTot3.setCellValue(totalCollected);

                Cell recTot4=rowRecTot.createCell(6);
                recTot4.setCellStyle(styleHeaderR);
                recTot4.setBlank();

                Cell recTot5=rowRecTot.createCell(7);
                recTot5.setCellStyle(styleHeaderR);
                recTot5.setCellValue(totalNsf);


                Cell recTot6=rowRecTot.createCell(8);
                recTot6.setCellStyle(styleHeaderR);
                recTot6.setCellValue(totalRemit);

            }


            Row rowRec=(rows.size()==0)?sheet.createRow(r.getAndIncrement()):rows.get(0);

            Cell formula2 = rowRec.createCell(11);
            styleHeaderF.setDataFormat(csFormat);
            formula2.setCellStyle(styleHeaderF);
            double form = infinityTotal-(achTotal+iclTotal+manualTotal+ccTotal);

            /*BigDecimal formInBigDecimal = new BigDecimal(form).setScale(2,BigDecimal.ROUND_DOWN);
            if (new BigDecimal("0").compareTo(formInBigDecimal) == 0 || new BigDecimal("0.00").compareTo(formInBigDecimal) == 0) {double zeroValue = 0.00;
                formula2.setCellValue(zeroValue);
            } else {
                formula2.setCellValue(form);
            }*/
            BigDecimal formInBigDecimal = new BigDecimal(form).setScale(2, RoundingMode.HALF_UP);
            //double formulaReduced = formInBigDecimal.doubleValue();
            formula2.setCellValue(formInBigDecimal.doubleValue());

            //formula2.setCellValue(form);

            Cell formula2B=rowRec.createCell(12);
            styleHeaderF.setDataFormat((short)0);
            formula2B.setCellStyle(styleHeaderF);
            formula2B.setCellValue("");

            sheet.addMergedRegion(new CellRangeAddress(formula.getRow().getRowNum(),formula.getRow().getRowNum(),formula.getColumnIndex(),formulaB.getColumnIndex()));
            sheet.addMergedRegion(new CellRangeAddress(formula2.getRow().getRowNum(),formula2.getRow().getRowNum(),formula2.getColumnIndex(),formula2B.getColumnIndex()));



            for (int i = 0; i < 2; i++) {
                Row vide = sheet.createRow(r.getAndIncrement());
                for (int j = 0; j < 17; j++) {
                    Cell cellule = vide.createCell(i);
                    cellule.setBlank();
                }
            }

            for(int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }

        });

        sheet.forEach(cells -> cells.forEach(cell -> {

            try{
                if (cell.getStringCellValue().trim().toLowerCase().equals("$-0.00")
                        || cell.getStringCellValue().trim().toLowerCase().equals("$-0,00")){
                    cell.setCellValue("$0,00");

                }
            }catch (Exception ignored){

            }
        }));

        //Start displaying infinity grand total amount data
        Row rowRecInfinityGrandTotal = sheet.createRow(r.getAndIncrement());
        Cell formulaa = rowRecInfinityGrandTotal.createCell(11);
        formulaa.setCellStyle(styleHeaderI);
        formulaa.setCellValue("INFINITY GRAND TOTALS");

        Cell formulabb = rowRecInfinityGrandTotal.createCell(12);
        formulabb.setCellStyle(styleHeaderI);
        formulabb.setBlank();

        Row rowRec2 = sheet.createRow(r.getAndIncrement());

        Font fontF00 = workbook.createFont();
        fontF00.setBold(true);
        CellStyle styleHeaderR11 = workbook.createCellStyle();
        styleHeaderR11.setFont(fontF00);

        Cell formula2 = rowRec2.createCell(11);
        styleHeaderR11.setDataFormat(csFormat);
        formula2.setCellStyle(styleHeaderR11);
        formula2.setCellValue(infinityBigTotal);

        Cell formula2Bb = rowRec2.createCell(12);
        formula2Bb.setCellStyle(styleHeaderR11);
        formula2Bb.setCellValue("");

        sheet.addMergedRegion(new CellRangeAddress(formulaa.getRow().getRowNum(),formulaa.getRow().getRowNum(),formulaa.getColumnIndex(),formulabb.getColumnIndex()));
        sheet.addMergedRegion(new CellRangeAddress(formula2.getRow().getRowNum(),formula2.getRow().getRowNum(),formula2.getColumnIndex(),formula2Bb.getColumnIndex()));


/*        Row rowRecTiana = sheet.createRow(r.getAndIncrement());
        Cell cell0=rowRecTiana.createCell(0);
        //cell0.setCellStyle(style);
        double formTiana = 2.22 + 3.33;
        cell0.setCellValue(formTiana);*/

        sheet.setFitToPage(true);

        OutputStream fileOut;
        //String fileName= Paths.get("src","main","resources") +File.separator+"trust_test.xlsx";
        String fileName= Paths.get("assets") +File.separator+"trust_test.xlsx";

        try {


            fileOut = new FileOutputStream(fileName);

            workbook.write(fileOut);

            fileOut.close();
            workbook.close();
            System.out.println("END......");

            return encodeFileToBase64Binary(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("END......");

        return "";
    }


    private static String encodeFileToBase64Binary(String fileName) throws IOException {
        File file = new File(fileName);
        byte[] encoded = Base64.encodeBase64(Files.readAllBytes(file.toPath()));
        return new String(encoded, StandardCharsets.US_ASCII);
    }


    private static <T, K extends Comparable<K>> Collector<T, ?, TreeMap<K, List<T>>>
    sortedGroupingBy(Function<T, K> function) {
        return Collectors.groupingBy(function,
                TreeMap::new, Collectors.toList());
    }


    private static <T> Set<T> mergeSet(Set<T> a, Set<T> b)
    {
        return new HashSet<T>() {{
            addAll(a);
            addAll(b);
        } };
    }
}