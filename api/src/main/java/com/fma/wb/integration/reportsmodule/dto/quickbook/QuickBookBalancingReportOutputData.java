package com.fma.wb.integration.reportsmodule.dto.quickbook;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class QuickBookBalancingReportOutputData implements Serializable {
    private String dateRequest = "";
    private List<CollectionsProcessingOutputData> collectionsProcessingOutputDataList = new ArrayList<>();
    private List<CollectionsProcessingOutputData> collectionsProcessingOutputDataNewFactorList = new ArrayList<>();

    public String getDateRequest() {
        return dateRequest;
    }

    public void setDateRequest(String dateRequest) {
        this.dateRequest = dateRequest;
    }

    public List<CollectionsProcessingOutputData> getCollectionsProcessingOutputDataList() {
        return collectionsProcessingOutputDataList;
    }

    public void setCollectionsProcessingOutputDataList(List<CollectionsProcessingOutputData> collectionsProcessingOutputDataList) {
        this.collectionsProcessingOutputDataList = collectionsProcessingOutputDataList;
    }

    public List<CollectionsProcessingOutputData> getCollectionsProcessingOutputDataNewFactorList() {
        return collectionsProcessingOutputDataNewFactorList;
    }

    public void setCollectionsProcessingOutputDataNewFactorList(List<CollectionsProcessingOutputData> collectionsProcessingOutputDataNewFactorList) {
        this.collectionsProcessingOutputDataNewFactorList = collectionsProcessingOutputDataNewFactorList;
    }

    public QuickBookBalancingReportOutputData() {
    }

    @Override
    public String toString() {
        return "QuickBookBalancingReportOutputData{" +
                "dateRequest='" + dateRequest + '\'' +
                ", collectionsProcessingOutputDataList=" + collectionsProcessingOutputDataList +
                ", collectionsProcessingOutputDataNewFactorList=" + collectionsProcessingOutputDataNewFactorList +
                '}';
    }
}