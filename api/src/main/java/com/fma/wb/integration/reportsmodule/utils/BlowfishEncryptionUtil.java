package com.fma.wb.integration.reportsmodule.utils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

import java.nio.charset.StandardCharsets;

import static com.fma.wb.integration.common.utils.ConfigurationUtils.BLOWFISH_KEY;


public class BlowfishEncryptionUtil
{

    private static final String encryptionTypeName = "Blowfish";
    private static SecretKey key;
    private static Cipher cipher;
    private static Base64 base64;

    /** This gives you an example on how to use this service.
     *
     * @param argv
     */
    public static void main(String[] argv)
    {
        BlowfishEncryptionUtil svc = BlowfishEncryptionUtil.getInstance();
        String[] pwd={"1234", "s655m2566e", "d493a772r", "BFrmm39!#", null};

        for(String str: pwd)
        {
            String enc = svc.encrypt(str);
            String dcr = svc.decrypt(enc);

            System.out.println("[" + str + "] encrypted=" + "["+enc+"]");
/*
            System.out.println("[" + enc + "] decrypted="+ "["+dcr+"]");
*/
        }

        String[] enc={"woYynSmIza8=", "woYynSmIza8=", "o-09yaG-TiuC8KjidCWStQ", "u94WlP1G8UrcbT-PpPH_JA"};
        for(String str : enc)
        {
            System.out.println(str+" decrypted=["+svc.decrypt(str)+"]");
        }



    }

    /** Pass in a cleartext string and it will return you an encrypted string.
     *
     * @param cleartext
     * @return
     * @throws RuntimeException
     */
    public String encrypt(String cleartext) throws RuntimeException
    {
        if(StringUtils.isBlank(cleartext))
            return "";

        if (cipher == null || key == null)
            throw new RuntimeException("Unable to initialize encryption. ");

        try
        {

            cipher.init(Cipher.ENCRYPT_MODE, key);
            return base64.encodeToString(cipher.doFinal(cleartext.getBytes())).trim();

        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
    }

    /** pass in a encrypted string that you  used this class to previously encrypt, and it wil
     * 		will decrypt it to it's clear text value.
     *
     * @param encryptedText
     * @return
     * @throws RuntimeException
     */
    public String decrypt(String encryptedText) throws RuntimeException
    {
        if(StringUtils.isBlank(encryptedText))
            return "";

        if (cipher == null || key == null)
            throw new RuntimeException("Unable to initialize encryption. ");

        try
        {
            cipher.init(Cipher.DECRYPT_MODE, key);

            byte[] encryptedData = Base64.decodeBase64(encryptedText);
            byte[] decrypted = cipher.doFinal(encryptedData);
            return new String(decrypted);

        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }
    }


    /** this is the only way you can instantiate this class.  It is a singleton.
     *
     * @return
     * @throws RuntimeException
     */
    public static BlowfishEncryptionUtil getInstance() throws RuntimeException
    {
        try
        {
            if (cipher == null)
            {

                byte[] kd = BLOWFISH_KEY.getBytes(StandardCharsets.UTF_8);
                key = new SecretKeySpec(kd, "Blowfish");
                cipher = Cipher.getInstance(encryptionTypeName);
                base64 = new Base64(true);
            }
        }
        catch (Exception ex)
        {
            throw new RuntimeException(ex);
        }

        return EncryptionServiceHolder.INSTANCE;
    }

    /** ensure that this class cannot be instantiated any other way besides getInstance().
     *
     */
    private BlowfishEncryptionUtil() {}

    /**
     * doing the singleton this way so that we are threadsafe if we ever move to a threaded environment, we don't have to change this code. see Bill
     * Pugh and double-checked locking on Singleton java implementation.
     *
     * @author nstaats
     *
     */
    private static class EncryptionServiceHolder
    {
        public static final BlowfishEncryptionUtil INSTANCE = new BlowfishEncryptionUtil();
    }

}