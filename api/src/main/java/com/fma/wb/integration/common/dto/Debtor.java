package com.fma.wb.integration.common.dto;

import java.io.Serializable;

/**
 * This model is going to be used for doing the mapping with the Debtor table within Infinity database
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class Debtor implements Serializable {
    private String first_name;
    private String last_name;
    private String debtor_id;
    private String client_id;

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getDebtor_id() {
        return debtor_id;
    }

    public void setDebtor_id(String debtor_id) {
        this.debtor_id = debtor_id;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    /**
     * Constructor without parameter
     */
    public Debtor() {
    }

    /**
     * Constructor with parameter
     * @param first_name
     * @param last_name
     * @param debtor_id
     * @param client_id
     */
    public Debtor(String first_name, String last_name, String debtor_id, String client_id) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.debtor_id = debtor_id;
        this.client_id = client_id;
    }

    @Override
    public String toString() {
        return "Debtor{" +
                "first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", debtor_id='" + debtor_id + '\'' +
                ", client_id='" + client_id + '\'' +
                '}';
    }
}