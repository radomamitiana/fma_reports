package com.fma.wb.integration.reportsmodule.utils.io;

import com.fma.wb.integration.reportsmodule.dto.quickbook.QuickBookBalancingReportOutputData;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicInteger;

public class QuickBookBalancingExcelFileGenerator {
    private QuickBookBalancingReportOutputData data = new QuickBookBalancingReportOutputData();
    private static String[] columnNames = {"Organization", "Client", "Gross PA", "Gross RA", "Gross PD", "Gross RD", "Net PA", "Net PD", "Net RA", "Net RD", "Fee", "Sales Tax", "Offset", "Total"};
    private DataFormat styleNumberCustomFormat;
    private short csStyleNumberCustomFormat;
    private DataFormat styleNumberCustomFormatSpecial;
    private short csStyleNumberCustomFormatSpecial;

    public QuickBookBalancingExcelFileGenerator() {
    }

    public QuickBookBalancingExcelFileGenerator(QuickBookBalancingReportOutputData data) {
        this.data = data;
    }

    public String getBase64GeneratedFile() {

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Quick Book Balancing Report" + System.currentTimeMillis());

        styleNumberCustomFormat = workbook.createDataFormat();
        csStyleNumberCustomFormat = styleNumberCustomFormat.getFormat("#,##0");

        styleNumberCustomFormatSpecial = workbook.createDataFormat();
        csStyleNumberCustomFormatSpecial = styleNumberCustomFormatSpecial.getFormat("#");

        AtomicInteger atomicInteger = new AtomicInteger();

        //QuickBookBalancingReportOutputData dataListToProcess = this.data;
        String requestDate = this.data.getDateRequest();

        OutputStream fileOut;
        String fileName = Paths.get("assets") + File.separator + "quickBookBalancingReport_test.xlsx";

        Font basicContentFont = workbook.createFont();
        basicContentFont.setFontHeightInPoints((short) 10);
        basicContentFont.setFontName("Arial");
        basicContentFont.setBold(false);

        Font header1Font = workbook.createFont();
        header1Font.setFontHeightInPoints((short) 10);
        header1Font.setFontName("Arial");
        header1Font.setColor(IndexedColors.WHITE.getIndex());
        header1Font.setBold(true);

        //Style header
        CellStyle styleHeader1 = workbook.createCellStyle();
        styleHeader1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleHeader1.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
        styleHeader1.setFillBackgroundColor(IndexedColors.DARK_BLUE.getIndex());
        styleHeader1.setFont(header1Font);
        styleHeader1.setAlignment(HorizontalAlignment.CENTER);

        //Style content sheet aligned left
        CellStyle styleContent1 = workbook.createCellStyle();
        styleContent1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleContent1.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        styleContent1.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        styleContent1.setFont(basicContentFont);
        styleContent1.setAlignment(HorizontalAlignment.LEFT);

        //Style content sheet aligned center
        CellStyle styleContent2 = workbook.createCellStyle();
        styleContent2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleContent2.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        styleContent2.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        styleContent2.setFont(basicContentFont);
        styleContent2.setAlignment(HorizontalAlignment.CENTER);
        styleContent2.setDataFormat(csStyleNumberCustomFormat);

        CellStyle styleContent3 = workbook.createCellStyle();
        styleContent3.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleContent3.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        styleContent3.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        styleContent3.setFont(basicContentFont);
        styleContent3.setAlignment(HorizontalAlignment.CENTER);
        styleContent3.setDataFormat(csStyleNumberCustomFormatSpecial);

        //Style content sheet aligned center with background in green
        CellStyle styleContentGreen = workbook.createCellStyle();
        styleContentGreen.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleContentGreen.setFillForegroundColor(IndexedColors.BRIGHT_GREEN1.getIndex());
        styleContentGreen.setFillBackgroundColor(IndexedColors.BRIGHT_GREEN1.getIndex());
        styleContentGreen.setFont(basicContentFont);
        styleContentGreen.setAlignment(HorizontalAlignment.CENTER);

        //Style content sheet aligned center with background in yellow
        CellStyle styleContentYellowCenter = workbook.createCellStyle();
        styleContentYellowCenter.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleContentYellowCenter.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        styleContentYellowCenter.setFillBackgroundColor(IndexedColors.YELLOW.getIndex());
        styleContentYellowCenter.setFont(basicContentFont);
        styleContentYellowCenter.setAlignment(HorizontalAlignment.CENTER);

        //Style content sheet aligned left with background in yellow
        CellStyle styleContentYellowLeft = workbook.createCellStyle();
        styleContentYellowLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleContentYellowLeft.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        styleContentYellowLeft.setFillBackgroundColor(IndexedColors.YELLOW.getIndex());
        styleContentYellowLeft.setFont(basicContentFont);
        styleContentYellowLeft.setAlignment(HorizontalAlignment.LEFT);

        // Create a Row for header
        Row headerRow = sheet.createRow(atomicInteger.getAndIncrement());

        this.data.getCollectionsProcessingOutputDataList().forEach(quickBookBalancingDto -> {
            System.out.println("-+++++++++++++++++++++++++++++++++++++++++++++++++++  quickBookBalancingDto I AM HERE " + quickBookBalancingDto.toString());
        });

        // Create cells
        for (int i = 0; i < columnNames.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columnNames[i]);
            cell.setCellStyle(styleHeader1);
            int columnIndex = cell.getColumnIndex();
            sheet.autoSizeColumn(columnIndex);
        }

        this.data.getCollectionsProcessingOutputDataList().forEach(quickBookBalancingDto -> {
            Row dataRow = sheet.createRow(atomicInteger.getAndIncrement());

            int i = 0;
            double minQuickBookBalancing = 2.00;

            //Name column
            Cell cell0 = dataRow.createCell(0);
            String value0 = "";
            value0 = "" + quickBookBalancingDto.getClient_id();

            cell0.setCellValue(value0);


            cell0.setCellStyle(styleContent1);


            int columnIndex1 = cell0.getColumnIndex();
            sheet.autoSizeColumn(columnIndex1);

            //Manager column
            Cell cell1 = dataRow.createCell(1);
            String value1 = "" + quickBookBalancingDto.getOrg_id();

            cell1.setCellValue(value1);
            cell1.setCellStyle(styleContent2);
            int columnIndex2 = cell1.getColumnIndex();
            sheet.autoSizeColumn(columnIndex2);

            //Number of Calls column
            Cell cell2 = dataRow.createCell(2);
            int value2 = quickBookBalancingDto.getRecord_id();

            cell2.setCellValue(value2);
            cell2.setCellStyle(styleContent3);
            int columnIndex3 = cell2.getColumnIndex();
            sheet.autoSizeColumn(columnIndex3);

            //Mins Duration column
            Cell cell3 = dataRow.createCell(3);
            int value3 = quickBookBalancingDto.isPa_add_gross() ? 1 : 0;

            cell3.setCellValue(value3);
            cell3.setCellStyle(styleContent3);
            int columnIndex4 = cell3.getColumnIndex();
            sheet.autoSizeColumn(columnIndex4);

            // Split time column
            Cell cell4 = dataRow.createCell(4);
            String value4 = "" + (quickBookBalancingDto.isPa_add_net() ? 0 : 1);


            BigDecimal quickBookBalancingInBigDecimal = new BigDecimal(value4).setScale(2, RoundingMode.HALF_UP);

            if (quickBookBalancingInBigDecimal.doubleValue() < minQuickBookBalancing) {
                styleContentGreen.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
                cell4.setCellStyle(styleContentGreen);
            } else {
                styleContent2.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
                cell4.setCellStyle(styleContent2);
            }
            cell4.setCellValue(quickBookBalancingInBigDecimal.doubleValue());

            int columnIndex5 = cell4.getColumnIndex();
            sheet.autoSizeColumn(columnIndex5);

            //Factor
            Cell cell5 = dataRow.createCell(5);
            String value5 = "" + (quickBookBalancingDto.isPa_subtract_tax() ? 0 : 1);


            BigDecimal factorInBigDecimal = new BigDecimal(value5).setScale(2, RoundingMode.HALF_UP);

            styleContent2.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
            cell5.setCellStyle(styleContent2);

            cell5.setCellValue(factorInBigDecimal.doubleValue());

            int columnIndex6 = cell5.getColumnIndex();
            sheet.autoSizeColumn(columnIndex6);

        });

        try {

            fileOut = new FileOutputStream(fileName);

            workbook.write(fileOut);

            fileOut.close();
            workbook.close();
            System.out.println("END......");

            return encodeFileToBase64Binary(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("END......");

        return "";
    }

    private static String encodeFileToBase64Binary(String fileName) throws IOException {
        File file = new File(fileName);
        byte[] encoded = Base64.encodeBase64(Files.readAllBytes(file.toPath()));
        return new String(encoded, StandardCharsets.US_ASCII);
    }

}
