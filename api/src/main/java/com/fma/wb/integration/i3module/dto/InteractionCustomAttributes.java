package com.fma.wb.integration.i3module.dto;

import org.joda.time.DateTime;
import java.io.Serializable;

/**
 * @author Tiana Andriamanalina
 * @version 1.0
 * InteractionCustomAttributes class is used for the mapping
 */
public class InteractionCustomAttributes implements Serializable {
    private String InteractionIDKey;
    private int SiteID;
    private int SeqNo;
    private int CustomNum1;
    private int CustomNum2;
    private int CustomNum3;
    private int CustomNum4;
    private int CustomNum5;
    private int CustomNum6;
    private String CustomString1;
    private String CustomString2;
    private String CustomString3;
    private String CustomString4;
    private String CustomString5;
    private String CustomString6;
    private DateTime CustomDateTime;
    private DateTime CustomDateTimeGMT;
    private DateTime I3TimeStampGMT;

    public String getInteractionIDKey() {
        return InteractionIDKey;
    }

    public void setInteractionIDKey(String interactionIDKey) {
        InteractionIDKey = interactionIDKey;
    }

    public int getSiteID() {
        return SiteID;
    }

    public void setSiteID(int siteID) {
        SiteID = siteID;
    }

    public int getSeqNo() {
        return SeqNo;
    }

    public void setSeqNo(int seqNo) {
        SeqNo = seqNo;
    }

    public int getCustomNum1() {
        return CustomNum1;
    }

    public void setCustomNum1(int customNum1) {
        CustomNum1 = customNum1;
    }

    public int getCustomNum2() {
        return CustomNum2;
    }

    public void setCustomNum2(int customNum2) {
        CustomNum2 = customNum2;
    }

    public int getCustomNum3() {
        return CustomNum3;
    }

    public void setCustomNum3(int customNum3) {
        CustomNum3 = customNum3;
    }

    public int getCustomNum4() {
        return CustomNum4;
    }

    public void setCustomNum4(int customNum4) {
        CustomNum4 = customNum4;
    }

    public int getCustomNum5() {
        return CustomNum5;
    }

    public void setCustomNum5(int customNum5) {
        CustomNum5 = customNum5;
    }

    public int getCustomNum6() {
        return CustomNum6;
    }

    public void setCustomNum6(int customNum6) {
        CustomNum6 = customNum6;
    }

    public String getCustomString1() {
        return CustomString1;
    }

    public void setCustomString1(String customString1) {
        CustomString1 = customString1;
    }

    public String getCustomString2() {
        return CustomString2;
    }

    public void setCustomString2(String customString2) {
        CustomString2 = customString2;
    }

    public String getCustomString3() {
        return CustomString3;
    }

    public void setCustomString3(String customString3) {
        CustomString3 = customString3;
    }

    public String getCustomString4() {
        return CustomString4;
    }

    public void setCustomString4(String customString4) {
        CustomString4 = customString4;
    }

    public String getCustomString5() {
        return CustomString5;
    }

    public void setCustomString5(String customString5) {
        CustomString5 = customString5;
    }

    public String getCustomString6() {
        return CustomString6;
    }

    public void setCustomString6(String customString6) {
        CustomString6 = customString6;
    }

    public DateTime getCustomDateTime() {
        return CustomDateTime;
    }

    public void setCustomDateTime(DateTime customDateTime) {
        CustomDateTime = customDateTime;
    }

    public DateTime getCustomDateTimeGMT() {
        return CustomDateTimeGMT;
    }

    public void setCustomDateTimeGMT(DateTime customDateTimeGMT) {
        CustomDateTimeGMT = customDateTimeGMT;
    }

    public DateTime getI3TimeStampGMT() {
        return I3TimeStampGMT;
    }

    public void setI3TimeStampGMT(DateTime i3TimeStampGMT) {
        I3TimeStampGMT = i3TimeStampGMT;
    }

    public InteractionCustomAttributes() {
    }

    @Override
    public String toString() {
        return "InteractionCustomAttributes{" +
                "InteractionIDKey='" + InteractionIDKey + '\'' +
                ", SiteID=" + SiteID +
                ", SeqNo=" + SeqNo +
                ", CustomNum1=" + CustomNum1 +
                ", CustomNum2=" + CustomNum2 +
                ", CustomNum3=" + CustomNum3 +
                ", CustomNum4=" + CustomNum4 +
                ", CustomNum5=" + CustomNum5 +
                ", CustomNum6=" + CustomNum6 +
                ", CustomString1='" + CustomString1 + '\'' +
                ", CustomString2='" + CustomString2 + '\'' +
                ", CustomString3='" + CustomString3 + '\'' +
                ", CustomString4='" + CustomString4 + '\'' +
                ", CustomString5='" + CustomString5 + '\'' +
                ", CustomString6='" + CustomString6 + '\'' +
                ", CustomDateTime=" + CustomDateTime +
                ", CustomDateTimeGMT=" + CustomDateTimeGMT +
                ", I3TimeStampGMT=" + I3TimeStampGMT +
                '}';
    }
}