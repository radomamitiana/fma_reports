package com.fma.wb.integration.reportsmodule.dto.quickbook;

public class QuickOrganizationClientInputData {
    private int organizationId;

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }
}
