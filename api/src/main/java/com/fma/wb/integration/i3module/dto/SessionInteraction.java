package com.fma.wb.integration.i3module.dto;

import dto.User;
import interactions.Interaction;
import interactions.DialerCallInteraction;
import interactions.CallInteraction;
import interactions.InteractionConference;
import queues.InteractionQueue;
import session.DialerSession;
import session.Session;
import status.Status;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class is used to store Interactions with the Web API.
 *
 * session: is the session the user is currently connected to
 * interactionQueue: is storing the next call that will be automatically made
 * interaction: stores the properties the current call taking place
 * transferToInteraction: is the call that is being transferred from one user to another
 * dialerSession: is the object that allows the user to accept inbound calls
 * dialerCallInteraction: is the current inbound call taking place
 * iod: determines if a call is: Inbound, Outbound, Dialer, IVR
 * markedComplete: is an indicator for Dialer Calls. If a call has finished, it is set to true. If a call is taking place, it is set to false
 * canDialOut: indicates if the user has the ability to make outbound calls
 * isTransfer: indicates if the current call is a transferred call
 * updateI3Status: is the indicator for updating the users I3 status or not.
 * inbound: stores the properties of the inbound being made
 * callInteraction: is the object that stores the call being made or in progress
 * interactionConference: is the object that stores the conference call currently being made or in progress
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class SessionInteraction implements Serializable {

    private Session session;
    private InteractionQueue interactionQueue;
    private Interaction interaction;
    private Interaction transferToInteraction;
    private DialerSession dialerSession;
    private DialerCallInteraction dialerCallInteraction;
    private int iod;
    private boolean markedComplete;
    private boolean canDialOut;
    private boolean isTransfer;
    private boolean updateI3Status;
    private Interaction inbound;
    private CallInteraction callInteraction;
    private InteractionConference interactionConference;
    private Status statusMessage;
    private User user;
    private String curConsultId;
    private ArrayList<String> currentConferenceTargets = new ArrayList<>();

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public InteractionQueue getInteractionQueue() {
        return interactionQueue;
    }

    public void setInteractionQueue(InteractionQueue interactionQueue) {
        this.interactionQueue = interactionQueue;
    }

    public Interaction getInteraction() {
        return interaction;
    }

    public void setInteraction(Interaction interaction) {
        this.interaction = interaction;
    }

    public Interaction getTransferToInteraction() {
        return transferToInteraction;
    }

    public void setTransferToInteraction(Interaction transferToInteraction) {
        this.transferToInteraction = transferToInteraction;
    }

    public DialerSession getDialerSession() {
        return dialerSession;
    }

    public void setDialerSession(DialerSession dialerSession) {
        this.dialerSession = dialerSession;
    }

    public DialerCallInteraction getDialerCallInteraction() {
        return dialerCallInteraction;
    }

    public void setDialerCallInteraction(DialerCallInteraction dialerCallInteraction) {
        this.dialerCallInteraction = dialerCallInteraction;
    }

    public int getIod() {
        return iod;
    }

    public void setIod(int iod) {
        this.iod = iod;
    }

    public boolean isMarkedComplete() {
        return markedComplete;
    }

    public void setMarkedComplete(boolean markedComplete) {
        this.markedComplete = markedComplete;
    }

    public boolean isCanDialOut() {
        return canDialOut;
    }

    public void setCanDialOut(boolean canDialOut) {
        this.canDialOut = canDialOut;
    }

    public boolean isTransfer() {
        return isTransfer;
    }

    public void setTransfer(boolean transfer) {
        isTransfer = transfer;
    }

    public boolean isUpdateI3Status() {
        return updateI3Status;
    }

    public void setUpdateI3Status(boolean updateI3Status) {
        this.updateI3Status = updateI3Status;
    }

    public Interaction getInbound() {
        return inbound;
    }

    public void setInbound(Interaction inbound) {
        this.inbound = inbound;
    }

    public CallInteraction getCallInteraction() {
        return callInteraction;
    }

    public void setCallInteraction(CallInteraction callInteraction) {
        this.callInteraction = callInteraction;
    }

    public InteractionConference getInteractionConference() {
        return interactionConference;
    }

    public void setInteractionConference(InteractionConference interactionConference) {
        this.interactionConference = interactionConference;
    }

    public Status getStatus() {
        return statusMessage;
    }

    public void setStatus(Status statusMessage) {
        this.statusMessage = statusMessage;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<String> getCurrentConferenceTargets() {
        return currentConferenceTargets;
    }

    public void setCurrentConferenceTargets(ArrayList<String> currentConferenceTargets) {
        this.currentConferenceTargets = currentConferenceTargets;
    }

    public SessionInteraction() {
    }

    public SessionInteraction(Session session, InteractionQueue interactionQueue, Interaction interaction, Interaction transferToInteraction, DialerSession dialerSession, DialerCallInteraction dialerCallInteraction, int iod, boolean markedComplete, boolean canDialOut, boolean isTransfer, boolean updateI3Status, Interaction inbound, CallInteraction callInteraction, InteractionConference interactionConference, Status statusMessage) {
        this.session = session;
        this.interactionQueue = interactionQueue;
        this.interaction = interaction;
        this.transferToInteraction = transferToInteraction;
        this.dialerSession = dialerSession;
        this.dialerCallInteraction = dialerCallInteraction;
        this.iod = iod;
        this.markedComplete = markedComplete;
        this.canDialOut = canDialOut;
        this.isTransfer = isTransfer;
        this.updateI3Status = updateI3Status;
        this.inbound = inbound;
        this.callInteraction = callInteraction;
        this.interactionConference = interactionConference;
        this.statusMessage = statusMessage;
    }

    @Override
    public String toString() {
        return "SessionInteraction{" +
                "session=" + session +
                ", interactionQueue=" + interactionQueue +
                ", interaction=" + interaction +
                ", transferToInteraction=" + transferToInteraction +
                ", dialerSession=" + dialerSession +
                ", dialerCallInteraction=" + dialerCallInteraction +
                ", iod=" + iod +
                ", markedComplete=" + markedComplete +
                ", canDialOut=" + canDialOut +
                ", isTransfer=" + isTransfer +
                ", updateI3Status=" + updateI3Status +
                ", inbound=" + inbound +
                ", callInteraction=" + callInteraction +
                ", interactionConference=" + interactionConference +
                ", statusMessage=" + statusMessage +
                ", curConsultId=" + curConsultId +
                '}';
    }

    public String getCurConsultId() {
        return curConsultId;
    }

    public void setCurConsultId(String curConsultId) {
        this.curConsultId = curConsultId;
    }

    public void addTarget(String target) {
        if (this.currentConferenceTargets == null) {
            this.currentConferenceTargets = new ArrayList();
        }
        this.currentConferenceTargets.add(target);
    }
}
