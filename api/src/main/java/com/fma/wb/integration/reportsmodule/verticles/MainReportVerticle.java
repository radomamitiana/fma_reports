package com.fma.wb.integration.reportsmodule.verticles;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fma.wb.integration.common.dto.User;
import com.fma.wb.integration.common.dto.api.ApiResponse;
import com.fma.wb.integration.common.dto.api.InternalResponse;
import com.fma.wb.integration.common.dto.enums.WsResponseType;
import com.fma.wb.integration.reportsmodule.dto.Option;
import com.fma.wb.integration.reportsmodule.dto.Organization;
import com.fma.wb.integration.reportsmodule.dto.healthcare.HealthCareInputData;
import com.fma.wb.integration.reportsmodule.dto.healthcare.HealthOutputData;
import com.fma.wb.integration.reportsmodule.dto.quickbook.*;
import com.fma.wb.integration.reportsmodule.dto.splittime.SplitTimeReportInputData;
import com.fma.wb.integration.reportsmodule.dto.splittime.SplitTimeReportOutputData;
import com.fma.wb.integration.reportsmodule.dto.trust.TrustBalancingInputData;
import com.fma.wb.integration.reportsmodule.dto.trust.TrustBalancingOutputData;
import com.fma.wb.integration.reportsmodule.services.HealthCareService;
import com.fma.wb.integration.reportsmodule.services.QuickBookBalancingService;
import com.fma.wb.integration.reportsmodule.services.SplitTimeReportService;
import com.fma.wb.integration.reportsmodule.services.TrustBalancingService;
import com.fma.wb.integration.reportsmodule.utils.io.HealthExcelAndPPTFileHelper;
import com.fma.wb.integration.reportsmodule.utils.io.QuickBookBalancingExcelFileGenerator;
import com.fma.wb.integration.reportsmodule.utils.io.SplitTimeReportExcelFileGenerator;
import com.fma.wb.integration.reportsmodule.utils.io.TrustExcelFileGenerator;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import common.Logger;
import common.LoggerTag;
import io.vertx.core.*;
import io.vertx.core.http.*;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.JWTAuthHandler;
import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.io.File;
import java.nio.file.Paths;
import java.util.*;

import static com.fma.wb.integration.common.utils.UrlConfig.*;
import static utils.Configuration.*;

/**
 * AutonomyMaintainerVerticle class. This class handles all processes needed for trust balancing.
 *
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class MainReportVerticle extends AbstractVerticle {
    private Context context;
    private Vertx vertx;
    private TrustBalancingService trustBalancingService;
    private QuickBookBalancingService quickBookBalancingService;
    private HealthCareService healthCareService;
    private Gson gson;
    private JWTAuthHandler authHandler;
    private JWTAuth authProvider;
    private SplitTimeReportService splitTimeReportService;
    public static ServerWebSocket mainReportWebSocket;


    /**
     * init method: the first method called when the verticle is deployed and runned
     *
     * @param vertx
     * @param context
     */
    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
        this.context = context;
        this.vertx = vertx;
        trustBalancingService = TrustBalancingService.getInstance(vertx);
        healthCareService = HealthCareService.getInstance(vertx);
        splitTimeReportService = SplitTimeReportService.getInstance(vertx);
        quickBookBalancingService = QuickBookBalancingService.getInstance(vertx);

        gson = new GsonBuilder().create();


        authProvider = JWTAuth.create(vertx, new JWTAuthOptions()
                .addPubSecKey(new PubSecKeyOptions()
                        .setAlgorithm("HS256")
                        .setPublicKey("FMA_REPORTS")
                        .setSymmetric(true)));


        authHandler = JWTAuthHandler.create(authProvider);
        authHandler.setIgnoreExpiration(true);

    }

    /**
     * start method: is launched after init
     *
     * @param fut
     */
    @Override
    public void start(Future<Void> fut) {

        System.out.println("Starting main report  verticle ...");
        Router router = Router.router(vertx);


        //we just start an application server
        HttpServerOptions options = new HttpServerOptions().setLogActivity(true);
        vertx.createHttpServer(options)
                .requestHandler(router)
                .websocketHandler(websocket -> {
                    //this.mainReportWebSocket is the mainReportWebSocket of the MainVerticle class
                    mainReportWebSocket = websocket;
                    //handle messages.
                    //message represents the message sent. It is a string
                    mainReportWebSocket.textMessageHandler(message -> {
                        Logger.getInstance("").log(LoggerTag.DEBUG, "Report module - Websocket message: [ " + message + " ]", "");

                    });


                    System.out.println("WS REQUESTED");

                    if (websocket.path().equals("/report_msg/")) {
                        System.out.println("WS connexion: " + websocket.path());
                        ApiResponse response = new ApiResponse();
                        response.setResponseType(WsResponseType.TYPE_EVENT);
                        response.setData("1");
                        response.setComment("Connection accepted");
                        websocket.writeTextMessage(Json.encode(response));


                    } else {
                        websocket.reject();
                    }
                    websocket.accept();
                })
                .listen(
                        config().getInteger("http.port", 9095), result -> {
                            if (result.succeeded()) {
                                fut.complete();

                            } else {
                                fut.fail(result.cause());
                            }
                        }
                );


        //TEST
        /*AtomicInteger i= new AtomicInteger(0);
        healthCareService.getOrganizationList(SYSA_ID,res->{

            res.forEach(organization -> {
                HealthCareInputData inputData=new HealthCareInputData();
                inputData.setOrganizationId(organization.getOrganization_id());
                inputData.setBeginDateRange(Instant.now().toString().split("T")[0]);
                inputData.setEndDateRange(DateUtils.minusMonthsFromDate(inputData.getBeginDateRange(),1));
                inputData.setShowClients(true);

                vertx.<InternalResponse>executeBlocking(future -> this.healthCareService.gethealthcare(SYSA_ID,inputData, future), asyncResult -> {

                    if (asyncResult.succeeded()) {

                        if (asyncResult.result().getCause()==1){

                            new HealthExcelAndPPTFileHelper(Json.decodeValue(asyncResult.result().getData(), HealthOutputData.class)).generatePPTFile(SYSA_ID+i.get());
                            new MailHandler(vertx).sendMail(Arrays.asList(
                                    Paths.get("assets","healthCareFiles") + File.separator+SYSA_ID+i.get()+".pptx",
                                    Paths.get("assets","healthCareFiles") + File.separator+SYSA_ID+i.get()+".xlsx"
                            ),"Health care reporting data for period [ "+inputData.getBeginDateRange()+ " - "+inputData.getEndDateRange(),"Healthcare report","");

                            i.incrementAndGet();

                        }


                    }
                });


            });

        });*/

        //Event handling

        vertx.eventBus().consumer("cur_trust", msg -> {
            if (msg != null) {
                String message = msg.body().toString();

            }

        });


        Set<HttpMethod> allowedMethods = new HashSet<>();
        allowedMethods.add(HttpMethod.GET);
        allowedMethods.add(HttpMethod.POST);
        allowedMethods.add(HttpMethod.OPTIONS);
        allowedMethods.add(HttpMethod.DELETE);
        allowedMethods.add(HttpMethod.PATCH);
        allowedMethods.add(HttpMethod.PUT);

        router.route().handler(CorsHandler.create("*")
                .allowedMethods(allowedMethods)
                .allowedHeader("Access-Control-Allow-Method")
                .allowedHeader("Access-Control-Allow-Origin")
                .allowedHeader("Access-Control-Allow-Credentials")
                .allowedHeader("X-Auth-Token")
                .allowedHeader("Content-Type")
                .allowedHeader("Origin")
                .allowedHeader("Cache-control")
                .allowedHeader("Pragma")
                .allowedHeader("Content-Type")
                .allowedHeader("token")
                .exposedHeader("token")
                .allowedHeader("Authorization")
                .allowedHeader("accept"));


        router.route("/ws/auth/*").handler(authHandler).failureHandler(event -> {
            HttpServerResponse response = event.response();
            ApiResponse errorResponse = new ApiResponse();
            errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
            errorResponse.setComment("You are not authorized to access this ressource");

            response
                    .setStatusCode(STATUS_CODE_401)
                    .end(this.gson.toJson(errorResponse));
        });


        router.route("/").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response
                    .putHeader("content-type", "text/html")
                    .end("<h1>MainReportVerticle is running</h1>");
        });


        //Setting global routes
        router.post(LOGIN_BACK_OFFICE_AND_REPORTS_APP)
                .produces("application/json")
                .handler(this::loginBackOfficeAndReportsApp);

        router.delete(LOGOUT_BACK_OFFICE_AND_REPORTS_APP + ":sessionId")
                .produces("application/json")
                .handler(this::logoutBackOfficeAndReportsApp);

        router.get(GET_OPTIONS_LIST + ":sessionId")
                .produces("application/json")
                .handler(this::getOptionsList);

        //Setting Trust routes
        router.get(GET_ORGANIZATION_LIST + ":sessionId")
                .produces("application/json")
                .handler(this::getOrganizationList);

        router.post(GET_TRUST_BALANCING_FORM_DATA + ":sessionId")
                .consumes("application/json")
                .handler(this::getTrustBalancingData);


        //Setting health routes
        router.post(GET_HEALTH_DATA + ":sessionId")
                .consumes("application/json")
                .handler(this::getHealthData);


        router.post(GET_COMMENTED_HEALTH_DATA + ":sessionId")
                .consumes("application/json")
                .handler(this::getCmHealthData);

        //Setting Trust routes
        router.get(GET_ORGANIZATION_LIST2 + ":sessionId")
                .produces("application/json")
                .handler(this::getHealthOrganizationList);


        //Setting Spilt time report routes
        router.post(GET_SPLIT_TIME_REPORT_DATA + ":sessionId")
                .produces("application/json")
                .handler(this::getSplitTimeData);

        //Setting Quick book routes
        router.get(GET_ORGANIZATION_QUICKBOOK_LIST + ":sessionId")
                .produces("application/json")
                .handler(this::getOrganizationQuickBookList);

        router.post(GET_CLIENT_ORGANIZATION_QUICK + ":sessionId")
                .produces("application/json")
                .handler(this::getClientByOrganization);

        router.post(SAVE_COLLECTIONS_PROCESSING_BY_ONE_CLIENT + ":sessionId")
                .produces("application/json")
                .handler(this::saveCollectionsProcessingByOneClient);

        router.post(SAVE_COLLECTIONS_PROCESSING_BY_ALL_CLIENT + ":sessionId")
                .produces("application/json")
                .handler(this::saveCollectionsProcessingByAllClient);

        router.post(GET_COLLECTIONS_PROCESSING + ":sessionId")
                .produces("application/json")
                .handler(this::getCollectionProcessingByOrganizationIdAndClientId);

        router.post(SAVE_OFFSET + ":sessionId")
                .produces("application/json")
                .handler(this::saveOffset);

        router.post(GET_OFFSET + ":sessionId")
                .produces("application/json")
                .handler(this::getOffset);

        router.get(GET_ORGANIZATION_QUICKBOOK_LIST + ":sessionId")
                .produces("application/json")
                .handler(this::getOrganizationQuickBookList);

        //Setting Quick book report routes
        router.get(PRINT_PROC_INSTR + ":sessionId")
                .produces("application/json")
                .handler(this::printProcInstr);

        router.post(QUICK_BOOK_BALANCING_REPORT_DATA + ":sessionId")
                .produces("application/json")
                .handler(this::quickBookBalancingReportData);


    }


    /**
     * This method allows the user to login to the BO & Report application
     * It allows the user to access to all his/her available options
     * depending on his/her roles
     *
     * @param routingContext
     */
    private void loginBackOfficeAndReportsApp(RoutingContext routingContext) {

        routingContext.request().bodyHandler(body -> {


            String reqBody = body.toString();
            User user = this.gson.fromJson(reqBody, User.class);
            Logger.getInstance("").log(LoggerTag.DEBUG, "User, [" + Json.encodePrettily(user) + "]", "");


            vertx.<User>executeBlocking(future -> MainReportVerticle.this.trustBalancingService.loginBackOfficeAndReportsApp(user, future::complete), asyncResult -> {

                if (asyncResult.succeeded()) {

                    if (asyncResult.result() == null) {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment("Login error, retry later or contact administrator");
                        routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));

                    } else if (!asyncResult.result().isConnected()) {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment("Wrong username or password");
                        routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(errorResponse));

                    } else {

                        ApiResponse response = new ApiResponse();
                        response.setResponseType(WsResponseType.TYPE_REQUEST);
                        response.setData(this.gson.toJson(asyncResult.result()));

                        //response.put(HttpHeaders.Names.AUTHORIZATION, "Basic " + base64key);

                        String token = authProvider.generateToken(
                                new JsonObject()
                                        .put("sessionid", user.getSessionId())
                        );

                        routingContext.response()
                                .setStatusCode(RESPONSE_STATUS_CODE_OK)
                                .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                .putHeader("token", token)
                                .end(this.gson.toJson(response));
                    }
                } else {
                    ApiResponse errorResponse = new ApiResponse();
                    errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                    errorResponse.setComment("Internal server error, contact administrator");
                    errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                    routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                    Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");
                }
            });
        });
    }

    /**
     * This method allows the user to logout from the BO & Report application
     *
     * @param routingContext
     */
    private void logoutBackOfficeAndReportsApp(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];
            Logger.getInstance("").log(LoggerTag.DEBUG, "sessionId, [" + sessionId + "]", "");

            vertx.<Boolean>executeBlocking(future -> {
                boolean responseResult = this.trustBalancingService.logoutBackOfficeAndReportsApp(sessionId);
                future.complete(responseResult);
            }, asyncResult -> {
                if (asyncResult.succeeded()) {

                    ApiResponse response = new ApiResponse();
                    response.setData("" + asyncResult.result());

                    routingContext.response()
                            .setStatusCode(RESPONSE_STATUS_CODE_OK)
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .end(this.gson.toJson(response));
                } else {
                    ApiResponse errorResponse = new ApiResponse();
                    errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                    errorResponse.setComment("An error has occured on server");
                    errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                    routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                    Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");
                }
            });
        });
    }

    /**
     * This method allows the user to get the list of options that can be accessed by the connected user
     * from the BO & Report application
     *
     * @param routingContext
     */
    private void getOptionsList(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];

            vertx.<ArrayList<Option>>executeBlocking(future -> this.trustBalancingService.getOptionsList(sessionId, future::complete), asyncResult -> {

                if (asyncResult.succeeded()) {


                    if (asyncResult.result() == null) {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment("The provided session id is not no longer connected or not exists on server");
                        routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(errorResponse));

                    } else {

                        ApiResponse response = new ApiResponse();
                        response.setResponseType(WsResponseType.TYPE_REQUEST);
                        response.setData(this.gson.toJson(asyncResult.result()));

                        routingContext.response()
                                .setStatusCode(RESPONSE_STATUS_CODE_OK)
                                .putHeader("content-type", "application/json; charset=utf-8")
                                .end(this.gson.toJson(response));

                    }
                } else {
                    ApiResponse errorResponse = new ApiResponse();
                    errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                    errorResponse.setComment("An error has occured on server");
                    errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                    routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                    Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");
                }
            });
        });
    }

    /**
     * This method allows to get the list of all organization list within Infinity database
     *
     * @param routingContext
     */
    private void getOrganizationList(RoutingContext routingContext) {

        routingContext.request().bodyHandler(body -> {

            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];
            Logger.getInstance("").log(LoggerTag.DEBUG, "sessionId, [" + sessionId + "]", "");

/*
            authProvider.authenticate(new JsonObject(), res->{
                if (res.succeeded()) {
                    io.vertx.ext.auth.User theUser = res.result();*/
            Logger.getInstance("").log(LoggerTag.DEBUG, "authentified, []", "");

            vertx.<ArrayList<String>>executeBlocking(future -> this.trustBalancingService.getOrganizationList(sessionId, future::complete), asyncResult -> {

                if (asyncResult.succeeded()) {

                    if (asyncResult.result() == null) {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment("The provided session id is not no longer connected or not exists on server");
                        routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(errorResponse));

                    } else if (!asyncResult.result().isEmpty() && asyncResult.result().get(0).toLowerCase().equals("err")) {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment("Database error: error getting orgnization list");
                        errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                        routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(errorResponse));
                        Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");

                    } else {
                        ApiResponse response = new ApiResponse();
                        response.setResponseType(WsResponseType.TYPE_REQUEST);
                        response.setData(this.gson.toJson(asyncResult.result()));
                        if (asyncResult.result().isEmpty())
                            response.setComment("No organization found");
                        routingContext.response()
                                .setStatusCode(RESPONSE_STATUS_CODE_OK)
                                .putHeader("content-type", "application/json; charset=utf-8")
                                .end(this.gson.toJson(response));
                    }


                } else {
                    ApiResponse errorResponse = new ApiResponse();
                    errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                    errorResponse.setComment("An error has occured on server (database error)");
                    errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                    routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                    Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");
                }
            });

                /*} else {
                    // Failed!
                    ApiResponse errorResponse = new ApiResponse();
                    errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                    errorResponse.setComment("You are not authorized to access this ressource");
                    routingContext.response().setStatusCode(STATUS_CODE_401).end(this.gson.toJson(errorResponse));
                    Logger.getInstance("").log(LoggerTag.ERROR,"authentification, [ FAILED :"+ ExceptionUtils.exceptionStackTraceAsString(res.cause()) +"]","");

                }
            });*/

        });
    }

    /**
     * This method allows to get the list of all organization list within Infinity database
     *
     * @param routingContext
     */
    private void getOrganizationQuickBookList(RoutingContext routingContext) {
        Logger.getInstance("").log(LoggerTag.DEBUG, "getOrganizationList quick book balancing", "");
        routingContext.request().bodyHandler(body -> {

            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];
            Logger.getInstance("").log(LoggerTag.DEBUG, "sessionId, [" + sessionId + "]", "");

            ApiResponse response = new ApiResponse();
            Logger.getInstance("").log(LoggerTag.DEBUG, "authentified, []", "");

            vertx.<List<QuickOrganization>>executeBlocking(future -> this.quickBookBalancingService.getOrganizationList(sessionId, future::complete), asyncResult -> {

                if (asyncResult.succeeded()) {

                    if (asyncResult.result() == null) {
                        response.setResponseType(WsResponseType.TYPE_ERROR);
                        response.setComment("The provided session id is not no longer connected or not exists on server");
                        routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(response));

                    } else if (!asyncResult.result().isEmpty() && asyncResult.result().get(0).getOrganization_id() < 1) {
                        response.setResponseType(WsResponseType.TYPE_ERROR);
                        response.setComment("Database error: error getting orgnization list");
                        response.setData(this.gson.toJson(asyncResult.cause()));
                        routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(response));
                        Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");

                    } else {
                        response.setResponseType(WsResponseType.TYPE_REQUEST);
                        response.setData(this.gson.toJson(asyncResult.result()));
                        if (asyncResult.result().isEmpty())
                            response.setComment("No organization found");
                        routingContext.response()
                                .setStatusCode(RESPONSE_STATUS_CODE_OK)
                                .putHeader("content-type", "application/json; charset=utf-8")
                                .end(this.gson.toJson(response));
                    }


                } else {
                    response.setResponseType(WsResponseType.TYPE_ERROR);
                    response.setComment("An error has occured on server (database error)");
                    response.setData(this.gson.toJson(asyncResult.cause()));
                    routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(response));
                    Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");
                }
            });

        });
    }


    private void getHealthOrganizationList(RoutingContext routingContext) {
        String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];
        Logger.getInstance("").log(LoggerTag.DEBUG, "sessionId, [" + sessionId + "]", "");

        Logger.getInstance("").log(LoggerTag.DEBUG, "authentified, []", "");

        vertx.<List<Organization>>executeBlocking(future -> this.healthCareService.getOrganizationList(sessionId, future::complete), asyncResult -> {

            if (asyncResult.succeeded()) {

                if (asyncResult.result() == null) {
                    ApiResponse errorResponse = new ApiResponse();
                    errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                    errorResponse.setComment("The provided session id is not no longer connected or not exists on server");
                    routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(errorResponse));

                } else if (asyncResult.result().get(0) == null) {
                    ApiResponse errorResponse = new ApiResponse();
                    errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                    errorResponse.setComment("Database error: error getting orgnization list");
                    errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                    routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(errorResponse));
                    Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");

                } else {
                    ApiResponse response = new ApiResponse();
                    response.setResponseType(WsResponseType.TYPE_REQUEST);
                    response.setData(this.gson.toJson(asyncResult.result()));
                    if (asyncResult.result().isEmpty())
                        response.setComment("No organization found");
                    routingContext.response()
                            .setStatusCode(RESPONSE_STATUS_CODE_OK)
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .end(this.gson.toJson(response));
                }


            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setComment("An error has occured on server (database error)");
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(asyncResult.cause()), "");
            }
        });


    }


    /**
     * This method allows to get the trust balancing data result related to the data selected by the user
     *
     * @param routingContext
     */
    private void getTrustBalancingData(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];
            Logger.getInstance("").log(LoggerTag.DEBUG, "TrustBalancingInputData, [" + reqBody + "]", "");
            TrustBalancingInputData trustBalancingInputData = this.gson.fromJson(reqBody, TrustBalancingInputData.class);

            vertx.<InternalResponse>executeBlocking(future -> this.trustBalancingService.getTrustBalancingData(sessionId, trustBalancingInputData, future), asyncResult -> {


                if (asyncResult.succeeded()) {

                    if (asyncResult.result().getCause() == 1) {

                        ApiResponse response = new ApiResponse();
                        response.setResponseType(WsResponseType.TYPE_REQUEST);


                        //List<TrustBalancingOutputData> data=Json.decodeValue(asyncResult.result().getData()List.class);
                        //String jsonData = Json.encode(Collections.singletonList(asyncResult.result().getData()));
                        List<TrustBalancingOutputData> data = Json.decodeValue(asyncResult.result().getData(), new TypeReference<List<TrustBalancingOutputData>>() {
                        });
                        //List<Foo> fooList = Json.decodeValue(bodyAsString, new TypeReference<List<Foo>>(){});
                        System.out.println("In Verticle LIST SIZE =  " + data.size());
                        System.out.println(Json.encodePrettily(asyncResult.result()));

                        response.setData(new TrustExcelFileGenerator(data).getBase64GeneratedFile());

                        routingContext.response()
                                .setStatusCode(RESPONSE_STATUS_CODE_OK)
                                .putHeader("content-type", "application/json; charset=utf-8")
                                .end(this.gson.toJson(response));

                    } else if (asyncResult.result().getCause() == 0) {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment(asyncResult.result().getData());
                        routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(errorResponse));


                    } else {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment("The provided session id is not no longer connected or not exists on server");
                        routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));

                    }

                } else {
                    ApiResponse errorResponse = new ApiResponse();
                    errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                    errorResponse.setComment("An error has occured on server");
                    errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                    routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                    Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");
                }
            });


        });
    }


    private void getCmHealthData(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];
            Logger.getInstance("").log(LoggerTag.DEBUG, "Comments, [" + reqBody + "]", "");
            String[] healthCareInputData = this.gson.fromJson(reqBody, String[].class);

            vertx.<InternalResponse>executeBlocking(future -> this.healthCareService.getCmhealthcare(sessionId, healthCareInputData, future), asyncResult -> {


                if (asyncResult.succeeded()) {

                    if (asyncResult.result().getCause() == 1) {

                        ApiResponse response = new ApiResponse();
                        response.setResponseType(WsResponseType.TYPE_REQUEST);


                        System.out.println(Json.encodePrettily(asyncResult.result()));

                        response.setData(asyncResult.result().getData());

                        routingContext.response()
                                .setStatusCode(RESPONSE_STATUS_CODE_OK)
                                .putHeader("content-type", "application/json; charset=utf-8")
                                .end(this.gson.toJson(response));


                        String fileName = Paths.get("assets", "healthCareFiles") + File.separator + sessionId.replace("-", "") + ".xlsx";
                        String file2Name = Paths.get("assets", "healthCareFiles") + File.separator + sessionId.replace("-", "") + "_2.pptx";
                        String file3Name = Paths.get("assets", "healthCareFiles") + File.separator + sessionId.replace("-", "") + "_cm.pptx";

                        String[] files = new String[]{fileName, file2Name, file3Name};

                        for (String file : files) {
                            vertx.fileSystem().exists(file, ex -> {
                                if (ex.succeeded()) {
                                    vertx.fileSystem().delete(file, del -> {
                                        if (del.succeeded()) {
                                            System.out.println("Cleaning File: [ " + file + " ]");
                                        }
                                    });

                                }
                            });

                        }


                    } else if (asyncResult.result().getCause() == 0) {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment(asyncResult.result().getData());
                        routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(errorResponse));


                    } else {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment("The provided session id is not no longer connected or not exists on server");
                        routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(errorResponse));

                    }

                } else {
                    ApiResponse errorResponse = new ApiResponse();
                    errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                    errorResponse.setComment("An error has occured on server");
                    errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                    routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                    Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");
                }
            });

        });

    }


    private void getHealthData(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];
            HealthCareInputData healthCareInputData = this.gson.fromJson(reqBody, HealthCareInputData.class);
            Logger.getInstance("").log(LoggerTag.DEBUG, "bd, [" + reqBody + "]", "");

            //For SQL COrrect Date
            healthCareInputData.setBeginDateRange(healthCareInputData.getBeginDateRange().split("T")[0]);
            healthCareInputData.setEndDateRange(healthCareInputData.getEndDateRange().split("T")[0]);
            Logger.getInstance("").log(LoggerTag.DEBUG, "HealthCareInputData, [" + Json.encodePrettily(healthCareInputData) + "]", "");


            vertx.<InternalResponse>executeBlocking(future -> this.healthCareService.gethealthcare(sessionId, healthCareInputData, future), asyncResult -> {


                if (asyncResult.succeeded()) {

                    if (asyncResult.result().getCause() == 1) {

                        ApiResponse response = new ApiResponse();
                        response.setResponseType(WsResponseType.TYPE_REQUEST);


                        response.setData(Json.encode(new HealthExcelAndPPTFileHelper(Json.decodeValue(asyncResult.result().getData(), HealthOutputData.class)).generatePPTFile(sessionId)));

                        routingContext.response()
                                .setStatusCode(RESPONSE_STATUS_CODE_OK)
                                .putHeader("content-type", "application/json; charset=utf-8")
                                .end(this.gson.toJson(response));

                    } else if (asyncResult.result().getCause() == 0) {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment(asyncResult.result().getData());
                        routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(errorResponse));


                    } else {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment("The provided session id is not no longer connected or not exists on server");
                        routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(errorResponse));

                    }

                } else {
                    ApiResponse errorResponse = new ApiResponse();
                    errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                    errorResponse.setComment("An error has occured on server");
                    errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                    routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                    Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");
                }
            });

        });

    }


    private void getSplitTimeData(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];
            Logger.getInstance("").log(LoggerTag.DEBUG, "SplitTimeReportInputData, [" + reqBody + "]", "");
            SplitTimeReportInputData splitTimeReportInputData = this.gson.fromJson(reqBody, SplitTimeReportInputData.class);

            vertx.<InternalResponse>executeBlocking(future -> this.splitTimeReportService.getSplitTimeData(sessionId, splitTimeReportInputData, future), asyncResult -> {

                if (asyncResult.succeeded()) {

                    if (asyncResult.result().getCause() == 1) {

                        ApiResponse response = new ApiResponse();
                        response.setResponseType(WsResponseType.TYPE_REQUEST);


                        //List<SplitTimeReportOutputData> data = Json.decodeValue(asyncResult.result().getData(),new TypeReference<List<SplitTimeReportOutputData>>(){});
                        SplitTimeReportOutputData data = Json.decodeValue(asyncResult.result().getData(), new TypeReference<SplitTimeReportOutputData>() {
                        });

                        System.out.println("In Verticle LIST SIZE =  " + data.getSplitTimeDtoList().size());
                        System.out.println(Json.encodePrettily(asyncResult.result()));

                        response.setData(new SplitTimeReportExcelFileGenerator(data).getBase64GeneratedFile());

                        routingContext.response()
                                .setStatusCode(RESPONSE_STATUS_CODE_OK)
                                .putHeader("content-type", "application/json; charset=utf-8")
                                .end(this.gson.toJson(response));

                    } else if (asyncResult.result().getCause() == 0) {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment(asyncResult.result().getData());
                        routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(errorResponse));

                    } else {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment("The provided session id is not no longer connected or not exists on server");
                        routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                    }
                } else {
                    ApiResponse errorResponse = new ApiResponse();
                    errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                    errorResponse.setComment("An error has occured on server");
                    errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                    routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                    Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");
                }
            });
        });
    }

    /**
     * This method is called when we stop the MainReportVerticle. Here, we are also closing database connection
     *
     * @throws Exception
     */
    @Override
    public void stop() {
        System.out.println("Stopping MainReportVerticle verticle");
        // close websocket connection

    }


    /**
     * This method allows to get the list of all organization list within Infinity database
     *
     * @param routingContext
     */
    private void getClientByOrganization(RoutingContext routingContext) {
        Logger.getInstance("").log(LoggerTag.DEBUG, "getClientByOrganization quick book balancing", "");
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();

            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];
            Logger.getInstance("").log(LoggerTag.DEBUG, "sessionId, [" + sessionId + "]", "");
            QuickOrganizationClientInputData quickOrganizationClientInputData = null;
            try {
                quickOrganizationClientInputData =
                        this.gson.fromJson(reqBody, QuickOrganizationClientInputData.class);
            } catch (Exception e) {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setComment("Organization Id is not found or empty");
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
            }
            Logger.getInstance("").log(LoggerTag.DEBUG, "bd, [" + reqBody + "]", "");
            Logger.getInstance("").log(LoggerTag.DEBUG, "authentified, []", "");
            if (quickOrganizationClientInputData != null && quickOrganizationClientInputData.getOrganizationId() > 0) {
                final int organizationId = quickOrganizationClientInputData.getOrganizationId();
                vertx.<List<QuickOrganizationClient>>executeBlocking(future ->
                        this.quickBookBalancingService.getClientByOrganization(sessionId, organizationId
                                , future::complete), asyncResult -> {
                    this.sendResponse(routingContext, asyncResult);
                });
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setComment("Organization Id is empty");
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
            }

        });
    }


    private void sendResponse(RoutingContext routingContext, AsyncResult<List<QuickOrganizationClient>> asyncResult) {
        ApiResponse response = new ApiResponse();
        if (asyncResult.succeeded()) {
            if (asyncResult.result() == null) {
                response.setResponseType(WsResponseType.TYPE_ERROR);
                response.setComment("The provided session id is not no longer connected or not exists on server");
                routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(response));

            } else if (!asyncResult.result().isEmpty() && asyncResult.result().get(0).getClientName() == "") {
                response.setResponseType(WsResponseType.TYPE_ERROR);
                response.setComment("Database error: error getting orgnization list");
                response.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(response));
                Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");

            } else {
                response.setResponseType(WsResponseType.TYPE_REQUEST);
                response.setData(this.gson.toJson(asyncResult.result()));
                response.setDataSize(asyncResult.result().size());
                if (asyncResult.result().isEmpty())
                    response.setComment("No organization found");
                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            }

        } else {
            response.setResponseType(WsResponseType.TYPE_ERROR);
            response.setComment("An error has occured on server (database error)");
            response.setData(this.gson.toJson(asyncResult.cause()));
            routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(response));
            Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");
        }
    }

    private void saveCollectionsProcessingByAllClient(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String requestBody = body.toString();
            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];
            Logger.getInstance("").log(LoggerTag.DEBUG, "CollectionsProcessingInputData, [" + requestBody + "]", "");
            CollectionsProcessingInputData collectionsProcessingInputData = this.gson.fromJson(requestBody, CollectionsProcessingInputData.class);
            Logger.getInstance("").log(LoggerTag.DEBUG, "collectionsProcessingInputData.getClient_id(), [" + collectionsProcessingInputData.getClient_id() + "]", "");
            ApiResponse response = new ApiResponse();
            vertx.<CollectionsProcessingInputData>executeBlocking(future -> this.quickBookBalancingService.saveCollectionsProcessingByAllClient(sessionId, collectionsProcessingInputData, future::complete), asyncResult -> {
                if (asyncResult.succeeded()) {
                    if (asyncResult.result() != null) {
                        response.setResponseType(WsResponseType.TYPE_REQUEST);
                        response.setData(this.gson.toJson(asyncResult.result()));
                        routingContext.response()
                                .setStatusCode(RESPONSE_STATUS_CODE_OK)
                                .putHeader("content-type", "application/json; charset=utf-8")
                                .end(this.gson.toJson(response));
                    } else {
                        response.setResponseType(WsResponseType.TYPE_ERROR);
                        response.setComment("Error save collections processing");
                        routingContext.response().setStatusCode(STATUS_CODE_500).putHeader("content-type", "application/json; charset=utf-8").end(this.gson.toJson(response));
                    }
                }
            });
        });
    }

    private void saveCollectionsProcessingByOneClient(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String requestBody = body.toString();
            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];
            Logger.getInstance("").log(LoggerTag.DEBUG, "CollectionsProcessingInputData, [" + requestBody + "]", "");
            CollectionsProcessingInputData collectionsProcessingInputData = this.gson.fromJson(requestBody, CollectionsProcessingInputData.class);
            Logger.getInstance("").log(LoggerTag.DEBUG, "collectionsProcessingInputData.getClient_id(), [" + collectionsProcessingInputData.getClient_id() + "]", "");
            ApiResponse response = new ApiResponse();
            vertx.<CollectionsProcessingInputData>executeBlocking(future -> this.quickBookBalancingService.saveCollectionsProcessingByOneClient(sessionId, collectionsProcessingInputData, future::complete), asyncResult -> {
                if (asyncResult.succeeded()) {
                    if (asyncResult.result() != null) {
                        response.setResponseType(WsResponseType.TYPE_REQUEST);
                        response.setData(this.gson.toJson(asyncResult.result()));
                        routingContext.response()
                                .setStatusCode(RESPONSE_STATUS_CODE_OK)
                                .putHeader("content-type", "application/json; charset=utf-8")
                                .end(this.gson.toJson(response));
                    } else {
                        response.setResponseType(WsResponseType.TYPE_ERROR);
                        response.setComment("Error save collections processing");
                        routingContext.response().setStatusCode(STATUS_CODE_500).putHeader("content-type", "application/json; charset=utf-8").end(this.gson.toJson(response));
                    }
                }
            });
        });
    }

    /**
     * This method allows to get the collection processing by organization and client
     *
     * @param routingContext
     */
    private void getCollectionProcessingByOrganizationIdAndClientId(RoutingContext routingContext) {
        Logger.getInstance("").log(LoggerTag.DEBUG, "getCollectionProcessingByOrganizationIdAndClientId quick book balancing", "");
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();

            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];
            Logger.getInstance("").log(LoggerTag.DEBUG, "sessionId, [" + sessionId + "]", "");
            RequestCollectionProcesing requestCollectionProcesing = null;
            try {
                requestCollectionProcesing =
                        this.gson.fromJson(reqBody, RequestCollectionProcesing.class);
            } catch (Exception e) {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setComment("Organization Id is not found or empty or client Id is not found or empty");
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
            }
            Logger.getInstance("").log(LoggerTag.DEBUG, "bd, [" + reqBody + "]", "");
            Logger.getInstance("").log(LoggerTag.DEBUG, "authentified, []", "");
            if (requestCollectionProcesing != null
                    && requestCollectionProcesing.getOrganizationId() > 0
                    && requestCollectionProcesing.getClientId() > 0) {
                final int organizationId = requestCollectionProcesing.getOrganizationId();
                final int clientId = requestCollectionProcesing.getClientId();
                vertx.<CollectionsProcessingOutputData>executeBlocking(future ->
                        this.quickBookBalancingService.getCollectionProcessingByOrganizationIdAndClientId(
                                sessionId, organizationId, clientId
                                , future::complete), asyncResult -> {
                    this.sendResponseCollectionProcessing(routingContext, asyncResult);
                });
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setComment("Organization Id is empty");
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
            }

        });
    }

    private void sendResponseCollectionProcessing(RoutingContext routingContext, AsyncResult<CollectionsProcessingOutputData> asyncResult) {

        if (asyncResult.succeeded()) {
            ApiResponse response = new ApiResponse();
            if (asyncResult.result() == null) {
                response.setResponseType(WsResponseType.TYPE_ERROR);
                response.setComment("The provided session id is not no longer connected or not exists on server");
                routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).putHeader("content-type", "application/json; charset=utf-8").end(this.gson.toJson(response));

            } else if (asyncResult.result().getRecord_id() > 0) {
                response.setResponseType(WsResponseType.TYPE_REQUEST);
                response.setData(this.gson.toJson(asyncResult.result()));
                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));

            } else {
                response.setResponseType(WsResponseType.TYPE_ERROR);
                response.setComment("No collection processing found");
                routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).putHeader("content-type", "application/json; charset=utf-8").end(this.gson.toJson(response));
            }

        } else {
            ApiResponse erroResp = new ApiResponse();
            erroResp.setResponseType(WsResponseType.TYPE_ERROR);
            erroResp.setComment("An error has occured on server (database error)");
            erroResp.setData(this.gson.toJson(asyncResult.cause()));
            routingContext.response().setStatusCode(STATUS_CODE_500).putHeader("content-type", "application/json; charset=utf-8").end(this.gson.toJson(erroResp));
            Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");
        }
    }

    private void saveOffset
            (RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String requestBody = body.toString();
            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];
            Logger.getInstance("").log(LoggerTag.DEBUG, "OffsetInputData, [" + requestBody + "]", "");
            OffsetInputData offsetInputData = this.gson.fromJson(requestBody, OffsetInputData.class);
            if (offsetInputData != null) {
                Logger.getInstance("").log(LoggerTag.DEBUG, "OffsetInputData.getClient_id(), [" + offsetInputData.getClient_id() + "]", "");
            }
            vertx.<OffsetInputData>executeBlocking(future -> this.quickBookBalancingService.saveOffset(sessionId, offsetInputData, future::complete), asyncResult -> {
                this.reponseResultForOffset(asyncResult, routingContext);
            });
        });
    }

    private void quickBookBalancingReportData(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String requestBody = body.toString();
            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];
            Logger.getInstance("").log(LoggerTag.DEBUG, "QuickBookBalancingReportInputData, [" + requestBody + "]", "");
            QuickBookBalancingReportInputData quickBookBalancingReportInputData = this.gson.fromJson(requestBody, QuickBookBalancingReportInputData.class);
            vertx.<InternalResponse>executeBlocking(future -> this.quickBookBalancingService.quickBookBalancingReportData(sessionId, quickBookBalancingReportInputData, future), asyncResult -> {

                if (asyncResult.succeeded()) {

                    if (asyncResult.result().getCause() == 1) {

                        ApiResponse response = new ApiResponse();
                        response.setResponseType(WsResponseType.TYPE_REQUEST);


                        QuickBookBalancingReportOutputData data = Json.decodeValue(asyncResult.result().getData(), new TypeReference<QuickBookBalancingReportOutputData>() {
                        });

                        System.out.println("In Verticle LIST SIZE =  " + data.getCollectionsProcessingOutputDataList().size());
                        System.out.println(Json.encodePrettily(asyncResult.result()));

                        response.setData(new QuickBookBalancingExcelFileGenerator(data).getBase64GeneratedFile());

                        routingContext.response()
                                .setStatusCode(RESPONSE_STATUS_CODE_OK)
                                .putHeader("content-type", "application/json; charset=utf-8")
                                .end(this.gson.toJson(response));

                    } else if (asyncResult.result().getCause() == 0) {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment(asyncResult.result().getData());
                        routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(errorResponse));

                    } else {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment("The provided session id is not no longer connected or not exists on server");
                        routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                    }
                } else {
                    ApiResponse errorResponse = new ApiResponse();
                    errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                    errorResponse.setComment("An error has occured on server");
                    errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                    routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                    Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");
                }
            });
        });
    }

    private void reponseResultForOffset(AsyncResult<OffsetInputData> asyncResult, RoutingContext routingContext) {
        if (asyncResult.succeeded()) {
            ApiResponse response = new ApiResponse();
            if (asyncResult.result() != null) {
                response.setResponseType(WsResponseType.TYPE_REQUEST);
                response.setData(this.gson.toJson(asyncResult.result()));
                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                response.setResponseType(WsResponseType.TYPE_ERROR);
                response.setComment("Error save offset");
                routingContext.response().setStatusCode(STATUS_CODE_500).putHeader("content-type", "application/json; charset=utf-8").end(this.gson.toJson(response));
            }
        }
    }

    private void getOffset(RoutingContext routingContext) {
        Logger.getInstance("").log(LoggerTag.DEBUG, "getOffset quick book balancing", "");
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();

            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];
            Logger.getInstance("").log(LoggerTag.DEBUG, "sessionId, [" + sessionId + "]", "");
            OffsetInputData offsetInputData = null;
            try {
                offsetInputData =
                        this.gson.fromJson(reqBody, OffsetInputData.class);
            } catch (Exception e) {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setComment("Organization Id is not found or empty");
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
            }
            Logger.getInstance("").log(LoggerTag.DEBUG, "bd, [" + reqBody + "]", "");
            Logger.getInstance("").log(LoggerTag.DEBUG, "authentified, []", "");
            if (offsetInputData != null && offsetInputData.getClient_id() > 0 && offsetInputData.getOrg_id() > 0) {
                final int clientId = offsetInputData.getClient_id();
                final int orgId = offsetInputData.getOrg_id();
                vertx.<List<OffsetOutputData>>executeBlocking(future ->
                        this.quickBookBalancingService.getOffsetByOrgIdAndClientId(sessionId, orgId, clientId
                                , future::complete), asyncResult -> {
                    this.sendResponseOffset(routingContext, asyncResult);
                });
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setComment("Client Id is empty");
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
            }

        });
    }

    private void sendResponseOffset(RoutingContext routingContext, AsyncResult<List<OffsetOutputData>> asyncResult) {
        ApiResponse response = new ApiResponse();
        if (asyncResult.succeeded()) {
            if (asyncResult.result() == null) {
                response.setResponseType(WsResponseType.TYPE_ERROR);
                response.setComment("The provided session id is not no longer connected or not exists on server");
                routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(response));

            } else if (!asyncResult.result().isEmpty() && asyncResult.result().get(0).getOffset_date() == "") {
                response.setResponseType(WsResponseType.TYPE_ERROR);
                response.setComment("Database error: error getting orgnization list");
                response.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(response));
                Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");

            } else {
                response.setResponseType(WsResponseType.TYPE_REQUEST);
                response.setData(this.gson.toJson(asyncResult.result()));
                response.setDataSize(asyncResult.result().size());
                if (asyncResult.result().isEmpty())
                    response.setComment("No offset found");
                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            }

        } else {
            response.setResponseType(WsResponseType.TYPE_ERROR);
            response.setComment("An error has occured on server (database error)");
            response.setData(this.gson.toJson(asyncResult.cause()));
            routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(response));
            Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");
        }
    }

    private void printProcInstr(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String sessionId = String.valueOf(routingContext.request().getParam("sessionId")).split(":")[1];

            vertx.<InternalResponse>executeBlocking(future -> this.quickBookBalancingService.printProcInstr(sessionId, future), asyncResult -> {

                if (asyncResult.succeeded()) {

                    if (asyncResult.result().getCause() == 1) {

                        ApiResponse response = new ApiResponse();
                        response.setResponseType(WsResponseType.TYPE_REQUEST);


                        QuickBookBalancingReportOutputData data = Json.decodeValue(asyncResult.result().getData(), new TypeReference<QuickBookBalancingReportOutputData>() {
                        });

                        System.out.println("In Verticle LIST SIZE =  " + data.getCollectionsProcessingOutputDataList().size());
                        System.out.println(Json.encodePrettily(asyncResult.result()));

                        response.setData(new QuickBookBalancingExcelFileGenerator(data).getBase64GeneratedFile());

                        routingContext.response()
                                .setStatusCode(RESPONSE_STATUS_CODE_OK)
                                .putHeader("content-type", "application/json; charset=utf-8")
                                .end(this.gson.toJson(response));

                    } else if (asyncResult.result().getCause() == 0) {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment(asyncResult.result().getData());
                        routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(errorResponse));

                    } else {
                        ApiResponse errorResponse = new ApiResponse();
                        errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                        errorResponse.setComment("The provided session id is not no longer connected or not exists on server");
                        routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                    }
                } else {
                    ApiResponse errorResponse = new ApiResponse();
                    errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                    errorResponse.setComment("An error has occured on server");
                    errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                    routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                    Logger.getInstance("").log(LoggerTag.ERROR, asyncResult.cause().getMessage(), "");
                }
            });
        });
    }

}




