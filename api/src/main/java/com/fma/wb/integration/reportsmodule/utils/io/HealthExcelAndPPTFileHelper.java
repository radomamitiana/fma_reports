package com.fma.wb.integration.reportsmodule.utils.io;

import com.fma.wb.integration.reportsmodule.dto.healthcare.HealthOutputData;
import com.fma.wb.integration.reportsmodule.utils.DateUtils;
import io.vertx.core.json.Json;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.sl.usermodel.ShapeType;
import org.apache.poi.sl.usermodel.TableCell;
import org.apache.poi.sl.usermodel.TextParagraph;
import org.apache.poi.sl.usermodel.TextShape;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xslf.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.awt.Color;
import java.awt.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.time.Month;
import java.util.*;
import java.util.List;
import java.util.function.Consumer;

import static com.fma.wb.integration.common.utils.ConfigurationUtils.SIGNATURE;
import static org.apache.poi.ss.usermodel.CellType.BLANK;


public class HealthExcelAndPPTFileHelper {


    private XSLFTable batchTrackTable;
    private HealthOutputData outputData;
    private Workbook ff;
    private FormulaEvaluator evaluator;

    public HealthExcelAndPPTFileHelper() {
    }


    public HealthExcelAndPPTFileHelper(HealthOutputData outputData) {
        this.outputData=outputData;

        System.out.println("OUTPUT GOT: "+ Json.encodePrettily(this.outputData));

    }

    private String[] getBase64GeneratedFiles(String sessionId) {
        //add sessionId

        String fileName= Paths.get("assets","healthCareFiles")+File.separator+sessionId.replace("-", "")+".xlsx";
        String file2Name= Paths.get("assets","healthCareFiles") +File.separator+sessionId.replace("-", "")+"_2.pptx";

        return new String[]{encodeFileToBase64Binary(fileName),encodeFileToBase64Binary(file2Name)};
    }

    public String getBase64CommentedPPT(String sessionId, String[] comments) {

        this.addCommentsToPPt(sessionId,comments);
        String file2Name= Paths.get("assets","healthCareFiles") +File.separator+sessionId.replace("-", "")+"_cm.pptx";

        return encodeFileToBase64Binary(file2Name);
    }




    public String[] generatePPTFile(String sessionId){

        new HealthCareExcelFileGenerator(this.outputData).getBase64GeneratedFile();

        try {
            String fileName= Paths.get("assets","healthCareFiles") +File.separator+"1.pptx";
            //String fileNameEx= Paths.get("assets","healthCareFiles") +File.separator+"temp.xlsx";
            String fileNameEx= Paths.get("assets","healthCareFiles") +File.separator+sessionId.replace("-", "")+".xlsx";
            FileInputStream ex = new FileInputStream(fileNameEx) ;
            ff=new XSSFWorkbook(ex);
            evaluator = ff.getCreationHelper().createFormulaEvaluator();

            Sheet ssh=ff.getSheet("Batch Track");
            Sheet ssh2=ff.getSheet("Financial Class");
            Sheet ssh3=ff.getSheet("Aging");
            Sheet ssh4=ff.getSheet("Overall Results");

            FileInputStream is = new FileInputStream(fileName) ;
            XMLSlideShow ppt = new XMLSlideShow(is);
            //ppt.setPageSize(new Dimension(ppt.getPageSize().width+25,ppt.getPageSize().height+25));
            XSLFSlide ss=ppt.getSlides().get(0);
            XSLFSlide ss2=ppt.getSlides().get(2);
            XSLFSlide ss3=ppt.getSlides().get(3);
            XSLFSlide ss4=ppt.getSlides().get(4);

            //Check and create extra slides for aging data

            List<Row> rrs=new ArrayList<>();
            for (int i = 0; i <= ssh3.getLastRowNum(); i++) {
                rrs.add(ssh3.getRow(i));
            }

            List<List<Row>> splittedAgingList=chopped(rrs,30);

            int slideNb=splittedAgingList.size();

            System.out.println("SPL: "+slideNb);



            for (int i = 0; i <slideNb; i++) {

                XSLFSlideLayout layout = ss2.getSlideLayout();
                XSLFSlide temp=ppt.createSlide(layout);
                temp.importContent(ss2);
                //temp.createAutoShape().setShapeType(ShapeType.RECT);


            }


            XSLFSlide ss5=ppt.getSlides().get(ppt.getSlides().size()-2);
            XSLFSlide ss6=ppt.getSlides().get(ppt.getSlides().size()-1);


            Rectangle rect=new Rectangle(0, 420, 720,100);
            XSLFTextBox bb2=ss2.createTextBox();
            XSLFTextParagraph textparagraph2 =bb2.addNewTextParagraph();
            XSLFTextRun text=textparagraph2.addNewTextRun();
            textparagraph2.setTextAlign(TextParagraph.TextAlign.CENTER);
            bb2.setAnchor(rect);
            text.setText(" ");
            bb2.setFillColor(Color.WHITE);
            text.setFontSize(12.0);
            text.setFontColor(Color.GRAY);
            bb2.setTextAutofit(TextShape.TextAutofit.NORMAL);

       /*     double pg2Width = ss2.getSlideShow().getPageSize().width;
            double pg2Height = ss2.getSlideShow().getPageSize().height;*/

            //int h=Integer.valueOf(hh.)
            rect.setSize(ss2.getSlideShow().getPageSize().width, (int)bb2.getTextHeight()+15);
            bb2.setAnchor(rect);
            //create1stTable(ss2);
            //bb2.moveTo(Float.valueOf(""+(pg2Height-bb2.get)));



            XSLFTextBox bb4=ss4.createTextBox();
            XSLFTextParagraph textparagraph4 =bb4.addNewTextParagraph();
            XSLFTextRun text4=textparagraph4.addNewTextRun();
            textparagraph4.setTextAlign(TextParagraph.TextAlign.CENTER);
            bb4.setAnchor(rect);
            text4.setText(" ");
            bb4.setFillColor(Color.WHITE);
            text4.setFontSize(12.0);
            text4.setFontColor(Color.GRAY);
            bb4.setTextAutofit(TextShape.TextAutofit.SHAPE);

            rect.setSize(ss4.getSlideShow().getPageSize().width, (int)bb4.getTextHeight()+15);
            bb4.setAnchor(rect);

            XSLFTextBox bb5=ss5.createTextBox();
            XSLFTextParagraph textparagraph5 =bb5.addNewTextParagraph();
            XSLFTextRun text5=textparagraph5.addNewTextRun();
            textparagraph5.setTextAlign(TextParagraph.TextAlign.CENTER);
            bb5.setAnchor(rect);
            text5.setText(" ");
            bb5.setFillColor(Color.WHITE);
            text5.setFontSize(12.0);
            text5.setFontColor(Color.GRAY);
            bb5.setTextAutofit(TextShape.TextAutofit.SHAPE);

            rect.setSize(ss5.getSlideShow().getPageSize().width, (int)bb5.getTextHeight()+15);
            bb5.setAnchor(rect);


            XSLFTextBox bb6=ss6.createTextBox();
            XSLFTextParagraph textparagraph6 =bb6.addNewTextParagraph();
            XSLFTextRun text6=textparagraph6.addNewTextRun();
            textparagraph6.setTextAlign(TextParagraph.TextAlign.CENTER);
            bb6.setAnchor(rect);
            text6.setText(" ");
            bb6.setFillColor(Color.WHITE);
            text6.setFontSize(12.0);
            text6.setFontColor(Color.GRAY);
            bb6.setTextAutofit(TextShape.TextAutofit.SHAPE);
            rect.setSize(ss6.getSlideShow().getPageSize().width, (int)bb6.getTextHeight()+15);
            bb6.setAnchor(rect);

            //ss.getShapes().forEach(xslfShape -> System.out.println("NN: "+xslfShape.getShapeName()+"--"+xslfShape.getShapeId()+"\n"));
            XSLFAutoShape aa=ss.createAutoShape();
            XSLFTextParagraph textparagraph =aa.addNewTextParagraph();
            XSLFTextRun compLay=textparagraph.addNewTextRun();
            compLay.setText(this.outputData.getShortOrganizationName());
            aa.setShapeType(ShapeType.FLOW_CHART_TERMINATOR);
            aa.setFillColor(Color.WHITE);
            compLay.setFontSize(19.0);
            compLay.setFontColor(Color.blue);
            aa.setTextAutofit(TextShape.TextAutofit.SHAPE);
            textparagraph.setTextAlign(TextParagraph.TextAlign.CENTER);
            aa.setAnchor(new java.awt.Rectangle(50, 340, 200, 50));


            XSLFAutoShape bb=ss.createAutoShape();
            XSLFTextParagraph textparagraphDate =bb.addNewTextParagraph();
            XSLFTextRun date=textparagraphDate.addNewTextRun();

            String fileDateText=Month.of(Integer.parseInt(DateUtils.getDateMonth(DateUtils.toDayDate()))).name()+" "+DateUtils.getDateYear(DateUtils.toDayDate());

            date.setText(fileDateText);
            date.setFontSize(30.0);
            date.setFontColor(Color.LIGHT_GRAY);
            textparagraphDate.setTextAlign(TextParagraph.TextAlign.RIGHT);
            bb.setTextAutofit(TextShape.TextAutofit.SHAPE);
            bb.setShapeType(ShapeType.RECT);
            bb.setAnchor(new java.awt.Rectangle(550, 80, 160, 100));
            //System.out.println(ss2.getXmlObject().toString());




            // write tables
            createBatchTrackTable(ss2,ssh);
            createFinClassTable(ss4,ssh2);

            for (int i = 0; i < slideNb; i++) {

                List<Row> lls=new ArrayList<>();
                if(i!=0)
                    lls.add(ssh3.getRow(7));
                lls.addAll(splittedAgingList.get(i));
                createAgingTable(ppt.getSlides().get(i+5),lls,(i==0));


            }

            createOverallTable(ss6,ssh4);

            try {
                //OPCPackage ppt2 = OPCPackage.open(fileNameF);

                TreeMap<String, List<String>> sorted=new TreeMap<>((o1, o2) -> {
                    String[] one = o1.split("-");
                    String[] two = o2.split("-");

                    if(Integer.valueOf(one[1])<Integer.valueOf(two[1])){
                        return 1;
                    }
                    else if(Integer.valueOf(one[1])>Integer.valueOf(two[1])){
                        return -1;
                    }
                    else{
                        return Integer.valueOf(two[0]).compareTo(Integer.valueOf(one[0]));
                    }
                });


                sorted.putAll(this.outputData.getBatchTrackAlldatas());
                this.outputData.setBatchTrackAlldatas(sorted);
                new CreatePPTXChartsXSSFWb(this.outputData,ss3);

            } catch (Exception e) {
                e.printStackTrace();
            }

            OutputStream fileOut;
            String fileNameF= Paths.get("assets","healthCareFiles") + File.separator+sessionId.replace("-", "")+"_2.pptx";



            fileOut = new FileOutputStream(fileNameF);
            ppt.write(fileOut);
            fileOut.close();
            ppt.close();






            ex.close();
            /*try {
                new File(fileNameF).delete();
            }catch (Exception e){
                e.printStackTrace();
            }*/
            System.out.println("END......");

            return getBase64GeneratedFiles(sessionId);


        } catch (IOException e) {
            e.printStackTrace();
            return new String[]{"", ""};
        }

    }


    private void createBatchTrackTable(XSLFSlide ss2,Sheet ssh){
        batchTrackTable =ss2.createTable();

        ssh.forEach(rowOr -> {
            XSLFTableRow row=batchTrackTable.addRow();


            for (int i = 0; i < 23; i++) {

                Cell cell=rowOr.getCell(i);


                if(cell!=null && !cell.getCellType().equals(BLANK)){

                    XSLFTableCell cc=row.addCell();
                    cc.setLeftInset(0.0);
                    cc.setRightInset(0.0);
                    cc.setTopInset(0.0);
                    cc.setBottomInset(0.0);
                    cc.setAnchor(new Rectangle(0, 0, 2, 2));
                    //cc.setFillColor(new Color(cell.getCellStyle().getFillBackgroundColor()));

                    XSLFTextParagraph textparagraph=cc.addNewTextParagraph();
                    XSLFTextRun text=textparagraph.addNewTextRun();
                    textparagraph.setTextAlign(TextParagraph.TextAlign.CENTER);
                    text.setFontSize(7.0);
                    cc.setTextAutofit(TextShape.TextAutofit.NORMAL);

                    //System.out.println("DEBUG1: "+cell.getCellType());

                    CellValue cellValue = evaluator.evaluate(cell);

                    switch (cellValue.getCellType()) {
                        case NUMERIC:

                            //System.out.println("DEBUG1 DTF: "+cell.getCellStyle().getDataFormatString());


                            String format = cell.getCellStyle().getDataFormatString().trim();
                            switch (format) {
                                case "\\$###,##0.00":
                                    DecimalFormat df = new DecimalFormat("###,##0.00");
                                    text.setText("$ " + df.format(cellValue.getNumberValue()));
                                    break;
                                case "$###,##0.00":
                                    DecimalFormat df1 = new DecimalFormat("###,##0.00");
                                    text.setText("$ " + df1.format(cellValue.getNumberValue()));
                                    break;
                                case "#,##0":
                                    DecimalFormat df2 = new DecimalFormat("#,##0");
                                    text.setText(df2.format(cellValue.getNumberValue()));
                                    break;
                                case "0.00%":
                                    DecimalFormat df3 = new DecimalFormat("0.00%");
                                    text.setText(df3.format(cellValue.getNumberValue()));
                                    break;
                                default:
                                    DecimalFormat df4 = new DecimalFormat("#");
                                    text.setText(df4.format(cellValue.getNumberValue()));
                                    break;
                            }



                            break;

                        case STRING:
                            if(!cellValue.getStringValue().equals(SIGNATURE))
                                text.setText(""+cellValue.getStringValue());                            break;

                        case FORMULA:
                            break;


                        case BLANK:
                            text.setText(" ");

                            break;


                        default:
                            text.setText(cellValue.getStringValue());

                            break;
                    }
                }else{
                    XSLFTableCell cellule=row.addCell();

                    //cellule.setAnchor(new Rectangle(0, 0, 2, 2));
                    XSLFTextParagraph paragraph=cellule.addNewTextParagraph();
                    XSLFTextRun text=paragraph.addNewTextRun();
                    paragraph.setTextAlign(TextParagraph.TextAlign.CENTER);
                    text.setFontSize(4.0);
                    cellule.setTextAutofit(TextShape.TextAutofit.NORMAL);
                    cellule.setLeftInset(0.0);
                    cellule.setRightInset(0.0);
                    cellule.setTopInset(0.0);
                    cellule.setBottomInset(0.0);

                    text.setText(" ");
                }






            }


        });


        XSLFTableRow rr0= batchTrackTable.getRows().get(0);
        rr0.mergeCells(0,rr0.getCells().size()-1);

        rr0.getCells().get(0).setFillColor(new Color(245, 114, 49));
        XSLFTextRun text_=rr0.getCells().get(0).getTextParagraphs().get(0).getTextRuns().get(0);
        text_.setFontColor(Color.WHITE);
        text_.setFontSize(12.8);
        text_.setBold(true);



        XSLFTableRow rr1= batchTrackTable.getRows().get(1);
        rr1.mergeCells(1,2);
        rr1.mergeCells(3,5);
        rr1.mergeCells(6,7);
        rr1.mergeCells(8,11);
        rr1.mergeCells(12,13);
        rr1.mergeCells(15,16);
        rr1.mergeCells(17,18);
        rr1.mergeCells(19,22);
        rr1.forEach(cell -> {
            cell.setFillColor(new Color(10, 50, 100));

            cell.setBorderWidth(TableCell.BorderEdge.top,1);
            cell.setBorderWidth(TableCell.BorderEdge.right,1);
            cell.setBorderWidth(TableCell.BorderEdge.left,1);
            cell.setBorderWidth(TableCell.BorderEdge.bottom,1);
            cell.setBorderColor(TableCell.BorderEdge.top,Color.BLACK);
            cell.setBorderColor(TableCell.BorderEdge.bottom,Color.BLACK);
            cell.setBorderColor(TableCell.BorderEdge.left,Color.BLACK);
            cell.setBorderColor(TableCell.BorderEdge.right,Color.BLACK);

            XSLFTextRun text=cell.getTextParagraphs().get(0).getTextRuns().get(0);
            text.setFontColor(Color.WHITE);
            text.setBold(true);

        });


        XSLFTableRow rr2= batchTrackTable.getRows().get(2);
        rr2.forEach(cell -> {
            cell.setFillColor(new Color(10, 50, 100));
            cell.setBorderWidth(TableCell.BorderEdge.top,1);
            cell.setBorderWidth(TableCell.BorderEdge.right,1);
            cell.setBorderWidth(TableCell.BorderEdge.left,1);
            cell.setBorderWidth(TableCell.BorderEdge.bottom,1);
            cell.setBorderColor(TableCell.BorderEdge.top,Color.BLACK);
            cell.setBorderColor(TableCell.BorderEdge.bottom,Color.BLACK);
            cell.setBorderColor(TableCell.BorderEdge.left,Color.BLACK);
            cell.setBorderColor(TableCell.BorderEdge.right,Color.BLACK);
            XSLFTextRun text=cell.getTextParagraphs().get(0).getTextRuns().get(0);
            text.setFontColor(Color.WHITE);


        });


        XSLFTableRow rrf= batchTrackTable.getRows().get(batchTrackTable.getNumberOfRows()-1);
        for (int i=0;i<rrf.getCells().size();i++) {
            XSLFTableCell cell = rrf.getCells().get(i);

            if(i==0){
                cell.setFillColor(new Color(40, 100, 255));
                XSLFTextRun text = cell.getTextParagraphs().get(0).getTextRuns().get(0);
                text.setFontColor(Color.WHITE);

            }
            else {
                cell.setFillColor(new Color(245, 139, 20));

                cell.setBorderWidth(TableCell.BorderEdge.top,0.7);
                cell.setBorderWidth(TableCell.BorderEdge.right,0.7);
                cell.setBorderWidth(TableCell.BorderEdge.left,0.7);
                cell.setBorderWidth(TableCell.BorderEdge.bottom,0.7);
                cell.setBorderColor(TableCell.BorderEdge.top,Color.DARK_GRAY);
                cell.setBorderColor(TableCell.BorderEdge.bottom,Color.DARK_GRAY);
                cell.setBorderColor(TableCell.BorderEdge.left,Color.DARK_GRAY);
                cell.setBorderColor(TableCell.BorderEdge.right,Color.DARK_GRAY);

            }

        }


        for (int i = 0; i < batchTrackTable.getNumberOfColumns() ; i++) {
            int sz= batchTrackTable.getCell(batchTrackTable.getNumberOfRows()-1,i).getTextParagraphs().get(0).getText().length();
            if(sz<2)
                batchTrackTable.setColumnWidth(i,5);
            else
                batchTrackTable.setColumnWidth(i,3*sz+8);

        }


        //batchTrackTable.setColumnWidth(7,20);

        for (int i = 3; i < batchTrackTable.getNumberOfRows() ; i++) {
            batchTrackTable.setRowHeight(i,12);

        }

        batchTrackTable.setRowHeight(0,25);

        batchTrackTable.setAnchor(new Rectangle(0, 0, 100, 60));


        //batchTrackTable.updateCellAnchor();
        for (int i = 2; i < batchTrackTable.getNumberOfRows(); i++) {
            batchTrackTable.getCell(i,5).setText("");
            batchTrackTable.mergeCells(i,i,4,5);
        }
        //batchTrackTable.mergeCells(1,1,3,6);

        //batchTrackTable.setColumnWidth(5,0.0);


    }



    private void createFinClassTable(XSLFSlide ss4,Sheet ssh2){


        XSLFTable tt2=ss4.createTable();


        ssh2.forEach(rowOr -> {
            XSLFTableRow row=tt2.addRow();


            for (int i = 0; i < 9; i++) {

                Cell cell=rowOr.getCell(i);


                if(cell!=null){
                    XSLFTableCell cellule=row.addCell();

                    //cellule.setAnchor(new Rectangle(0, 0, 2, 2));
                    XSLFTextParagraph paragraph=cellule.addNewTextParagraph();
                    XSLFTextRun text=paragraph.addNewTextRun();
                    paragraph.setTextAlign(TextParagraph.TextAlign.CENTER);
                    text.setFontSize(7.0);
                    cellule.setTextAutofit(TextShape.TextAutofit.NORMAL);
                    cellule.setLeftInset(0.0);
                    cellule.setRightInset(0.0);
                    cellule.setTopInset(0.0);
                    cellule.setBottomInset(0.0);

                    CellValue cellValue = evaluator.evaluate(cell);

                    switch (cellValue.getCellType()) {
                        case NUMERIC:

                            //System.out.println("DEBUG1 DTF: "+cell.getCellStyle().getDataFormatString());


                            String format = cell.getCellStyle().getDataFormatString().trim();
                            switch (format) {
                                case "\\$###,##0.00":
                                    DecimalFormat df = new DecimalFormat("###,##0.00");
                                    text.setText("$ " + df.format(cellValue.getNumberValue()));
                                    break;
                                case "$###,##0.00":
                                    DecimalFormat df1 = new DecimalFormat("###,##0.00");
                                    text.setText("$ " + df1.format(cellValue.getNumberValue()));
                                    break;
                                case "#,##0":
                                    DecimalFormat df2 = new DecimalFormat("#,##0");
                                    text.setText(df2.format(cellValue.getNumberValue()));
                                    break;
                                case "0.00%":
                                    DecimalFormat df3 = new DecimalFormat("0.00%");
                                    text.setText(df3.format(cellValue.getNumberValue()));
                                    break;
                                default:
                                    text.setText("" + cellValue.getNumberValue());
                                    break;
                            }



                            break;

                        case STRING:
                            if(!cellValue.getStringValue().equals(SIGNATURE))
                                text.setText(""+cellValue.getStringValue());
                            break;

                        case FORMULA:
                            break;


                        case BLANK:
                            text.setText(" ");

                            break;


                        default:
                            text.setText(cellValue.getStringValue());

                            break;
                    }
                }else{
                    XSLFTableCell cellule=row.addCell();

                    //cellule.setAnchor(new Rectangle(0, 0, 2, 2));
                    XSLFTextParagraph paragraph=cellule.addNewTextParagraph();
                    XSLFTextRun text=paragraph.addNewTextRun();
                    paragraph.setTextAlign(TextParagraph.TextAlign.CENTER);
                    text.setFontSize(7.0);
                    cellule.setTextAutofit(TextShape.TextAutofit.NORMAL);
                    cellule.setLeftInset(0.0);
                    cellule.setRightInset(0.0);
                    cellule.setTopInset(0.0);
                    cellule.setBottomInset(0.0);

                    text.setText(" ");
                }




            }


            /*rowOr.forEach(cell -> {

            });*/

        });


        tt2.forEach(row -> {

            row.getCells().forEach(cell -> {
                System.out.println("TT: "+cell.getText());
                if (cell.getText().toLowerCase().contains("Active Inventory #".toLowerCase())||
                        cell.getText().toLowerCase().contains("Active Inventory $".toLowerCase())||
                        cell.getText().equals("Average Age")){

                    //row.mergeCells(0,row.getCells().size()-1);
                    cell.setFillColor(new Color(42, 80, 255));
                    cell.getTextParagraphs().get(0).setTextAlign(TextParagraph.TextAlign.LEFT);
                    cell.getTextParagraphs().get(0).getTextRuns().get(0).setFontColor(Color.WHITE);

                    cell.setBorderWidth(TableCell.BorderEdge.top,1);
                    cell.setBorderWidth(TableCell.BorderEdge.right,1);
                    cell.setBorderWidth(TableCell.BorderEdge.left,1);
                    cell.setBorderWidth(TableCell.BorderEdge.bottom,1);
                    cell.setBorderColor(TableCell.BorderEdge.top,Color.BLACK);
                    cell.setBorderColor(TableCell.BorderEdge.bottom,Color.BLACK);
                    cell.setBorderColor(TableCell.BorderEdge.left,Color.BLACK);
                    cell.setBorderColor(TableCell.BorderEdge.right,Color.BLACK);

                }


                if (cell.getText().equals("Financial Class")){

                    //row.mergeCells(0,row.getCells().size()-1);
                    row.forEach(colorCells -> {
                        colorCells.setFillColor(new Color(42, 80, 255));
                        colorCells.setBorderWidth(TableCell.BorderEdge.top,1);
                        colorCells.setBorderWidth(TableCell.BorderEdge.right,1);
                        colorCells.setBorderWidth(TableCell.BorderEdge.left,1);
                        colorCells.setBorderWidth(TableCell.BorderEdge.bottom,1);
                        colorCells.setBorderColor(TableCell.BorderEdge.top,Color.BLACK);
                        colorCells.setBorderColor(TableCell.BorderEdge.bottom,Color.BLACK);
                        colorCells.setBorderColor(TableCell.BorderEdge.left,Color.BLACK);
                        colorCells.setBorderColor(TableCell.BorderEdge.right,Color.BLACK);
                        colorCells.getTextParagraphs().get(0).getTextRuns().get(0).setFontColor(Color.WHITE);

                    });
                    cell.getTextParagraphs().get(0).setTextAlign(TextParagraph.TextAlign.CENTER);
                    cell.getTextParagraphs().get(0).getTextRuns().get(0).setFontColor(Color.WHITE);


                }


                if (cell.getText().equals("Grand Total")){

                    //row.mergeCells(0,row.getCells().size()-1);
                    row.forEach(colorCells -> {

                        colorCells.getTextParagraphs().get(0).getTextRuns().get(0).setBold(true);

                    });

                }
            });
        });


        XSLFTableRow rr01=tt2.getRows().get(0);
        rr01.mergeCells(0,rr01.getCells().size()-1);
        rr01.forEach(cell -> {
            cell.setFillColor(new Color(245, 114, 49));
            XSLFTextRun text=cell.getTextParagraphs().get(0).getTextRuns().get(0);
            text.setFontColor(Color.WHITE);
            text.setFontSize(10.3);
            text.setBold(true);


        });




       /* for (int i = 0; i <tt2.getNumberOfColumns() ; i++) {
            //int sz=tt2.getCell(3,i).getTextParagraphs().get(0).getText().length();
            tt2.setColumnWidth(i,71);

        }
        for (int i = 0; i <tt2.getNumberOfRows() ; i++) {
            tt2.setRowHeight(i,10);

        }
        tt2.setRowHeight(0,19);*/




        for (int i = 0; i < tt2.getNumberOfColumns() ; i++) {
            tt2.setColumnWidth(i,71);

        }

        //tt2.setColumnWidth(7,20);


        for (int i = 0; i < tt2.getNumberOfRows() ; i++) {
            tt2.setRowHeight(i,7);

        }

        tt2.setRowHeight(0,17);

        tt2.setAnchor(new Rectangle(0, 0,100, 60));
        //tt2.updateCellAnchor();

    }



    private void createAgingTable(XSLFSlide ss5,List<Row> sp,boolean first){


        XSLFTable tt3=ss5.createTable();

        sp.forEach(rowOr -> {
            XSLFTableRow row=tt3.addRow();

            //System.out.println("CELL BEF: "+ rowOr.getRowNum()+" - "+rowOr.getPhysicalNumberOfCells());

            for (int i = 0; i < 9; i++) {

                Cell cell=rowOr.getCell(i);


                if(cell!=null && !cell.getCellType().equals(BLANK)){
                    XSLFTableCell cellule=row.addCell();

                    //cellule.setAnchor(new Rectangle(0, 0, 2, 2));
                    XSLFTextParagraph paragraph=cellule.addNewTextParagraph();
                    XSLFTextRun text=paragraph.addNewTextRun();
                    paragraph.setTextAlign(TextParagraph.TextAlign.CENTER);
                    text.setFontSize(7.0);
                    cellule.setTextAutofit(TextShape.TextAutofit.NORMAL);
                    cellule.setLeftInset(0.0);
                    cellule.setRightInset(0.0);
                    cellule.setTopInset(0.0);
                    cellule.setBottomInset(0.0);

                    CellValue cellValue = evaluator.evaluate(cell);

                    switch (cellValue.getCellType()) {
                        case NUMERIC:

                            //System.out.println("DEBUG1 DTF: "+cell.getCellStyle().getDataFormatString());


                            String format = cell.getCellStyle().getDataFormatString().trim();
                            switch (format) {
                                case "\\$###,##0.00":
                                    DecimalFormat df = new DecimalFormat("###,##0.00");
                                    text.setText("$ " + df.format(cellValue.getNumberValue()));
                                    break;
                                case "$###,##0.00":
                                    DecimalFormat df1 = new DecimalFormat("###,##0.00");
                                    text.setText("$ " + df1.format(cellValue.getNumberValue()));
                                    break;
                                case "#,##0":
                                    DecimalFormat df2 = new DecimalFormat("#,##0");
                                    text.setText(df2.format(cellValue.getNumberValue()));
                                    break;
                                case "0.00%":
                                    DecimalFormat df3 = new DecimalFormat("0.00%");
                                    text.setText(df3.format(cellValue.getNumberValue()));
                                    break;
                                default:
                                    text.setText("" + cellValue.getNumberValue());
                                    break;
                            }



                            break;

                        case STRING:
                            if(!cellValue.getStringValue().equals(SIGNATURE))
                                text.setText(""+cellValue.getStringValue());                            break;

                        case FORMULA:
                            break;


                        case BLANK:
                            text.setText(" ");

                            break;


                        default:
                            text.setText(cellValue.getStringValue());

                            break;
                    }
                }else{
                    XSLFTableCell cellule=row.addCell();
                    //cellule.setAnchor(new Rectangle(0, 0, 2, 2));
                    XSLFTextParagraph paragraph=cellule.addNewTextParagraph();
                    XSLFTextRun text=paragraph.addNewTextRun();
                    paragraph.setTextAlign(TextParagraph.TextAlign.CENTER);
                    text.setFontSize(7.0);
                    cellule.setTextAutofit(TextShape.TextAutofit.NORMAL);
                    cellule.setLeftInset(0.0);
                    cellule.setRightInset(0.0);
                    cellule.setTopInset(0.0);
                    cellule.setBottomInset(0.0);
                    text.setText(" ");
                }






            }


        });



        int strt=(first)?2:1;
        for (int i = strt; i < tt3.getNumberOfRows(); i++) {

            tt3.getCell(i,0).getTextParagraphs().get(0).setTextAlign(TextParagraph.TextAlign.LEFT);
        }

        if(first) {
            tt3.getCell(1, 0).setFillColor(new Color(42, 80, 255));
            tt3.getCell(1, 0).setBorderWidth(TableCell.BorderEdge.top, 1);
            tt3.getCell(1, 0).setBorderWidth(TableCell.BorderEdge.right, 1);
            tt3.getCell(1, 0).setBorderWidth(TableCell.BorderEdge.left, 1);
            tt3.getCell(1, 0).setBorderWidth(TableCell.BorderEdge.bottom, 1);
            tt3.getCell(1, 0).setBorderColor(TableCell.BorderEdge.top, Color.BLACK);
            tt3.getCell(1, 0).setBorderColor(TableCell.BorderEdge.bottom, Color.BLACK);
            tt3.getCell(1, 0).setBorderColor(TableCell.BorderEdge.left, Color.BLACK);
            tt3.getCell(1, 0).setBorderColor(TableCell.BorderEdge.right, Color.BLACK);
            tt3.getCell(1, 0).getTextParagraphs().get(0).getTextRuns().get(0).setFontColor(Color.WHITE);

        }

        tt3.forEach(row -> row.getCells().forEach(cell -> {
            if (cell.getText()!=null && (cell.getText().toLowerCase().contains("Active Accounts #".toLowerCase())||
                    cell.getText().toLowerCase().contains("Active Accounts $".toLowerCase())||
                    cell.getText().equals("% of Inventory"))){

                //row.mergeCells(0,row.getCells().size()-1);
                cell.setFillColor(new Color(42, 80, 255));
                cell.getTextParagraphs().get(0).setTextAlign(TextParagraph.TextAlign.LEFT);
                cell.getTextParagraphs().get(0).getTextRuns().get(0).setFontColor(Color.WHITE);

                cell.setBorderWidth(TableCell.BorderEdge.top,1);
                cell.setBorderWidth(TableCell.BorderEdge.right,1);
                cell.setBorderWidth(TableCell.BorderEdge.left,1);
                cell.setBorderWidth(TableCell.BorderEdge.bottom,1);
                cell.setBorderColor(TableCell.BorderEdge.top,Color.BLACK);
                cell.setBorderColor(TableCell.BorderEdge.bottom,Color.BLACK);
                cell.setBorderColor(TableCell.BorderEdge.left,Color.BLACK);
                cell.setBorderColor(TableCell.BorderEdge.right,Color.BLACK);

            }



            if (cell.getText()!=null &&cell.getText().equals("Financial Class")){

                //row.mergeCells(0,row.getCells().size()-1);
                row.forEach(colorCells -> {
                    colorCells.setFillColor(new Color(42, 80, 255));
                    colorCells.setBorderWidth(TableCell.BorderEdge.top,1);
                    colorCells.setBorderWidth(TableCell.BorderEdge.right,1);
                    colorCells.setBorderWidth(TableCell.BorderEdge.left,1);
                    colorCells.setBorderWidth(TableCell.BorderEdge.bottom,1);
                    colorCells.setBorderColor(TableCell.BorderEdge.top,Color.BLACK);
                    colorCells.setBorderColor(TableCell.BorderEdge.bottom,Color.BLACK);
                    colorCells.setBorderColor(TableCell.BorderEdge.left,Color.BLACK);
                    colorCells.setBorderColor(TableCell.BorderEdge.right,Color.BLACK);
                    colorCells.getTextParagraphs().get(0).getTextRuns().get(0).setFontColor(Color.WHITE);

                });
                cell.getTextParagraphs().get(0).setTextAlign(TextParagraph.TextAlign.CENTER);
                cell.getTextParagraphs().get(0).getTextRuns().get(0).setFontColor(Color.WHITE);


            }


            if (cell.getText()!=null && cell.getText().equals("Grand Total")){

                //row.mergeCells(0,row.getCells().size()-1);
                row.forEach(colorCells -> {

                    colorCells.getTextParagraphs().get(0).getTextRuns().get(0).setBold(true);

                });

            }
        }));


        if(first) {
            XSLFTableRow rr01 = tt3.getRows().get(0);
            rr01.mergeCells(0, rr01.getCells().size() - 1);
            rr01.forEach(cell -> {
                cell.setFillColor(new Color(245, 114, 49));
                XSLFTextRun text = cell.getTextParagraphs().get(0).getTextRuns().get(0);
                text.setFontColor(Color.WHITE);
                text.setFontSize(12.3);
                text.setBold(true);


            });
        }

        for (int i = 0; i <tt3.getNumberOfColumns() ; i++) {
            //int sz=tt2.getCell(3,i).getTextParagraphs().get(0).getText().length();
            tt3.setColumnWidth(i,70);

        }


        for (int i = 0; i <tt3.getNumberOfRows() ; i++) {
            tt3.setRowHeight(i,12);

        }

        tt3.setRowHeight(0,20);
        tt3.setAnchor(new Rectangle(0, 0,200, 200));
        //tt3.updateCellAnchor();
    }


    private void createOverallTable(XSLFSlide ss6,Sheet ssh4){


        XSLFTable tt4=ss6.createTable();

        ssh4.forEach(rowOr -> {
            XSLFTableRow row=tt4.addRow();

            if(rowOr!=null && row!=null){

                for (int i = 0; i < 14; i++) {

                    Cell cell=rowOr.getCell(i);

                    if(cell!=null && !cell.getCellType().equals(BLANK)){

                        XSLFTableCell cellule=row.addCell();

                        //cellule.setAnchor(new Rectangle(0, 0, 2, 2));
                        XSLFTextParagraph paragraph=cellule.addNewTextParagraph();
                        XSLFTextRun text=paragraph.addNewTextRun();
                        paragraph.setTextAlign(TextParagraph.TextAlign.CENTER);
                        text.setFontSize(7.0);
                        cellule.setTextAutofit(TextShape.TextAutofit.NORMAL);
                        cellule.setLeftInset(0.0);
                        cellule.setRightInset(0.0);
                        cellule.setTopInset(0.0);
                        cellule.setBottomInset(0.0);

                        CellValue cellValue = evaluator.evaluate(cell);

                        switch (cellValue.getCellType()) {
                            case NUMERIC:

                                //System.out.println("DEBUG1 DTF: "+cell.getCellStyle().getDataFormatString());


                                String format = cell.getCellStyle().getDataFormatString().trim();
                                switch (format) {
                                    case "\\$###,##0.00":
                                        DecimalFormat df = new DecimalFormat("###,##0.00");
                                        text.setText("$ " + df.format(cellValue.getNumberValue()));
                                        break;
                                    case "$###,##0.00":
                                        DecimalFormat df1 = new DecimalFormat("###,##0.00");
                                        text.setText("$ " + df1.format(cellValue.getNumberValue()));
                                        break;
                                    case "#,##0":
                                        DecimalFormat df2 = new DecimalFormat("#,##0");
                                        text.setText(df2.format(cellValue.getNumberValue()));
                                        break;
                                    case "0.00%":
                                        DecimalFormat df3 = new DecimalFormat("0.00%");
                                        text.setText(df3.format(cellValue.getNumberValue()));
                                        break;
                                    default:
                                        text.setText("" + cellValue.getNumberValue());
                                        break;
                                }



                                break;

                            case STRING:
                                if(!cellValue.getStringValue().equals(SIGNATURE))
                                    text.setText(""+cellValue.getStringValue());                                break;

                            case FORMULA:
                                break;


                            case BLANK:
                                text.setText(" ");

                                break;


                            default:
                                text.setText(cellValue.getStringValue());

                                break;
                        }
                    }else{
                        XSLFTableCell cellule=row.addCell();

                        //cellule.setAnchor(new Rectangle(0, 0, 2, 2));
                        XSLFTextParagraph paragraph=cellule.addNewTextParagraph();
                        XSLFTextRun text=paragraph.addNewTextRun();
                        paragraph.setTextAlign(TextParagraph.TextAlign.CENTER);
                        text.setFontSize(7.0);
                        cellule.setTextAutofit(TextShape.TextAutofit.NORMAL);
                        cellule.setLeftInset(0.0);
                        cellule.setRightInset(0.0);
                        cellule.setTopInset(0.0);
                        cellule.setBottomInset(0.0);
                        text.setText(" ");
                    }

                }

            }


        });


        /*for (int i = 2; i < tt4.getNumberOfRows()-1; i++) {

            tt4.getCell(i,0).getTextParagraphs().get(0).setTextAlign(TextParagraph.TextAlign.LEFT);
        }*/

        tt4.forEach(row -> row.getCells().forEach(cell -> {
            System.out.println("TT2: "+cell.getText());
            if ( cell.getText()!=null && (cell.getText().toLowerCase().equals("MONTHLY".toLowerCase())||
                    cell.getText().toLowerCase().equals("Placements".toLowerCase())||
                    cell.getText().toLowerCase().equals("Change".toLowerCase())||
                    cell.getText().toLowerCase().equals("Changes".toLowerCase())||
                    cell.getText().toLowerCase().equals("YEARLY".toLowerCase())||
                    cell.getText().toLowerCase().equals("Collections".toLowerCase())||
                    cell.getText().toLowerCase().equals("YTD".toLowerCase())||
                    cell.getText().toLowerCase().equals("Previous YTD".toLowerCase())||
                    cell.getText().toLowerCase().equals("$".toLowerCase())||
                    cell.getText().toLowerCase().equals("%".toLowerCase()))
            ){

                //row.mergeCells(0,row.getCells().size()-1);
                cell.setFillColor(new Color(42, 80, 255));
                //cell.getTextParagraphs().get(0).setTextAlign(TextParagraph.TextAlign.LEFT);
                cell.getTextParagraphs().get(0).getTextRuns().get(0).setFontColor(Color.WHITE);

                cell.setBorderWidth(TableCell.BorderEdge.top,1);
                cell.setBorderWidth(TableCell.BorderEdge.right,1);
                cell.setBorderWidth(TableCell.BorderEdge.left,1);
                cell.setBorderWidth(TableCell.BorderEdge.bottom,1);
                cell.setBorderColor(TableCell.BorderEdge.top,Color.BLACK);
                cell.setBorderColor(TableCell.BorderEdge.bottom,Color.BLACK);
                cell.setBorderColor(TableCell.BorderEdge.left,Color.BLACK);
                cell.setBorderColor(TableCell.BorderEdge.right,Color.BLACK);

            }


            if (cell.getText()!=null && cell.getText().toLowerCase().contains("Prior 3 Mo Avg".toLowerCase())){

                //row.mergeCells(0,row.getCells().size()-1);
                cell.setFillColor(new Color(42, 80, 255));
                cell.getTextParagraphs().get(0).setTextAlign(TextParagraph.TextAlign.LEFT);
                cell.getTextParagraphs().get(0).getTextRuns().get(0).setFontColor(Color.WHITE);

                cell.setBorderWidth(TableCell.BorderEdge.top,1);
                cell.setBorderWidth(TableCell.BorderEdge.right,1);
                cell.setBorderWidth(TableCell.BorderEdge.left,1);
                cell.setBorderWidth(TableCell.BorderEdge.bottom,1);
                cell.setBorderColor(TableCell.BorderEdge.top,Color.BLACK);
                cell.setBorderColor(TableCell.BorderEdge.bottom,Color.BLACK);
                cell.setBorderColor(TableCell.BorderEdge.left,Color.BLACK);
                cell.setBorderColor(TableCell.BorderEdge.right,Color.BLACK);


                for (int i = 1; i < 5; i++) {
                    XSLFTableCell cc=row.getCells().get(i);
                    cc.setFillColor(new Color(42, 80, 255));
                    cc.getTextParagraphs().get(0).setTextAlign(TextParagraph.TextAlign.LEFT);
                    cc.getTextParagraphs().get(0).getTextRuns().get(0).setFontColor(Color.WHITE);

                    cc.setBorderWidth(TableCell.BorderEdge.top,1);
                    cc.setBorderWidth(TableCell.BorderEdge.right,1);
                    cc.setBorderWidth(TableCell.BorderEdge.left,1);
                    cc.setBorderWidth(TableCell.BorderEdge.bottom,1);
                    cc.setBorderColor(TableCell.BorderEdge.top,Color.BLACK);
                    cc.setBorderColor(TableCell.BorderEdge.bottom,Color.BLACK);
                    cc.setBorderColor(TableCell.BorderEdge.left,Color.BLACK);
                    cc.setBorderColor(TableCell.BorderEdge.right,Color.BLACK);
                }

            }





            if (cell.getText()!=null && cell.getText().equals("Total")){

                //row.mergeCells(0,row.getCells().size()-1);
                row.forEach(colorCells -> {

                    colorCells.getTextParagraphs().get(0).getTextRuns().get(0).setBold(true);

                });

            }
        }));


        XSLFTableRow rr01=tt4.getRows().get(0);
        rr01.mergeCells(0,rr01.getCells().size()-1);
        rr01.forEach(cell -> {
            cell.setFillColor(new Color(245, 114, 49));
            XSLFTextRun text=cell.getTextParagraphs().get(0).getTextRuns().get(0);
            text.setFontColor(Color.WHITE);
            text.setFontSize(14.1);
            text.setBold(true);


        });

        for (int i = 1; i <tt4.getNumberOfColumns() ; i++) {
            int sz=tt4.getCell(3,i).getTextParagraphs().get(0).getText().length();


            tt4.setColumnWidth(i,55);
            tt4.setColumnWidth(8,5);
            tt4.setColumnWidth(0,23);
            tt4.setColumnWidth(9,23);

        }


        for (int i = 0; i <tt4.getNumberOfRows() ; i++) {
            tt4.setRowHeight(i,5);

        }

        tt4.setRowHeight(0,20);

        tt4.setAnchor(new Rectangle(0, 0, 315, 60));
        //tt4.updateCellAnchor();

    }



    private void addCommentsToPPt(String sessionId, String[] comments){


        try {
            String fileName= Paths.get("assets","healthCareFiles") +File.separator+sessionId.replace("-", "")+"_2.pptx";

            FileInputStream is = new FileInputStream(fileName) ;
            XMLSlideShow ppt = new XMLSlideShow(is);
            //XSLFSlide ss=ppt.getSlides().get(0);
            XSLFSlide ss2=ppt.getSlides().get(2);
            XSLFSlide ss4=ppt.getSlides().get(4);
            XSLFSlide ss5=ppt.getSlides().get(ppt.getSlides().size()-2);
            XSLFSlide ss6=ppt.getSlides().get(ppt.getSlides().size()-1);




            Rectangle rect=new Rectangle(0, 420, 720,70);

            XSLFTextBox bb2=ss2.createTextBox();
            XSLFTextParagraph textparagraph2 =bb2.addNewTextParagraph();
            XSLFTextRun text=textparagraph2.addNewTextRun();
            textparagraph2.setTextAlign(TextParagraph.TextAlign.CENTER);
            bb2.setAnchor(rect);
            text.setText(comments[0]);
            bb2.setFillColor(Color.WHITE);
            text.setFontSize(12.0);
            text.setFontColor(Color.GRAY);
            bb2.setTextAutofit(TextShape.TextAutofit.NORMAL);
            rect.setSize(ss2.getSlideShow().getPageSize().width, (int)bb2.getTextHeight()+15);
            bb2.setAnchor(rect);


            XSLFTextBox bb4=ss4.createTextBox();
            XSLFTextParagraph textparagraph4 =bb4.addNewTextParagraph();
            XSLFTextRun text4=textparagraph4.addNewTextRun();
            textparagraph4.setTextAlign(TextParagraph.TextAlign.CENTER);
            bb4.setAnchor(rect);
            text4.setText(comments[1]);
            bb4.setFillColor(Color.WHITE);
            text4.setFontSize(12.0);
            text4.setFontColor(Color.GRAY);
            bb4.setTextAutofit(TextShape.TextAutofit.SHAPE);
            rect.setSize(ss4.getSlideShow().getPageSize().width, (int)bb4.getTextHeight()+15);
            bb4.setAnchor(rect);


            XSLFTextBox bb5=ss5.createTextBox();
            XSLFTextParagraph textparagraph5 =bb5.addNewTextParagraph();
            XSLFTextRun text5=textparagraph5.addNewTextRun();
            textparagraph5.setTextAlign(TextParagraph.TextAlign.CENTER);
            bb5.setAnchor(rect);
            text5.setText(comments[2]);
            bb5.setFillColor(Color.WHITE);
            text5.setFontSize(12.0);
            text5.setFontColor(Color.GRAY);
            bb5.setTextAutofit(TextShape.TextAutofit.SHAPE);
            rect.setSize(ss5.getSlideShow().getPageSize().width, (int)bb5.getTextHeight()+15);
            bb5.setAnchor(rect);


            XSLFTextBox bb6=ss6.createTextBox();
            XSLFTextParagraph textparagraph6 =bb6.addNewTextParagraph();
            XSLFTextRun text6=textparagraph6.addNewTextRun();
            textparagraph6.setTextAlign(TextParagraph.TextAlign.CENTER);
            bb6.setAnchor(rect);
            text6.setText(comments[3]);
            bb6.setFillColor(Color.WHITE);
            text6.setFontSize(12.0);
            text6.setFontColor(Color.GRAY);
            bb6.setTextAutofit(TextShape.TextAutofit.SHAPE);
            rect.setSize(ss6.getSlideShow().getPageSize().width, (int)bb6.getTextHeight()+15);
            bb6.setAnchor(rect);



            /*XSLFAutoShape aa=ss.createAutoShape();
            XSLFTextParagraph textparagraph =aa.addNewTextParagraph();
            XSLFTextRun compLay=textparagraph.addNewTextRun();
            compLay.setText("Test test test");
            aa.setShapeType(ShapeType.FLOW_CHART_TERMINATOR);
            aa.setFillColor(Color.WHITE);
            compLay.setFontSize(19.0);
            compLay.setFontColor(Color.blue);
            aa.setTextAutofit(TextShape.TextAutofit.SHAPE);
            textparagraph.setTextAlign(TextParagraph.TextAlign.CENTER);
            aa.setAnchor(new java.awt.Rectangle(50, 340, 200, 50));


            XSLFAutoShape bb=ss.createAutoShape();
            XSLFTextParagraph textparagraphDate =bb.addNewTextParagraph();
            XSLFTextRun date=textparagraphDate.addNewTextRun();
            date.setText("February 2019");
            date.setFontSize(30.0);
            date.setFontColor(Color.LIGHT_GRAY);
            textparagraphDate.setTextAlign(TextParagraph.TextAlign.RIGHT);
            bb.setTextAutofit(TextShape.TextAutofit.SHAPE);
            bb.setShapeType(ShapeType.RECT);
            bb.setAnchor(new java.awt.Rectangle(550, 80, 160, 100));*/


            OutputStream fileOut;
            String fileNameF= Paths.get("assets","healthCareFiles") + File.separator+sessionId.replace("-", "")+"_cm.pptx";



            fileOut = new FileOutputStream(fileNameF);
            ppt.write(fileOut);
            fileOut.close();
            ppt.close();
            fileOut.close();

            System.out.println("END......");



        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private static <T> List<List<T>> chopped(List<T> list, final int L) {
        List<List<T>> parts = new ArrayList<List<T>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<T>(
                    list.subList(i, Math.min(N, i + L)))
            );

           /* ArrayList<T> toAdd=new ArrayList<>();
            toAdd.add(list.get(0));
            toAdd.add(list.get(8));

            toAdd.addAll(2,list.subList(i, Math.min(N, i + L)));
            parts.add(toAdd);

            if(i!=0){

                toAdd.addAll(list.subList(i, Math.min(N, i + L)));
                parts.add(toAdd);
            }else{
                parts.add(new ArrayList<T>(
                        list.subList(i, Math.min(N, i + L)))
                );
            }*/
        }
        return parts;



    }

    private static String encodeFileToBase64Binary(String fileName){
        try {
            File file = new File(fileName);
            byte[] encoded = Base64.encodeBase64(Files.readAllBytes(file.toPath()));
            return new String(encoded, StandardCharsets.US_ASCII);
        }catch (IOException e){
            return "";
        }
    }


    public HealthOutputData getOutputData() {
        return outputData;
    }

    public void setOutputData(HealthOutputData outputData) {
        this.outputData = outputData;
    }
}
