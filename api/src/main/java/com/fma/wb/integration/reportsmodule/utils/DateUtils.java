package com.fma.wb.integration.reportsmodule.utils;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.Locale;

public class DateUtils {



    public static String minusMonthsFromDate(String date,int months){
        String[] stDt;
        stDt=date.split("-");

        LocalDate dt=LocalDate.of(Integer.valueOf(stDt[0]),Integer.valueOf(stDt[1]),Integer.valueOf(stDt[2]));
        return dt.minusMonths(months).toString();
    }

    public static  String getDateMonthShortName(String date){

        //nn=(nn.length()>=3)?nn.substring(0,3):nn;
        return Month.of(Integer.parseInt(getDateMonth(date))).getDisplayName(TextStyle.SHORT_STANDALONE, Locale.getDefault());
    }

    public static String setDateTo1fstDay(String date){
        String[] stDt;
        stDt=date.split("-");
        LocalDate dt=LocalDate.of(Integer.valueOf(stDt[0]),Integer.valueOf(stDt[1]),1);
        return dt.toString();
    }

    public static String setDateTo1fstDayAndMonth(String date){
        String[] stDt;
        stDt=date.split("-");
        LocalDate dt=LocalDate.of(Integer.valueOf(stDt[0]),1,1);
        return dt.toString();
    }


    public static String minusDaysFromDate(String date,int days){
        String[] stDt;
        stDt=date.split("-");

        LocalDate dt=LocalDate.of(Integer.valueOf(stDt[0]),Integer.valueOf(stDt[1]),Integer.valueOf(stDt[2]));
        return dt.minusDays(days).toString();
    }


    public static String getSqlDate(String date){

        return date.replace("-","");
    }


    public static String addMonthsToDate(String date,int months){
        String[] stDt;
        stDt=date.split("-");

        LocalDate dt=LocalDate.of(Integer.valueOf(stDt[0]),Integer.valueOf(stDt[1]),Integer.valueOf(stDt[2]));
        return dt.plusMonths(months).toString();
    }


    public static String newDate(String year,String month,String day){

        LocalDate dt=LocalDate.of(Integer.valueOf(year),Integer.valueOf(month),Integer.valueOf(day));

        return dt.toString();
    }

    public static String getDateDay(String date){
        return date.split("-")[2];

    }


    public static String getDateMonth(String date){

        return date.split("-")[1];
    }


    public static int getDateMonthInt(String date){
        String[] stDt=date.split("-");
        LocalDate dt=LocalDate.of(Integer.valueOf(stDt[0]),Integer.valueOf(stDt[1]),Integer.valueOf(stDt[2]));

        return dt.getMonth().getValue();
    }


    public static String getDateYear(String date){
        return date.split("-")[0];
    }


    public static String toDayDate(){

        LocalDate dt=LocalDate.now();

        return dt.toString();
    }


    public static String addDaysToDate(String date,int days){
        String[] stDt;
        stDt=date.split("-");

        LocalDate dt=LocalDate.of(Integer.valueOf(stDt[0]),Integer.valueOf(stDt[1]),Integer.valueOf(stDt[2]));
        return dt.plusDays(days).toString();
    }
}
