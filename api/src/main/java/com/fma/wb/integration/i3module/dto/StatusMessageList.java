package com.fma.wb.integration.i3module.dto;


import java.io.Serializable;
import java.util.ArrayList;

public class StatusMessageList implements Serializable {

    private PeopleManager peopleManager;
    private ArrayList<String> statues = new ArrayList<>();

    public PeopleManager getPeopleManager() {
        return peopleManager;
    }

    public void setPeopleManager(PeopleManager peopleManager) {
        this.peopleManager = peopleManager;
    }

    public StatusMessageList() {
    }

    public StatusMessageList(PeopleManager peopleManager) {
        this.peopleManager = peopleManager;
    }

    @Override
    public String toString() {
        return "StatusMessageList{" +
                "peopleManager=" + peopleManager +
                '}';
    }

    public void startWatching(){

    }

    public void addStatue(String statut){
        this.statues.add(statut);
    }

    public void remove(String statut){
        this.statues.remove(statut);
    }

    public ArrayList<String> getStatues() {
        return statues;
    }

    public void setStatues(ArrayList<String> statues) {
        this.statues = statues;
    }
}
