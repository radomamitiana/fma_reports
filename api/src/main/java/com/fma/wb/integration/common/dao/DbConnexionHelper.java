package com.fma.wb.integration.common.dao;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

/**
 * DbConnexionHelper class.
 *
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class DbConnexionHelper {
    private JDBCClient dbConnection;
    private static DbConnexionHelper dbConnexionHelper;
    private Vertx vertx;
    private HashMap<String, String> credentials;

    private SQLConnection fmaConnection;
    private SQLConnection genesysConnection;
    private SQLConnection voxConnection;
    private SQLConnection rpConnection;
    private SQLConnection quickBookConnection;
    private JDBCClient clientFma;
    private JDBCClient clientIC;
    private JDBCClient clientVox;
    private JDBCClient clientRP;
    private JDBCClient clientQuickBook;

    public DbConnexionHelper(Vertx vertx, HashMap<String, String> credentials) {
        this.vertx = vertx;
        this.credentials = credentials;
    }


    public static DbConnexionHelper getInstance(Vertx vertx, HashMap<String, String> credentials) {
        if (dbConnexionHelper == null) {
            return new DbConnexionHelper(vertx, credentials);
        } else {
            return dbConnexionHelper;
        }
    }

    public void closeConnection() {
        System.out.println("Closing connexion...");
        this.dbConnection.close();
    }

    public JDBCClient getDbConnection(HashMap<String, String> credentials) throws SQLException, ClassNotFoundException {

        this.dbConnection = DBConnector.connect(credentials.get("host"), credentials.get("db"), credentials.get("user"), this.credentials.get("psswd"), this.vertx);
        return this.dbConnection;
    }

    public void setDbConnection(JDBCClient dbConnection) {
        this.dbConnection = dbConnection;
    }

    public HashMap<String, String> getCredentials() {
        return credentials;
    }

    public void setCredentials(HashMap<String, String> credentials) {
        this.credentials = credentials;
    }

    public Vertx getVertx() {
        return vertx;
    }

    public void setVertx(Vertx vertx) {
        this.vertx = vertx;
    }


    public static <T> Stream<T> returnQueryStream(List<JsonObject> res, Class<T> typeOfT) {
        ArrayList<T> ll = new ArrayList<>();
        res.forEach(entries -> ll.add(entries.mapTo(typeOfT)));
        return ll.stream();
    }

    public static <T> ArrayList<T> returnQueryList(List<JsonObject> res, Class<T> typeOfT) {
        ArrayList<T> ll = new ArrayList<>();
        res.forEach(entries -> ll.add(entries.mapTo(typeOfT)));

        return ll;
    }

    public static <T> T returnQueryObject(JsonObject res, Class<T> typeOfT) {
        return res.mapTo(typeOfT);
    }


    public JDBCClient getDbConnection() {
        return dbConnection;
    }

    public SQLConnection getFmaConnection() {
        return fmaConnection;
    }

    public void setFmaConnection(SQLConnection fmaConnection) {
        this.fmaConnection = fmaConnection;
    }

    public SQLConnection getGenesysConnection() {
        return genesysConnection;
    }

    public void setGenesysConnection(SQLConnection genesysConnection) {
        this.genesysConnection = genesysConnection;
    }

    public SQLConnection getRpConnection() {
        return rpConnection;
    }

    public void setRpConnection(SQLConnection rpConnection) {
        this.rpConnection = rpConnection;
    }

    public SQLConnection getVoxConnection() {
        return voxConnection;
    }

    public void setVoxConnection(SQLConnection voxConnection) {
        this.voxConnection = voxConnection;
    }

    public JDBCClient getClientFma() {
        return clientFma;
    }

    public void setClientFma(JDBCClient clientFma) {
        this.clientFma = clientFma;
    }

    public JDBCClient getClientIC() {
        return clientIC;
    }

    public void setClientIC(JDBCClient clientIC) {
        this.clientIC = clientIC;
    }

    public JDBCClient getClientVox() {
        return clientVox;
    }

    public void setClientVox(JDBCClient clientVox) {
        this.clientVox = clientVox;
    }

    public JDBCClient getClientRP() {
        return clientRP;
    }

    public void setClientRP(JDBCClient clientRP) {
        this.clientRP = clientRP;
    }

    public SQLConnection getQuickBookConnection() {
        return quickBookConnection;
    }

    public void setQuickBookConnection(SQLConnection quickBookConnection) {
        this.quickBookConnection = quickBookConnection;
    }

    public JDBCClient getClientQuickBook() {
        return clientQuickBook;
    }

    public void setClientQuickBook(JDBCClient clientQuickBook) {
        this.clientQuickBook = clientQuickBook;
    }
}