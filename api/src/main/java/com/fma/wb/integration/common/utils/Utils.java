package com.fma.wb.integration.common.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.function.BiConsumer;
import java.util.regex.Pattern;

public class Utils {

    public static boolean isDate(CharSequence date) {
        // some regular expression
        String time = "(\\s(([01]?\\d)|(2[0123]))[:](([012345]\\d)|(60))"
                + "[:](([012345]\\d)|(60)))?"; // with a space before, zero or one time

        // no check for leap years (Schaltjahr)
        // and 31.02.2006 will also be correct
        String day = "(([12]\\d)|(3[01])|(0?[1-9]))"; // 01 up to 31
        String month = "((1[012])|(0\\d))"; // 01 up to 12
        String year = "\\d{4}";

        // define here all date format
        ArrayList<Pattern> patterns = new ArrayList<Pattern>();
        patterns.add(Pattern.compile(month + "/" + day + "/" + year));
        // here you can add more date formats if you want

        // check dates
        for (Pattern p : patterns) {
            if (p.matcher(date).matches()) {
                return true;
            }
        }

        return false;
    }

    public static String getNumericFromString(String data) {
        StringBuilder stringBuilderResult = new StringBuilder();
        if (!data.isEmpty()) {
            int len = data.length();
            for (int i = 0; i < len ; i++) {
                char ch = data.charAt(i);
                if ((ch >= '0' && ch <= '9')) {
                    stringBuilderResult.append(ch);
                }
            }
        }
        return stringBuilderResult.toString();
    }



    public static String toUsCentralTimeFormat(String dateTimeUtc){

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        SimpleDateFormat formatterUtc = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        formatter.setLenient(true);
        try {

            Date date = formatter.parse(dateTimeUtc);
            DateTime dt= DateTime.parse(formatterUtc.format(date)).withZone(DateTimeZone.forID("US/Central"));
            return dt.toString("yyyy-MM-dd HH:mm:ss.SSS");

        } catch (ParseException e) {
            try {
                Date date = formatterUtc.parse(dateTimeUtc);
                DateTime dt= DateTime.parse(formatterUtc.format(date)).withZone(DateTimeZone.forID("US/Central"));
                return dt.toString("yyyy-MM-dd HH:mm:ss.SSS");
            } catch (ParseException e1) {
                e1.printStackTrace();
                return null;
            }
        }

    }




    public static String toGMTDate(String centralUSDate){


        //Use required format
        String centralDateFormat="yyyy-MM-dd HH:mm:ss.SSS";
        SimpleDateFormat formatter = new SimpleDateFormat(centralDateFormat);
        try {

            //TODO: uncomment if date is not in US/Central timeZone
            //formatter.setTimeZone(TimeZone.getTimeZone("US/Central"));
            Date dt=formatter.parse(centralUSDate);
            System.out.println("US date : "+dt.toString());
            formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
            return formatter.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }


    }

    /**
     * For checking if a string is not null and not empty
     * @param string
     * @return
     */
    public static boolean isNotNullAndNotEmpty(String string) {
        if (string != null && !string.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Function that allows to determine if a string is null or empty
     * @param string
     * @return
     */
    public static boolean isNullOrEmpty(String string) {
        if (string == null) {
            return true;
        } else {
            if (string.isEmpty()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static void main(String[] args) {

        //Example
       //System.out.println("GMT Date: "+Utils.toGMTDate("2019-07-15 20:14:25.000"));
        String dateString = "2019-07-21T00:00:00-05:00".replaceAll("T"," ");
        System.out.println("dateString:   "+dateString);
        System.out.println("GMT Date: "+Utils.toGMTDate(dateString));

    }

    public static String applyFormatPhoneNumberCorrect(String phoneNumberFromPhoneSystem) {
        String result = "";
        String phoneNumber = "";
        String sip = "sip:";
        String atString = "@";
        String semicolon = ";";
        String plusOne = "+1";

        if (phoneNumberFromPhoneSystem != null && !phoneNumberFromPhoneSystem.isEmpty()) {
            if (phoneNumberFromPhoneSystem.length() > 1) {
                if ((phoneNumberFromPhoneSystem).contains(sip)) {
                    if (phoneNumberFromPhoneSystem.contains(atString)) {
                        //extract everything between sip: and before @
                        phoneNumber = phoneNumberFromPhoneSystem.substring(phoneNumberFromPhoneSystem.indexOf(":") + 1, phoneNumberFromPhoneSystem.indexOf(atString));
                    } else if (phoneNumberFromPhoneSystem.contains(semicolon)) {
                        //extract everything between sip: and before ;
                        phoneNumber = phoneNumberFromPhoneSystem.substring(phoneNumberFromPhoneSystem.indexOf(":") + 1, phoneNumberFromPhoneSystem.indexOf(semicolon));
                    }
                    if (phoneNumber.startsWith(plusOne)) {
                        phoneNumber = phoneNumber.replace(plusOne, "");
                    }
                    result = phoneNumber;

                } else {

                    //Cleaning phone number
                    phoneNumber = phoneNumberFromPhoneSystem.replaceAll("[^-?0-9]+", "");

                    if (phoneNumber.startsWith("1") && (phoneNumber.length() >= 10)) {
                        phoneNumber = phoneNumber.substring(1);
                    }

                    if (phoneNumber.length() >= 10) {
                        phoneNumber = phoneNumber.substring(0, 10);
                    }
                    result = phoneNumber;
                }
            }
        }
        if(!result.isEmpty())
            return result;
        else return phoneNumberFromPhoneSystem;
    }


}