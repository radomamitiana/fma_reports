package com.fma.wb.integration.reportsmodule.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fma.wb.integration.common.dao.DbConnexionHelper;
import com.fma.wb.integration.common.dto.api.InternalResponse;
import com.fma.wb.integration.common.dto.enums.DBType;
import com.fma.wb.integration.reportsmodule.dto.Organization;
import com.fma.wb.integration.reportsmodule.dto.healthcare.HealthCareInputData;
import com.fma.wb.integration.reportsmodule.dto.healthcare.HealthOutputData;
import com.fma.wb.integration.reportsmodule.utils.DateUtils;
import com.fma.wb.integration.reportsmodule.utils.io.HealthExcelAndPPTFileHelper;
import common.Logger;
import common.LoggerTag;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.sql.SQLException;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static com.fma.wb.integration.reportsmodule.utils.CommonBackOfficeAndReportingQueries.*;

public class HealthCareService {
    private static HealthCareService healthCareService;
    private Vertx vertx;
    private static DbConnexionHelper dbConnexionHelper;
    private  HashMap<String,String> credentials=new HashMap<>();
    private static List<Organization> organizationsListGlobal = new ArrayList<>();
    public HealthCareService(Vertx vertx) {
        this.vertx=vertx;
        this.setDbConnexionHelper(new DbConnexionHelper(vertx,credentials));

    }


    public static HealthCareService getInstance(Vertx vertx) {
        if (healthCareService == null) {
            healthCareService = new HealthCareService(vertx);
        }
        return healthCareService;
    }



    public void gethealthcare(String sessionId, HealthCareInputData healthCareInputData, Future<InternalResponse>  future){
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: getHealth care data ~~~~~~~~~~~~~~~~~~~~~~","");
        System.out.println("SESS: "+ Json.encodePrettily(TrustBalancingService.getActiveSessions()));
        System.out.println("Id: "+ sessionId);

        //Add on days to EndDateRange
        //healthCareInputData.setEndDateRange(Utils.addDaysToDate(healthCareInputData.getEndDateRange(),1));

        if(!TrustBalancingService.getActiveSessions().containsKey(sessionId)){
            InternalResponse resp=new InternalResponse();
            resp.setCause(-1);
            if(!future.isComplete())
                future.complete(resp);
        }else{

            if (dbConnexionHelper.getFmaConnection() == null) {
                try {
                    TrustBalancingService.connectToDb(DBType.Infinity2, event -> {

                        if (event.succeeded()) {
                            dbConnexionHelper.setFmaConnection(event.result());

                            //Start healthCare datas gettings
                            processHealthCare(healthCareInputData,future, sessionId);


                        }else{
                            InternalResponse resp=new InternalResponse();
                            resp.setCause(0);
                            resp.setData(event.cause().getMessage());
                            if(!future.isComplete())
                                future.complete(resp);
                        }

                    },dbConnexionHelper);
                } catch (SQLException | ClassNotFoundException e) {
                    e.printStackTrace();
                    InternalResponse resp=new InternalResponse();
                    resp.setCause(0);
                    resp.setData(e.getMessage());
                    if(!future.isComplete())
                        future.complete(resp);
                }
            }else{
                //Start healthCare datas gettings
                processHealthCare(healthCareInputData,future, sessionId);

            }
        }


    }


    private void processHealthCare(HealthCareInputData healthCareInputData, Future<InternalResponse> future, String sessionId){
        getAllFinClassDatas(healthCareInputData,sessionId).setHandler(finRes->{
            if(finRes.succeeded()){

                if(finRes.result().getFiniancialClass().isEmpty()){
                    InternalResponse resp=new InternalResponse();
                    resp.setCause(0);
                    resp.setData("NO FINANCIAL CLASS FOUND FOR THIS PERIOD");
                    if(!future.isComplete())
                        future.complete(resp);
                }

                getAllAggingDatas(healthCareInputData,finRes.result()).setHandler(finRes2->{
                    if(finRes2.succeeded()){

                        getAllOverResDatas(healthCareInputData,finRes2.result()).setHandler(finRes3->{

                            if (finRes3.succeeded()){


                                getAllMonthDatas(healthCareInputData,finRes3.result()).setHandler(finRes4->{


                                    if (finRes4.succeeded()){

                                        //TODO: CALL HEN ALL DATAS ARE READY
                                        InternalResponse resp=new InternalResponse();
                                        resp.setCause(1);
                                        resp.setData(Json.encode(finRes4.result()));
                                        if(!future.isComplete())
                                            future.complete(resp);
                                    }else{
                                        InternalResponse resp=new InternalResponse();
                                        resp.setCause(0);
                                        resp.setData(finRes4.cause().getMessage());
                                        if(!future.isComplete())
                                            future.complete(resp);
                                    }


                                });

                            }else{
                                InternalResponse resp=new InternalResponse();
                                resp.setCause(0);
                                resp.setData(finRes3.cause().getMessage());
                                if(!future.isComplete())
                                    future.complete(resp);
                            }

                        });



                    }else{
                        InternalResponse resp=new InternalResponse();
                        resp.setCause(0);
                        resp.setData(finRes2.cause().getMessage());
                        if(!future.isComplete())
                            future.complete(resp);
                    }
                });


            } else {
                InternalResponse resp=new InternalResponse();
                resp.setCause(0);
                resp.setData(finRes.cause().getMessage());
                if(!future.isComplete())
                    future.complete(resp);
            }
        });
    }


    private Future<HealthOutputData> getAllFinClassDatas(HealthCareInputData healthCareInputData, String sessionId){
        Future<HealthOutputData> future =Future.future();

        //FIRST GET CLIENTS AND FIN classes related to current selected Organization
        this.vertx.<HealthOutputData>executeBlocking(resultats1->this.getClientsAndFinClasses(healthCareInputData,resultats1,sessionId),true,asynch->{
            if(asynch.succeeded()){

                // SECOND GET FINANCIALCLASS DATA #
                this.vertx.<HealthOutputData>executeBlocking(resultats2->this.getFinancialClassDatas1(healthCareInputData,asynch.result(),resultats2),true,asynch2->{

                    if(asynch2.succeeded()){

                        // SECOND GET FINANCIALCLASS DATA $
                        this.vertx.<HealthOutputData>executeBlocking(resultats3->this.getFinancialClassDatas2(healthCareInputData,asynch.result(),resultats3),true,asynch3->{

                            if(asynch3.succeeded()){


                                // SECOND GET FINANCIALCLASS DATA 3
                                this.vertx.<HealthOutputData>executeBlocking(resultats4->this.getFinancialClassDatas3(healthCareInputData,asynch.result(),resultats4),true,asynch4->{

                                    if(asynch4.succeeded()){

                                        if(!future.isComplete())
                                            future.complete(asynch4.result());

                                    }else{
                                        /*FIXME: CUSTOM MESSAGE HERE*/
                                        if(!future.isComplete())
                                            future.fail("Error Getting FINANCIAL CLASS DATAS Agging");
                                    }
                                });



                            }else{
                                /*FIXME: CUSTOM MESSAGE HERE*/
                                if(!future.isComplete())
                                    future.fail("Error Getting FINANCIAL CLASS DATAS Active Inventory #");
                            }
                        });

                    }else{
                        /*FIXME: CUSTOM MESSAGE HERE*/
                        if(!future.isComplete())
                            future.fail("Error Getting FINANCIAL CLASS DATAS");
                    }

                });

            }else{

                /*FIXME: CUSTOM MESSAGE HERE*/
                if(!future.isComplete())
                    future.fail("Error Getting Clients Or financial classes");
            }

        });

        return future;
    }


    private Future<HealthOutputData> getAllAggingDatas(HealthCareInputData healthCareInputData,HealthOutputData outputData){
        Future<HealthOutputData> future = Future.future();

        this.vertx.<HealthOutputData>executeBlocking(aging1->this.getAgingDatas1(healthCareInputData,outputData,aging1), asynch->{

            if (asynch.succeeded()){

                this.vertx.<HealthOutputData>executeBlocking(aging2->this.getAgingDatas2(healthCareInputData,outputData,aging2),asynch2->{


                    if (asynch2.succeeded()){

                        if(!future.isComplete())
                            future.complete(asynch2.result());

                    }else{

                        /*FIXME: CUSTOM MESSAGE HERE*/
                        if(!future.isComplete())
                            future.fail("Error Getting Agging result 1");

                    }


                });


            }else{

                /*FIXME: CUSTOM MESSAGE HERE*/
                if(!future.isComplete())
                    future.fail("Error Getting Agging result 1");

            }

        });
        return future;
    }


    private Future<HealthOutputData> getAllOverResDatas(HealthCareInputData healthCareInputData,HealthOutputData outputData){
        Future<HealthOutputData> future = Future.future();

        this.vertx.<HealthOutputData>executeBlocking(over1->getOverallDatas1(healthCareInputData,outputData,over1),asynch->{
            if (asynch.succeeded()){

                if(!future.isComplete())
                    future.complete(asynch.result());

            }else{

                /*FIXME: CUSTOM MESSAGE HERE*/
                if(!future.isComplete())
                    future.fail("Error Getting Overall result 1");

            }
        });

        return future;
    }


    private Future<HealthOutputData> getAllMonthDatas(HealthCareInputData healthCareInputData, HealthOutputData outputData){
        Future<HealthOutputData> future = Future.future();

        AtomicInteger i=new AtomicInteger();
        outputData.getClientsIds().forEach(client -> {
            Map<String,List<String>> datas=new HashMap<>();

            String quer=String.format(HEALTH_GetInventory,client,DateUtils.getSqlDate(healthCareInputData.getBeginDateRange()),DateUtils.getSqlDate(DateUtils.addDaysToDate(healthCareInputData.getEndDateRange(),1)));
            System.out.println(quer);
            dbConnexionHelper.getFmaConnection().query(quer,invent->{

                i.getAndIncrement();
                if (invent.succeeded()){

                    List<JsonArray> resG=invent.result().getResults();

                    resG=resG.stream().filter(Objects::nonNull).collect(Collectors.toList());
                    System.out.println("CLI1: "+Json.encodePrettily(resG));


                    resG.forEach(objects -> {

                        List<String> init=new ArrayList<>();

                        for (int j = 0; j < 22; j++) {
                            init.add(" ");
                        }

                        for (int j = 2; j < objects.size(); j++) {
                            init.set(j-2,""+objects.getValue(j));

                        }


                        datas.put(objects.getInteger(1)+"-"+objects.getInteger(0),init);


                    });


                    outputData.addMonthlyList(outputData.getClientsNames().get(outputData.getClientsIds().indexOf(client)),datas);

                    if(i.get()==outputData.getClientsNames().size() && !future.isComplete()){
                        System.out.println("OUT9: "+Json.encodePrettily(outputData));

                        getAdjustments(outputData).setHandler(finAdj->{

                            if(finAdj.succeeded()){

                                getPIF(finAdj.result()).setHandler(finPif->{

                                    if(finPif.succeeded()){

                                        getClientsReturns(finPif.result()).setHandler(finCR->{

                                            if(finCR.succeeded()){

                                                getClientsMonthRecalls(finCR.result()).setHandler(finMRC->{
                                                    if(finMRC.succeeded()){

                                                        getClientsRecalls(finMRC.result()).setHandler(finCRC->{

                                                            if(finCRC.succeeded()){
                                                                getClaimsDenied(finCRC.result()).setHandler(finCD->{

                                                                    if(finCD.succeeded()){
                                                                        getMonthCollections(healthCareInputData,finCD.result()).setHandler(finMC->{

                                                                            if(finMC.succeeded()){

                                                                                getLTDCollections(healthCareInputData,finMC.result()).setHandler(finLTD->{

                                                                                    if(finLTD.succeeded()){
                                                                                        getAverageAge(finLTD.result()).setHandler(finAV->{

                                                                                            if(finAV.succeeded()){

                                                                                                getReportDetails(healthCareInputData,finAV.result()).setHandler(finRD->{

                                                                                                    if(finRD.succeeded()){
                                                                                                        //End getting datas...
                                                                                                        if(!future.isComplete()) {
                                                                                                            //Claculate missing monthly fields
                                                                                                            //HealthOutputData finalDatas = calculateMonthlyFields(finRD.result());
                                                                                                            HealthOutputData finalDatas = finRD.result();
                                                                                                            future.complete(finalDatas);

                                                                                                        }

                                                                                                    }else{
                                                                                                        if(!future.isComplete())
                                                                                                            future.fail("Error Getting Repports Details");

                                                                                                    }

                                                                                                });


                                                                                            }else{
                                                                                                if(!future.isComplete())
                                                                                                    future.fail("Error Getting Client Average Age");

                                                                                            }

                                                                                        });

                                                                                    }else{
                                                                                        if(!future.isComplete())
                                                                                            future.fail("Error Getting Life to date collections");

                                                                                    }

                                                                                });



                                                                            }else{
                                                                                if(!future.isComplete())
                                                                                    future.fail("Error Getting Client Month collections");

                                                                            }

                                                                        });

                                                                    }else{
                                                                        if(!future.isComplete())
                                                                            future.fail("Error Getting Client Claims denied");

                                                                    }

                                                                });


                                                            }else{
                                                                if(!future.isComplete())
                                                                    future.fail("Error Getting Client Recalls");

                                                            }

                                                        });


                                                    }else{
                                                        if(!future.isComplete())
                                                            future.fail("Error Getting client Months recalls");

                                                    }

                                                });




                                            }else{
                                                if(!future.isComplete())
                                                    future.fail("Error Getting Client returns");

                                            }

                                        });



                                    }else{
                                        if(!future.isComplete())
                                            future.fail("Error Getting PIF datas");

                                    }

                                });


                            }else{
                                if(!future.isComplete())
                                    future.fail("Error Getting Client Adjustments");

                            }

                        });

                    }


                }else{

                    /*FIXME: CUSTOM MESSAGE HERE*/
                    if(!future.isComplete())
                        future.fail("Error Getting Client Inventory");

                }

            });

        });

        return future;
    }



    private Future<HealthOutputData> getAdjustments(HealthOutputData outputData){

        Future<HealthOutputData> future = Future.future();

                AtomicInteger i=new AtomicInteger();
                AtomicInteger total= new AtomicInteger(outputData.getMonthlyList().values().size());
                total.set(outputData.getMonthlyList().values().stream()
                        .mapToInt(Map::size)
                        .sum());
                        System.out.println("SIZEEEEE ADJUSTMENTS: "+total);

                        if(total.get() ==0){
                            System.out.println("OUT10: "+Json.encodePrettily(outputData));
                            future.complete(outputData);
                        }

                outputData.getMonthlyList().keySet().forEach(client -> {

                    Map<String, List<String>> datas=outputData.getMonthlyList().get(client);

                    datas.keySet().forEach(datesEntry -> {

                    String qq=String.format(HEALTH_getAdjustments,outputData.getClientsIds().get(outputData.getClientsNames().indexOf(client)),datesEntry.split("-")[1],datesEntry.split("-")[0]);

                    System.out.println(qq+"\n");

                    dbConnexionHelper.getFmaConnection().query(qq,adj->{


                        if (adj.succeeded()){

                            i.getAndIncrement();

                            List<JsonArray> resG=adj.result().getResults();


                            //resG=resG.stream().filter(Objects::nonNull).collect(Collectors.toList());
                            System.out.println("CLI2a: "+Json.encodePrettily(resG));



                            resG.forEach(objects -> {

                                String ssKey=objects.getInteger(1)+"-"+objects.getInteger(0);

                                System.out.println("SSK: "+ssKey);

                                List<String> old=datas.get(ssKey);
                                if(old==null){
                                    old=new ArrayList<>(21);
                                    for (int j = 0; j < 21; j++) {
                                        old.add(" ");
                                    }
                                }
                                System.out.println("SSList: "+Json.encodePrettily(old));

                                old.set(3,""+objects.getValue(3));

                                datas.put(ssKey,old);


                            });


                            outputData.setMonthlyList(client,datas);
                            System.out.println("III: "+i.get());

                            if(i.get()== total.get()){
                                System.out.println("III: "+i.get());
                                if(!future.isComplete())
                                    future.complete(outputData);

                            }



                        }else{

                            /*FIXME: CUSTOM MESSAGE HERE*/
                            if(!future.isComplete())
                                future.fail("Error Getting Client Adjustments");

                        }

                    });

                });


            });

        return future;
    }



    private Future<HealthOutputData> getPIF(HealthOutputData outputData){

        Future<HealthOutputData> future = Future.future();

        AtomicInteger i=new AtomicInteger();
        AtomicInteger total= new AtomicInteger(outputData.getMonthlyList().values().size());
        total.set(outputData.getMonthlyList().values().stream()
                .mapToInt(Map::size)
                .sum());
        System.out.println("SIZEEEEE: "+total);

        if(total.get() ==0){
            System.out.println("OUT11: "+Json.encodePrettily(outputData));
            future.complete(outputData);
        }

        outputData.getMonthlyList().keySet().forEach(client -> {

            Map<String, List<String>> datas=outputData.getMonthlyList().get(client);

            datas.keySet().forEach(datesEntry -> {

                String qq=String.format(HEALTH_GetPIF,outputData.getClientsIds().get(outputData.getClientsNames().indexOf(client)),datesEntry.split("-")[1],datesEntry.split("-")[0]);

                System.out.println(qq+"\n");

                dbConnexionHelper.getFmaConnection().query(qq,adj->{


                    if (adj.succeeded()){

                        i.getAndIncrement();

                        List<JsonArray> resG=adj.result().getResults();

                        //resG=resG.stream().filter(Objects::nonNull).collect(Collectors.toList());
                        System.out.println("CLI3: "+Json.encodePrettily(resG));


                        resG.forEach(objects -> {

                            List<String> old=datas.get(datesEntry);
                            old.set(4,""+objects.getValue(0));

                            datas.put(datesEntry,old);


                        });


                        outputData.setMonthlyList(client,datas);
                        System.out.println("III: "+i.get());

                        if(i.get()== total.get()){
                            System.out.println("III: "+i.get());
                            if(!future.isComplete())
                                future.complete(outputData);

                        }



                    }else{

                        /*FIXME: CUSTOM MESSAGE HERE*/
                        if(!future.isComplete())
                            future.fail("Error Getting Client Adjustments");

                    }

                });

            });


        });

        return future;
    }



    private Future<HealthOutputData> getClientsReturns(HealthOutputData outputData){

        Future<HealthOutputData> future = Future.future();

        AtomicInteger i=new AtomicInteger();
        AtomicInteger total= new AtomicInteger(outputData.getMonthlyList().values().size());
        total.set(outputData.getMonthlyList().values().stream()
                .mapToInt(Map::size)
                .sum());
        System.out.println("SIZEEEEE RETURNS: "+total);

        if(total.get() ==0){
            System.out.println("OUT12: "+Json.encodePrettily(outputData));
            future.complete(outputData);
        }

        outputData.getMonthlyList().keySet().forEach(client -> {

            Map<String, List<String>> datas=outputData.getMonthlyList().get(client);

            datas.keySet().forEach(datesEntry -> {

                String qq=String.format(HEALTH_GetClientReturns,outputData.getClientsIds().get(outputData.getClientsNames().indexOf(client)),datesEntry.split("-")[1],datesEntry.split("-")[0]);

                System.out.println(qq+"\n");

                dbConnexionHelper.getFmaConnection().query(qq,adj->{


                    if (adj.succeeded()){

                        i.getAndIncrement();

                        List<JsonArray> resG=adj.result().getResults();

                        resG=resG.stream().filter(Objects::nonNull).collect(Collectors.toList());
                        System.out.println("CLI4: "+Json.encodePrettily(resG));


                        resG.forEach(objects -> {

                            List<String> old=datas.get(datesEntry);
                            old.set(5,""+objects.getValue(2));
                            old.set(6,""+objects.getValue(3));

                            datas.put(datesEntry,old);


                        });

                        outputData.setMonthlyList(client,datas);
                        System.out.println("III: "+i.get());

                        if(i.get()== total.get()){
                            System.out.println("III: "+i.get());
                            if(!future.isComplete())
                                future.complete(outputData);

                        }



                    }else{

                        /*FIXME: CUSTOM MESSAGE HERE*/
                        if(!future.isComplete())
                            future.fail("Error Getting Client Adjustments");

                    }

                });

            });


        });

        return future;
    }



    private Future<HealthOutputData> getClientsMonthRecalls(HealthOutputData outputData){

        Future<HealthOutputData> future = Future.future();

        AtomicInteger i=new AtomicInteger();
        AtomicInteger total= new AtomicInteger(outputData.getMonthlyList().values().size());
        total.set(outputData.getMonthlyList().values().stream()
                .mapToInt(Map::size)
                .sum());
        System.out.println("SIZEEEEE RECALLS: "+total);

        if(total.get() ==0){
            System.out.println("OUT13A: "+Json.encodePrettily(outputData));
            future.complete(outputData);
        }

        outputData.getMonthlyList().keySet().forEach(client -> {

            Map<String, List<String>> datas=outputData.getMonthlyList().get(client);

            datas.keySet().forEach(datesEntry -> {

                String qq=healthGetClientMonthRecall(""+outputData.getClientsIds().get(outputData.getClientsNames().indexOf(client)),datesEntry.split("-")[1],datesEntry.split("-")[0]);

                System.out.println(qq+"\n");

                dbConnexionHelper.getFmaConnection().query(qq,adj->{


                    if (adj.succeeded()){

                        i.getAndIncrement();

                        List<JsonArray> resG=adj.result().getResults();

                        resG=resG.stream().filter(Objects::nonNull).collect(Collectors.toList());
                        System.out.println("CLI5: "+Json.encodePrettily(resG));

                        resG.forEach(objects -> {

                            List<String> old=datas.get(datesEntry);
                            old.set(7,""+objects.getValue(0));
                            old.set(8,""+objects.getValue(1));

                            datas.put(datesEntry,old);


                        });


                        outputData.setMonthlyList(client,datas);
                        System.out.println("III: "+i.get());

                        if(i.get()== total.get()){
                            System.out.println("III: "+i.get());
                            if(!future.isComplete())
                                future.complete(outputData);

                        }



                    }else{

                        /*FIXME: CUSTOM MESSAGE HERE*/
                        if(!future.isComplete())
                            future.fail("Error Getting Client Adjustments");

                    }

                });

            });


        });

        return future;
    }



    private Future<HealthOutputData> getClientsRecalls(HealthOutputData outputData){

        Future<HealthOutputData> future = Future.future();

        AtomicInteger i=new AtomicInteger();
        AtomicInteger total= new AtomicInteger(outputData.getMonthlyList().values().size());
        total.set(outputData.getMonthlyList().values().stream()
                .mapToInt(Map::size)
                .sum());
        System.out.println(" C_RECALLS: "+total);

        if(total.get() ==0){
            System.out.println("OUT13: "+Json.encodePrettily(outputData));
            future.complete(outputData);
        }

        outputData.getMonthlyList().keySet().forEach(client -> {

            Map<String, List<String>> datas=outputData.getMonthlyList().get(client);

            datas.keySet().forEach(datesEntry -> {

                String qq=healthGetClientRecalls(""+outputData.getClientsIds().get(outputData.getClientsNames().indexOf(client)),datesEntry.split("-")[1],datesEntry.split("-")[0]);

                System.out.println(qq+"\n");

                dbConnexionHelper.getFmaConnection().query(qq,adj->{


                    if (adj.succeeded()){

                        i.getAndIncrement();

                        List<JsonArray> resG=adj.result().getResults();

                        resG=resG.stream().filter(Objects::nonNull).collect(Collectors.toList());
                        System.out.println("CLI6: "+Json.encodePrettily(resG));


                        resG.forEach(objects -> {

                            List<String> old=datas.get(datesEntry);
                            old.set(9,""+objects.getValue(2));
                            old.set(10,""+objects.getValue(3));

                            datas.put(datesEntry,old);


                        });


                        outputData.setMonthlyList(client,datas);
                        System.out.println("III: "+i.get());

                        if(i.get()== total.get()){
                            System.out.println("III: "+i.get());
                            if(!future.isComplete())
                                future.complete(outputData);

                        }



                    }else{

                        /*FIXME: CUSTOM MESSAGE HERE*/
                        if(!future.isComplete())
                            future.fail("Error Getting Client Adjustments");

                    }

                });

            });


        });

        return future;
    }


    private Future<HealthOutputData> getClaimsDenied(HealthOutputData outputData){

        Future<HealthOutputData> future = Future.future();

        AtomicInteger i=new AtomicInteger();
        AtomicInteger total= new AtomicInteger(outputData.getMonthlyList().values().size());
        total.set(outputData.getMonthlyList().values().stream()
                .mapToInt(Map::size)
                .sum());
        System.out.println("SIZEEEEE CLAIMS: "+total);

        if(total.get() ==0){
            System.out.println("OUT14: "+Json.encodePrettily(outputData));
            future.complete(outputData);
        }

        outputData.getMonthlyList().keySet().forEach(client -> {

            Map<String, List<String>> datas=outputData.getMonthlyList().get(client);

            datas.keySet().forEach(datesEntry -> {

                String qq=healthGetClaimsDenied(""+outputData.getClientsIds().get(outputData.getClientsNames().indexOf(client)),datesEntry.split("-")[1],datesEntry.split("-")[0]);

                System.out.println(qq+"\n");

                dbConnexionHelper.getFmaConnection().query(qq,adj->{


                    if (adj.succeeded()){

                        i.getAndIncrement();

                        List<JsonArray> resG=adj.result().getResults();

                        resG=resG.stream().filter(Objects::nonNull).collect(Collectors.toList());
                        System.out.println("CLI7: "+Json.encodePrettily(resG));


                        resG.forEach(objects -> {

                            List<String> old=datas.get(datesEntry);
                            old.set(11,""+objects.getValue(2));
                            old.set(12,""+objects.getValue(3));

                            datas.put(datesEntry,old);


                        });


                        outputData.setMonthlyList(client,datas);
                        System.out.println("III: "+i.get());

                        if(i.get()== total.get()){
                            System.out.println("III: "+i.get());
                            if(!future.isComplete())
                                future.complete(outputData);

                        }



                    }else{

                        /*FIXME: CUSTOM MESSAGE HERE*/
                        if(!future.isComplete())
                            future.fail("Error Getting Client Adjustments");

                    }

                });

            });


        });

        return future;
    }



    private Future<HealthOutputData> getMonthCollections(HealthCareInputData healthCareInputData,HealthOutputData outputData){

        Future<HealthOutputData> future = Future.future();

        AtomicInteger i=new AtomicInteger();
        AtomicInteger total= new AtomicInteger(outputData.getMonthlyList().values().size());
        total.set(outputData.getMonthlyList().values().stream()
                .mapToInt(Map::size)
                .sum());
        System.out.println("SIZEEEEE M_COLLECTIONS: "+total);

        if(total.get() ==0){
            System.out.println("OUT14: "+Json.encodePrettily(outputData));
            future.complete(outputData);
        }

        outputData.getMonthlyList().keySet().forEach(client -> {

            Map<String, List<String>> datas=outputData.getMonthlyList().get(client);

            datas.keySet().forEach(datesEntry -> {

                String dt= DateUtils.minusMonthsFromDate(DateUtils.addDaysToDate(healthCareInputData.getEndDateRange(),1),1);
                String qq=String.format(HEALTH_GetMonthCollections,outputData.getClientsIds().get(outputData.getClientsNames().indexOf(client)),datesEntry.split("-")[1],datesEntry.split("-")[0],DateUtils.getDateYear(dt),DateUtils.getDateMonth(dt));

                System.out.println(qq+"\n");

                dbConnexionHelper.getFmaConnection().query(qq,adj->{


                    if (adj.succeeded()){

                        i.getAndIncrement();

                        List<JsonArray> resG=adj.result().getResults();

                        resG=resG.stream().filter(Objects::nonNull).collect(Collectors.toList());
                        System.out.println("CLI8: "+Json.encodePrettily(resG));




                        resG.forEach(objects -> {

                            List<String> old=datas.get(datesEntry);
                            old.set(14,""+objects.getValue(2));

                            datas.put(datesEntry,old);

                        });




                        outputData.setMonthlyList(client,datas);
                        System.out.println("III: "+i.get());

                        if(i.get()== total.get()){
                            System.out.println("III: "+i.get());
                            if(!future.isComplete())
                                future.complete(outputData);

                        }



                    }else{

                        /*FIXME: CUSTOM MESSAGE HERE*/
                        if(!future.isComplete())
                            future.fail("Error Getting Client Adjustments");

                    }

                });

            });


        });

        return future;
    }



    private Future<HealthOutputData> getLTDCollections(HealthCareInputData healthCareInputData,HealthOutputData outputData){

        Future<HealthOutputData> future = Future.future();

        AtomicInteger i=new AtomicInteger();
        AtomicInteger total= new AtomicInteger(outputData.getMonthlyList().values().size());
        total.set(outputData.getMonthlyList().values().stream()
                .mapToInt(Map::size)
                .sum());
        System.out.println("SIZEEEEE L_COLLECTIONS: "+total);

        if(total.get() ==0){
            System.out.println("OUT15: "+Json.encodePrettily(outputData));
            future.complete(outputData);
        }

        outputData.getMonthlyList().keySet().forEach(client -> {

            Map<String, List<String>> datas=outputData.getMonthlyList().get(client);

            datas.keySet().forEach(datesEntry -> {

                String qq=healthGetLTDCollections(outputData.getClientsIds().get(outputData.getClientsNames().indexOf(client)),datesEntry.split("-")[1],datesEntry.split("-")[0],healthCareInputData.getEndDateRange());

                System.out.println(qq+"\n");

                dbConnexionHelper.getFmaConnection().query(qq,adj->{


                    if (adj.succeeded()){

                        i.getAndIncrement();

                        List<JsonArray> resG=adj.result().getResults();

                        resG=resG.stream().filter(Objects::nonNull).collect(Collectors.toList());
                        System.out.println("CLI9: "+Json.encodePrettily(resG));


                        resG.forEach(objects -> {

                            List<String> old=datas.get(datesEntry);
                            old.set(16,""+objects.getValue(2));

                            datas.put(datesEntry,old);


                        });



                        outputData.setMonthlyList(client,datas);
                        System.out.println("III: "+i.get());

                        if(i.get()== total.get()){
                            System.out.println("III: "+i.get());
                            if(!future.isComplete())
                                future.complete(outputData);

                        }



                    }else{

                        /*FIXME: CUSTOM MESSAGE HERE*/
                        if(!future.isComplete())
                            future.fail("Error Getting Client Adjustments");

                    }

                });

            });


        });

        return future;
    }




    private Future<HealthOutputData> getAverageAge(HealthOutputData outputData){

        Future<HealthOutputData> future = Future.future();

        AtomicInteger i=new AtomicInteger();
        AtomicInteger total= new AtomicInteger(outputData.getMonthlyList().values().size());
        total.set(outputData.getMonthlyList().values().stream()
                .mapToInt(Map::size)
                .sum());
        System.out.println("SIZEEEEE AVG_AGE: "+total);

        if(total.get() ==0){
            System.out.println("OUT16: "+Json.encodePrettily(outputData));
            future.complete(outputData);
        }

        outputData.getMonthlyList().keySet().forEach(client -> {

            Map<String, List<String>> datas=outputData.getMonthlyList().get(client);

            datas.keySet().forEach(datesEntry -> {

                String qq=healthGetAverageAge(outputData.getClientsIds().get(outputData.getClientsNames().indexOf(client)),datesEntry.split("-")[1],datesEntry.split("-")[0]);

                System.out.println(qq+"\n");

                dbConnexionHelper.getFmaConnection().query(qq,adj->{


                    if (adj.succeeded()){

                        i.getAndIncrement();

                        List<JsonArray> resG=adj.result().getResults();

                        resG=resG.stream().filter(Objects::nonNull).collect(Collectors.toList());
                        System.out.println("CLI10: "+Json.encodePrettily(resG));


                        resG.forEach(objects -> {

                            List<String> old=datas.get(datesEntry);
                            old.set(21,""+objects.getValue(0));

                            datas.put(datesEntry,old);


                        });


                        outputData.setMonthlyList(client,datas);
                        System.out.println("III: "+i.get());

                        if(i.get()== total.get()){
                            System.out.println("III: "+i.get());
                            if(!future.isComplete())
                                future.complete(outputData);

                        }



                    }else{

                        /*FIXME: CUSTOM MESSAGE HERE*/
                        if(!future.isComplete())
                            future.fail("Error Getting Client Adjustments");

                    }

                });

            });


        });

        return future;
    }


    /*private HealthOutputData calculateBatchTrack(HealthOutputData outputData){


        Map<String, List<String>> mergedDatas=new HashMap<>();

        Stream<Map<String, List<String>>> ddt=outputData.getMonthlyList().values().stream().filter(stringListMap -> !stringListMap.isEmpty());

        ddt.collect(Collectors.toList()).forEach(stringListMap -> {
            stringListMap.forEach((s, strings) -> {
                if(!strings.isEmpty()){
                    if(!mergedDatas.containsKey(s)){
                        mergedDatas.put(s,strings);
                    }else{
                        double s1;

                        for (int i = 0; i < strings.size(); i++) {
                            s1=Double.valueOf((strings.get(i).trim().isEmpty()?"0.0":strings.get(i)))+Double.valueOf((mergedDatas.get(s).get(i).trim().isEmpty()?"0.0":mergedDatas.get(s).get(i)));
                            strings.set(i,""+s1);
                        }
                        mergedDatas.replace(s,strings);



                    }
                }

            });

        });

        outputData.setBatchTrackAlldatas(mergedDatas);
        return outputData;

    }*/

    private HealthOutputData calculateMonthlyFields(HealthOutputData outputData){

        outputData.getMonthlyList().forEach((s, stringListMap) -> {

            stringListMap.forEach((s1, strings) -> {

                /* try {
                    strings.set(13, "" + (Double.valueOf(strings.get(1)) - Double.valueOf(strings.get(3)) - Double.valueOf(strings.get(9)) - Double.valueOf(strings.get(5))));
                }catch (Exception ignored){

                }

                try{

                    strings.set(16,""+(Double.valueOf(strings.get(15))/Double.valueOf(strings.get(1))));

                }catch (Exception ignored){

                }


               try{
                    strings.set(18,""+(Double.valueOf(strings.get(0))-Double.valueOf(strings.get(4))-Double.valueOf(strings.get(8))));
                }catch (Exception ignored){

                }

                try{
                    strings.set(19,""+(Double.valueOf(strings.get(1))+(Double.valueOf(strings.get(3))-Double.valueOf(strings.get(16))-Double.valueOf(strings.get(6)))));
                }catch (Exception ignored){

                }

                try{
                    strings.set(19,""+(Double.valueOf(strings.get(18))/Double.valueOf(strings.get(0))));
                }catch (Exception ignored){

                }*/

                stringListMap.put(s1,strings);

                outputData.addMonthlyList(s,stringListMap);

            });

        });

        return outputData;

    }


    private Future<HealthOutputData> getReportDetails(HealthCareInputData healthCareInputData,HealthOutputData outputData){

        Future<HealthOutputData> future = Future.future();

        future.complete(outputData);

        /*AtomicInteger i=new AtomicInteger();

        outputData.getClientsIds().forEach(client -> {

            String qq=healthGetReportDetails(client,healthCareInputData.getBeginDateRange(),DateUtils.addMonthsToDate(healthCareInputData.getEndDateRange(),1));

            System.out.println(qq+"\n");

            dbConnexionHelper.getFmaConnection().query(qq,adj->{


                if (adj.succeeded()){

                    i.getAndIncrement();

                    List<JsonArray> resG=adj.result().getResults();

                    resG=resG.stream().filter(Objects::nonNull).collect(Collectors.toList());
                    System.out.println("CLI11: "+Json.encodePrettily(resG));

                    List<String> list=new ArrayList<>();

                    resG.forEach(objects -> {
                        for (int j = 0; j < objects.size(); j++) {
                            list.add(""+objects.getValue(j));

                        }

                    });


                    outputData.addReportDetails(outputData.getClientsNames().get(outputData.getClientsIds().indexOf(client)),list);
                    System.out.println("III: "+i.get());

                    if(i.get()== outputData.getClientsIds().size()){
                        System.out.println("III: "+i.get());
                        if(!future.isComplete())
                            future.complete(outputData);

                    }



                }else{

                    if(!future.isComplete())
                        future.fail("Error Getting Client Adjustments");

                }

            });


        });*/

        return future;
    }



    private Future<HealthOutputData> getGrandTotDatas(){
        Future<HealthOutputData> future = Future.future();

        return future;
    }


    private void getClientsAndFinClasses(HealthCareInputData healthCareInputData,Future<HealthOutputData> out,String sessionId){
        HealthOutputData outputData=new HealthOutputData();
        outputData.setEndDateSelected(healthCareInputData.getEndDateRange());
        outputData.setStartDateSelected(healthCareInputData.getBeginDateRange());
        outputData.setShowClients(healthCareInputData.isShowClients());
        outputData.setSessionId(sessionId);

       String shortName= organizationsListGlobal.stream().filter(organization -> organization.getOrganization_id()==healthCareInputData.getOrganizationId()).collect(Collectors.toList()).get(0).getRpt_short_name();

        outputData.setShortOrganizationName((shortName==null)?"":shortName);
        String quer1=String.format(GET_CLI_BY_ORG,""+healthCareInputData.getOrganizationId());
        System.out.println(quer1);
        dbConnexionHelper.getFmaConnection().query(quer1,resultats->{
            if(resultats.succeeded()){
                 System.out.println("DEBUG: "+Json.encodePrettily(resultats.result().getRows()));

                ArrayList<String> clNames=new ArrayList<>();
                ArrayList<Integer> clIds=new ArrayList<>();
                resultats.result().getResults().forEach(objects -> {
                    int id=Integer.valueOf(objects.getString(0).trim());
                    clNames.add(id+" "+objects.getString(1).trim());
                    clIds.add(id);
                });
                outputData.setClientsNames(clNames);
                outputData.setClientsIds(clIds);


                outputData.getClientsNames().forEach(s -> {
                    List<String> init=new ArrayList<>();

                    for (int i = 0; i < 17; i++) {
                        init.add(" ");
                    }

                    outputData.addReportDetails(s,init);

                });



                //OLD: String quer2=String.format(GET_FIN_CLAS,healthCareInputData.getOrganizationId(),DateUtils.getSqlDate(healthCareInputData.getEndDateRange()));
                String quer2=String.format(GET_FIN_CLAS,healthCareInputData.getOrganizationId(),DateUtils.getSqlDate(DateUtils.addMonthsToDate(healthCareInputData.getEndDateRange(),1)));
                System.out.println("REQ FCLS: "+quer2);
                dbConnexionHelper.getFmaConnection().query(quer2,res->{
                    if(res.succeeded()){
                        ArrayList<String> finName=new ArrayList<>();
                        res.result().getResults().forEach(objects -> {
                            try {
                                String cc=objects.getString(0).trim();
                                //if(!cc.isEmpty())
                                    finName.add(cc);
                            }catch (Exception ignored){

                            }
                        });
                        outputData.setFiniancialClass(finName);
                         System.out.println("OUT1: "+Json.encodePrettily(outputData));
                        out.complete(outputData);

                    }else{
                        res.cause().printStackTrace();
                        out.fail(res.cause());

                    }
                });

            }else{
                resultats.cause().printStackTrace();
                out.fail(resultats.cause());
                Logger.getInstance("").log(LoggerTag.ERROR,ExceptionUtils.exceptionStackTraceAsString(resultats.cause()),"");
            }
        });


    }


    private void getFinancialClassDatas1(HealthCareInputData healthCareInputData, HealthOutputData outputData,Future<HealthOutputData> out){

        AtomicInteger i= new AtomicInteger();
        HashMap<String, ArrayList<String>> reqs= getFinClasOcc(outputData.getFiniancialClass(),healthCareInputData.getOrganizationId(), healthCareInputData.getEndDateRange());
        int tot= reqs.values().stream().mapToInt(ArrayList::size).sum();

        if(reqs.isEmpty() && !out.isComplete()) {
            System.out.println("NO FINANCIAL CLASS FOUND FOR THIS PERIOD");
            out.fail("NO FINANCIAL CLASS FOUND FOR THIS PERIOD");
        }

        reqs.keySet().forEach(s -> {

            ArrayList<String> fcd=new ArrayList<>();
            reqs.get(s).forEach(query -> {

                dbConnexionHelper.getFmaConnection().query(query,hand->{

                    i.getAndIncrement();

                    if(hand.succeeded()){
                        int dt=hand.result().getRows().get(0).getInteger("");
                        fcd.add(""+dt);
                        outputData.addFinClassDatas1(s,fcd);


                        if (i.get()==tot) {
                             System.out.println("OUT2: " + Json.encodePrettily(outputData));
                            out.complete(outputData);
                        }

                    }else{
                        hand.cause().printStackTrace();
                        if(!out.isComplete())
                            out.fail(hand.cause());

                    }

                });



            });


        });

    }

    private void getFinancialClassDatas2( HealthCareInputData healthCareInputData, HealthOutputData outputData, Future<HealthOutputData> out){
        HashMap<String, ArrayList<String>> reqs2=getFinClasAmt(outputData.getFiniancialClass(),healthCareInputData.getOrganizationId(),healthCareInputData.getEndDateRange());
        AtomicInteger j= new AtomicInteger();
        int tot2= reqs2.values().stream().mapToInt(ArrayList::size).sum();

        reqs2.keySet().forEach(s1 -> {

            ArrayList<String> fcd2=new ArrayList<>();

            reqs2.get(s1).forEach(query2 -> {

                dbConnexionHelper.getFmaConnection().query(query2,hand2-> {

                    j.getAndIncrement();

                    if (hand2.succeeded()){

                        String dt = ""+(hand2.result().getRows().get(0).getDouble("")==null?0.0:hand2.result().getRows().get(0).getDouble(""));
                        fcd2.add(dt);
                        outputData.addFinClassDatas2(s1, fcd2);

                        if (j.get() == tot2) {
                             System.out.println("OUT3: " + Json.encodePrettily(outputData));

                            if (!out.isComplete())
                                out.complete(outputData);
                        }

                    }

                    else{
                        hand2.cause().printStackTrace();
                        if(!out.isComplete())
                            out.fail(hand2.cause());
                    }

                });

            });


        });
    }


    private void getFinancialClassDatas3(HealthCareInputData healthCareInputData,HealthOutputData outputData,Future<HealthOutputData> out){

        HashMap<String,String> queries=getFinClasAge(outputData.getFiniancialClass(),healthCareInputData.getOrganizationId(),healthCareInputData.getEndDateRange());

        AtomicInteger j= new AtomicInteger();
        int total= queries.values().size();

        queries.keySet().forEach(s1 -> {

            ArrayList<String[]> fcd3=new ArrayList<>();

            dbConnexionHelper.getFmaConnection().query(queries.get(s1),hand2-> {

                j.getAndIncrement();

                if (hand2.succeeded()){

                    List<JsonArray> resObj=hand2.result().getResults();
                    System.out.println("EEEE: "+resObj);

                    if(resObj.size()!=0){

                        resObj.forEach(entries -> {

                            try {

                                String nn=Month.of(entries.getInteger(1)).getDisplayName(TextStyle.FULL_STANDALONE, Locale.US);
                                nn=(nn.length()>=3)?nn.substring(0,3):nn;

                                fcd3.add(new String[]{nn+" - "+entries.getInteger(0), ""+entries.getInteger(2)});

                            }catch (Exception ex){
                                ex.printStackTrace();
                            }
                        });

                    }


                    outputData.addFinClassDatas3(s1, fcd3);

                    if (j.get() == total) {
                        System.out.println("OUT3: " + Json.encodePrettily(outputData));
                        if (!out.isComplete())
                            out.complete(outputData);
                    }

                }

                else{
                    hand2.cause().printStackTrace();
                    if(!out.isComplete())
                        out.fail(hand2.cause());

                }

            });


        });
    }








    private void getAgingDatas1(HealthCareInputData healthCareInputData, HealthOutputData outputData,Future<HealthOutputData> out){

        AtomicInteger i= new AtomicInteger();
        String[] reqs= get180Age(healthCareInputData.getOrganizationId(),healthCareInputData.getEndDateRange());

        for (String ss : reqs) {
            dbConnexionHelper.getFmaConnection().query(ss,hand->{

                i.getAndIncrement();

                if(hand.succeeded()){
                    //System.out.println("RESS: " + Json.encodePrettily(hand.result().getRows()));

                    List<JsonObject> res=hand.result().getRows();
                    res.forEach(entries -> {
                        outputData.addAggingDatas1(new String[]{""+entries.getInteger("one"),""+entries.getDouble("two"),""+entries.getInteger("")});
                    });


                    if (i.get()==3) {
                          System.out.println("OUT4: " + Json.encodePrettily(outputData));
                        out.complete(outputData);
                    }

                }else{
                    hand.cause().printStackTrace();
                    if(!out.isComplete())
                        out.fail(hand.cause());

                }

            });

        }


    }



    private void getAgingDatas2(HealthCareInputData healthCareInputData, HealthOutputData outputData,Future<HealthOutputData> out){

        String quer1=String.format(GET_ACTIVE_CODES,""+healthCareInputData.getOrganizationId());
        System.out.println(quer1);

        dbConnexionHelper.getFmaConnection().query(quer1,resAct-> {
           if(resAct.succeeded()){

               List<JsonObject> acts=resAct.result().getRows();
               //System.out.println("FINFIN: "+Json.encodePrettily(resAct.result().getRows()));

               acts.forEach(entries -> {
                   outputData.addActivCode(""+entries.getInteger("activity_code"));
                   outputData.addActivDesc(""+entries.getString("description"));
               });


               HashMap<String, List<String>> reqs=getActiveAct(outputData.getActivCodeList(),healthCareInputData.getOrganizationId(),healthCareInputData.getEndDateRange());

               //System.out.println("FINFIN2: "+Json.encodePrettily(reqs));

               AtomicInteger i= new AtomicInteger();

               int tot2= reqs.values().stream().mapToInt(List::size).sum();

               reqs.keySet().forEach(s1 -> {

                   ArrayList<String> fcd=new ArrayList<>();
                   reqs.get(s1).forEach(query -> {

                       dbConnexionHelper.getFmaConnection().query(query,hand->{
                           i.getAndIncrement();

                           if(hand.succeeded()){
                                System.out.println("ICI: "+Json.encodePrettily(hand.result().getRows()));

                               double dt=(hand.result().getRows().get(0).getValue("",0.0)==null?0:hand.result().getRows().get(0).getDouble("",0.0));
                               fcd.add(""+dt);
                               outputData.addAgingDatas2(outputData.getActivDescList().get(outputData.getActivCodeList().indexOf(s1)),fcd);

                               if (i.get() == tot2) {
                                    System.out.println("OUT5: " + Json.encodePrettily(outputData));
                                   if (!out.isComplete())
                                       out.complete(outputData);
                               }

                           }else{
                               System.out.println(hand.cause().getMessage()+"\n "+ExceptionUtils.exceptionStackTraceAsString(hand.cause()));
                               if(!out.isComplete())
                                   out.fail(hand.cause());
                           }


                       });

                   });

               });





           }else{
               resAct.cause().printStackTrace();
               if(!out.isComplete())
                   out.fail(resAct.cause());
           }
        });




    }


    private void getOverallDatas1(HealthCareInputData healthCareInputData, HealthOutputData outputData,Future<HealthOutputData> out){


        String quer2=String.format(GET_FIN_CLAS,healthCareInputData.getOrganizationId(), DateUtils.getSqlDate(DateUtils.addDaysToDate(healthCareInputData.getEndDateRange(),1)));

        dbConnexionHelper.getFmaConnection().query(quer2,res->{
            if(res.succeeded()){
                ArrayList<String> finName=new ArrayList<>();
                res.result().getResults().forEach(objects -> {
                    try {
                        String cc=objects.getString(0).trim();
                        //if(!cc.isEmpty())
                            finName.add(cc);
                    }catch (Exception ignored){

                    }
                });

                outputData.setFiniancialClassOverall(finName);
                 System.out.println("OUT6: "+Json.encodePrettily(outputData));

                //This function will get OverresPlacements and complete the process...
                getOverresPlacements(outputData,healthCareInputData).setHandler(end->{
                    if(end.succeeded()){
                        getOverresPlacements2(end.result(),healthCareInputData,out);

                    }else{
                        res.cause().printStackTrace();
                        out.fail(end.cause());
                    }
                });

            }else{
                res.cause().printStackTrace();
                out.fail(res.cause());

            }
        });

    }






    private Future<HealthOutputData>  getOverresPlacements(HealthOutputData outputData,HealthCareInputData healthCareInputData){

        Future<HealthOutputData> out =Future.future();
        HashMap<String,String> queries=getOverResPlacements(outputData.getFiniancialClassOverall(),healthCareInputData.getOrganizationId(),healthCareInputData.getBeginDateRange(),healthCareInputData.getEndDateRange());

        //System.out.println(Json.encodePrettily(queries));

        AtomicInteger j= new AtomicInteger();
        int total= queries.values().size();

        queries.keySet().forEach(s1 -> {


            dbConnexionHelper.getFmaConnection().query(queries.get(s1),hand2-> {

                j.getAndIncrement();
                if (hand2.succeeded()){
                    List<String> fcd3=new ArrayList<>();

                    JsonArray resObj=hand2.result().getResults().get(0);
                     System.out.println("RESSSSS:"+Json.encodePrettily(resObj));

                    if(resObj.size()!=0){


                        try {
                            fcd3=Json.decodeValue(resObj.toString(),new TypeReference<List<String>>(){});

                        }catch (Exception ignored){}




                    }

                    outputData.addPlacementsAndChanges(s1, fcd3);

                    if (j.get() == total) {
                        System.out.println("OUT7: " + Json.encodePrettily(outputData));
                        if (!out.isComplete())
                            out.complete(outputData);
                    }

                }

                else{
                    hand2.cause().printStackTrace();
                    if(!out.isComplete())
                        out.fail(hand2.cause());

                }

            });


        });

        return out;

    }




    private void getOverresPlacements2(HealthOutputData outputData,HealthCareInputData healthCareInputData,Future<HealthOutputData> out){

        HashMap<String,String> queries=getOverResCollections(outputData.getFiniancialClassOverall(),healthCareInputData.getOrganizationId(),healthCareInputData.getBeginDateRange(),healthCareInputData.getEndDateRange());

        System.out.println(Json.encodePrettily(queries));

        AtomicInteger j= new AtomicInteger();
        int total= queries.values().size();

        queries.keySet().forEach(s1 -> {

            dbConnexionHelper.getFmaConnection().query(queries.get(s1),hand2-> {

                j.getAndIncrement();
                if (hand2.succeeded()){
                    List<String> fcd3=new ArrayList<>();
                     System.out.println("AAAESSS:"+Json.encodePrettily(hand2.result().getResults()));



                            hand2.result().getResults().forEach(objects -> {
                                objects.forEach(o -> {
                                    fcd3.add(o.toString());
                                });
                            });


                    outputData.addCollectionsAndChanges(s1, fcd3);

                    if (j.get() == total) {
                        System.out.println("OUT8: " + Json.encodePrettily(outputData));
                        if (!out.isComplete())
                            out.complete(outputData);
                    }

                }

                else{
                    hand2.cause().printStackTrace();
                    if(!out.isComplete())
                        out.fail(hand2.cause());

                }

            });


        });



    }


    public void getCmhealthcare(String sessionId,  String[] comments, Future<InternalResponse>  future){
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: getHealth care comented data ~~~~~~~~~~~~~~~~~~~~~~","");
        System.out.println("SESS: "+ Json.encodePrettily(TrustBalancingService.getActiveSessions()));
        System.out.println("Id: "+ sessionId);

        if(!TrustBalancingService.getActiveSessions().containsKey(sessionId)){
            InternalResponse resp=new InternalResponse();
            resp.setCause(-1);
            if(!future.isComplete())
                future.complete(resp);
        }else{
            InternalResponse resp=new InternalResponse();
            resp.setCause(1);
            resp.setData(new HealthExcelAndPPTFileHelper().getBase64CommentedPPT(sessionId, comments));
            if(!future.isComplete())
                future.complete(resp);
        }
    }


    public void getOrganizationList(String sessionId,Handler<List<Organization>> handler) {

        if (!TrustBalancingService.getActiveSessions().containsKey(sessionId)){
            handler.handle(null);
        } else {

            List<Organization> organizationsList = new ArrayList<>();
            Logger.getInstance("").log(LoggerTag.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~START: getHealthOrganizationList ~~~~~~~~~~~~~~~~~~~~~~", "");
            try {
                if (dbConnexionHelper.getFmaConnection() == null) {
                    TrustBalancingService.connectToDb(DBType.Infinity2, event -> {

                        if (event.succeeded()) {
                            dbConnexionHelper.setFmaConnection(event.result());
                            dbConnexionHelper.getFmaConnection().query(GET_ORGANIZATIONS, result -> {
                                //dbConnexionHelper.getFmaConnection().query(ORGANIZATIONS_QUERY, result -> {
                                if (result.succeeded()) {
                                    System.out.println("Result "+result.result().getRows().toString());

                                    organizationsListGlobal=DbConnexionHelper.returnQueryList(result.result().getRows(),Organization.class);
                                    handler.handle(organizationsListGlobal);
                                } else {
                                    Logger.getInstance("").log(LoggerTag.ERROR, "Error getting organization list: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
                                    organizationsList.add(null);
                                    handler.handle(organizationsList);
                                }

                            });

                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, "Error logging to Infinity: " + ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                            handler.handle(null);
                        }

                    },dbConnexionHelper);
                } else {
                    dbConnexionHelper.getFmaConnection().query(GET_ORGANIZATIONS, result -> {
                        //dbConnexionHelper.getFmaConnection().query(ORGANIZATIONS_QUERY, result -> {
                        if (result.succeeded()) {
                            organizationsListGlobal=DbConnexionHelper.returnQueryList(result.result().getRows(),Organization.class);
                            handler.handle(organizationsListGlobal);
                        } else {
                            System.out.println("Result NOT succeded  "+result.cause().getMessage());
                            Logger.getInstance("").log(LoggerTag.ERROR, "Error getting organization list: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
                            organizationsList.add(null);
                        }

                    });

                }
            } catch (SQLException | ClassNotFoundException e) {
                Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e), "");
                handler.handle(null);
            }
        }
    }

    public DbConnexionHelper getDbConnexionHelper() {
        return dbConnexionHelper;
    }

    private void setDbConnexionHelper(DbConnexionHelper db) {
        dbConnexionHelper = db;
    }

}
