package com.fma.wb.integration.reportsmodule.verticles;

import io.vertx.core.*;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.common.template.TemplateEngine;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.VirtualHostHandler;
import io.vertx.ext.web.templ.handlebars.HandlebarsTemplateEngine;

import java.util.logging.Level;
import java.util.logging.Logger;


public class UIVerticle extends AbstractVerticle {

  private Context context;
  private static Vertx sVertx;
  private TemplateEngine engine;

  @Override
  public void init(Vertx vertx, Context context) {
    super.init(vertx, context);
    this.context = context;
    sVertx = vertx;
  }

  @Override
  public void start(Future<Void> startFuture) {
    Router router = Router.router(vertx);
    HttpServerOptions options = new HttpServerOptions();//.setLogActivity(true);

    vertx.createHttpServer(options)
      .requestHandler(router::accept)
      .listen(
        config().getInteger("http.port", 80),
        result -> {
          if (result.succeeded()) {
            startFuture.complete();
          } else {
            startFuture.fail(result.cause());
          }
        });




    engine = HandlebarsTemplateEngine.create(vertx).setMaxCacheSize(100);
    router.route("/assets/*").handler(StaticHandler.create("assets").setCacheEntryTimeout(1000).setMaxAgeSeconds(0));
    //router.route().handler(BodyHandler.create());

//-------------------------------------------------------------------------------------------------------



    router.get("/*").handler(ctx -> {
      // we define a hardcoded title for our application
      JsonObject data = new JsonObject()
        .put("title", "FMA ALLIANCE - BACKOFFICE AND REPORTS APP");
      // and now delegate to the engine to render it.
      engine.render(data, "templates/index.hbs", res -> {
        if (res.succeeded()) {
          ctx.response()
            .putHeader("content-type", "text/html; charset=utf-8")
            .end(res.result().toString());
        } else {
          ctx.response().end("ERROR");
        }
      });
    });





  }



}
