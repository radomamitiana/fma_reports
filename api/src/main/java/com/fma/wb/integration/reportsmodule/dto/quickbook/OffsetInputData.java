package com.fma.wb.integration.reportsmodule.dto.quickbook;

public class OffsetInputData {

    private int offset_id;
    private int org_id;
    private int client_id;
    private String offset_date;
    private Double wire_offset_amt;
    private Double ar_offset_amt;
    private String created_by;
    private String created_on;
    private String changed_by;
    private String changed_on;
    private int cancel_status;

    public int getOffset_id() {
        return offset_id;
    }

    public void setOffset_id(int offset_id) {
        this.offset_id = offset_id;
    }

    public int getOrg_id() {
        return org_id;
    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public String getOffset_date() {
        return offset_date;
    }

    public void setOffset_date(String offset_date) {
        this.offset_date = offset_date;
    }

    public Double getWire_offset_amt() {
        return wire_offset_amt;
    }

    public void setWire_offset_amt(Double wire_offset_amt) {
        this.wire_offset_amt = wire_offset_amt;
    }

    public Double getAr_offset_amt() {
        return ar_offset_amt;
    }

    public void setAr_offset_amt(Double ar_offset_amt) {
        this.ar_offset_amt = ar_offset_amt;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getChanged_by() {
        return changed_by;
    }

    public void setChanged_by(String changed_by) {
        this.changed_by = changed_by;
    }

    public String getChanged_on() {
        return changed_on;
    }

    public void setChanged_on(String changed_on) {
        this.changed_on = changed_on;
    }

    public int getCancel_status() {
        return cancel_status;
    }

    public void setCancel_status(int cancel_status) {
        this.cancel_status = cancel_status;
    }
}
