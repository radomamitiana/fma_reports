package com.fma.wb.integration.reportsmodule.dto.quickbook;

public class RequestCollectionProcesing {

    private int organizationId;
    private int clientId;

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }
}
