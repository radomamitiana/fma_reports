package com.fma.wb.integration.i3module.dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author Tiana Andriamanalina
 * @version 1.0
 * InteractionCustomAttributes class is used for the mapping
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class InteractionSummary implements Serializable {
    @JsonProperty("InteractionIDKey")
    private String InteractionIDKey;
    @JsonProperty("SiteID")
    private int SiteID;
    @JsonProperty("SeqNo")
    private int SeqNo;
    @JsonProperty("InteractionID")
    private int InteractionID;
    @JsonProperty("StartDateTimeUTC")
    private String StartDateTimeUTC;
    @JsonProperty("StartDTOffset")
    private int StartDTOffset;
    @JsonProperty("Direction")
    private int Direction;
    @JsonProperty("ConnectionType")
    private int ConnectionType;
    @JsonProperty("MediaType")
    private int MediaType;
    @JsonProperty("RemoteID")
    private String RemoteID;
    @JsonProperty("DNIS_LocalID")
    private String DNIS_LocalID;
    private int tDialing;
    private int tIVRWait;
    private int tQueueWait;
    private int tAlert;
    private int tConnected;
    private int tHeld;
    private int tSuspend;
    private int tConference;
    private int tExternal;
    private int tACW;
    private int nIVR;
    private int nQueueWait;
    private int nTalk;
    private int nConference;
    private int nHeld;
    private int nTransfer;
    private int nExternal;
    @JsonProperty("Disposition")
    private int Disposition;
    @JsonProperty("DispositionCode")
    private int DispositionCode;
    @JsonProperty("WrapUpCode")
    private String WrapUpCode;
    @JsonProperty("AccountCode")
    private String AccountCode;
    @JsonProperty("IsRecorded")
    private boolean IsRecorded;
    @JsonProperty("IsSurveyed")
    private boolean IsSurveyed;
    @JsonProperty("MediaServerID")
    private String MediaServerID;
    @JsonProperty("IndivID")
    private String IndivID;
    @JsonProperty("OrgID")
    private String OrgID;
    @JsonProperty("LineId")
    private String LineId;
    @JsonProperty("LastStationId")
    private String LastStationId;
    @JsonProperty("LastLocalUserId")
    private String LastLocalUserId;
    @JsonProperty("LastAssignedWorkgroupID")
    private String LastAssignedWorkGroupID;
    @JsonProperty("LastLocalNumber")
    private String LastLocalNumber;
    @JsonProperty("LastLocalName")
    private String LastLocalName;
    @JsonProperty("RemoteICUserID")
    private String RemoteICUserID;
    @JsonProperty("RemoteNumberCountry")
    private int RemoteNumberCountry;
    @JsonProperty("RemoteNumberLoComp1")
    private String RemoteNumberLoComp1;
    @JsonProperty("RemoteNumberLoComp2")
    private String RemoteNumberLoComp2;
    @JsonProperty("RemoteNumberFmt")
    private String RemoteNumberFmt;
    @JsonProperty("RemoteNumberCallId")
    private String RemoteNumberCallId;
    @JsonProperty("RemoteName")
    private String RemoteName;
    @JsonProperty("InitiatedDateTimeUTC")
    private String InitiatedDateTimeUTC;
    @JsonProperty("ConnectedDateTimeUTC")
    private String ConnectedDateTimeUTC;
    @JsonProperty("TerminatedDateTimeUTC")
    private String TerminatedDateTimeUTC;
    @JsonProperty("LineDuration")
    private int LineDuration;
    @JsonProperty("CallEventLog")
    private String CallEventLog;
    @JsonProperty("PurposeCode")
    private int PurposeCode;
    @JsonProperty("CallNote")
    private String CallNote;
    private int nSecuredIVR;
    private int tSecuredIVR;
    @JsonProperty("FirstAssignedAcdSkillSet")
    private String FirstAssignedAcdSkillSet;

    public String getInteractionIDKey() {
        return InteractionIDKey;
    }

    public void setInteractionIDKey(String interactionIDKey) {
        InteractionIDKey = interactionIDKey;
    }

    public int getSiteID() {
        return SiteID;
    }

    public void setSiteID(int siteID) {
        SiteID = siteID;
    }

    public int getSeqNo() {
        return SeqNo;
    }

    public void setSeqNo(int seqNo) {
        SeqNo = seqNo;
    }

    public int getInteractionID() {
        return InteractionID;
    }

    public void setInteractionID(int interactionID) {
        InteractionID = interactionID;
    }

    public String getStartDateTimeUTC() {
        return StartDateTimeUTC;
    }

    public void setStartDateTimeUTC(String StartDateTimeUTC) {
        this.StartDateTimeUTC = StartDateTimeUTC;
    }

    public int getStartDTOffset() {
        return StartDTOffset;
    }

    public void setStartDTOffset(int startDTOffset) {
        StartDTOffset = startDTOffset;
    }

    public int getDirection() {
        return Direction;
    }

    public void setDirection(int direction) {
        Direction = direction;
    }

    public int getConnectionType() {
        return ConnectionType;
    }

    public void setConnectionType(int connectionType) {
        ConnectionType = connectionType;
    }

    public int getMediaType() {
        return MediaType;
    }

    public void setMediaType(int mediaType) {
        MediaType = mediaType;
    }

    public String getRemoteID() {
        return RemoteID;
    }

    public void setRemoteID(String remoteID) {
        RemoteID = remoteID;
    }

    public String getDNIS_LocalID() {
        return DNIS_LocalID;
    }

    public void setDNIS_LocalID(String DNIS_LocalID) {
        this.DNIS_LocalID = DNIS_LocalID;
    }

    public int gettDialing() {
        return tDialing;
    }

    public void settDialing(int tDialing) {
        this.tDialing = tDialing;
    }

    public int gettIVRWait() {
        return tIVRWait;
    }

    public void settIVRWait(int tIVRWait) {
        this.tIVRWait = tIVRWait;
    }

    public int gettQueueWait() {
        return tQueueWait;
    }

    public void settQueueWait(int tQueueWait) {
        this.tQueueWait = tQueueWait;
    }

    public int gettAlert() {
        return tAlert;
    }

    public void settAlert(int tAlert) {
        this.tAlert = tAlert;
    }

    public int gettConnected() {
        return tConnected;
    }

    public void settConnected(int tConnected) {
        this.tConnected = tConnected;
    }

    public int gettHeld() {
        return tHeld;
    }

    public void settHeld(int tHeld) {
        this.tHeld = tHeld;
    }

    public int gettSuspend() {
        return tSuspend;
    }

    public void settSuspend(int tSuspend) {
        this.tSuspend = tSuspend;
    }

    public int gettConference() {
        return tConference;
    }

    public void settConference(int tConference) {
        this.tConference = tConference;
    }

    public int gettExternal() {
        return tExternal;
    }

    public void settExternal(int tExternal) {
        this.tExternal = tExternal;
    }

    public int gettACW() {
        return tACW;
    }

    public void settACW(int tACW) {
        this.tACW = tACW;
    }

    public int getnIVR() {
        return nIVR;
    }

    public void setnIVR(int nIVR) {
        this.nIVR = nIVR;
    }

    public int getnQueueWait() {
        return nQueueWait;
    }

    public void setnQueueWait(int nQueueWait) {
        this.nQueueWait = nQueueWait;
    }

    public int getnTalk() {
        return nTalk;
    }

    public void setnTalk(int nTalk) {
        this.nTalk = nTalk;
    }

    public int getnConference() {
        return nConference;
    }

    public void setnConference(int nConference) {
        this.nConference = nConference;
    }

    public int getnHeld() {
        return nHeld;
    }

    public void setnHeld(int nHeld) {
        this.nHeld = nHeld;
    }

    public int getnTransfer() {
        return nTransfer;
    }

    public void setnTransfer(int nTransfer) {
        this.nTransfer = nTransfer;
    }

    public int getnExternal() {
        return nExternal;
    }

    public void setnExternal(int nExternal) {
        this.nExternal = nExternal;
    }

    public int getDisposition() {
        return Disposition;
    }

    public void setDisposition(int disposition) {
        Disposition = disposition;
    }

    public int getDispositionCode() {
        return DispositionCode;
    }

    public void setDispositionCode(int dispositionCode) {
        DispositionCode = dispositionCode;
    }

    public String getWrapUpCode() {
        return WrapUpCode;
    }

    public void setWrapUpCode(String wrapUpCode) {
        WrapUpCode = wrapUpCode;
    }

    public String getAccountCode() {
        return AccountCode;
    }

    public void setAccountCode(String accountCode) {
        AccountCode = accountCode;
    }

    public boolean isRecorded() {
        return IsRecorded;
    }

    public void setRecorded(boolean recorded) {
        IsRecorded = recorded;
    }

    public boolean isSurveyed() {
        return IsSurveyed;
    }

    public void setSurveyed(boolean surveyed) {
        IsSurveyed = surveyed;
    }

    public String getMediaServerID() {
        return MediaServerID;
    }

    public void setMediaServerID(String mediaServerID) {
        MediaServerID = mediaServerID;
    }

    public String getIndivID() {
        return IndivID;
    }

    public void setIndivID(String indivID) {
        IndivID = indivID;
    }

    public String getOrgID() {
        return OrgID;
    }

    public void setOrgID(String orgID) {
        OrgID = orgID;
    }

    public String getLineId() {
        return LineId;
    }

    public void setLineId(String lineId) {
        LineId = lineId;
    }

    public String getLastStationId() {
        return LastStationId;
    }

    public void setLastStationId(String lastStationId) {
        LastStationId = lastStationId;
    }

    public String getLastLocalUserId() {
        return LastLocalUserId;
    }

    public void setLastLocalUserId(String lastLocalUserId) {
        LastLocalUserId = lastLocalUserId;
    }

    public String getLastAssignedWorkGroupID() {
        return LastAssignedWorkGroupID;
    }

    public void setLastAssignedWorkGroupID(String lastAssignedWorkGroupID) {
        this.LastAssignedWorkGroupID = lastAssignedWorkGroupID;
    }

    public String getLastLocalNumber() {
        return LastLocalNumber;
    }

    public void setLastLocalNumber(String lastLocalNumber) {
        LastLocalNumber = lastLocalNumber;
    }

    public String getLastLocalName() {
        return LastLocalName;
    }

    public void setLastLocalName(String lastLocalName) {
        LastLocalName = lastLocalName;
    }

    public String getRemoteICUserID() {
        return RemoteICUserID;
    }

    public void setRemoteICUserID(String remoteICUserID) {
        RemoteICUserID = remoteICUserID;
    }

    public int getRemoteNumberCountry() {
        return RemoteNumberCountry;
    }

    public void setRemoteNumberCountry(int remoteNumberCountry) {
        RemoteNumberCountry = remoteNumberCountry;
    }

    public String getRemoteNumberLoComp1() {
        return RemoteNumberLoComp1;
    }

    public void setRemoteNumberLoComp1(String remoteNumberLoComp1) {
        RemoteNumberLoComp1 = remoteNumberLoComp1;
    }

    public String getRemoteNumberLoComp2() {
        return RemoteNumberLoComp2;
    }

    public void setRemoteNumberLoComp2(String remoteNumberLoComp2) {
        RemoteNumberLoComp2 = remoteNumberLoComp2;
    }

    public String getRemoteNumberFmt() {
        return RemoteNumberFmt;
    }

    public void setRemoteNumberFmt(String remoteNumberFmt) {
        RemoteNumberFmt = remoteNumberFmt;
    }

    public String getRemoteNumberCallId() {
        return RemoteNumberCallId;
    }

    public void setRemoteNumberCallId(String remoteNumberCallId) {
        RemoteNumberCallId = remoteNumberCallId;
    }

    public String getRemoteName() {
        return RemoteName;
    }

    public void setRemoteName(String remoteName) {
        RemoteName = remoteName;
    }

    public String getInitiatedDateTimeUTC() {
        return InitiatedDateTimeUTC;
    }

    public void setInitiatedDateTimeUTC(String initiatedDateTimeUTC) {
        InitiatedDateTimeUTC = initiatedDateTimeUTC;
    }

    public String getConnectedDateTimeUTC() {
        return ConnectedDateTimeUTC;
    }

    public void setConnectedDateTimeUTC(String connectedDateTimeUTC) {
        ConnectedDateTimeUTC = connectedDateTimeUTC;
    }

    public String getTerminatedDateTimeUTC() {
        return TerminatedDateTimeUTC;
    }

    public void setTerminatedDateTimeUTC(String terminatedDateTimeUTC) {
        TerminatedDateTimeUTC = terminatedDateTimeUTC;
    }

    public int getLineDuration() {
        return LineDuration;
    }

    public void setLineDuration(int lineDuration) {
        LineDuration = lineDuration;
    }

    public String getCallEventLog() {
        return CallEventLog;
    }

    public void setCallEventLog(String callEventLog) {
        CallEventLog = callEventLog;
    }

    public int getPurposeCode() {
        return PurposeCode;
    }

    public void setPurposeCode(int purposeCode) {
        PurposeCode = purposeCode;
    }

    public String getCallNote() {
        return CallNote;
    }

    public void setCallNote(String callNote) {
        CallNote = callNote;
    }

    public int getnSecuredIVR() {
        return nSecuredIVR;
    }

    public void setnSecuredIVR(int nSecuredIVR) {
        this.nSecuredIVR = nSecuredIVR;
    }

    public int gettSecuredIVR() {
        return tSecuredIVR;
    }

    public void settSecuredIVR(int tSecuredIVR) {
        this.tSecuredIVR = tSecuredIVR;
    }

    public String getFirstAssignedAcdSkillSet() {
        return FirstAssignedAcdSkillSet;
    }

    public void setFirstAssignedAcdSkillSet(String firstAssignedAcdSkillSet) {
        FirstAssignedAcdSkillSet = firstAssignedAcdSkillSet;
    }

    public InteractionSummary() {
    }

    @Override
    public String toString() {
        return "InteractionSummary{" +
                "InteractionIDKey='" + InteractionIDKey + '\'' +
                ", SiteID=" + SiteID +
                ", SeqNo=" + SeqNo +
                ", InteractionID=" + InteractionID +
                ", StartDateTimeUTC=" + StartDateTimeUTC +
                ", StartDTOffset=" + StartDTOffset +
                ", Direction=" + Direction +
                ", ConnectionType=" + ConnectionType +
                ", MediaType=" + MediaType +
                ", RemoteID='" + RemoteID + '\'' +
                ", DNIS_LocalID='" + DNIS_LocalID + '\'' +
                ", tDialing=" + tDialing +
                ", tIVRWait=" + tIVRWait +
                ", tQueueWait=" + tQueueWait +
                ", tAlert=" + tAlert +
                ", tConnected=" + tConnected +
                ", tHeld=" + tHeld +
                ", tSuspend=" + tSuspend +
                ", tConference=" + tConference +
                ", tExternal=" + tExternal +
                ", tACW=" + tACW +
                ", nIVR=" + nIVR +
                ", nQueueWait=" + nQueueWait +
                ", nTalk=" + nTalk +
                ", nConference=" + nConference +
                ", nHeld=" + nHeld +
                ", nTransfer=" + nTransfer +
                ", nExternal=" + nExternal +
                ", Disposition=" + Disposition +
                ", DispositionCode=" + DispositionCode +
                ", WrapUpCode='" + WrapUpCode + '\'' +
                ", AccountCode='" + AccountCode + '\'' +
                ", IsRecorded=" + IsRecorded +
                ", IsSurveyed=" + IsSurveyed +
                ", MediaServerID='" + MediaServerID + '\'' +
                ", IndivID='" + IndivID + '\'' +
                ", OrgID='" + OrgID + '\'' +
                ", LineId='" + LineId + '\'' +
                ", LastStationId='" + LastStationId + '\'' +
                ", LastLocalUserId='" + LastLocalUserId + '\'' +
                ", lastAssignedWorkGroupID='" + LastAssignedWorkGroupID + '\'' +
                ", LastLocalNumber='" + LastLocalNumber + '\'' +
                ", LastLocalName='" + LastLocalName + '\'' +
                ", RemoteICUserID='" + RemoteICUserID + '\'' +
                ", RemoteNumberCountry=" + RemoteNumberCountry +
                ", RemoteNumberLoComp1='" + RemoteNumberLoComp1 + '\'' +
                ", RemoteNumberLoComp2='" + RemoteNumberLoComp2 + '\'' +
                ", RemoteNumberFmt='" + RemoteNumberFmt + '\'' +
                ", RemoteNumberCallId='" + RemoteNumberCallId + '\'' +
                ", RemoteName='" + RemoteName + '\'' +
                ", InitiatedDateTimeUTC=" + InitiatedDateTimeUTC +
                ", ConnectedDateTimeUTC=" + ConnectedDateTimeUTC +
                ", TerminatedDateTimeUTC=" + TerminatedDateTimeUTC +
                ", LineDuration=" + LineDuration +
                ", CallEventLog='" + CallEventLog + '\'' +
                ", PurposeCode=" + PurposeCode +
                ", CallNote='" + CallNote + '\'' +
                ", nSecuredIVR=" + nSecuredIVR +
                ", tSecuredIVR=" + tSecuredIVR +
                ", FirstAssignedAcdSkillSet='" + FirstAssignedAcdSkillSet + '\'' +
                '}';
    }
}