package com.fma.wb.integration.common.dao;

import java.sql.SQLException;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;


/**
 * For handling all db connections and inserts
 */
public class DBConnector {

    private static JDBCClient jdbcClient;

    public DBConnector() {
    }

    //This is is the method used to connect to the database
    public  static JDBCClient connect(String host,String db,String user,String psswd,Vertx vertx) throws ClassNotFoundException, SQLException {
        // the sql server driver string
        String driverClass="com.microsoft.sqlserver.jdbc.SQLServerDriver";
        Class.forName(driverClass);
        // the sql server url
        String url = "jdbc:sqlserver://"+host+";databaseName="+db+";user="+user+";password="+psswd;
        //String url = "jdbc:sqlserver://"+host+";databaseName="+db+";integratedSecurity=true";

        System.out.println("URL: [ "+url+" ]");
        //castUUID: allows to convert directly the UUID to the right format
        //max_pool_size: nombre de connection simultanees.
        JsonObject config = new JsonObject()
                .put("url", url)
                .put("driver_class", driverClass)
/*
                .put("max_idle_time",0)
*/
                .put("max_pool_size", 500);


        return JDBCClient.createNonShared(vertx,config);

    }




}