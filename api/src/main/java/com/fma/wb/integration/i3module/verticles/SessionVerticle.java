package com.fma.wb.integration.i3module.verticles;

import com.fma.wb.integration.i3module.services.I3WService;
import com.fma.wb.integration.i3module.dto.CompositeType;
import com.fma.wb.integration.common.dto.api.ApiResponse;
import com.fma.wb.integration.common.dto.enums.WsResponseType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import common.Logger;
import common.LoggerTag;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import static com.fma.wb.integration.common.utils.UrlConfig.*;
import static utils.Configuration.RESPONSE_STATUS_CODE_OK;
import static utils.Configuration.STATUS_CODE_500;

/**
 * A verticle is a component of the application. We can deploy verticles to run the components.
 * The start method will be called when verticle is deployed. And notice this start method takes a parameter typed Future<Void>,
 * which means this is asynchronous start method. The Future indicates whether your actions have been done.
 * After done, you can call complete on the Future (or fail) to notify that you are done (success or failure).
 *
 * This represents the Session verticle. This will contain all resource calls directly related to the Session.
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class SessionVerticle extends AbstractVerticle {

    private Context context;
    private Vertx vertx;
    private I3WService i3WService;
    private Gson gson;


    /**
     * init method
     * @param vertx
     * @param context
     */
    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
        this.context = context;
        this.vertx = vertx;
        gson = new GsonBuilder().create();
    }

    /**
     * start method
     * @param fut
     */
    @Override
    public void start(Future<Void> fut)  {

        System.out.println( "starting session verticle..." );

        Router router = Router.router(vertx);

        router.route("/").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response
                    .putHeader("content-type", "text/html")
                    .end("<h1>Session verticle is running</h1>");
        });

        HttpServerOptions options = new HttpServerOptions().setLogActivity(true);
        vertx.createHttpServer(options)
                .requestHandler(router::accept)
                .listen(
                        config().getInteger("http.port", 9091),result -> {
                            if (result.succeeded()) {
                                fut.complete();

                            } else {
                                fut.fail(result.cause());
                            }
                        }
                );

        //Settings routes
        router.post(LOG_OFF_CAMPAIGN)
        .produces("application/json")
        .consumes("application/json")
		.handler(this::logoffCampaign);
		
        router.post(GET_SESSION_MANAGER_CONNECTED_SESSION)
		
                .produces("application/json")
                .consumes("application/json")
		.handler(this::getSessionManagerConnectedSession);
		
        router.post(LOG_OFF)
		
                .produces("application/json")
                .consumes("application/json")
		.handler(this::logoff);
		
        router.post(ADMIN_LOGOUT_USER)
		
                .produces("application/json")
                .consumes("application/json")
		.handler(this::adminLogoutUser);
        //router.post(LOGOUT_GRANTED).handler(this::logoutGranted);

        i3WService = I3WService.getInstance(this.vertx,null);


    }

    /**
     * logoffCampaign method
     * @param routingContext
     */
    private void logoffCampaign(RoutingContext routingContext) {
		
		routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);
            Logger.getInstance("").log(LoggerTag.DEBUG,"CParam2, [" +cParam.toString()+ "]","");

        //this request may block event loop, so we use executeBlocking for handle it
        vertx.<Integer>executeBlocking(future -> {

            int responseResult = this.i3WService.logoffCampaign(cParam);
            future.complete(responseResult);
        }, asyncResult -> {

            if (asyncResult.succeeded()) {
                ApiResponse response = new ApiResponse();
                response.setData(""+asyncResult.result());

                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * getSessionManagerConnectedSession method
     * @param routingContext
     */
    private void getSessionManagerConnectedSession(RoutingContext routingContext) {
   
	routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);
            Logger.getInstance("").log(LoggerTag.DEBUG,"CParam2, [" +cParam.toString()+ "]","");

        vertx.<Long>executeBlocking(future -> {

            long responseResult = SessionVerticle.this.i3WService.getSessionManagerConnectedSession(cParam);
            future.complete(responseResult);
        }, asyncResult -> {

            if (asyncResult.succeeded()) {
                ApiResponse response = new ApiResponse();
                response.setData("" + asyncResult.result());

                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * logoff method
     * @param routingContext
     */
    private void logoff(RoutingContext routingContext) {
       routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);
            Logger.getInstance("").log(LoggerTag.DEBUG,"CParam2, [" +cParam.toString()+ "]","");

        vertx.<Integer>executeBlocking(future -> {

            int responseResult = this.i3WService.logoff(cParam);
            future.complete(responseResult);
        }, asyncResult -> {

            if (asyncResult.succeeded()) {

                ApiResponse response = new ApiResponse();
                response.setData(""+asyncResult.result());

                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * adminLogoutUser method
     * @param routingContext
     */
    private void adminLogoutUser(RoutingContext routingContext) {
        
		routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);
            Logger.getInstance("").log(LoggerTag.DEBUG,"CParam2, [" +cParam.toString()+ "]","");

        vertx.<Boolean>executeBlocking(future -> {

            boolean responseResult = this.i3WService.adminLogoutUser(cParam);

            future.complete(responseResult);

        }, asyncResult -> {

            if (asyncResult.succeeded()) {

                ApiResponse response = new ApiResponse();
                response.setData(""+asyncResult.result());

                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    @Override
    public void stop() {
        System.out.println("Stopping session verticle");

    }
}