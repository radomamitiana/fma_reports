package com.fma.wb.integration.i3module.dto;

import java.io.Serializable;

public class UserStatusUpdate implements Serializable {

    private StatusMessageList statusMessageDetails;
    private PeopleManager peopleManager;

    public UserStatusUpdate() {
    }

    public UserStatusUpdate(StatusMessageList statusMessageDetails) {
        this.statusMessageDetails = statusMessageDetails;
        peopleManager = new PeopleManager();
    }

    public UserStatusUpdate(PeopleManager peopleManager) {
        this.peopleManager = peopleManager;
        statusMessageDetails = new StatusMessageList();
    }

    public void updateRequest(){

    }

    public StatusMessageList getStatusMessageDetails() {
        return statusMessageDetails;
    }

    public void setStatusMessageDetails(StatusMessageList statusMessageDetails) {
        this.statusMessageDetails = statusMessageDetails;
    }

    public PeopleManager getPeopleManager() {
        return peopleManager;
    }

    public void setPeopleManager(PeopleManager peopleManager) {
        this.peopleManager = peopleManager;
    }
}
