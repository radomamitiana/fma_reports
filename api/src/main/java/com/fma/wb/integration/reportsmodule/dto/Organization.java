package com.fma.wb.integration.reportsmodule.dto;

import java.io.Serializable;

/**
 * This model is going to be used for doing the mapping with the Organization table within Infinity database
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class Organization implements Serializable {
    private int organization_id;
    private String name;
    private int segment_id;
    private int trust_id;
    private String match_switch;
    private String active;
    private String converted;
    private String status;
    private String credit_card_sweep_category;
    private String is_portfolio_org;
    private String remit_with_organization_id;
    private String pay_principle_first;
    private String match_invoice_category;
    private String match_invoice_subtype;
    private String mask_account_number;
    private String mask_ssn;
    private String allowimc;
    private String imc_phone;
    private int payment_letter;
    private String override_Placement;
    private String email_indicator;
    private String payonline_indicator;
    private String created_on;
    private String created_by;
    private String modified_on;
    private String export_receiver_id;
    private String export_source_id;
    private String modified_by;
    private String pif_activity_id;
    private String pif_activity_days;
    private String sif_activity_id;
    private String sif_activity_days;
    private String special_invoice_window_category;
    private String special_invoice_window_subtype;
    private String special_invoice_window_subtype2;
    private String special_invoice_window_category2;
    private String settlement_window_equivalent_activity_code;
    private String allow_aux_e01;
    private String convenience_fee;
    private String webrecon_activity;
    private String surefire_activity;
    private String allow_click_dial;
    private String statute_use_lastpymtdte;
    private String statute_policy_name;
    private String block_cell_calls;
    private String file_root;
    private String file_chars;
    private String rpt_short_name;

    public int getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(int organization_id) {
        this.organization_id = organization_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSegment_id() {
        return segment_id;
    }

    public void setSegment_id(int segment_id) {
        this.segment_id = segment_id;
    }

    public int getTrust_id() {
        return trust_id;
    }

    public void setTrust_id(int trust_id) {
        this.trust_id = trust_id;
    }

    public String getMatch_switch() {
        return match_switch;
    }

    public void setMatch_switch(String match_switch) {
        this.match_switch = match_switch;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getConverted() {
        return converted;
    }

    public void setConverted(String converted) {
        this.converted = converted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCredit_card_sweep_category() {
        return credit_card_sweep_category;
    }

    public void setCredit_card_sweep_category(String credit_card_sweep_category) {
        this.credit_card_sweep_category = credit_card_sweep_category;
    }

    public String getIs_portfolio_org() {
        return is_portfolio_org;
    }

    public void setIs_portfolio_org(String is_portfolio_org) {
        this.is_portfolio_org = is_portfolio_org;
    }

    public String getRemit_with_organization_id() {
        return remit_with_organization_id;
    }

    public void setRemit_with_organization_id(String remit_with_organization_id) {
        this.remit_with_organization_id = remit_with_organization_id;
    }

    public String getPay_principle_first() {
        return pay_principle_first;
    }

    public void setPay_principle_first(String pay_principle_first) {
        this.pay_principle_first = pay_principle_first;
    }

    public String getMatch_invoice_category() {
        return match_invoice_category;
    }

    public void setMatch_invoice_category(String match_invoice_category) {
        this.match_invoice_category = match_invoice_category;
    }

    public String getMatch_invoice_subtype() {
        return match_invoice_subtype;
    }

    public void setMatch_invoice_subtype(String match_invoice_subtype) {
        this.match_invoice_subtype = match_invoice_subtype;
    }

    public String getMask_account_number() {
        return mask_account_number;
    }

    public void setMask_account_number(String mask_account_number) {
        this.mask_account_number = mask_account_number;
    }

    public String getMask_ssn() {
        return mask_ssn;
    }

    public void setMask_ssn(String mask_ssn) {
        this.mask_ssn = mask_ssn;
    }

    public String getAllowimc() {
        return allowimc;
    }

    public void setAllowimc(String allowimc) {
        this.allowimc = allowimc;
    }

    public String getImc_phone() {
        return imc_phone;
    }

    public void setImc_phone(String imc_phone) {
        this.imc_phone = imc_phone;
    }

    public int getPayment_letter() {
        return payment_letter;
    }

    public void setPayment_letter(int payment_letter) {
        this.payment_letter = payment_letter;
    }

    public String getOverride_Placement() {
        return override_Placement;
    }

    public void setOverride_Placement(String override_Placement) {
        this.override_Placement = override_Placement;
    }

    public String getEmail_indicator() {
        return email_indicator;
    }

    public void setEmail_indicator(String email_indicator) {
        this.email_indicator = email_indicator;
    }

    public String getPayonline_indicator() {
        return payonline_indicator;
    }

    public void setPayonline_indicator(String payonline_indicator) {
        this.payonline_indicator = payonline_indicator;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getModified_on() {
        return modified_on;
    }

    public void setModified_on(String modified_on) {
        this.modified_on = modified_on;
    }

    public String getExport_receiver_id() {
        return export_receiver_id;
    }

    public void setExport_receiver_id(String export_receiver_id) {
        this.export_receiver_id = export_receiver_id;
    }

    public String getExport_source_id() {
        return export_source_id;
    }

    public void setExport_source_id(String export_source_id) {
        this.export_source_id = export_source_id;
    }

    public String getModified_by() {
        return modified_by;
    }

    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public String getPif_activity_id() {
        return pif_activity_id;
    }

    public void setPif_activity_id(String pif_activity_id) {
        this.pif_activity_id = pif_activity_id;
    }

    public String getPif_activity_days() {
        return pif_activity_days;
    }

    public void setPif_activity_days(String pif_activity_days) {
        this.pif_activity_days = pif_activity_days;
    }

    public String getSif_activity_id() {
        return sif_activity_id;
    }

    public void setSif_activity_id(String sif_activity_id) {
        this.sif_activity_id = sif_activity_id;
    }

    public String getSif_activity_days() {
        return sif_activity_days;
    }

    public void setSif_activity_days(String sif_activity_days) {
        this.sif_activity_days = sif_activity_days;
    }

    public String getSpecial_invoice_window_category() {
        return special_invoice_window_category;
    }

    public void setSpecial_invoice_window_category(String special_invoice_window_category) {
        this.special_invoice_window_category = special_invoice_window_category;
    }

    public String getSpecial_invoice_window_subtype() {
        return special_invoice_window_subtype;
    }

    public void setSpecial_invoice_window_subtype(String special_invoice_window_subtype) {
        this.special_invoice_window_subtype = special_invoice_window_subtype;
    }

    public String getSpecial_invoice_window_subtype2() {
        return special_invoice_window_subtype2;
    }

    public void setSpecial_invoice_window_subtype2(String special_invoice_window_subtype2) {
        this.special_invoice_window_subtype2 = special_invoice_window_subtype2;
    }

    public String getSpecial_invoice_window_category2() {
        return special_invoice_window_category2;
    }

    public void setSpecial_invoice_window_category2(String special_invoice_window_category2) {
        this.special_invoice_window_category2 = special_invoice_window_category2;
    }

    public String getSettlement_window_equivalent_activity_code() {
        return settlement_window_equivalent_activity_code;
    }

    public void setSettlement_window_equivalent_activity_code(String settlement_window_equivalent_activity_code) {
        this.settlement_window_equivalent_activity_code = settlement_window_equivalent_activity_code;
    }

    public String getAllow_aux_e01() {
        return allow_aux_e01;
    }

    public void setAllow_aux_e01(String allow_aux_e01) {
        this.allow_aux_e01 = allow_aux_e01;
    }

    public String getConvenience_fee() {
        return convenience_fee;
    }

    public void setConvenience_fee(String convenience_fee) {
        this.convenience_fee = convenience_fee;
    }

    public String getWebrecon_activity() {
        return webrecon_activity;
    }

    public void setWebrecon_activity(String webrecon_activity) {
        this.webrecon_activity = webrecon_activity;
    }

    public String getSurefire_activity() {
        return surefire_activity;
    }

    public void setSurefire_activity(String surefire_activity) {
        this.surefire_activity = surefire_activity;
    }

    public String getAllow_click_dial() {
        return allow_click_dial;
    }

    public void setAllow_click_dial(String allow_click_dial) {
        this.allow_click_dial = allow_click_dial;
    }

    public String getStatute_use_lastpymtdte() {
        return statute_use_lastpymtdte;
    }

    public void setStatute_use_lastpymtdte(String statute_use_lastpymtdte) {
        this.statute_use_lastpymtdte = statute_use_lastpymtdte;
    }

    public String getStatute_policy_name() {
        return statute_policy_name;
    }

    public void setStatute_policy_name(String statute_policy_name) {
        this.statute_policy_name = statute_policy_name;
    }

    public String getBlock_cell_calls() {
        return block_cell_calls;
    }

    public void setBlock_cell_calls(String block_cell_calls) {
        this.block_cell_calls = block_cell_calls;
    }

    public String getFile_root() {
        return file_root;
    }

    public void setFile_root(String file_root) {
        this.file_root = file_root;
    }

    public String getFile_chars() {
        return file_chars;
    }

    public void setFile_chars(String file_chars) {
        this.file_chars = file_chars;
    }

    /**
     * Constructor without parameter
     */
    public Organization() {
    }


    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getRpt_short_name() {
        return rpt_short_name;
    }

    public void setRpt_short_name(String rpt_short_name) {
        this.rpt_short_name = rpt_short_name;
    }
}