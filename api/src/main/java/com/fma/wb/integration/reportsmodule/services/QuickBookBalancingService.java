package com.fma.wb.integration.reportsmodule.services;

import com.fma.wb.integration.common.dao.DbConnexionHelper;
import com.fma.wb.integration.common.dto.User;
import com.fma.wb.integration.common.dto.api.InternalResponse;
import com.fma.wb.integration.common.dto.enums.DBType;
import com.fma.wb.integration.reportsmodule.dto.Organization;
import com.fma.wb.integration.reportsmodule.dto.quickbook.*;
import com.fma.wb.integration.reportsmodule.utils.CommonBackOfficeAndReportingQueries;
import common.Logger;
import common.LoggerTag;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.SQLConnection;
import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;

import static com.fma.wb.integration.reportsmodule.utils.CommonBackOfficeAndReportingQueries.GET_ORGANIZATION_INFO_QUICKBOOK_BALANCING;

public class QuickBookBalancingService {

    private static QuickBookBalancingService quickBookBalancingService;
    private Vertx vertx;
    private static HashMap<String, User> activeSessions = new HashMap<>();

    private static DbConnexionHelper dbConnexionHelper;
    private static HashMap<String, String> credentials = new HashMap<>();
    private List<Organization> organizationsListGlobal = new ArrayList<>();

    public DbConnexionHelper getDbConnexionHelper() {
        return dbConnexionHelper;
    }

    private void setDbConnexionHelper(DbConnexionHelper db) {
        dbConnexionHelper = db;
    }

    public static HashMap<String, User> getActiveSessions() {
        return activeSessions;
    }

    public void setActiveSessions(HashMap<String, User> activeSessionsG) {
        activeSessions = activeSessionsG;
    }

    /**
     * This constructor accepts no parameters and will initialize vertx properties.
     */
    public QuickBookBalancingService() {
        this.vertx = Vertx.vertx();
    }

    /**
     * This constructor with parameter
     *
     * @param vertx
     */
    public QuickBookBalancingService(Vertx vertx) {
        this.vertx = vertx;
        this.setDbConnexionHelper(DbConnexionHelper.getInstance(vertx, credentials));

    }

    /**
     * This method returns an instance of QuickBookBalancingService
     *
     * @param vertx
     * @return QuickBookBalancingService instance
     */
    public static QuickBookBalancingService getInstance(Vertx vertx) {
        if (quickBookBalancingService == null) {
            quickBookBalancingService = new QuickBookBalancingService(vertx);
        }

        return quickBookBalancingService;
    }

    /**
     * This method returns the list of all organizations that will be displayed and selected
     * from the trust quickbooking balancing application and retrieved from Infinity database
     *
     * @return a list of organization name
     */
    public void getOrganizationList(String sessionId, Handler<List<QuickOrganization>> handler) {

        Logger.getInstance("").log(LoggerTag.DEBUG, "getOrganizationList quick book service", "");
        if (!checkSession(sessionId)) {
            handler.handle(null);
        } else {

            List<QuickOrganization> organizationsList = new ArrayList<>();
            Logger.getInstance("").log(LoggerTag.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~START: getOrganizationList ~~~~~~~~~~~~~~~~~~~~~~", "");
            try {
                if (dbConnexionHelper.getFmaConnection() == null) {
                    TrustBalancingService.connectToDb(DBType.Infinity, event -> {

                        if (event.succeeded()) {
                            dbConnexionHelper.setFmaConnection(event.result());
                            dbConnexionHelper.getFmaConnection().query(GET_ORGANIZATION_INFO_QUICKBOOK_BALANCING, result -> {
                                System.out.println("RESULT => " + result);
                                if (result.succeeded()) {
                                    result.result().getRows().forEach(entries -> {
                                        final QuickOrganization orgGot = DbConnexionHelper.returnQueryObject(entries, QuickOrganization.class);
                                        if (!isContainsOrganization(organizationsList, orgGot))
                                            organizationsList.add(orgGot);
                                    });

                                } else {
                                    System.out.println("Result NOT succeded  " + result.result().getRows());
                                    Logger.getInstance("").log(LoggerTag.ERROR, "Error getting organization list: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
                                    handler.handle(null);
                                }
                                handler.handle(organizationsList);
                            });

                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, "Error logging to Infinity: " + ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                            handler.handle(null);
                        }

                    }, dbConnexionHelper);
                } else {
                    dbConnexionHelper.getFmaConnection().query(GET_ORGANIZATION_INFO_QUICKBOOK_BALANCING, result -> {
                        //dbConnexionHelper.getFmaConnection().query(ORGANIZATIONS_QUERY, result -> {
                        if (result.succeeded()) {
                            result.result().getRows().forEach(entries -> {
                                final QuickOrganization orgGot = DbConnexionHelper.returnQueryObject(entries, QuickOrganization.class);
                                if (!isContainsOrganization(organizationsList, orgGot))
                                    organizationsList.add(orgGot);
                            });

                        } else {
                            System.out.println("Result NOT succeded  " + result.result().getRows());
                            Logger.getInstance("").log(LoggerTag.ERROR, "Error getting organization list: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
                            handler.handle(null);
                        }
                        //This is just for unit testing
                                /*System.out.println("==================================== Connection not already exist. Here is the list of organization ====================================");
                                for (String org : organizationsList) {
                                    System.out.println(org);
                                }*/
                        handler.handle(organizationsList);
                    });

                }
            } catch (SQLException | ClassNotFoundException e) {
                Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e), "");
                handler.handle(null);
            }
        }
    }


    public void getClientByOrganization(String sessionId, int organizationId, Handler<List<QuickOrganizationClient>> handler) {
        if (!checkSession(sessionId)) {
            handler.handle(null);
        } else {
            Logger.getInstance("").log(LoggerTag.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~START: getClientByOrganization ~~~~~~~~~~~~~~~~~~~~~~", "");
            List<QuickOrganizationClient> quickOrganizationClientList = new ArrayList<>();
            try {
                if (dbConnexionHelper.getFmaConnection() == null) {
                    TrustBalancingService.connectToDb(DBType.Infinity, event -> {

                        if (event.succeeded()) {
                            dbConnexionHelper.setFmaConnection(event.result());
                            dbConnexionHelper.getFmaConnection().query(CommonBackOfficeAndReportingQueries.getClientByOrganization(organizationId), result -> {
                                System.out.println("RESULT => " + result);
                                if (result.succeeded()) {
                                    List<JsonObject> res1 = result.result().getRows(true);
                                    result.result().getRows().forEach(entries -> {
                                        List<QuickOrganizationClient> quickOrganizationClientListGotStream = DbConnexionHelper.returnQueryList(res1, QuickOrganizationClient.class);
                                        quickOrganizationClientList.clear();
                                        quickOrganizationClientListGotStream.forEach(quickOrganizationClient -> quickOrganizationClientList.add(quickOrganizationClient));
                                    });

                                } else {
                                    System.out.println("Result NOT succeded  " + result.result().getRows());
                                    Logger.getInstance("").log(LoggerTag.ERROR, "Error getting client organization quick list: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
                                }
                                handler.handle(quickOrganizationClientList);
                            });

                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, "Error logging to Infinity: " + ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                            handler.handle(null);
                        }

                    }, dbConnexionHelper);
                } else {
                    dbConnexionHelper.getFmaConnection().query(CommonBackOfficeAndReportingQueries.getClientByOrganization(organizationId), result -> {
                        System.out.println("RESULT => " + result);
                        if (result.succeeded()) {
                            List<JsonObject> res1 = result.result().getRows(true);
                            result.result().getRows().forEach(entries -> {
                                List<QuickOrganizationClient> quickOrganizationClientListGotStream = DbConnexionHelper.returnQueryList(res1, QuickOrganizationClient.class);
                                quickOrganizationClientList.clear();
                                quickOrganizationClientListGotStream.forEach(quickOrganizationClient -> quickOrganizationClientList.add(quickOrganizationClient));
                            });

                        } else {
                            System.out.println("Result NOT succeded  " + result.result().getRows());
                            Logger.getInstance("").log(LoggerTag.ERROR, "Error getting client organization quick list: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
                        }
                        handler.handle(quickOrganizationClientList);
                    });
                }
            } catch (SQLException | ClassNotFoundException e) {
                Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e), "");
                handler.handle(null);
            }
        }
    }

    public void saveCollectionsProcessingByOneClient(String sessionId, CollectionsProcessingInputData collectionsProcessingInputData, Handler<CollectionsProcessingInputData> handler) {
        if (!checkSession(sessionId)) {
            handler.handle(null);
        } else {
            if (collectionsProcessingInputData.getClient_id() > 0 && collectionsProcessingInputData.getOrg_id() > 0) {
                Logger.getInstance("").log(LoggerTag.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~START~: saveCollectionsProcessingByOneClient ~~~~~~~~~~~~~~~~~~~~~~", "");
                try {
                    if (dbConnexionHelper.getQuickBookConnection() == null) {
                        TrustBalancingService.connectToDb(DBType.Quick_Book, event -> {
                            if (event.succeeded()) {
                                dbConnexionHelper.setQuickBookConnection(event.result());
                                this.processAddOrUpdateCollectionsProcessign(collectionsProcessingInputData, this.dbConnexionHelper.getQuickBookConnection(), handler);
                            }
                        }, dbConnexionHelper);
                    } else {
                        this.processAddOrUpdateCollectionsProcessign(collectionsProcessingInputData, this.dbConnexionHelper.getQuickBookConnection(), handler);
                    }

                } catch (SQLException | ClassNotFoundException e) {
                    Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e), "");
                    handler.handle(null);
                }
            } else {
                handler.handle(null);
            }
        }
    }

    public void saveCollectionsProcessingByAllClient(String sessionId, CollectionsProcessingInputData collectionsProcessingInputData, Handler<CollectionsProcessingInputData> handler) {
        if (!checkSession(sessionId)) {
            handler.handle(null);
        } else {
            Logger.getInstance("").log(LoggerTag.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~START: saveCollectionsProcessingByAllClient ~~~~~~~~~~~~~~~~~~~~~~", "");
            try {
                if (dbConnexionHelper.getFmaConnection() == null) {
                    TrustBalancingService.connectToDb(DBType.Infinity, event -> {

                        if (event.succeeded()) {
                            dbConnexionHelper.setFmaConnection(event.result());
                            handleSaveClientProcessigByAllClient(sessionId, collectionsProcessingInputData, dbConnexionHelper.getFmaConnection(), handler);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, "Error logging to Infinity: " + ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                            handler.handle(null);
                        }

                    }, dbConnexionHelper);
                } else {
                    handleSaveClientProcessigByAllClient(sessionId, collectionsProcessingInputData, dbConnexionHelper.getFmaConnection(), handler);
                }
            } catch (SQLException | ClassNotFoundException e) {
                Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e), "");
                handler.handle(null);
            }
        }
    }

    private boolean checkSession(String sessionId) {
        return TrustBalancingService.getActiveSessions().containsKey(sessionId);
    }

    private void processAddOrUpdateCollectionsProcessign(CollectionsProcessingInputData collectionsProcessingInputData, SQLConnection quickConnection, Handler<CollectionsProcessingInputData> handler) {
        if (collectionsProcessingInputData.getRecord_id() > 0) {
            updateCollectionProcessing(quickConnection, collectionsProcessingInputData, handler);
        } else {
            quickConnection.query(CommonBackOfficeAndReportingQueries.getCollectionsProcessingByOrgIdAndClientId(collectionsProcessingInputData.getOrg_id(), collectionsProcessingInputData.getClient_id()), resultGetInfo -> {
                if (!resultGetInfo.succeeded()) {
                    System.out.println("Result NOT succeded  " + resultGetInfo.result().getRows());
                    Logger.getInstance("").log(LoggerTag.ERROR, "Error getting collection processing by organization and client."
                            + ExceptionUtils.exceptionStackTraceAsString(resultGetInfo.cause()), "");
                    handler.handle(null);
                } else {
                    if (resultGetInfo.result().getRows().size() > 0
                            && resultGetInfo.result().getRows().get(0).getInteger("record_id") > 0) {
                        collectionsProcessingInputData.setRecord_id(resultGetInfo.result().getRows().get(0).getInteger("record_id"));
                        updateCollectionProcessing(quickConnection, collectionsProcessingInputData, handler);
                    } else {
                        System.err.println("MOP");
                        addCollectionsProcessing(quickConnection, collectionsProcessingInputData, handler);
                    }
                    handler.handle(collectionsProcessingInputData);
                }
            });
        }
    }

    private boolean isContainsOrganization(List<QuickOrganization> quickOrganizationList, QuickOrganization organization) {
        return quickOrganizationList.stream().anyMatch(org -> org.getOrganization_id() == organization.getOrganization_id()
                && org.getName() != null && org.getName().equals(organization.getName()));
    }

    private void addCollectionsProcessing(SQLConnection quickConnection, CollectionsProcessingInputData collectionsProcessingInputData,
                                          Handler<CollectionsProcessingInputData> handler) {
        quickConnection.query(CommonBackOfficeAndReportingQueries.addCollectionsProcessing(collectionsProcessingInputData), resultAdd -> {
            if (!resultAdd.succeeded()) {
                Logger.getInstance("").log(LoggerTag.DEBUG, ExceptionUtils.exceptionStackTraceAsString(resultAdd.cause()), "");
                handler.handle(null);
            }
        });
    }

    private void updateCollectionProcessing(SQLConnection quickConnection, CollectionsProcessingInputData collectionsProcessingInputData,
                                            Handler<CollectionsProcessingInputData> handler) {
        quickConnection.query(CommonBackOfficeAndReportingQueries.updateCollectionsProcessing(collectionsProcessingInputData), resultUpdate -> {
            if (!resultUpdate.succeeded()) {
                Logger.getInstance("").log(LoggerTag.DEBUG, ExceptionUtils.exceptionStackTraceAsString(resultUpdate.cause()), "");
                handler.handle(null);
            }
        });
    }

    public void getCollectionProcessingByOrganizationIdAndClientId(String sessionId, int organizationId, int clientId, Handler<CollectionsProcessingOutputData> handler) {
        if (!checkSession(sessionId)) {
            handler.handle(null);
        } else {
            Logger.getInstance("").log(LoggerTag.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~START: getCollectionProcessingByOrganizationIdAndClientId ~~~~~~~~~~~~~~~~~~~~~~", "");
            try {
                if (dbConnexionHelper.getQuickBookConnection() == null) {
                    TrustBalancingService.connectToDb(DBType.Quick_Book, event -> {

                        if (event.succeeded()) {
                            dbConnexionHelper.setQuickBookConnection(event.result());
                            processToGetCollectonProcessing(organizationId, clientId, dbConnexionHelper.getQuickBookConnection(), handler);
                        }

                    }, dbConnexionHelper);
                } else {
                    processToGetCollectonProcessing(organizationId, clientId, dbConnexionHelper.getQuickBookConnection(), handler);
                }
            } catch (SQLException | ClassNotFoundException e) {
                Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e), "");
                handler.handle(null);
            }
        }
    }

    private void processToGetCollectonProcessing(int organizationId, int clientId,
                                                 SQLConnection quickConnection, Handler<CollectionsProcessingOutputData> handler) {
        quickConnection.query(CommonBackOfficeAndReportingQueries.getCollectionsProcessingByOrgIdAndClientId(organizationId, clientId), result -> {
            if (result.succeeded()) {
                if (result.result().getRows().size() > 0) {
                    result.result().getRows().forEach(entries -> {
                        final CollectionsProcessingOutputData collectionsProcessingOutputData = DbConnexionHelper.returnQueryObject(entries, CollectionsProcessingOutputData.class);
                        handler.handle(collectionsProcessingOutputData);
                    });
                } else {
                    handler.handle(new CollectionsProcessingOutputData());
                }
            } else {
                Logger.getInstance("").log(LoggerTag.ERROR, "Error getting client organization quick list: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
                handler.handle(null);
            }
        });
    }

    private void handleSaveClientProcessigByAllClient(String sessionId, CollectionsProcessingInputData collectionsProcessingInputData, SQLConnection fmaConnection, Handler<CollectionsProcessingInputData> handler) {
        fmaConnection.query(CommonBackOfficeAndReportingQueries.getClientByOrganization(collectionsProcessingInputData.getOrg_id()), result -> {
            if (result.succeeded()) {
                List<JsonObject> res1 = result.result().getRows(true);
                result.result().getRows().forEach(entries -> {
                    List<QuickOrganizationClient> quickOrganizationClientListGotStream = DbConnexionHelper.returnQueryList(res1, QuickOrganizationClient.class);
                    quickOrganizationClientListGotStream.forEach(quickOrganizationClient -> {
                        collectionsProcessingInputData.setClient_id(quickOrganizationClient.getClient_id());
                        this.saveCollectionsProcessingByOneClient(sessionId, collectionsProcessingInputData, handler);
                    });
                });

            } else {
                System.out.println("Result NOT succeded  " + result.result().getRows());
                Logger.getInstance("").log(LoggerTag.ERROR, "Error getting client organization quick list: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
            }
            handler.handle(new CollectionsProcessingInputData());
        });
    }

    public void quickBookBalancingReportData(String sessionId, QuickBookBalancingReportInputData quickBookBalancingReportInputData, Future<InternalResponse> handler) {
        Logger.getInstance("").log(LoggerTag.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~START: quickBookBalancingReport data ~~~~~~~~~~~~~~~~~~~~~~", "");

        if (!checkSession(sessionId)) {
            InternalResponse resp = new InternalResponse();
            resp.setCause(-1);
            if (!handler.isComplete()) {
                handler.complete(resp);
            }
        } else {

            //  1) get all money posted by date range
        }
    }


    public void saveOffset(String sessionId, OffsetInputData offsetInputData, Handler<OffsetInputData> handler) {
        if (!checkSession(sessionId)) {
            handler.handle(null);
        } else {
            if (offsetInputData.getClient_id() > 0 && offsetInputData.getOrg_id() > 0) {
                Logger.getInstance("").log(LoggerTag.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~START~: saveOffset ~~~~~~~~~~~~~~~~~~~~~~", "");
                try {
                    if (dbConnexionHelper.getQuickBookConnection() == null) {
                        TrustBalancingService.connectToDb(DBType.Quick_Book, event -> {
                            if (event.succeeded()) {
                                dbConnexionHelper.setQuickBookConnection(event.result());
                                this.processAddOrUpdateOffset(offsetInputData, this.dbConnexionHelper.getQuickBookConnection(), handler);
                            }
                        }, dbConnexionHelper);
                    } else {
                        this.processAddOrUpdateOffset(offsetInputData, this.dbConnexionHelper.getQuickBookConnection(), handler);
                    }

                } catch (SQLException | ClassNotFoundException e) {
                    Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e), "");
                    handler.handle(null);
                }
            } else {
                handler.handle(null);
            }

        }
    }

    private void processAddOrUpdateOffset(OffsetInputData offsetInputData, SQLConnection quickConnection, Handler<OffsetInputData> handler) {
        if (offsetInputData.getOffset_date() != null) {
            offsetInputData.setOffset_date(offsetInputData.getOffset_date().substring(0, 10).trim());
            offsetInputData.setCreated_on(LocalDate.now().toString());
            Logger.getInstance("").log(LoggerTag.DEBUG, "offsetInputData.getOffset_date(), [" + offsetInputData.getOffset_date() + "]", "");
            if (offsetInputData.getOffset_id() > 0) {
                updateOffset(quickConnection, offsetInputData, handler);
            } else {
                addOffset(quickConnection, offsetInputData, handler);
            }
        } else {
            handler.handle(null);
        }
    }

    private void updateOffset(SQLConnection quickConnection, OffsetInputData offsetInputData,
                              Handler<OffsetInputData> handler) {
        quickConnection.query(CommonBackOfficeAndReportingQueries.updateOffset(offsetInputData), resultUpdate -> {
            if (!resultUpdate.succeeded()) {
                Logger.getInstance("").log(LoggerTag.DEBUG, ExceptionUtils.exceptionStackTraceAsString(resultUpdate.cause()), "");
                handler.handle(null);
            } else {
                handler.handle(offsetInputData);
            }
        });
    }

    private void addOffset(SQLConnection quickConnection, OffsetInputData offsetInputData,
                           Handler<OffsetInputData> handler) {
        quickConnection.query(CommonBackOfficeAndReportingQueries.addOffset(offsetInputData), resultAdd -> {
            if (!resultAdd.succeeded()) {
                Logger.getInstance("").log(LoggerTag.DEBUG, ExceptionUtils.exceptionStackTraceAsString(resultAdd.cause()), "");
                handler.handle(null);
            } else {
                handler.handle(offsetInputData);
            }
        });
    }

    public void getOffsetByOrgIdAndClientId(String sessionId, int orgId, int clientId, Handler<List<OffsetOutputData>> handler) {
        if (!checkSession(sessionId)) {
            handler.handle(null);
        } else {
            Logger.getInstance("").log(LoggerTag.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~START: getOffsetByClient ~~~~~~~~~~~~~~~~~~~~~~", "");
            List<OffsetOutputData> offsetOutputDataList = new ArrayList<>();
            try {
                if (dbConnexionHelper.getQuickBookConnection() == null) {
                    TrustBalancingService.connectToDb(DBType.Quick_Book, event -> {

                        if (event.succeeded()) {
                            dbConnexionHelper.setQuickBookConnection(event.result());
                            dbConnexionHelper.getQuickBookConnection().query(CommonBackOfficeAndReportingQueries.getOffsetByOrgIdAndClientId(orgId, clientId), result -> {
                                System.out.println("RESULT => " + result);
                                if (result.succeeded()) {
                                    List<JsonObject> res1 = result.result().getRows(true);
                                    result.result().getRows().forEach(entries -> {
                                        List<OffsetOutputData> offsetOutputDataGotStream = DbConnexionHelper.returnQueryList(res1, OffsetOutputData.class);
                                        offsetOutputDataList.clear();
                                        offsetOutputDataGotStream.forEach(offsetOutputData -> offsetOutputDataList.add(offsetOutputData));
                                    });

                                } else {
                                    System.out.println("Result NOT succeded  " + result.result().getRows());
                                    Logger.getInstance("").log(LoggerTag.ERROR, "Error getting getOffsetByClient quick list: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
                                }
                                handler.handle(offsetOutputDataList);
                            });

                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, "Error logging to Infinity: " + ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                            handler.handle(null);
                        }

                    }, dbConnexionHelper);
                } else {
                    dbConnexionHelper.getQuickBookConnection().query(CommonBackOfficeAndReportingQueries.getOffsetByOrgIdAndClientId(orgId, clientId), result -> {
                        System.out.println("RESULT => " + result);
                        if (result.succeeded()) {
                            List<JsonObject> res1 = result.result().getRows(true);
                            result.result().getRows().forEach(entries -> {
                                List<OffsetOutputData> offsetOutputDataGotStream = DbConnexionHelper.returnQueryList(res1, OffsetOutputData.class);
                                offsetOutputDataList.clear();
                                offsetOutputDataGotStream.forEach(offsetOutputData -> offsetOutputDataList.add(offsetOutputData));
                            });

                        } else {
                            System.out.println("Result NOT succeded  " + result.result().getRows());
                            Logger.getInstance("").log(LoggerTag.ERROR, "Error getting client organization quick list: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
                        }
                        handler.handle(offsetOutputDataList);
                    });
                }
            } catch (SQLException | ClassNotFoundException e) {
                Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e), "");
                handler.handle(null);
            }
        }
    }

    public void printProcInstr(String sessionId, Future<InternalResponse> handler) {
        Logger.getInstance("").log(LoggerTag.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~START: printProcInstr data ~~~~~~~~~~~~~~~~~~~~~~", "");

        QuickBookBalancingReportOutputData quickBookBalancingReportOutputData = new QuickBookBalancingReportOutputData();

        if (!checkSession(sessionId)) {
            InternalResponse resp = new InternalResponse();
            resp.setCause(-1);
            if (!handler.isComplete()) {
                handler.complete(resp);
            }
        } else {

            if (dbConnexionHelper.getFmaConnection() == null) {
                try {


                    TrustBalancingService.connectToDb(DBType.Quick_Book, event -> {
                        if (event.succeeded()) {
                            dbConnexionHelper.setQuickBookConnection(event.result());

                            //Begin check phone system connection
                            if (dbConnexionHelper.getQuickBookConnection() == null) {
                                try {
                                    TrustBalancingService.connectToDb(DBType.Quick_Book, eventPhoneSystem -> {

                                        if (eventPhoneSystem.succeeded()) {
                                            dbConnexionHelper.setQuickBookConnection(eventPhoneSystem.result());

                                            //Start split time calculation data retrieval
                                            this.processprintProcInstr(handler, quickBookBalancingReportOutputData);

                                        } else {
                                            InternalResponse response1 = new InternalResponse();
                                            response1.setCause(0);
                                            response1.setData(eventPhoneSystem.cause().getMessage());
                                            if (!handler.isComplete()) {
                                                handler.complete(response1);
                                            }
                                        }
                                    }, dbConnexionHelper);
                                } catch (SQLException | ClassNotFoundException e) {
                                    e.printStackTrace();
                                    InternalResponse response1 = new InternalResponse();
                                    response1.setCause(0);
                                    response1.setData(e.getMessage());
                                    if (!handler.isComplete()) {
                                        handler.complete(response1);
                                    }
                                }
                            } else {
                                //Start split time calculation data retrieval
                                this.processprintProcInstr(handler, quickBookBalancingReportOutputData);
                            }

                            //End check phone system connection

                        } else {
                            InternalResponse resp = new InternalResponse();
                            resp.setCause(0);
                            resp.setData(event.cause().getMessage());
                            if (!handler.isComplete()) {
                                handler.complete(resp);
                            }
                        }
                    }, dbConnexionHelper);

                } catch (SQLException | ClassNotFoundException e) {
                    e.printStackTrace();
                    InternalResponse resp = new InternalResponse();
                    resp.setCause(0);
                    resp.setData(e.getMessage());
                    if (!handler.isComplete()) {
                        handler.complete(resp);
                    }
                }
            } else {
                //Check phone system connection
                if (dbConnexionHelper.getQuickBookConnection() == null) {
                    try {
                        TrustBalancingService.connectToDb(DBType.Quick_Book, event -> {

                            if (event.succeeded()) {
                                dbConnexionHelper.setQuickBookConnection(event.result());

                                //Start split time calculation data retrieval
                                this.processprintProcInstr(handler, quickBookBalancingReportOutputData);

                            } else {
                                InternalResponse resp = new InternalResponse();
                                resp.setCause(0);
                                resp.setData(event.cause().getMessage());
                                if (!handler.isComplete()) {
                                    handler.complete(resp);
                                }
                            }

                        }, dbConnexionHelper);
                    } catch (SQLException | ClassNotFoundException e) {
                        e.printStackTrace();
                        InternalResponse resp = new InternalResponse();
                        resp.setCause(0);
                        resp.setData(e.getMessage());
                        if (!handler.isComplete()) {
                            handler.complete(resp);
                        }
                    }
                } else {
                    //Check phone system connection
                    //Start Split time report data retrieval
                    this.processprintProcInstr(handler, quickBookBalancingReportOutputData);
                }
            } // End big else
        }
    }

    private void processprintProcInstr(Future<InternalResponse> handler, QuickBookBalancingReportOutputData quickBookBalancingReportOutputData) {

        String query = "SELECT * FROM collections_processing";

        dbConnexionHelper.getQuickBookConnection().query(String.format(query), result01 -> {
            if (result01.succeeded()) {
                List<JsonObject> results01 = result01.result().getRows(true);
                if (results01.size() == 0) {
                    InternalResponse resp = new InternalResponse();
                    resp.setCause(0);
                    resp.setData("No transaction found within infinity database for the selected period");
                    if (!handler.isComplete()) {
                        handler.complete(resp);
                    }
                } else {
                    final List<CollectionsProcessingOutputData> collectionsProcessingOutputDataDtoStream = DbConnexionHelper.returnQueryList(results01, CollectionsProcessingOutputData.class);
                    collectionsProcessingOutputDataDtoStream.forEach(collectionsProcessingOutputData -> {
                        quickBookBalancingReportOutputData.getCollectionsProcessingOutputDataList().add(collectionsProcessingOutputData);

                        System.out.println("quickBookBalancingReportOutputData LIST SIZE:    " + quickBookBalancingReportOutputData.getCollectionsProcessingOutputDataList().size());
                    });
                    InternalResponse resp = new InternalResponse();
                    resp.setCause(1);
                    resp.setData(Json.encode(quickBookBalancingReportOutputData));
                    if (!handler.isComplete()) {
                        handler.complete(resp);
                    }
                }
            } else {
                Logger.getInstance("").log(LoggerTag.ERROR, "Error getting split time data from the [fmars102].[I3_IC_40].[dbo].[user_calldetail_viw] view " + ExceptionUtils.exceptionStackTraceAsString(result01.cause()), "");
                InternalResponse resp = new InternalResponse();
                resp.setCause(0);
                resp.setData("An error happens while getting transactions from infinity database");
                if (!handler.isComplete()) {
                    handler.complete(resp);
                }
            }
        });
    }
}
