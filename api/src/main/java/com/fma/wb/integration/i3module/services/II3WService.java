package com.fma.wb.integration.i3module.services;

import com.fma.wb.integration.i3module.dto.CompositeType;

/**
 * This represents the interface of the web service
 *
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public interface II3WService {

    long getSessionManagerConnectedSession(CompositeType cparam);
    String makePhoneCall(CompositeType cparam);
    void disconnectCall(CompositeType cparam);
    int makeTransferCall(CompositeType cparam);
    int makeConferenceCall(CompositeType cparam);
    int connectTransferCall(CompositeType cparam);
    int connectConfCall(CompositeType cparam);
    void noConfCall(CompositeType cparam);
    void disconnectTransferCall(CompositeType cparam);
    void setI3AvailableStatus(CompositeType cparam);
    String[] getAvailableCampaign(CompositeType cparam);
    int logonCampaign(CompositeType cparam);
    int logoffCampaign(CompositeType cparam);
    int logoff(CompositeType cparam);
    int agentLessCallComplete(CompositeType cparam);
    boolean markDialerCallComplete(CompositeType cparam);
    String[] getDialerCallAcctIdAndMode(CompositeType cparam);
    int receiveNewDialerCall(CompositeType cparam);
    int disconnectDialerCall(CompositeType cparam);
    String[] getUsersInfo();
    boolean adminLogoutUser(CompositeType cparam);
    String[] showUsers();
    String[] getIncomingCallInfo(CompositeType cparam);
    int beginPlayAudio(CompositeType cparam);
    boolean recordingCall(CompositeType cparam);
    boolean recordingSwitch(CompositeType cparam, boolean onOff);
    String[] hasIncoming(CompositeType cparam);
}
