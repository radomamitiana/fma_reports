package com.fma.wb.integration.reportsmodule.dto.splittime;

import com.fma.wb.integration.common.utils.Utils;
import java.io.Serializable;

/**
 * This class represents the input data for split time report
 */
public class SplitTimeReportInputData implements Serializable {
    private String initiatedDateTimeBegin;
    private String initiatedDateTimeEnd;

    public String getInitiatedDateTimeBegin() {
        return initiatedDateTimeBegin;
    }

    public void setInitiatedDateTimeBegin(String initiatedDateTimeBegin) {
        this.initiatedDateTimeBegin = initiatedDateTimeBegin;
    }

    public String getInitiatedDateTimeEnd() {
        return initiatedDateTimeEnd;
    }

    public void setInitiatedDateTimeEnd(String initiatedDateTimeEnd) {
        this.initiatedDateTimeEnd = initiatedDateTimeEnd;
    }

    public SplitTimeReportInputData() {
    }

    public void synchCorrectDateFields(){
        try {
            this.initiatedDateTimeBegin = Utils.toUsCentralTimeFormat(this.initiatedDateTimeBegin);
            this.initiatedDateTimeBegin = Utils.toUsCentralTimeFormat(this.initiatedDateTimeBegin);
        } catch ( Exception exception) {

        }
    }
}