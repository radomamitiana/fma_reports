package com.fma.wb.integration.reportsmodule.dto.splittime;

import java.io.Serializable;

public class SplitTimeDto implements Serializable {
    private String name = "";
    private String manager = "";
    private int numberOfCalls = 0;
    private String minsDuration = "";
    private String splitTime = "";
    private String callsShort = "";
    private String factor = "";
    private String fullPt = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public int getNumberOfCalls() {
        return numberOfCalls;
    }

    public void setNumberOfCalls(int numberOfCalls) {
        this.numberOfCalls = numberOfCalls;
    }

    public String getMinsDuration() {
        return minsDuration;
    }

    public void setMinsDuration(String minsDuration) {
        this.minsDuration = minsDuration;
    }

    public String getSplitTime() {
        return splitTime;
    }

    public void setSplitTime(String splitTime) {
        this.splitTime = splitTime;
    }

    public String getCallsShort() {
        return callsShort;
    }

    public void setCallsShort(String callsShort) {
        this.callsShort = callsShort;
    }

    public String getFactor() {
        return factor;
    }

    public void setFactor(String factor) {
        this.factor = factor;
    }

    public String getFullPt() {
        return fullPt;
    }

    public void setFullPt(String fullPt) {
        this.fullPt = fullPt;
    }

    public SplitTimeDto() {
    }

    @Override
    public String toString() {
        return "SplitTimeDto{" +
                "name='" + name + '\'' +
                ", manager='" + manager + '\'' +
                ", numberOfCalls=" + numberOfCalls +
                ", minsDuration='" + minsDuration + '\'' +
                ", splitTime='" + splitTime + '\'' +
                ", callsShort='" + callsShort + '\'' +
                ", factor='" + factor + '\'' +
                ", fullPt='" + fullPt + '\'' +
                '}';
    }
}