package com.fma.wb.integration.reportsmodule.dto.trust;

import java.io.Serializable;

/**
 * This model is going to be used for doing the mapping with the client table within Infinity database
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class Client implements Serializable {
    private int organization_id;
    private String client_id;
    private String client_identifier;
    private String name;
    private String active;

    public int getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(int organization_id) {
        this.organization_id = organization_id;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_identifier() {
        return client_identifier;
    }

    public void setClient_identifier(String client_identifier) {
        this.client_identifier = client_identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    /**
     * Constructor without parameter
     */
    public Client() {
    }

    /**
     * Constructor with parameters
     * @param organization_id
     * @param client_id
     * @param client_identifier
     * @param name
     * @param active
     */
    public Client(int organization_id, String client_id, String client_identifier, String name, String active) {
        this.organization_id = organization_id;
        this.client_id = client_id;
        this.client_identifier = client_identifier;
        this.name = name;
        this.active = active;
    }

    @Override
    public String toString() {
        return "Client{" +
                "organization_id=" + organization_id +
                ", client_id='" + client_id + '\'' +
                ", client_identifier='" + client_identifier + '\'' +
                ", name='" + name + '\'' +
                ", active='" + active + '\'' +
                '}';
    }
}