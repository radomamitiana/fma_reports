package com.fma.wb.integration.reportsmodule.utils;

import com.fma.wb.integration.reportsmodule.dto.quickbook.CollectionsProcessingInputData;
import com.fma.wb.integration.reportsmodule.dto.quickbook.OffsetInputData;
import io.vertx.core.json.Json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.fma.wb.integration.common.utils.ConfigurationUtils.*;
import static com.fma.wb.integration.reportsmodule.utils.DateUtils.setDateTo1fstDay;

/**
 * This class is used for storing all queries used for the back office and reports application
 * Queries are centralized here
 *
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class CommonBackOfficeAndReportingQueries {

    //Trust Querries
    public static final String INFINITY_TRANSACTIONS = " SELECT monetary_transaction.date, monetary_transaction.transaction_type, " +
            " monetary_transaction.amount, monetary_transaction.monetary_transaction_id, " +
            " monetary_transaction.reversing_id, monetary_transaction.debtor_id, " +
            " debtor.first_name, debtor.last_name, " +
            " monetary_transaction.active, " +
            " client.organization_id, client.client_id, client.client_identifier," +
            " payment_types.type_name, invoice.client_account_number, " +
            " additional_information.description " +
            " FROM " + CLIENT_TABLE_NAME + " INNER JOIN " + MONETARY_TRANSACTION_TABLE_NAME + " "
            + " ON client.client_id = monetary_transaction.client_id " +
            " INNER JOIN " + DEBTOR_TABLE_NAME + " "
            + " ON monetary_transaction.debtor_id = debtor.debtor_id " +
            " INNER JOIN " + INVOICES_TABLE_NAME + " "
            + " ON monetary_transaction.invoice_id = invoice.invoice_id " +
            " INNER JOIN " + ADDITIONAL_INFORMATION_TABLE_NAME + " "
            + " ON invoice.invoice_id = additional_information.invoice_id " +
            " LEFT OUTER JOIN " + PAYMENT_TYPES_TABLE_NAME + " "
            + " ON monetary_transaction.payment_type_id = payment_types.payment_type_id " +
            " WHERE monetary_transaction.date >= %s " +
            " AND monetary_transaction.date <= %s " +
            " AND client.organization_id in ( %s ) " +
            " AND monetary_transaction.transaction_type in ('PA','RA') " +
            " AND additional_information.source = 'I' " +
            " AND additional_information.type = 'Loan' " +
            " ORDER BY client.organization_id, date, client_id, transaction_type";

    public static final String TRUST_ORGS_AND_CLIENTS = "SELECT trust.trust_id, trust.name as trustName, trust.account_number, " +
            " organization.organization_id, organization.name as orgName," +
            " client.client_id, client.name AS clientName, client.client_identifier" +
            " FROM " + TRUST_TABLE_NAME + " INNER JOIN " + ORGANIZATION_TABLE_NAME + " "
            + " ON trust.trust_id = organization.trust_id" +
            " INNER JOIN " + CLIENT_TABLE_NAME + " ON organization.organization_id = client.organization_id" +
            " WHERE organization.active <> 'f' " +
            " AND client.active <> 'f' " +
            " AND trust.active <> 'f' " +
            " ORDER BY trust.name, organization.name, client.name";

    public static final String GET_ORGANIZATION_INFO = "SELECT organization.organization_id, organization.name" +
            " FROM " + TRUST_TABLE_NAME + " INNER JOIN " + ORGANIZATION_TABLE_NAME + " "
            + " ON trust.trust_id = organization.trust_id" +
            " INNER JOIN " + CLIENT_TABLE_NAME + " ON organization.organization_id = client.organization_id" +
            " WHERE organization.active <> 'f' " +
            " AND client.active <> 'f' " +
            " AND trust.active <> 'f' " +
            " ORDER BY organization.name, client.name";


    public static final String GET_ORGANIZATION_INFO_QUICKBOOK_BALANCING = "SELECT organization.organization_id, organization.name " +
            "FROM organization INNER JOIN client " +
            "ON organization.organization_id = client.organization_id INNER JOIN " +
            "accounting_type ON client.accounting_type = accounting_type.accounting_type_id " +
            "WHERE organization.active<> 'f' " +
            "AND client.active <> 'f' " +
            "AND accounting_type.active <> 'f' " +
            "ORDER BY organization.name, client.name";

    public static final String GET_ORGANIZATIONS = "SELECT *" +
            " FROM " + ORGANIZATION_TABLE_NAME +
            " WHERE organization.active <> 'f' and segment_id='2000009'" +
            " ORDER BY organization.name";


    public static final String ORG_TRUSTS = "SELECT trust.trust_id, trust.name as trustName, " +
            " trust.account_number, " +
            " organization.organization_id, organization.name as orgName, " +
            " client.client_id, client.name AS clientName, client.client_identifier " +

            " FROM " + TRUST_TABLE_NAME + " INNER JOIN " + ORGANIZATION_TABLE_NAME + " "
            + " ON trust.trust_id = organization.trust_id " +
            " INNER JOIN " + CLIENT_TABLE_NAME + " ON organization.organization_id = client.organization_id " +
            " WHERE organization.active <> 'f' " +
            " AND client.active <> 'f' " +
            " AND trust.active <> 'f' " +
            " ORDER BY organization.name, client.name";

    public static final String TRANSACTIONS_MATCHING_ORG_AND_AMOUNT = "SELECT monetary_transaction.date, monetary_transaction.transaction_type, " +
            " monetary_transaction.amount, monetary_transaction.monetary_transaction_id, " +
            " monetary_transaction.reversing_id, monetary_transaction.debtor_id, " +
            " debtor.first_name, debtor.last_name, " +
            " monetary_transaction.active, " +
            " client.organization_id, client.client_id, client.client_identifier, " +
            " payment_types.type_name, invoice.client_account_number, " +
            " additional_information.description " +

            " FROM " + CLIENT_TABLE_NAME + " INNER JOIN " + MONETARY_TRANSACTION_TABLE_NAME + " "
            + " ON client.client_id = monetary_transaction.client_id " +
            " INNER JOIN " + DEBTOR_TABLE_NAME + " "
            + " ON monetary_transaction.debtor_id = debtor.debtor_id " +
            " INNER JOIN " + INVOICES_TABLE_NAME + " "
            + " ON monetary_transaction.invoice_id = invoice.invoice_id " +
            " INNER JOIN " + ADDITIONAL_INFORMATION_TABLE_NAME + " "
            + " ON invoice.invoice_id = additional_information.invoice_id " +
            " LEFT OUTER JOIN " + PAYMENT_TYPES_TABLE_NAME + " "
            + " ON monetary_transaction.payment_type_id = payment_types.payment_type_id " +

            " WHERE date >= %s " +
            " AND date <= %s " +
            " AND client.organization_id = %s " +
            " AND transaction_type in ('PA','RA') " +
            " AND monetary_transaction.active <> 'f' " +
            " AND amount = %s ";

    public static final String DOES_DEBTOR_BELONG_TO_ORG = " SELECT client.organization_id " +
            " FROM debtor INNER JOIN " + CLIENT_TABLE_NAME + " "
            + " ON debtor.client_id = client.client_id " +
            //" WHERE debtor.debtor_id = '" & debtorID.Trim.PadLeft(10, "0") & "'
            " WHERE debtor.debtor_id = %s ";

    public static final String RETURN_CLIENT_ID = "SELECT client_id " +
            " FROM " + DEBTOR_TABLE_NAME + " "
            + "WHERE debtor_id = %s ";

    public static final String RETURN_DEBTOR_NAME = "SELECT first_name, last_name " +
            " FROM " + DEBTOR_TABLE_NAME + " "
            + " WHERE debtor_id = %s ";

    public static final String RETURN_MATCH_IDS_USING_RP_SOLUTION_DEBTORID = " SELECT debtor_id FROM " + DEBTOR_TABLE_NAME + " "
            + " WHERE match_id = (SELECT match_id FROM " + DEBTOR_TABLE_NAME + " WHERE debtor_id = %s ) ";


    public static final String ADD_RP_TRANSACTIONS_DATA = " SELECT AMOUNT, ACCOUNT, POCKET_GROUP, TRANS_TYPE " +
            " FROM " + DOC_TABLE_NAME + " "
            + " WHERE JOB_KEY like '%s' " +
            " AND POCKET_GROUP like '%s' " +
            " AND (REJECT is null or  REJECT <> 'R') " +
            " ORDER BY ACCOUNT, AMOUNT, POCKET_GROUP ";

    public static final String DAILY_RP_TOTALS = "SELECT PROC_DATE, POCKET_GROUP, sum(NUM_CHECKS) as numCks, sum(TOTAL_AMOUNT) as ttlCks " +
            " FROM " + DEPOSIT_BUNDLE_SUM_TABLE_NAME + " "
            // ????????? Je ne sais pas comment ecrire le like ci dessous
            //" WHERE PROC_DATE like '" & DateAndTime.FormatDate(dte, "yyyyMMdd") & "%'
            // ????????? Je ne sais pas comment ecrire le like ci dessous
            //" AND POCKET_GROUP like '%" & objOrg.ObjTrust.TrustAcctNum & "'
            + " GROUP BY PROC_DATE, POCKET_GROUP ";

    public static final String USER_QUERY = "select user_id, username, password, first_name, last_name, email " +
            "FROM " + USERS_TABLE_NAME + " WHERE username = '%s' and active = 't' ";

    public static final String USER_ROLES_QUERY = "select role_id" +
            " FROM " + USER_ROLES_TABLE_NAME + " WHERE user_id='%s' and active = 't'";

    public static final String ORGANIZATIONS_QUERY = "select organization.organization_id, organization.name " +
            "FROM " + ORGANIZATION_TABLE_NAME + "  "
            + " ORDER BY organization.name";


    //Healthcare querries
    public static final String HEALTH_GetInventory = "select DATEPART(year, placement_date), " +
            "DATEPART(month, placement_date),count(*),sum(original_balance), " +
            "cast(round(sum(original_balance)/count(*),2) as numeric(36,2)) " +
            "from invoice " + "where client_id='%s' " +
            "and placement_date>='%s' " +
            "and placement_date<'%s' " +
            "group by DATEPART(year, placement_date),DATEPART(month, placement_date) " +
            "order by DATEPART(year, placement_date) desc,DATEPART(month, placement_date) desc";



    /*public static final String HEALTH_getAdjustments= "select "+
                "DATEPART(year, d.placement_date), "+
                "DATEPART(month, d.placement_date), "+
                "sum( "+
                "CASE "+
                "WHEN mt.transaction_type = 'AC' THEN mt.amount "+
                "WHEN mt.transaction_type = 'AD' THEN mt.amount*-1 "+
                "END),d.financial_class "+
                "from monetary_transaction mt"+
                " left join debtor_invoice_view d on d.invoice_id=mt.invoice_id "+
                " where mt.client_id = '%s' "+
                " and mt.transaction_type in ('AC','AD') "+
                " and mt.status!='V' "+
                " and DATEPART(year, d.placement_date)= %s"
                + " and DATEPART(month, d.placement_date)= %s"
                +" group by DATEPART(year, d.placement_date),DATEPART(month, d.placement_date),d.financial_class";*/


    public static final String HEALTH_getAdjustments = "select " +
            "DATEPART(year, d.placement_date), " +
            "DATEPART(month, d.placement_date), " +
            "count(MT.transaction_type), " +
            "sum( " +
            "CASE " +
            "WHEN mt.transaction_type = 'AC' THEN mt.amount " +
            "WHEN mt.transaction_type = 'AD' THEN mt.amount*-1 " +
            "END),d.financial_class " +
            "from monetary_transaction mt" +
            " left join debtor_invoice_view d on d.invoice_id=mt.invoice_id " +
            " where mt.client_id = '%s' " +
            " and mt.transaction_type in ('AC','AD') " +
            " and mt.status!='V' " +
            " and DATEPART(year, d.placement_date)= %s "
            + "and DATEPART(month, d.placement_date)= %s "
            + "group by DATEPART(year, d.placement_date),DATEPART(month, d.placement_date),d.financial_class";


    public static final String HEALTH_GetClientReturns = "select DATEPART(year, placement_date), " +
            "DATEPART(month, placement_date),count(*),sum(balance), " +
            "financial_class " + "from invoice " + "where client_id='%s' and status_code ='R' " +
            "and activity_code not in ('100240') " +
            "and DATEPART(year, placement_date)=%s " +
            "and DATEPART(month, placement_date)= %s "
            + " group by DATEPART(year, placement_date),DATEPART(month, placement_date), financial_class";


    public static String healthGetClaimsDenied(String cliid, String yrpt, String mopt) {

        String sqlStr = "select DATEPART(year, i.placement_date), " +
                "DATEPART(month, i.placement_date),count(*),sum(i.original_balance) " +
                "from invoice i " +
                "where i.client_id='" + cliid + "' " +
                "and DATEPART(year, i.placement_date)=" + yrpt + " " +
                "and DATEPART(month, i.placement_date)=" + mopt + " " +
                "and(select count(*) " +
                "from invoice_activity ia " +
                "where ia.invoice_id=i.invoice_id " +
                "and ia.new_activity_code in ('100330','100331','100431'))>0 ";

        sqlStr += "group by DATEPART(year, i.placement_date),DATEPART(month, i.placement_date) ";

        return sqlStr.trim();
    }

    public static String healthGetClientRecalls(String cliid, String yrpt, String mopt) {

        String sqlStr = "select DATEPART(year, placement_date), " +
                "DATEPART(month, placement_date),count(*),sum(balance) " +
                "from invoice " + "where client_id='" + cliid + "' " +
                "and status_code ='R' " + "and activity_code in ('100240') " +
                "and DATEPART(year, placement_date)=" + yrpt + " " +
                "and DATEPART(month, placement_date)=" + mopt + " ";

        sqlStr += "group by DATEPART(year, placement_date),DATEPART(month, placement_date) ";

        return sqlStr.trim();
    }

    public static String healthGetClientMonthRecall(String cliid, String yrpt, String mopt) {

        String sqlStr = "select count(*),isnull(sum(balance),0) " + "from invoice " +
                "where client_id='" + cliid + "' " + "and status_code ='R' " +
                "and activity_code in ('100240') " +
                "and DATEPART(year, close_date)=" + yrpt + " " +
                "and DATEPART(month, close_date)=" + mopt + " " +
                "and DATEPART(year, placement_date)=" + yrpt + " " +
                "and DATEPART(month, placement_date)=" + mopt + " ";

        return sqlStr.trim();
    }

    public static final String HEALTH_GetMonthCollections = "select " +
            "DATEPART(year, d.placement_date), " +
            "DATEPART(month, d.placement_date), " +
            "sum( " +
            "CASE " +
            "WHEN mt.transaction_type = 'RD' THEN mt.amount*-1 " +
            "WHEN mt.transaction_type = 'PD' THEN mt.amount " +
            "WHEN mt.transaction_type = 'RA' THEN mt.amount*-1 " +
            "WHEN mt.transaction_type = 'PA' THEN mt.amount " +
            "END) " +
            "from monetary_transaction mt " +
            "left join debtor_invoice_view d on d.invoice_id=mt.invoice_id " +
            "where mt.client_id='%s' " +
            "and mt.transaction_type in ('PA','PD','RD','RA') " +
            "and mt.status!='V' " +
            "and DATEPART(year, d.placement_date)=%s " +
            "and DATEPART(month, d.placement_date)=%s " +
            "and DATEPART(year, mt.date)= %s " +
            "and DATEPART(month, mt.date)= %s "
            + " group by DATEPART(year, d.placement_date),DATEPART(month, d.placement_date) ";


    public static String healthGetLTDCollections(int cliid, String yrpt, String mopt, String toDt) {

        String sqlStr = "select " +
                "DATEPART(year, d.placement_date), " +
                "DATEPART(month, d.placement_date), " +
                "sum( " +
                "CASE " +
                "WHEN mt.transaction_type = 'RD' THEN mt.amount*-1 " +
                "WHEN mt.transaction_type = 'PD' THEN mt.amount " +
                "WHEN mt.transaction_type = 'RA' THEN mt.amount*-1 " +
                "WHEN mt.transaction_type = 'PA' THEN mt.amount " +
                "END) " +
                "from monetary_transaction mt " +
                "left join debtor_invoice_view d on d.invoice_id=mt.invoice_id " +
                "where mt.client_id ='" + cliid + "' " +
                "and mt.transaction_type in ('PA','PD','RD','RA') " +
                "and mt.status!='V' " +
                "and DATEPART(year, d.placement_date)=" + yrpt + " " +
                "and DATEPART(month, d.placement_date)=" + mopt + " " +
                "and mt.date< '" + DateUtils.getSqlDate(toDt) + "' " +
                "group by DATEPART(year, d.placement_date),DATEPART(month, d.placement_date) ";

        return sqlStr.trim();
    }

    public static String healthGetAverageAge(int cliid, String yrpt, String mopt) {

        String sqlStr = "select isnull(sum(DATEDIFF(DAY,ai.description,d.placement_date))/count(*),0) " +
                "from additional_information ai " +
                "left join debtor_invoice_view d on ai.invoice_id=d.invoice_id " +
                "where d.client_id = '" + cliid + "' " +
                "and d.status_code !='R' " +
                "and ai.source='I' " +
                "and ai.type='MedInfo' " +
                "and ai.sub_type='dischdte' " +
                "and DATEPART(year, d.placement_date)=" + yrpt + " " +
                "and DATEPART(month, d.placement_date)=" + mopt + " " +
                "and ISDATE(ai.description)!=0";

        return sqlStr.trim();
    }

    public static String HEALTH_GetPIF = "select count(*) " + "from debtor_invoice_view d " +
            "where d.client_id = '%s' " +
            "and d.status_code !='R' " +
            "and DATEPART(year, d.placement_date)= %s " +
            "and DATEPART(month, d.placement_date)= %s " +
            "and balance<0.01";


    public static String healthGetReportDetails(int cliid, String fromDt, String toDt) {

        return "select d.debtor_id,d.client_account_number, " +
                "d.client_invoice_number,d.client_id,d.client_name, " +
                "CONVERT(VARCHAR(7), d.placement_date, 126) yyyy_mm, " +
                "d.placement_date,d.original_balance,d.balance, " +
                "isnull(convert(varchar(12),d.last_payment_date,101),''), " +
                "isnull((select sum( " +
                "CASE " +
                "WHEN mt.transaction_type = 'AC' THEN mt.amount*-1 " +
                "WHEN mt.transaction_type = 'AD' THEN mt.amount " +
                "END) " +
                "from monetary_transaction mt " +
                "where mt.invoice_id=d.invoice_id " +
                "and mt.transaction_type in ('AC','AD') " +
                "and mt.status='P'),0) as adjustments, " +
                "isnull((select sum( " +
                "CASE " +
                "WHEN mt.transaction_type = 'RA' THEN mt.amount*-1 " +
                "WHEN mt.transaction_type = 'PA' THEN mt.amount " +
                "END) " +
                "from monetary_transaction mt " +
                "where mt.invoice_id=d.invoice_id " +
                "and mt.transaction_type in ('RA','PA') " +
                "and mt.status='P'),0) as 'agency_payments', " +
                "isnull((select sum( " +
                "CASE " +
                "WHEN mt.transaction_type = 'RD' THEN mt.amount*-1 " +
                "WHEN mt.transaction_type = 'PD' THEN mt.amount " +
                "END) " +
                "from monetary_transaction mt " +
                "where mt.invoice_id=d.invoice_id " +
                "and mt.transaction_type in ('RD','PD')),0) as 'client_payments', " +
                "d.activity_code,a.description,d.status_code, " + "CASE " +
                "WHEN d.status_code != 'R' THEN 'Active' " +
                "ELSE 'Inactive' " + "END as status,isnull(d.close_date,'') " +
                "from debtor_invoice_view d " +
                "left join activity a on a.activity_id=d.activity_code " +
                "where d.client_id = '" + cliid + "' " +
                "and d.placement_date >= '" + DateUtils.getSqlDate(fromDt) + "' " +
                "and d.placement_date < '" + DateUtils.getSqlDate(toDt) + "'";
    }


    /*Params: orgId*/
    public static final String GET_CLI_BY_ORG = "select client_id,name " + "from client " +
            "where organization_id=%s" +
            " and active='t' " +
            " and name not like '%%DO NOT USE%%'";


    public static final String GET_ACTIVE_CODES = "select distinct a.description,d.activity_code " +
            "from debtor_invoice_view d " +
            "left join activity a on d.activity_code=a.activity_id " +
            "where d.organization_id='%s' " +
            "and d.status_code !='R' ";


    public static final String GET_FIN_CLAS = "select distinct isnull(financial_class,'') " +
            "from debtor_invoice_view " +
            "where organization_id='%s' " +
            "and placement_date<'%s' ";


    /*public static HashMap<String, ArrayList<String>> getFinClasOcc(List<String> finCl, int orgid, String toDt) {

        if (finCl == null || finCl.isEmpty())
            return new HashMap<>();
        else {

            HashMap<String,ArrayList<String>> statements=new HashMap<>();

            for (String value : finCl) {
                String endDt=DateUtils.addDaysToDate(toDt,1);
                String toDtP = endDt;
                String stDtP = DateUtils.minusMonthsFromDate(toDtP,1);

                ArrayList<String> qs = new ArrayList<>();

                for (int j = 0; j < 7; j++) {

                    String stt = "select count(*) " + "from debtor_invoice_view " +
                            "where organization_id='" + orgid + "'"
                            + "and (status_code !='R' " +
                            "or (close_date >='" + DateUtils.getSqlDate(toDt)+ "' " +
                            "and close_date <'" + DateUtils.getSqlDate(endDt) + "' )) ";

                    if (j < 6) {
                        stt += "and placement_date >= '" + DateUtils.getSqlDate(stDtP) + "' " +
                                "and placement_date < '" + DateUtils.getSqlDate(toDtP) + "' ";
                    } else {
                        stt += "and placement_date<'" + DateUtils.getSqlDate(toDtP) + "' ";
                    }

                    stt +="and financial_class='" + value.trim() + "' ";

                    qs.add(stt);
                    toDtP = DateUtils.minusMonthsFromDate(toDtP,1);
                    stDtP = DateUtils.minusMonthsFromDate(stDtP,1);

                    //System.out.println("DTTT: "+ toDt+":::"+stDt+":::"+prevDt);
                    System.out.println(j+":\n "+stt);
                }
                statements.put(value,qs);
            }
            return statements;
        }
    }*/

    public static HashMap<String, ArrayList<String>> getFinClasOcc(List<String> finCl, int orgid, String toDt) {

        if (finCl == null || finCl.isEmpty())
            return new HashMap<>();
        else {

            HashMap<String, ArrayList<String>> statements = new HashMap<>();

            for (String value : finCl) {
                String endDt = DateUtils.addDaysToDate(toDt, 1);
                String toDtP = endDt;
                String stDtP = DateUtils.minusMonthsFromDate(toDtP, 1);

                ArrayList<String> qs = new ArrayList<>();

                for (int j = 0; j < 7; j++) {

                    String stt = "select count(*) " + "from debtor_invoice_view " +
                            "where organization_id='" + orgid + "'"
                            + "and (status_code !='R') ";

                    if (j < 6) {
                        stt += "and placement_date >= '" + DateUtils.getSqlDate(stDtP) + "' " +
                                "and placement_date < '" + DateUtils.getSqlDate(toDtP) + "' ";
                    } else {
                        stt += "and placement_date<'" + DateUtils.getSqlDate(toDtP) + "' ";
                    }

                    stt += "and financial_class='" + value.trim() + "' ";

                    qs.add(stt);
                    toDtP = DateUtils.minusMonthsFromDate(toDtP, 1);
                    stDtP = DateUtils.minusMonthsFromDate(stDtP, 1);

                    //System.out.println("DTTT: "+ toDt+":::"+stDt+":::"+prevDt);
                    System.out.println(j + ":\n " + stt);
                }
                statements.put(value, qs);
            }
            return statements;
        }
    }

    /*public static HashMap<String, ArrayList<String>> getFinClasAmt(List<String> finCl, int orgid, String toDt) {


        if(finCl==null || finCl.isEmpty())
            return new HashMap<>();
        else{

            HashMap<String,ArrayList<String>> statements=new HashMap<>();


            for (String value : finCl) {
                String endDt=DateUtils.addDaysToDate(toDt,1);
                String toDtP = endDt;
                String stDtP = DateUtils.minusMonthsFromDate(toDtP,1);

                ArrayList<String> qs=new ArrayList<>();


                for (int j = 0; j < 7; j++) {


                    String stt = "select "+
                            "sum(d.original_balance)- "+
                            "isnull((select SUM(case(transaction_type) "+
                            "when 'PD' then mt.amount "+
                            "when 'PA' then mt.amount "+
                            "when 'AC' then mt.amount "+
                            "when 'RA' then -1*mt.amount "+
                            "when 'RD' then -1*mt.amount "+
                            "when 'AD' then -1*mt.amount end) "+
                            "from monetary_transaction mt "+
                            "left join client c on mt.client_id=c.client_id "+
                            "left join invoice i on mt.invoice_id=i.invoice_id "+
                            "where c.organization_id='"+orgid+"' "+
                            "and mt.transaction_type not in ('NB') "+
                            "and mt.date>i.placement_date "+
                            "and i.financial_class='"+value+"' "+
                            "and mt.status='P' ";
                    if (j < 6) {
                        stt += "and i.placement_date>='" + DateUtils.getSqlDate(stDtP) + "' "+
                                "and i.placement_date<'" + DateUtils.getSqlDate(toDtP) + "' ";
                    }
                    else {
                        stt += "and i.placement_date<'" + DateUtils.getSqlDate(toDtP) + "' ";
                    }
                    stt+="and (i.status_code !='R' "+
                            "or (i.close_date >='"+DateUtils.getSqlDate(toDt)+ "' "+
                            "and i.close_date<'" + DateUtils.getSqlDate(endDt) + "' "+
                            "and i.close_date is not null))),0) "+
                            "from debtor_invoice_view d "+
                            "where d.organization_id='" + orgid + "' "+
                            "and (d.status_code !='R' "+
                            "or (d.close_date>= '"+DateUtils.getSqlDate(toDt)+ "' "+
                            "and d.close_date< '"+DateUtils.getSqlDate(endDt)+"' )) ";
                    if (j < 6) {
                        stt += "and placement_date>='" + DateUtils.getSqlDate(stDtP)+ "' "+
                                "and placement_date<'" + DateUtils.getSqlDate(toDtP)+ "' ";
                    }
                    else {
                        stt += "and placement_date<'" + DateUtils.getSqlDate(toDtP)+ "' ";
                    }
                    stt += "and financial_class='" + value + "' ";

                    qs.add(stt);

                    toDtP = DateUtils.minusMonthsFromDate(toDtP,1);
                    stDtP = DateUtils.minusMonthsFromDate(stDtP,1);

                    System.out.println(j+":\n "+stt);

                }

                statements.put(value,qs);


            }

            //System.out.println(Json.encodePrettily(statements));
            return statements;
        }
    }*/

    public static HashMap<String, ArrayList<String>> getFinClasAmt(List<String> finCl, int orgid, String toDt) {


        if (finCl == null || finCl.isEmpty())
            return new HashMap<>();
        else {

            HashMap<String, ArrayList<String>> statements = new HashMap<>();


            for (String value : finCl) {
                String endDt = DateUtils.addDaysToDate(toDt, 1);
                String toDtP = endDt;
                String stDtP = DateUtils.minusMonthsFromDate(toDtP, 1);

                ArrayList<String> qs = new ArrayList<>();


                for (int j = 0; j < 7; j++) {


                    String stt = "select " +
                            "sum(d.original_balance)- " +
                            "isnull((select SUM(case(transaction_type) " +
                            "when 'PD' then mt.amount " +
                            "when 'PA' then mt.amount " +
                            "when 'AC' then mt.amount " +
                            "when 'RA' then -1*mt.amount " +
                            "when 'RD' then -1*mt.amount " +
                            "when 'AD' then -1*mt.amount end) " +
                            "from monetary_transaction mt " +
                            "left join client c on mt.client_id=c.client_id " +
                            "left join invoice i on mt.invoice_id=i.invoice_id " +
                            "where c.organization_id='" + orgid + "' " +
                            "and mt.transaction_type not in ('NB') " +
                            "and mt.date>i.placement_date " +
                            "and i.financial_class='" + value + "' " +
                            "and mt.status='P' ";
                    if (j < 6) {
                        stt += "and i.placement_date>='" + DateUtils.getSqlDate(stDtP) + "' " +
                                "and i.placement_date<'" + DateUtils.getSqlDate(toDtP) + "' ";
                    } else {
                        stt += "and i.placement_date<'" + DateUtils.getSqlDate(toDtP) + "' ";
                    }
                    stt += "and i.status_code !='R' " +
                            "),0) " +
                            "from debtor_invoice_view d " +
                            "where d.organization_id='" + orgid + "' " +
                            "and d.status_code !='R' ";
                    if (j < 6) {
                        stt += "and placement_date>='" + DateUtils.getSqlDate(stDtP) + "' " +
                                "and placement_date<'" + DateUtils.getSqlDate(toDtP) + "' ";
                    } else {
                        stt += "and placement_date<'" + DateUtils.getSqlDate(toDtP) + "' ";
                    }
                    stt += "and financial_class='" + value + "' ";

                    qs.add(stt);

                    toDtP = DateUtils.minusMonthsFromDate(toDtP, 1);
                    stDtP = DateUtils.minusMonthsFromDate(stDtP, 1);

                    System.out.println(j + ":\n " + stt);

                }

                statements.put(value, qs);


            }

            //System.out.println(Json.encodePrettily(statements));
            return statements;
        }
    }

    public static HashMap<String, String> getOverResPlacements(List<String> finCl, int orgid, String fromDt, String endDt) {


        if (finCl == null || finCl.isEmpty())
            return new HashMap<>();
        else {

            HashMap<String, String> statements = new HashMap<>();

            finCl.forEach(s -> {


                String toDt = DateUtils.addDaysToDate(endDt, 1);
                String staMoDt = DateUtils.minusMonthsFromDate(setDateTo1fstDay(toDt), 1);
                String prevmoDt1 = DateUtils.minusMonthsFromDate(setDateTo1fstDay(staMoDt), 1);
                String prevmoDt2 = DateUtils.minusMonthsFromDate(setDateTo1fstDay(staMoDt), 2);
                String threeMoDt = DateUtils.minusMonthsFromDate(setDateTo1fstDay(staMoDt), 3);
                String staLasYrDt = DateUtils.minusMonthsFromDate(DateUtils.setDateTo1fstDayAndMonth(toDt.split("-")[0]), 12);
                String lastYrToDt;
                String thisYrDt = DateUtils.toDayDate();

                lastYrToDt = staLasYrDt.split("-")[0] + "-" + toDt.split("-")[1] + "-" + toDt.split("-")[2];

                if (Integer.valueOf(thisYrDt.split("-")[1]) == 1) {
                    thisYrDt = DateUtils.setDateTo1fstDayAndMonth(fromDt.split("-")[0]);
                    staLasYrDt = DateUtils.minusMonthsFromDate(staLasYrDt, 12);
                } else {
                    thisYrDt = DateUtils.setDateTo1fstDayAndMonth(toDt.split("-")[0]);
                }


                //String sqlStr = "select d.organization_id,d.financial_class, "+
                String sqlStr = "select isnull((select sum(div.original_balance) " +
                        "from debtor_invoice_view div " +
                        "where div.organization_id=d.organization_id " +
                        "and div.financial_class=d.financial_class " +
                        "and div.placement_date>= '" + DateUtils.getSqlDate(staMoDt) + "' " +
                        "and div.placement_date< '" + DateUtils.getSqlDate(toDt) + "'),0), " +
                        "isnull((select sum(div.original_balance) " +
                        "from debtor_invoice_view div " +
                        "where div.organization_id=d.organization_id " +
                        "and div.financial_class=d.financial_class " +
                        "and div.placement_date>= '" + DateUtils.getSqlDate(prevmoDt1) + "' " +
                        "and div.placement_date< '" + DateUtils.getSqlDate(staMoDt) + "'),0), " +
                        "isnull((select sum(div.original_balance) " +
                        "from debtor_invoice_view div " +
                        "where div.organization_id=d.organization_id " +
                        "and div.financial_class=d.financial_class " +
                        "and div.placement_date>= '" + DateUtils.getSqlDate(prevmoDt2) + "' " +
                        "and div.placement_date< '" + DateUtils.getSqlDate(prevmoDt1) + "'),0), " +
                        "isnull((select sum(div.original_balance) " +
                        "from debtor_invoice_view div " +
                        "where div.organization_id=d.organization_id " +
                        "and div.financial_class=d.financial_class " +
                        "and div.placement_date>= '" + DateUtils.getSqlDate(threeMoDt) + "' " +
                        "and div.placement_date< '" + DateUtils.getSqlDate(prevmoDt2) + "'),0), " +
                        "isnull((select sum(div.original_balance)/3 " +
                        "from debtor_invoice_view div " +
                        "where div.organization_id=d.organization_id " +
                        "and div.financial_class=d.financial_class " +
                        "and div.placement_date>= '" + DateUtils.getSqlDate(threeMoDt) + "' " +
                        "and div.placement_date< '" + DateUtils.getSqlDate(staMoDt) + "'),0), " +
                        "isnull((select sum(div.original_balance) " +
                        "from debtor_invoice_view div " +
                        "where div.organization_id=d.organization_id " +
                        "and div.financial_class=d.financial_class " +
                        "and div.placement_date>= '" + DateUtils.getSqlDate(thisYrDt) + "' " +
                        "and div.placement_date< '" + DateUtils.getSqlDate(toDt) + "'),0), " +
                        "isnull((select sum(div.original_balance) " +
                        "from debtor_invoice_view div " +
                        "where div.organization_id=d.organization_id " +
                        "and div.financial_class=d.financial_class " +
                        "and div.placement_date>= '" + DateUtils.getSqlDate(staLasYrDt) + "' " +
                        "and div.placement_date< '" + DateUtils.getSqlDate(lastYrToDt) + "' ),0) " +
                        "from debtor_invoice_view d " +
                        "where d.organization_id=" + orgid + " " +
                        "and d.financial_class='" + s + "' " +
                        "group by d.organization_id,d.financial_class ";

                statements.put(s, sqlStr);

                System.out.println("MDATE: " + sqlStr + "\n");


            });

            return statements;


        }


    }


    public static HashMap<String, String> getOverResCollections(List<String> finCl, int orgid, String fromDt, String endDt) {


        if (finCl == null || finCl.isEmpty())
            return new HashMap<>();
        else {

            HashMap<String, String> statements = new HashMap<>();

            finCl.forEach(s -> {


                String toDt = DateUtils.addDaysToDate(endDt, 1);
                String staMoDt = DateUtils.minusMonthsFromDate(setDateTo1fstDay(toDt), 1);
                String prevmoDt1 = DateUtils.minusMonthsFromDate(setDateTo1fstDay(staMoDt), 1);
                String prevmoDt2 = DateUtils.minusMonthsFromDate(setDateTo1fstDay(staMoDt), 2);
                String threeMoDt = DateUtils.minusMonthsFromDate(setDateTo1fstDay(staMoDt), 3);
                String staLasYrDt = DateUtils.minusMonthsFromDate(DateUtils.setDateTo1fstDayAndMonth(toDt.split("-")[0]), 12);
                String lastYrToDt;
                String thisYrDt = DateUtils.toDayDate();

                lastYrToDt = staLasYrDt.split("-")[0] + "-" + toDt.split("-")[1] + "-" + toDt.split("-")[2];

                if (Integer.valueOf(thisYrDt.split("-")[1]) == 1) {
                    thisYrDt = DateUtils.setDateTo1fstDayAndMonth(fromDt.split("-")[0]);
                    staLasYrDt = DateUtils.minusMonthsFromDate(staLasYrDt, 12);
                } else {
                    thisYrDt = DateUtils.setDateTo1fstDayAndMonth(toDt.split("-")[0]);
                }


                //String sqlStr = "select d.organization_id,d.financial_class, "+
                String sqlStr = "select isnull((select sum(CASE " +
                        "WHEN montran.transaction_type = 'PD' THEN montran.amount " +
                        "WHEN montran.transaction_type = 'PA' THEN montran.amount " +
                        "WHEN montran.transaction_type = 'RD' THEN montran.amount*-1 " +
                        "WHEN montran.transaction_type = 'RA' THEN montran.amount*-1 " +
                        "end) " +
                        "from monetary_transaction montran " +
                        "left join debtor_invoice_view div on montran.invoice_id=div.invoice_id " +
                        "where div.organization_id=d.organization_id " +
                        "and div.financial_class=d.financial_class " +
                        "and DATEPART(year, montran.date)= " + staMoDt.split("-")[0] + " " +
                        "and DATEPART(month, montran.date)= " + staMoDt.split("-")[1] + " " +
                        "and montran.transaction_type in ('PA','PD','RA','RD')),0), " +
                        "isnull((select sum(CASE " +
                        "WHEN montran.transaction_type = 'PD' THEN montran.amount " +
                        "WHEN montran.transaction_type = 'PA' THEN montran.amount " +
                        "WHEN montran.transaction_type = 'RD' THEN montran.amount*-1 " +
                        "WHEN montran.transaction_type = 'RA' THEN montran.amount*-1 " +
                        "end) " +
                        "from monetary_transaction montran " +
                        "left join debtor_invoice_view div on montran.invoice_id=div.invoice_id " +
                        "where div.organization_id=d.organization_id " +
                        "and div.financial_class=d.financial_class " +
                        "and DATEPART(year, montran.date)= " + prevmoDt1.split("-")[0] + " " +
                        "and DATEPART(month, montran.date)= " + prevmoDt1.split("-")[1] + " " +
                        "and montran.transaction_type in ('PA','PD','RA','RD')),0), " +
                        "isnull((select sum(CASE " +
                        "WHEN montran.transaction_type = 'PD' THEN montran.amount " +
                        "WHEN montran.transaction_type = 'PA' THEN montran.amount " +
                        "WHEN montran.transaction_type = 'RD' THEN montran.amount*-1 " +
                        "WHEN montran.transaction_type = 'RA' THEN montran.amount*-1 " +
                        "end) " +
                        "from monetary_transaction montran " +
                        "left join debtor_invoice_view div on montran.invoice_id=div.invoice_id " +
                        "where div.organization_id=d.organization_id " +
                        "and div.financial_class=d.financial_class " +
                        "and DATEPART(year, montran.date)= " + prevmoDt2.split("-")[0] + " " +
                        "and DATEPART(month, montran.date)= " + prevmoDt2.split("-")[1] + " " +
                        "and montran.transaction_type in ('PA','PD','RA','RD')),0), " +
                        "isnull((select sum(CASE " +
                        "WHEN montran.transaction_type = 'PD' THEN montran.amount " +
                        "WHEN montran.transaction_type = 'PA' THEN montran.amount " +
                        "WHEN montran.transaction_type = 'RD' THEN montran.amount*-1 " +
                        "WHEN montran.transaction_type = 'RA' THEN montran.amount*-1 " +
                        "end) " +
                        "from monetary_transaction montran " +
                        "left join debtor_invoice_view div on montran.invoice_id=div.invoice_id " +
                        "where div.organization_id=d.organization_id " +
                        "and div.financial_class=d.financial_class " +
                        "and DATEPART(year, montran.date)= " + threeMoDt.split("-")[0] + " " +
                        "and DATEPART(month, montran.date)= " + threeMoDt.split("-")[1] + " " +
                        "and montran.transaction_type in ('PA','PD','RA','RD')),0), " +
                        "isnull((select sum(CASE " +
                        "WHEN montran.transaction_type = 'PD' THEN montran.amount " +
                        "WHEN montran.transaction_type = 'PA' THEN montran.amount " +
                        "WHEN montran.transaction_type = 'RD' THEN montran.amount*-1 " +
                        "WHEN montran.transaction_type = 'RA' THEN montran.amount*-1 " +
                        "end)/3 " +
                        "from monetary_transaction montran " +
                        "left join debtor_invoice_view div on montran.invoice_id=div.invoice_id " +
                        "where div.organization_id=d.organization_id " +
                        "and div.financial_class=d.financial_class " +
                        "and montran.date>= '" + DateUtils.getSqlDate(threeMoDt) + "' " +
                        "and montran.date< '" + DateUtils.getSqlDate(staMoDt) + "' " +
                        "and montran.transaction_type in ('PA','PD','RA','RD')),0), " +
                        "isnull((select sum(CASE " +
                        "WHEN montran.transaction_type = 'PD' THEN montran.amount " +
                        "WHEN montran.transaction_type = 'PA' THEN montran.amount " +
                        "WHEN montran.transaction_type = 'RD' THEN montran.amount*-1 " +
                        "WHEN montran.transaction_type = 'RA' THEN montran.amount*-1 " +
                        "end) " +
                        "from monetary_transaction montran " +
                        "left join debtor_invoice_view div on montran.invoice_id=div.invoice_id " +
                        "where div.organization_id=d.organization_id " +
                        "and div.financial_class=d.financial_class " +
                        "and montran.date>= '" + DateUtils.getSqlDate(thisYrDt) + "' " +
                        "and montran.date< '" + DateUtils.getSqlDate(toDt) + "' " +
                        "and montran.transaction_type in ('PA','PD','RA','RD')),0), " +
                        "isnull((select sum(CASE " +
                        "WHEN montran.transaction_type = 'PD' THEN montran.amount " +
                        "WHEN montran.transaction_type = 'PA' THEN montran.amount " +
                        "WHEN montran.transaction_type = 'RD' THEN montran.amount*-1 " +
                        "WHEN montran.transaction_type = 'RA' THEN montran.amount*-1 " +
                        "end) " +
                        "from monetary_transaction montran " +
                        "left join debtor_invoice_view div on montran.invoice_id=div.invoice_id " +
                        "where div.organization_id=d.organization_id " +
                        "and div.financial_class=d.financial_class " +
                        "and montran.date>= '" + DateUtils.getSqlDate(staLasYrDt) + "' " +
                        "and montran.date< '" + DateUtils.getSqlDate(lastYrToDt) + "' " +
                        "and montran.transaction_type in ('PA','PD','RA','RD')),0) " +
                        "from monetary_transaction mt " +
                        "left join debtor_invoice_view d on mt.invoice_id=d.invoice_id " +
                        "where d.organization_id=" + orgid + " " +
                        "and d.financial_class='" + s + "' " +
                        "and mt.transaction_type in ('PA','PD') " +
                        "group by d.organization_id,d.financial_class ";

                statements.put(s, sqlStr);

            });

            return statements;


        }
    }




    /*public static HashMap<String, List<String>> getActiveAct(List<String> actCodes, int orgid, String toDt) {


        if(actCodes==null || actCodes.isEmpty())
            return new HashMap<>();
        else {

            HashMap<String, List<String>> statements = new HashMap<>();

            for (String value : actCodes) {

                String toDtP = DateUtils.addDaysToDate(toDt,1);
                String stDtP = DateUtils.minusMonthsFromDate(toDtP,1);

                ArrayList<String> qs = new ArrayList<>();

                for (int j = 0; j < 7; j++) {

                    String stt= "select isnull(sum(d.original_balance),0) - "+
                            "isnull((select SUM(case(transaction_type) "+
                            "when 'PD' then mt.amount "+
                            "when 'PA' then mt.amount "+
                            "when 'AC' then mt.amount "+
                            "when 'RA' then -1*mt.amount "+
                            "when 'RD' then -1*mt.amount "+
                            "when 'AD' then -1*mt.amount end) "+
                            "from monetary_transaction mt "+
                            "left join client c on mt.client_id=c.client_id "+
                            "left join invoice i on mt.invoice_id=i.invoice_id "+
                            "where c.organization_id='"+orgid+"' "+
                            "and mt.transaction_type not in ('NB') "+
                            "and mt.date>i.placement_date "+
                            "and i.activity_code='"+value+"' "+
                            "and mt.status='P' ";


                    if (j < 6) {
                        stt += "and i.placement_date>='" + DateUtils.getSqlDate(stDtP)+ "' "+
                                "and i.placement_date<'" + DateUtils.getSqlDate(toDtP)+ "' ";
                    }
                    else {
                        stt += "and i.placement_date<'" + DateUtils.getSqlDate(toDtP)+ "' ";
                    }
                    stt+="and i.activity_code='"+value+"'),0) "+
                            "from debtor_invoice_view d "+
                            "where d.organization_id='"+orgid+"' "+
                            "and d.status_code !='R' "+
                            "and d.activity_code='"+value+"' ";

                    if (j < 6){
                        stt += "and placement_date>='"+DateUtils.getSqlDate(stDtP)+"' "+
                                "and placement_date<'"+DateUtils.getSqlDate(toDtP)+"' ";
                    }
                    else {
                        stt += "and placement_date<'"+DateUtils.getSqlDate(toDtP)+"' ";
                    }

                    qs.add(stt);

                    toDtP = DateUtils.minusMonthsFromDate(toDtP,1);
                    stDtP = DateUtils.minusMonthsFromDate(stDtP,1);

                    System.out.println("==>>> Aging / Financial class section: j: "+j);
                    System.out.println(stt);
                }

                statements.put(value,qs);

            }

            return statements;

        }
    }*/

    public static HashMap<String, List<String>> getActiveAct(List<String> actCodes, int orgid, String toDt) {


        if (actCodes == null || actCodes.isEmpty())
            return new HashMap<>();
        else {

            HashMap<String, List<String>> statements = new HashMap<>();

            for (String value : actCodes) {

                String toDtP = DateUtils.addDaysToDate(toDt, 1);
                String stDtP = DateUtils.minusMonthsFromDate(toDtP, 1);

                ArrayList<String> qs = new ArrayList<>();

                for (int j = 0; j < 7; j++) {

                    String stt = "select isnull(sum(d.original_balance),0) - " +
                            "isnull((select SUM(case(transaction_type) " +
                            "when 'PD' then mt.amount " +
                            "when 'PA' then mt.amount " +
                            "when 'AC' then mt.amount " +
                            "when 'RA' then -1*mt.amount " +
                            "when 'RD' then -1*mt.amount " +
                            "when 'AD' then -1*mt.amount end) " +
                            "from monetary_transaction mt " +
                            "left join client c on mt.client_id=c.client_id " +
                            "left join invoice i on mt.invoice_id=i.invoice_id " +
                            "where c.organization_id='" + orgid + "' " +
                            "and mt.transaction_type not in ('NB') " +
                            "and mt.date>i.placement_date " +
                            "and i.activity_code='" + value + "' " +
                            "and mt.status='P' ";


                    if (j < 6) {
                        stt += "and i.placement_date>='" + DateUtils.getSqlDate(stDtP) + "' " +
                                "and i.placement_date<'" + DateUtils.getSqlDate(toDtP) + "' ";
                    } else {
                        stt += "and i.placement_date<'" + DateUtils.getSqlDate(toDtP) + "' ";
                    }
                    stt += "and i.activity_code='" + value + "'),0) " +
                            "from debtor_invoice_view d " +
                            "where d.organization_id='" + orgid + "' " +
                            "and d.status_code !='R' " +
                            "and d.activity_code='" + value + "' ";

                    if (j < 6) {
                        stt += "and placement_date>='" + DateUtils.getSqlDate(stDtP) + "' " +
                                "and placement_date<'" + DateUtils.getSqlDate(toDtP) + "' ";
                    } else {
                        stt += "and placement_date<'" + DateUtils.getSqlDate(toDtP) + "' ";
                    }

                    qs.add(stt);

                    toDtP = DateUtils.minusMonthsFromDate(toDtP, 1);
                    stDtP = DateUtils.minusMonthsFromDate(stDtP, 1);

                    System.out.println("==>>> Aging / Financial class section: j: " + j);
                    System.out.println(stt);
                }

                statements.put(value, qs);

            }

            return statements;

        }
    }


    public static HashMap<String, String> getFinClasAge(List<String> finCl, int orgid, String toDt) {

        if (finCl == null || finCl.isEmpty())
            return new HashMap<>();
        else {

            HashMap<String, String> statements = new HashMap<>();

            toDt = DateUtils.addMonthsToDate(setDateTo1fstDay(toDt), 1);
            String stDt = DateUtils.minusMonthsFromDate(setDateTo1fstDay(toDt), 6);

            for (String value : finCl) {

                String stt = "select DATEPART(year, placement_date), " +
                        "DATEPART(month, placement_date), " +
                        "sum(DATEDIFF(DAY,ai.description,d.placement_date))/count(*) " +
                        "from additional_information ai " +
                        "left join debtor_invoice_view d on ai.invoice_id=d.invoice_id " +
                        "where d.organization_id='" + orgid + "' " +
                        "and d.placement_date>= '" + DateUtils.getSqlDate(stDt) + "' " +
                        "and d.placement_date< '" + DateUtils.getSqlDate(toDt) + "' " +
                        "and d.financial_class='" + value + "' " +
                        "and d.status_code !='R' " +
                        "and ai.source='I' " +
                        "and ai.type='MedInfo' " +
                        "and ai.sub_type='dischdte' " +
                        "and ISDATE(ai.description)!=0 " +
                        "group by DATEPART(year, placement_date),DATEPART(month, placement_date) " +
                        "order by DATEPART(year, placement_date),DATEPART(month, placement_date) ";
                statements.put(value, stt);

            }


            System.out.println(Json.encodePrettily(statements));
            return statements;
        }
    }


    /*public static String[] get180Age(int orgid,String toDt) {


        String endDt=DateUtils.addDaysToDate(toDt,1);


        String sqlStr = "select count(*) as one, "+
                " sum(d.original_balance)-(select SUM(case(transaction_type) "+
                "when 'PD' then mt.amount "+
                "when 'PA' then mt.amount "+
                "when 'AC' then mt.amount "+
                "when 'RA' then -1*mt.amount "+
                "when 'RD' then -1*mt.amount "+
                "when 'AD' then -1*mt.amount end) "+
                "from monetary_transaction mt "+
                "left join client c on mt.client_id=c.client_id "+
                "left join invoice i on mt.invoice_id=i.invoice_id "+
                "where c.organization_id='"+orgid+"' "+
                "and mt.transaction_type not in ('NB') "+
                "and mt.date>i.placement_date "+
                "and mt.status='P' "+
                "and i.placement_date<'" + DateUtils.getSqlDate(endDt)+ "' "+
                "and (i.status_code !='R' "+
                "or (i.close_date >='"+DateUtils.getSqlDate(toDt)+ "' "+
                "and i.close_date<'" + DateUtils.getSqlDate(endDt)+ "' "+
                "and i.close_date is not null))) as two , "+
                "(select count(*) "+
                "from debtor_invoice_view "+
                "where organization_id='" + orgid+ "' "+
                "and status_code !='R') "+
                "from debtor_invoice_view d "+
                "left join additional_information ai on d.invoice_id=ai.invoice_id "+
                "and ai.source='I' "+
                "and ai.type='MedInfo' "+
                "and ai.sub_type='dischdte' "+
                "where d.organization_id='" + orgid+ "' "+
                "and placement_date<'" + DateUtils.getSqlDate(endDt)+ "' "+
                "and (d.status_code !='R' "+
                "or (close_date>='"+DateUtils.getSqlDate(toDt)+ "' "+
                "and close_date<'"+DateUtils.getSqlDate(endDt)+"'))";

        String less= "and DATEDIFF(DAY,ai.description,d.placement_date) < 181 ";
        String more= "and DATEDIFF(DAY,ai.description,d.placement_date) > 180 ";
        String no= "and (isdate(ai.description)=0 or ai.description is null)";

        String[] statments=new String[]{sqlStr+less,sqlStr+more,sqlStr+no};

        System.out.println(Json.encodePrettily(statments));

        return statments;
    }*/

    public static String[] get180Age(int orgid, String toDt) {


        String endDt = DateUtils.addDaysToDate(toDt, 1);
        //we go up to 6 months before the selected end date, then we add 1 day
        String toDtMinusSixMonthsPlusOneDay = DateUtils.addDaysToDate(DateUtils.minusMonthsFromDate(setDateTo1fstDay(toDt), 6), 1);

        String sqlStrLess = "select count(*) as one, " +
                " sum(d.original_balance)-(select SUM(case(transaction_type) " +
                "when 'PD' then mt.amount " +
                "when 'PA' then mt.amount " +
                "when 'AC' then mt.amount " +
                "when 'RA' then -1*mt.amount " +
                "when 'RD' then -1*mt.amount " +
                "when 'AD' then -1*mt.amount end) " +
                "from monetary_transaction mt " +
                "left join client c on mt.client_id=c.client_id " +
                "left join invoice i on mt.invoice_id=i.invoice_id " +
                "where c.organization_id='" + orgid + "' " +
                "and mt.transaction_type not in ('NB') " +
                "and mt.date>i.placement_date " +
                "and mt.status='P' " +
                //"and i.placement_date<'" + DateUtils.getSqlDate(endDt)+ "' "+
                "and i.placement_date>='" + DateUtils.getSqlDate(toDtMinusSixMonthsPlusOneDay) + "' " +
                "and i.placement_date<'" + DateUtils.getSqlDate(endDt) + "' " +
                "and i.status_code !='R' ) as two , " +
                "(select count(*) " +
                "from debtor_invoice_view " +
                "where organization_id='" + orgid + "' " +
                "and status_code !='R' AND ((placement_date>='" + DateUtils.getSqlDate(toDtMinusSixMonthsPlusOneDay) + "' " +
                " and placement_date<'" + DateUtils.getSqlDate(endDt) + "' " +
                " ) OR (placement_date < '" + DateUtils.getSqlDate(toDtMinusSixMonthsPlusOneDay) + "' " +
                "))            ) " +
                //(select count(*) from debtor_invoice_view where organization_id='147' and status_code !='R'
                // and ((placement_date>='20190202' and placement_date<'20190802') OR (placement_date<'20190202')) ) as Four
                "from debtor_invoice_view d " +
                //"left join additional_information ai on d.invoice_id=ai.invoice_id "+
                //"and ai.source='I' "+
                //"and ai.type='MedInfo' "+
                //"and ai.sub_type='dischdte' "+
                "where d.organization_id='" + orgid + "' " +
                //"and placement_date<'" + DateUtils.getSqlDate(endDt)+ "' "+
                "and placement_date>='" + DateUtils.getSqlDate(toDtMinusSixMonthsPlusOneDay) + "' " +
                "and placement_date<'" + DateUtils.getSqlDate(endDt) + "' " +
                "and (d.status_code !='R' )";


        String sqlStrMore = "select count(*) as one, " +
                " sum(d.original_balance)-(select SUM(case(transaction_type) " +
                "when 'PD' then mt.amount " +
                "when 'PA' then mt.amount " +
                "when 'AC' then mt.amount " +
                "when 'RA' then -1*mt.amount " +
                "when 'RD' then -1*mt.amount " +
                "when 'AD' then -1*mt.amount end) " +
                "from monetary_transaction mt " +
                "left join client c on mt.client_id=c.client_id " +
                "left join invoice i on mt.invoice_id=i.invoice_id " +
                "where c.organization_id='" + orgid + "' " +
                "and mt.transaction_type not in ('NB') " +
                "and mt.date>i.placement_date " +
                "and mt.status='P' " +
                "and i.placement_date<'" + DateUtils.getSqlDate(toDtMinusSixMonthsPlusOneDay) + "' " +
                //"and i.placement_date>='" + DateUtils.getSqlDate(toDtMinusSixMonths)+ "' "+
                //"and i.placement_date<'" + DateUtils.getSqlDate(endDt)+ "' "+
                "and i.status_code !='R' ) as two , " +
                "(select count(*) " +
                "from debtor_invoice_view " +
                "where organization_id='" + orgid + "' " +
                "and status_code !='R' AND ((placement_date>='" + DateUtils.getSqlDate(toDtMinusSixMonthsPlusOneDay) + "' " +
                " and placement_date<'" + DateUtils.getSqlDate(endDt) + "' " +
                " ) OR (placement_date < '" + DateUtils.getSqlDate(toDtMinusSixMonthsPlusOneDay) + "' " +
                "))            ) " +
                "from debtor_invoice_view d " +
                //"left join additional_information ai on d.invoice_id=ai.invoice_id "+
                //"and ai.source='I' "+
                //"and ai.type='MedInfo' "+
                //"and ai.sub_type='dischdte' "+
                "where d.organization_id='" + orgid + "' " +
                "and placement_date<'" + DateUtils.getSqlDate(toDtMinusSixMonthsPlusOneDay) + "' " +
                //"and i.placement_date>='" + DateUtils.getSqlDate(toDtMinusSixMonths)+ "' "+
                //"and i.placement_date<'" + DateUtils.getSqlDate(endDt)+ "' "+
                "and (d.status_code !='R' )";

        String sqlStrNoDate = "select count(*) as one, " +
                " sum(d.original_balance)-(select SUM(case(transaction_type) " +
                "when 'PD' then mt.amount " +
                "when 'PA' then mt.amount " +
                "when 'AC' then mt.amount " +
                "when 'RA' then -1*mt.amount " +
                "when 'RD' then -1*mt.amount " +
                "when 'AD' then -1*mt.amount end) " +
                "from monetary_transaction mt " +
                "left join client c on mt.client_id=c.client_id " +
                "left join invoice i on mt.invoice_id=i.invoice_id " +
                "where c.organization_id='" + orgid + "' " +
                "and mt.transaction_type not in ('NB') " +
                "and mt.date>i.placement_date " +
                "and mt.status='P' " +
                "and i.placement_date is null " +
                //"and i.placement_date>='" + DateUtils.getSqlDate(toDtMinusSixMonths)+ "' "+
                //"and i.placement_date<'" + DateUtils.getSqlDate(endDt)+ "' "+
                "and i.status_code !='R' ) as two , " +
                "(select count(*) " +
                "from debtor_invoice_view " +
                "where organization_id='" + orgid + "' " +
                "and status_code !='R' AND ((placement_date>='" + DateUtils.getSqlDate(toDtMinusSixMonthsPlusOneDay) + "' " +
                " and placement_date<'" + DateUtils.getSqlDate(endDt) + "' " +
                " ) OR placement_date IS NULL" +
                " OR (placement_date < '" + DateUtils.getSqlDate(toDtMinusSixMonthsPlusOneDay) + "' " +
                "))            ) " +
                "from debtor_invoice_view d " +
                //"left join additional_information ai on d.invoice_id=ai.invoice_id "+
                //"and ai.source='I' "+
                //"and ai.type='MedInfo' "+
                //"and ai.sub_type='dischdte' "+
                "where d.organization_id='" + orgid + "' " +
                "and placement_date is null " +
                //"and i.placement_date>='" + DateUtils.getSqlDate(toDtMinusSixMonths)+ "' "+
                //"and i.placement_date<'" + DateUtils.getSqlDate(endDt)+ "' "+
                "and (d.status_code !='R' )";


        //String less= "and DATEDIFF(DAY,ai.description,d.placement_date) < 181 ";
        //String more= "and DATEDIFF(DAY,ai.description,d.placement_date) > 180 ";
        //String no= "and (isdate(ai.description)=0 or ai.description is null)";

        //String[] statments=new String[]{sqlStr+less,sqlStr+more,sqlStr+no};
        String[] statements = new String[]{sqlStrLess, sqlStrMore, sqlStrNoDate};

        System.out.println(Json.encodePrettily(statements));

        return statements;
    }


    // Query used in quick book balancing
    public static String getClientByOrganization(int organizationId) {
        return "SELECT  client.client_id, client.name AS clientName, client.fee_grace_period, \n" +
                "accounting_type.paid_agency, accounting_type.paid_direct, \n" +
                "accounting_type.nsf_agency, accounting_type.nsf_direct, \n" +
                "accounting_type.description \n" +
                "FROM organization INNER JOIN client \n" +
                "ON organization.organization_id = client.organization_id INNER JOIN \n" +
                "accounting_type ON client.accounting_type = accounting_type.accounting_type_id \n" +
                "WHERE organization.active<> 'f' \n" +
                "AND client.active <> 'f' \n" +
                "AND accounting_type.active <> 'f' \n" +
                "AND organization.organization_id =" + organizationId + " " +
                "ORDER BY organization.name, client.name";
    }

    public static String addCollectionsProcessing(CollectionsProcessingInputData collectionsProcessingInputData) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT INTO collections_processing " +
                "(org_id, client_id, pa_add_gross, pa_add_net, pa_subtract_tax, " +
                "pa_ar_add_fees, pa_ar_add_tax, ra_add_gross, ra_add_net, " +
                "ra_add_fees, ra_subtract_gross, ra_subtract_net, ra_subtract_fees, " +
                "ra_do_nothing, ra_add_tax, ra_ar_add_net, ra_ar_add_gross, " +
                "ra_ar_subtract_fee, ra_ar_subtract_tax, pd_subtract_fee, pd_subtract_tax, " +
                "pd_ar_add_fee, pd_ar_add_tax, rd_add_fee, rd_add_tax, rd_ar_subtract_fee, " +
                "rd_ar_subtract_tax, cancel_status ) VALUES (");
        stringBuilder.append(collectionsProcessingInputData.getOrg_id());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.getClient_id());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isPa_add_gross());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isPa_add_net());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isPa_subtract_tax());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isPa_ar_add_fees());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isPa_ar_add_tax());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRa_add_gross());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRa_add_net());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRa_add_fees());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRa_subtract_gross());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRa_subtract_net());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRa_subtract_fees());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRa_do_nothing());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRa_add_tax());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRa_ar_add_net());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRa_add_gross());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRa_ar_subtract_fee());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRa_ar_subtract_tax());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isPd_subtract_fee());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isPd_subtract_tax());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isPd_ar_add_fee());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isPd_ar_add_tax());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRd_add_fee());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRd_add_tax());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRd_ar_subtract_fee());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isRd_ar_subtract_tax());
        stringBuilder.append(", ");
        stringBuilder.append(collectionsProcessingInputData.isCancel_status());
        stringBuilder.append(" )");
        System.err.println("--------------------BEGIN-------------------------------");
        System.err.println(stringBuilder.toString());
        System.err.println("----------------------END-----------------------------");
        return stringBuilder.toString();
    }

    public static String getCollectionsProcessingByOrgIdAndClientId(int orgId, int clientId) {
        String query = "SELECT * FROM collections_processing " +
                "WHERE org_id = " + orgId + " " +
                "AND client_id = " + clientId;
        System.err.println("--------------------BEGIN-------------------------------");
        System.err.println(query);
        System.err.println("----------------------END-----------------------------");
        return query;

    }

    public static String updateCollectionsProcessing(CollectionsProcessingInputData collectionsProcessingInputData) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE collections_processing SET ");
        stringBuilder.append("org_id=");
        stringBuilder.append(collectionsProcessingInputData.getOrg_id());
        stringBuilder.append(", client_id=");
        stringBuilder.append(collectionsProcessingInputData.getClient_id());
        stringBuilder.append(", pa_add_gross=");
        stringBuilder.append(collectionsProcessingInputData.isPa_add_gross());
        stringBuilder.append(", pa_add_net=");
        stringBuilder.append(collectionsProcessingInputData.isPa_add_net());
        stringBuilder.append(", pa_subtract_tax=");
        stringBuilder.append(collectionsProcessingInputData.isPa_subtract_tax());
        stringBuilder.append(", pa_ar_add_fees=");
        stringBuilder.append(collectionsProcessingInputData.isPa_ar_add_fees());
        stringBuilder.append(", pa_ar_add_tax=");
        stringBuilder.append(collectionsProcessingInputData.isPa_ar_add_tax());
        stringBuilder.append(", ra_add_gross=");
        stringBuilder.append(collectionsProcessingInputData.isRa_add_gross());
        stringBuilder.append(", ra_add_net=");
        stringBuilder.append(collectionsProcessingInputData.isRa_add_net());
        stringBuilder.append(", ra_add_fees=");
        stringBuilder.append(collectionsProcessingInputData.isRa_add_fees());
        stringBuilder.append(", ra_subtract_gross=");
        stringBuilder.append(collectionsProcessingInputData.isRa_subtract_gross());
        stringBuilder.append(", ra_subtract_net=");
        stringBuilder.append(collectionsProcessingInputData.isRa_subtract_net());
        stringBuilder.append(", ra_subtract_fees=");
        stringBuilder.append(collectionsProcessingInputData.isRa_subtract_fees());
        stringBuilder.append(", ra_do_nothing=");
        stringBuilder.append(collectionsProcessingInputData.isRa_do_nothing());
        stringBuilder.append(", ra_add_tax=");
        stringBuilder.append(collectionsProcessingInputData.isRa_add_tax());
        stringBuilder.append(", ra_ar_add_net=");
        stringBuilder.append(collectionsProcessingInputData.isRa_ar_add_net());
        stringBuilder.append(", ra_ar_add_gross=");
        stringBuilder.append(collectionsProcessingInputData.isRa_add_gross());
        stringBuilder.append(", ra_ar_subtract_fee=");
        stringBuilder.append(collectionsProcessingInputData.isRa_ar_subtract_fee());
        stringBuilder.append(", ra_ar_subtract_tax=");
        stringBuilder.append(collectionsProcessingInputData.isRa_ar_subtract_tax());
        stringBuilder.append(", pd_subtract_fee=");
        stringBuilder.append(collectionsProcessingInputData.isPd_subtract_fee());
        stringBuilder.append(", pd_subtract_tax=");
        stringBuilder.append(collectionsProcessingInputData.isPd_subtract_tax());
        stringBuilder.append(", pd_ar_add_fee=");
        stringBuilder.append(collectionsProcessingInputData.isPd_ar_add_fee());
        stringBuilder.append(", pd_ar_add_tax=");
        stringBuilder.append(collectionsProcessingInputData.isPd_ar_add_tax());
        stringBuilder.append(", rd_add_fee=");
        stringBuilder.append(collectionsProcessingInputData.isRd_add_fee());
        stringBuilder.append(", rd_add_tax=");
        stringBuilder.append(collectionsProcessingInputData.isRd_add_tax());
        stringBuilder.append(", rd_ar_subtract_fee=");
        stringBuilder.append(collectionsProcessingInputData.isRd_ar_subtract_fee());
        stringBuilder.append(", rd_ar_subtract_tax=");
        stringBuilder.append(collectionsProcessingInputData.isRd_ar_subtract_tax());
        stringBuilder.append(", cancel_status=");
        stringBuilder.append(collectionsProcessingInputData.isCancel_status());
        stringBuilder.append(" WHERE record_id = ");
        stringBuilder.append(collectionsProcessingInputData.getRecord_id());
        return stringBuilder.toString();
    }

    public static String addOffset(OffsetInputData offsetInputData) {

        String sql = "INSERT INTO offset (org_id, client_id, offset_date, wire_offset_amt, " +
                "ar_offset_amt, created_by, created_on, changed_by, changed_on, cancel_status ) " +
                "VALUES (" + offsetInputData.getOrg_id() + ", " + offsetInputData.getClient_id() + ", " +
                "" + offsetInputData.getOffset_date() + "," + offsetInputData.getWire_offset_amt() + ", " +
                "" + offsetInputData.getAr_offset_amt() + ", '" + offsetInputData.getCreated_by() + "', " +
                "" + offsetInputData.getCreated_on() + ", '" + offsetInputData.getChanged_by() + "', " +
                "" + offsetInputData.getChanged_on() + "," + offsetInputData.getCancel_status() + ")";
        System.out.println("sql => " + sql);
        return sql;
    }

    public static String updateOffset(OffsetInputData offsetInputData) {

        return "UPDATE offset SET org_id=" + offsetInputData.getOrg_id() + ", client_id=" + offsetInputData.getClient_id() + ", " +
                "offset_date=" + offsetInputData.getOffset_date() + ", wire_offset_amt=" + offsetInputData.getWire_offset_amt() + "" +
                ", ar_offset_amt=" + offsetInputData.getAr_offset_amt() + ", created_by='" + offsetInputData.getCreated_by() + "', " +
                "created_on=" + offsetInputData.getCreated_on() + ", changed_by='" + offsetInputData.getChanged_by() + "', " +
                "changed_on=" + offsetInputData.getChanged_on() + ", cancel_status=" + offsetInputData.getCancel_status() + "" +
                " WHERE offset_id = " + offsetInputData.getOffset_id();
    }

    // Query used in quick book balancing
    public static String getOffsetByOrgIdAndClientId(int org_id, int clientId) {
        String sql = "SELECT * FROM offset WHERE org_id  = " + org_id+"" +
                " AND client_id = "+clientId+"" +
                " AND cancel_status = 0 " +
                " ORDER by offset_date desc ";
        System.out.println("sql => " + sql);
        return sql;
    }
}