package com.fma.wb.integration.i3module.dto;

import com.fma.wb.integration.common.dto.enums.ClassOfService;
import com.fma.wb.integration.common.dto.enums.DeviceType;
import dto.SessionInformation;
import java.io.Serializable;

public class SessionSettings extends SessionInformation implements Serializable {
    //private SessionSettings sessionSettings = new SessionSettings();

    private ClassOfService classOfService;
    //ex: General

    private DeviceType deviceType;
    //ex: Web

    public ClassOfService getClassOfService() {
        return classOfService;
    }

    public void setClassOfService(ClassOfService classOfService) {
        this.classOfService = classOfService;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public SessionSettings() {
    }
}
