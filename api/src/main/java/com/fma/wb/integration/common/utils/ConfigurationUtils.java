package com.fma.wb.integration.common.utils;

/**
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class ConfigurationUtils {
    public static final String CALL_TYPE_MANUAL_DIALER_CALL = "Manual Dialer Call";

    //User calling status
    public static final String USER_CALLING_STATUS = "Do Not Disturb";

    //Play wave audio
    public static final String PATH_PLAY_WAVE_AUDIO = "\\\\fmars64\\InfinityShare\\productionResources\\audio\\";

    //I3Status status
    public static final String I3STATUS_AVAILABLE = "Available";
    public static final String I3STATUS_GONE_HOME = "Gone Home";
    public static final String I3STATUS_MANUAL_DIALER_CALL = "Manual Dialer Call";
    public static final String I3STATUS_AWAY_FROM_DESK = "Away from desk";
    public static final String I3STATUS_DO_NOT_DISTURB = "Do Not Disturb";
    public static final String I3STATUS_AT_LUNCH = "At Lunch";
    public static final String I3STATUS_IN_A_MEETING = "In a Meeting";
    public static final String I3STATUS_DIALER_CALL = "Dialer Call";
    public static final String I3STATUS_INBOUND_CALL = "Inbound";

    public static final String EIC_CALL_DIRECTION = "Eic_CallDirection";
    public static final String CALL_ID_KEY = "EIC_CallIdKey";
    public static final String EIC_CALL_DIRECTION_VALUE_O = "O";
    public static final String EIC_REMOTE_ADDRESS = "Eic_RemoteAddress";

    //IOD values
    public static final int IOD_VALUE_1 = 1;
    public static final int IOD_VALUE_2 = 2;
    public static final int IOD_VALUE_3 = 3;
    public static final int IOD_VALUE_4 = 4;

    public static final String APPLICATION_NAME = "I3WService";

    //default language
    public static final String DEFAULT_LANGUAGE = "en-us";

    //default application name
    public static final String DEFAULT_APPLICATION_NAME = "I3WService";

    //Call attributes keys
    public static final String IS_ATTR_CAMPAIGN_ID = "is_attr_campaignid";
    public static final String IS_ATTR_DEBTOR_ID = "is_attr_debtor_id";
    //IMC debtor id attribute name
    public static final String IMC_DEBTOR_ID = "debtor_id";

    //Disposition call results
    public static final String FAILURE_LABEL = "Failure";
    public static final String WRONG_PARTY_LABEL = "WrongParty";
    public static final String SUCCESS_LABEL = "Success";
    public static final String MACHINE_LABEL = "Machine";
    public static final String NO_ANSWER_LABEL= "NoAnswer";
    public static final String REMOTE_HANGUP_LABEL = "RemoteHangup";
    public static final String TRANSFERRED_LABEL = "Transferred";
    public static final String SKIPPED_LABEL = "Skipped";
    public static final String PHONE_NUMBER_SUCCESS_LABEL = "PhoneNumberSuccess";
    public static final String PHONE_NUMBER_DELETED_LABEL = "PhoneNumberDeleted";
    public static final String FAX_LABEL = "Fax";
    public static final String SCHEDULED_LABEL = "Scheduled";
    public static final String BUSY_LABEL = "Busy";
    public static final String SIT_UNCALLABLE_LABEL = "SITUncallable";
    public static final String SIT_CALLABLE_LABEL = "SITCallable";
    public static final String NO_LINES_LABEL = "NoLines";
    public static final String DELETED_LABEL = "Deleted";
    public static final String SIT_LABEL = "SIT";
    public static final String AMBIGUOUS_LABEL = "Ambiguous";

    //Call DispositionParameters
    public static final String DISPOSITION_CALL_PARAM_SKIPPED = "Skipped";
    public static final String DISPOSITION_CALL_PARAM_FAILURE = "Failure";
    public static final String DISPOSITION_CALL_PARAM_WRONGPARTY = "WrongParty";
    public static final String DISPOSITION_CALL_PARAM_SUCCESS = "Success";
    public static final String DISPOSITION_CALL_PARAM_MACHINE = "Machine";
    public static final String DISPOSITION_CALL_PARAM_NOANSWER = "NoAnswer";
    public static final String DISPOSITION_CALL_PARAM_REMOTEHANGUP = "RemoteHangup";
    public static final String DISPOSITION_CALL_PARAM_TRANSFERRED = "Transferred";
    public static final String DISPOSITION_CALL_PARAM_PHONENUMBERSUCCESS = "PhoneNumberSuccess";
    public static final String DISPOSITION_CALL_PARAM_PHONENUMBERDELETED = "PhoneNumberDeleted";
    public static final String DISPOSITION_CALL_PARAM_FAX = "Fax";
    public static final String DISPOSITION_CALL_PARAM_SCHEDULED = "Scheduled";
    public static final String DISPOSITION_CALL_PARAM_BUSY = "Busy";
    public static final String DISPOSITION_CALL_PARAM_SITUNCALLABLE = "SITUncallable";
    public static final String DISPOSITION_CALL_PARAM_SITCALLABLE = "SITCallable";
    public static final String DISPOSITION_CALL_PARAM_NOLINES = "NoLines";
    public static final String DISPOSITION_CALL_PARAM_DELETED = "Deleted";
    public static final String DISPOSITION_CALL_PARAM_SIT = "SIT";
    public static final String DISPOSITION_CALL_PARAM_AMBIGIOUS = "Ambiguous";

    public static final String IS_ATTR_CAMPAIGN_NAME = "is_attr_campaignname";
    public static final String EIC_STATE = "Eic_State";

    //SQL Server Db configs
    public static final String INVOICE_TABLE_NAME = "invoice_activity";
    public static final String INTERACTION_SUMMARY_TABLE_NAME = "InteractionSummary";
    public static final String IR_RECORDING_MEDIA_TABLE_NAME = "ir_recordingMedia";
    public static final String AUT_RECORDING_MET_TABLE_NAME = "autonomy_recordings_metadata";
    public static final String CALL_RESPONSE_LOG_TABLE_NAME = "call_response_log";
    public static final String CALL_HISTORY_TABLE_NAME = "CallHistory";
    public static final String DIRECTORY_TABLE_NAME = "directory";
    public static final String LIVE_VOX_TABLE_NAME = "live_vox";

    //SQL Table names for Trust Balancing application
    public static final String CLIENT_TABLE_NAME = "Client";
    public static final String MONETARY_TRANSACTION_TABLE_NAME = "Monetary_transaction";
    public static final String DEBTOR_TABLE_NAME = "Debtor";
    public static final String INVOICES_TABLE_NAME = "Invoice";
    public static final String ADDITIONAL_INFORMATION_TABLE_NAME = "Additional_information";
    public static final String PAYMENT_TYPES_TABLE_NAME = "Payment_types";
    public static final String TRUST_TABLE_NAME = "Trust";
    public static final String ORGANIZATION_TABLE_NAME = "Organization";
    public static final String DOC_TABLE_NAME = "Doc";
    public static final String DEPOSIT_BUNDLE_SUM_TABLE_NAME = "Deposit_Bundle_Sum";
    public static final String USERS_TABLE_NAME = "users";
    public static final String USER_ROLES_TABLE_NAME = "user_role";

    //Edit
    //Phone system database connection information
    public static final String IC_DB_NAME = "I3_IC_40";
    public static final String IC_DB_HOST = "fmars102";
    public static final String IC_DB_USER = "IC_Admin";
    //public static final String IC_DB_USER = "ic_readonly";
    public static final String IC_DB_PSSWD = "$3100Timmons$";

    //Infinity database connection information


    public static final String INFINITY_DB_NAME = "FMAtest";
    //public static final String INF_DB_HOST = "fmars31";
    //public static final String INF_DB_USER = "fma";
    //public static final String INF_DB_PSSWD = "fma_infinity";
    public static final String INF_DB_HOST = "localhost";
    public static final String INF_DB_USER = "fma_vb";
    public static final String INF_DB_PSSWD = "Wookie1";


    // Quickbook Balancing database connection information
    public static final String Q_B_DB_NAME = "Quickbook Balancing";
    public static final String Q_B_DB_HOST = "localhost";
    public static final String Q_B_DB_USER = "fma_vb";
    public static final String Q_B_DB_PSSWD = "Wookie1";


    public static final String INFINITY_DB_NAME32 = "FMAReports";
    public static final String INF_DB_HOST32 = "fmars32";
    public static final String INF_DB_USER32 = "fma";
    public static final String INF_DB_PSSWD32 = "fma_infinity";

    //LiveVox database connection information
    public static final String LIVE_VOX_DB_NAME = "LiveVox";
    public static final String LIVE_DB_HOST = "fmars15";
    public static final String LIVE_DB_USER = "fma_vb";
    public static final String LIVE_DB_PSSWD = "Wookie1";

    //Trust balancing connection information
    //The host name (fmars31) and server name (FMA) used for Trust balancing is the same as the autonomy recording,
    // however the username/pwd used are not the same
    public static final String  INFINITY_DB_USER_BO_AND_REPORT_APP = "fma_vb";
    public static final String  INFINITY_DB_PSSWD_BO_AND_REPORT_APP = "Wookie1";

    //RP connection information used by Trust Balancing app
    public static final String  RP_DB_NAME = "xprpsreports";
    public static final String  RP_DB_HOST = "fmars23";
    public static final String  RP_DB_USER = "fma_vb";
    public static final String  RP_DB_PSSWD = "Wookie1";

    public static final String BLOWFISH_KEY= "fbscbccb";
	
    public static String mailHost="smtp.gmail.com";
    public static String mailUsr="hajatiana@gmail.com";
    public static String mailPsswd="Technologie1234";
    public static String reportersmails="hajatiana@gmail.com;";
    public static String SYSA_ID="sysa_002_553";


    public static String SIGNATURE="This information is confidential, proprietary and the intellectual property of FMA Alliance, Ltd. The unauthorized use, copy, distribution, or any form of dissemination, in full or part, is strictly prohibited.";



}