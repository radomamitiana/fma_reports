package com.fma.wb.integration.i3module.dto;

import dto.User;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

/**
 * isConnected: stores whether or not the user is connected to a session.
 * host: stores the host that the user is logged into.
 * userID: stores the user logged in
 * machine: stores the machine that the user is logged into
 * phoneID: stores the phone number of the user
 * sessionID: stores the session that the user is connected to
 * campaign: stores the campaign that the user is listed under
 * i3Status: stores the online status of the user: Available, Gone Home, Manual Dialer Call, Away from desk, Do not Disturb, At Lunch, In a Meeting
 * result: used to store results from completing an agentless or dialer call: Failure,WrongParty, Success, Machine, NoAnswer, RemoteHangup, Transferred, Skipped, PhoneNumberSuccess, PhoneNumberDeleted, Fax, Scheduled, Busy, SITUncallable, SITCallable, NoLines, Deleted, SIT
 * disclosureID: used to store the path of an audio recording for audio playback
 * noRestrict: determines whether or not that the user can dial out or not
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class CompositeType implements Serializable {
    private boolean isConnected = false;
    private String host = "";
    private String userID = "";
    private String machine = "";
    private String phoneID = "";
    private long sessionID = 0;
    private String campaign = "";
    private String i3Status = "";
    private String result = "";
    private String disclosureID = "";
    private String noRestrict = "";
    private String password = "";
    private String debtor_id = "";
    private List<String> userRoles = new ArrayList<>();

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getMachine() {
        return machine;
    }

    public void setMachine(String machine) {
        this.machine = machine;
    }

    public String getPhoneID() {
        return phoneID;
    }

    public void setPhoneID(String phoneID) {
        this.phoneID = phoneID;
    }

    public long getSessionID() {
        return sessionID;
    }

    public void setSessionID(long sessionID) {
        this.sessionID = sessionID;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public String getI3Status() {
        return i3Status;
    }

    public void setI3Status(String i3Status) {
        this.i3Status = i3Status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDisclosureID() {
        return disclosureID;
    }

    public void setDisclosureID(String disclosureID) {
        this.disclosureID = disclosureID;
    }

    public String getNoRestrict() {
        return noRestrict;
    }

    public void setNoRestrict(String noRestrict) {
        this.noRestrict = noRestrict;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<String> userRoles) {
        this.userRoles = userRoles;
    }

    /**
     * Constructor without parameter
     */
    public CompositeType() {
        super();
    }

    /**
     * Constructor with parameters
     */
    public CompositeType(boolean isConnected, String host, String userID, String machine, String phoneID, long sessionID, String campaign, String i3Status, String result, String disclosureID, String noRestrict) {
        super();
        this.isConnected = isConnected;
        this.host = host;
        this.userID = userID;
        this.machine = machine;
        this.phoneID = phoneID;
        this.sessionID = sessionID;
        this.campaign = campaign;
        this.i3Status = i3Status;
        this.result = result;
        this.disclosureID = disclosureID;
        this.noRestrict = noRestrict;
    }

    public User toUser(){
        User user = new User();
        user.setUserID(this.getUserID());
        user.setPassWord(this.getPassword());
        user.setApplicationName("I3WService");
        user.setMachineName(this.getMachine());
        return user;
    }

    @Override
    public String toString() {
        return "CompositeType{" +
                "isConnected=" + isConnected +
                ", host='" + host + '\'' +
                ", userID='" + userID + '\'' +
                ", machine='" + machine + '\'' +
                ", phoneID='" + phoneID + '\'' +
                ", sessionID=" + sessionID +
                ", campaign='" + campaign + '\'' +
                ", i3Status='" + i3Status + '\'' +
                ", result='" + result + '\'' +
                ", disclosureID='" + disclosureID + '\'' +
                ", noRestrict='" + noRestrict + '\'' +
                '}';
    }

    public String getDebtor_id() {
        return debtor_id;
    }

    public void setDebtor_id(String debtor_id) {
        this.debtor_id = debtor_id;
    }
}
