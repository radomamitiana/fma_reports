package com.fma.wb.integration.reportsmodule.dto.trust;

import com.fma.wb.integration.reportsmodule.dto.Organization;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TrustOrganizationClientsGrouped implements Serializable {
    private Trust trust;
    private Organization organization;
    private List<Client> clients = null;

    public Trust getTrust() {
        return trust;
    }

    public void setTrust(Trust trust) {
        this.trust = trust;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

    public TrustOrganizationClientsGrouped() {
        clients = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "TrustOrganizationClientsGrouped{" +
                "trust=" + trust +
                ", organization=" + organization +
                ", clients=" + clients +
                '}';
    }
}