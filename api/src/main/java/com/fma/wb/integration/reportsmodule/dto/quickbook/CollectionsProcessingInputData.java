package com.fma.wb.integration.reportsmodule.dto.quickbook;

import java.io.Serializable;

public class CollectionsProcessingInputData implements Serializable {

    private int record_id;
    private int org_id;
    private int client_id;
    private byte pa_add_gross;
    private byte pa_add_net;
    private byte pa_subtract_tax;
    private byte pa_ar_add_fees;
    private byte pa_ar_add_tax;
    private byte ra_add_gross;
    private byte ra_add_net;
    private byte ra_add_fees;
    private byte ra_subtract_gross;
    private byte ra_subtract_net;
    private byte ra_subtract_fees;
    private byte ra_do_nothing;
    private byte ra_add_tax;
    private byte ra_ar_add_net;
    private byte ra_ar_add_gross;
    private byte ra_ar_subtract_fee;
    private byte ra_ar_subtract_tax;
    private byte pd_subtract_fee;
    private byte pd_subtract_tax;
    private byte pd_ar_add_fee;
    private byte pd_ar_add_tax;
    private byte rd_add_fee;
    private byte rd_add_tax;
    private byte rd_ar_subtract_fee;
    private byte rd_ar_subtract_tax;
    private byte cancel_status;

    public int getRecord_id() {
        return record_id;
    }

    public void setRecord_id(int record_id) {
        this.record_id = record_id;
    }

    public int getOrg_id() {
        return org_id;
    }

    public void setOrg_id(int org_id) {
        this.org_id = org_id;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public byte isPa_add_gross() {
        return pa_add_gross;
    }

    public void setPa_add_gross(byte pa_add_gross) {
        this.pa_add_gross = pa_add_gross;
    }

    public byte isPa_add_net() {
        return pa_add_net;
    }

    public void setPa_add_net(byte pa_add_net) {
        this.pa_add_net = pa_add_net;
    }

    public byte isPa_subtract_tax() {
        return pa_subtract_tax;
    }

    public void setPa_subtract_tax(byte pa_subtract_tax) {
        this.pa_subtract_tax = pa_subtract_tax;
    }

    public byte isPa_ar_add_fees() {
        return pa_ar_add_fees;
    }

    public void setPa_ar_add_fees(byte pa_ar_add_fees) {
        this.pa_ar_add_fees = pa_ar_add_fees;
    }

    public byte isPa_ar_add_tax() {
        return pa_ar_add_tax;
    }

    public void setPa_ar_add_tax(byte pa_ar_add_tax) {
        this.pa_ar_add_tax = pa_ar_add_tax;
    }

    public byte isRa_add_gross() {
        return ra_add_gross;
    }

    public void setRa_add_gross(byte ra_add_gross) {
        this.ra_add_gross = ra_add_gross;
    }

    public byte isRa_add_net() {
        return ra_add_net;
    }

    public void setRa_add_net(byte ra_add_net) {
        this.ra_add_net = ra_add_net;
    }

    public byte isRa_add_fees() {
        return ra_add_fees;
    }

    public void setRa_add_fees(byte ra_add_fees) {
        this.ra_add_fees = ra_add_fees;
    }

    public byte isRa_subtract_gross() {
        return ra_subtract_gross;
    }

    public void setRa_subtract_gross(byte ra_subtract_gross) {
        this.ra_subtract_gross = ra_subtract_gross;
    }

    public byte isRa_subtract_net() {
        return ra_subtract_net;
    }

    public void setRa_subtract_net(byte ra_subtract_net) {
        this.ra_subtract_net = ra_subtract_net;
    }

    public byte isRa_subtract_fees() {
        return ra_subtract_fees;
    }

    public void setRa_subtract_fees(byte ra_subtract_fees) {
        this.ra_subtract_fees = ra_subtract_fees;
    }

    public byte isRa_do_nothing() {
        return ra_do_nothing;
    }

    public void setRa_do_nothing(byte ra_do_nothing) {
        this.ra_do_nothing = ra_do_nothing;
    }

    public byte isRa_add_tax() {
        return ra_add_tax;
    }

    public void setRa_add_tax(byte ra_add_tax) {
        this.ra_add_tax = ra_add_tax;
    }

    public byte isRa_ar_add_net() {
        return ra_ar_add_net;
    }

    public void setRa_ar_add_net(byte ra_ar_add_net) {
        this.ra_ar_add_net = ra_ar_add_net;
    }

    public byte isRa_ar_add_gross() {
        return ra_ar_add_gross;
    }

    public void setRa_ar_add_gross(byte ra_ar_add_gross) {
        this.ra_ar_add_gross = ra_ar_add_gross;
    }

    public byte isRa_ar_subtract_fee() {
        return ra_ar_subtract_fee;
    }

    public void setRa_ar_subtract_fee(byte ra_ar_subtract_fee) {
        this.ra_ar_subtract_fee = ra_ar_subtract_fee;
    }

    public byte isRa_ar_subtract_tax() {
        return ra_ar_subtract_tax;
    }

    public void setRa_ar_subtract_tax(byte ra_ar_subtract_tax) {
        this.ra_ar_subtract_tax = ra_ar_subtract_tax;
    }

    public byte isPd_subtract_fee() {
        return pd_subtract_fee;
    }

    public void setPd_subtract_fee(byte pd_subtract_fee) {
        this.pd_subtract_fee = pd_subtract_fee;
    }

    public byte isPd_subtract_tax() {
        return pd_subtract_tax;
    }

    public void setPd_subtract_tax(byte pd_subtract_tax) {
        this.pd_subtract_tax = pd_subtract_tax;
    }

    public byte isPd_ar_add_fee() {
        return pd_ar_add_fee;
    }

    public void setPd_ar_add_fee(byte pd_ar_add_fee) {
        this.pd_ar_add_fee = pd_ar_add_fee;
    }

    public byte isPd_ar_add_tax() {
        return pd_ar_add_tax;
    }

    public void setPd_ar_add_tax(byte pd_ar_add_tax) {
        this.pd_ar_add_tax = pd_ar_add_tax;
    }

    public byte isRd_add_fee() {
        return rd_add_fee;
    }

    public void setRd_add_fee(byte rd_add_fee) {
        this.rd_add_fee = rd_add_fee;
    }

    public byte isRd_add_tax() {
        return rd_add_tax;
    }

    public void setRd_add_tax(byte rd_add_tax) {
        this.rd_add_tax = rd_add_tax;
    }

    public byte isRd_ar_subtract_fee() {
        return rd_ar_subtract_fee;
    }

    public void setRd_ar_subtract_fee(byte rd_ar_subtract_fee) {
        this.rd_ar_subtract_fee = rd_ar_subtract_fee;
    }

    public byte isRd_ar_subtract_tax() {
        return rd_ar_subtract_tax;
    }

    public void setRd_ar_subtract_tax(byte rd_ar_subtract_tax) {
        this.rd_ar_subtract_tax = rd_ar_subtract_tax;
    }

    public byte isCancel_status() {
        return cancel_status;
    }

    public void setCancel_status(byte cancel_status) {
        this.cancel_status = cancel_status;
    }
}