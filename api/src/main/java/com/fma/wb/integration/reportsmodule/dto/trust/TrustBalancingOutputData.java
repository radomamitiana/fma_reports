package com.fma.wb.integration.reportsmodule.dto.trust;

import io.vertx.core.json.Json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This model represents the output data send from the trust balancing form interface
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class TrustBalancingOutputData implements Serializable {
    private String infDate;
    private HashMap<String,String> listTrust =  new HashMap<>();
    private String achTotal;
    private String iclTotal;
    private String rpSolutionsTotal;
    //private String formula = "";
    private HashMap<String, ArrayList<String>> excelFileBottomData = new HashMap<>();

    public HashMap<String, ArrayList<String>> getExcelFileBottomData() {
        return excelFileBottomData;
    }

    public void setExcelFileBottomData(HashMap<String, ArrayList<String>> excelFileBottomData) {
        this.excelFileBottomData = excelFileBottomData;
    }


    public HashMap<String, String> getListTrust() {
        return listTrust;
    }

    public void setListTrust(HashMap<String, String> listTrust) {
        this.listTrust = listTrust;
    }

    public void addTrust(String key,String value) {
        this.listTrust.put(key,value);
    }

    public String getInfDate(){
        infDate = this.getListTrust().get("infinityDate");
        //return this.getListTrust().get("infinityDate");
        return infDate;
    }

    public void addBottomData(String key,ArrayList<String> value) {
        this.excelFileBottomData.put(key,value);
    }



    public String getAchTotal() {
        return achTotal;
    }

    public void setAchTotal(String achTotal) {
        this.achTotal = achTotal;
    }

    public String getIclTotal() {
        return iclTotal;
    }

    public void setIclTotal(String iclTotal) {
        this.iclTotal = iclTotal;
    }

    public String getRpSolutionsTotal() {
        return rpSolutionsTotal;
    }

    public void setRpSolutionsTotal(String rpSolutionsTotal) {
        this.rpSolutionsTotal = rpSolutionsTotal;
    }

    /**
     * Constructor without parameter
     */
    public TrustBalancingOutputData() {
     /*   this.listTrust.put("stream","value");
        this.listTrust.put("cc","value");

        ArrayList<String> recovererCode=new ArrayList<>();
        recovererCode.add("176");
        recovererCode.add("177");

        ArrayList<String> collected=new ArrayList<>();
        recovererCode.add("538.27");
        recovererCode.add("685.72");


        this.excelFileBottomData.put("recovererCode",recovererCode);
        this.excelFileBottomData.put("collected",collected);
        this.excelFileBottomData.put("nfs",new ArrayList());*/
    }

    public TrustBalancingOutputData(String infDate, HashMap<String, String> listTrust, String achTotal, String iclTotal, String rpSolutionsTotal, HashMap<String, ArrayList<String>> excelFileBottomData) {
        this.infDate = infDate;
        this.listTrust = listTrust;
        this.achTotal = achTotal;
        this.iclTotal = iclTotal;
        this.rpSolutionsTotal = rpSolutionsTotal;
        this.excelFileBottomData = excelFileBottomData;
    }

    @Override
    public String toString() {
        return Json.encode(this);
    }
}

	
	
	
