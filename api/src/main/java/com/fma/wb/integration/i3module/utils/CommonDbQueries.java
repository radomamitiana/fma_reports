package com.fma.wb.integration.i3module.utils;

import static com.fma.wb.integration.common.utils.ConfigurationUtils.*;

public class CommonDbQueries {

   public static final String COLUMN_MULTISTORE_Q="CREATE CLUSTERED COLUMNSTORE INDEX columnstoreindex ON %s";

   public static final String GET_I3_CALL_METRIC="select insu.RemoteNumberCallId as remote_number, " +
           "DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), " +
           "insu.InitiatedDateTimeUTC) as initiated_date, " +
           "DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), insu.ConnectedDateTimeUTC)" +
           " as connected_date, DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), " +
           "insu.TerminatedDateTimeUTC) as terminated_date," +
           " CASE WHEN (ConnectedDateTimeUTC <= '1970-01-02' OR"+
           " TerminatedDateTimeUTC <= '1970-01-02') THEN 0 WHEN " +
           "(ConnectedDateTimeUTC > TerminatedDateTimeUTC) THEN 0 ELSE DATEDIFF(SECOND, "+
           " ConnectedDateTimeUTC, TerminatedDateTimeUTC) END AS call_duration,"+
           " CAST(ROUND(insu.tHeld / 1000., 0) AS INTEGER) AS hold_duration,"+
           " insu.LastLocalUserId as voice_user_id,insu.LastLocalName as voice_full_name,"+
           " irrm.MediaURI as recording_file_name, irrm.FileSize as recording_file_size, " +
           " CAST(CASE insu.Direction  WHEN 0 THEN 'Unknown'  WHEN 1 THEN 'I'  WHEN 2 THEN 'O'  WHEN 3 THEN 'Intercom'  ELSE 'Invalid value' END AS VARCHAR(20))" +
           "AS direction, insu.InteractionIDKey as recorded_call_id_key,"+
           " REPLACE(REPLACE(insu.RemoteNumberCallId,'+',''),'/','') as ani, insu.DNIS_LocalID as dnis, "+
           " insu.LastLocalNumber as local_number, insu.RemoteNumberCallId as remote_number " +
           "from "+INTERACTION_SUMMARY_TABLE_NAME+" insu left join "+IR_RECORDING_MEDIA_TABLE_NAME+" irrm on insu.InteractionIDKey=irrm.queueobjectidkey "+
           " WHERE insu.InteractionID=%s and irrm.MediaURI not like '%%fmars94%%' and irrm.MediaURI not like '%%fmars95%%' AND irrm.MediaURI like '%%.sasf' ";

   public static final String GET_DEBTOR_FROM_CALL_HISTORY="Select customData1 from " +
           ""+CALL_HISTORY_TABLE_NAME+" call_hst left join " +
           ""+INTERACTION_SUMMARY_TABLE_NAME+" insu on insu.InteractionIDKey=call_hst.callIdKey " +
           "WHERE insu.InteractionID=%s";

   public static final String GET_LIVE_VOX_CALL_METRICS="SELECT live_vox.*, directory.directory_name, agent_name.agent_name "
                    +"FROM "+LIVE_VOX_TABLE_NAME+" INNER JOIN "+DIRECTORY_TABLE_NAME+" "
                    +"ON live_vox.directory_id = directory.directory_id INNER JOIN "
                    +"agent_name ON live_vox.agent_name_id = agent_name.agent_name_id";

    public static final String GET_LIVE_VOX_CALL_METRICS_BY_DATE="SELECT live_vox.*, directory.directory_name, agent_name.agent_name "
            +"FROM "+LIVE_VOX_TABLE_NAME+" INNER JOIN "+DIRECTORY_TABLE_NAME+" "
            +"ON live_vox.directory_id = directory.directory_id INNER JOIN "
            +"agent_name ON live_vox.agent_name_id = agent_name.agent_name_id WHERE call_date > %s";

   public static final String GET_LIVE_VOX_ENTRY_NOT_IN_AUTONOMY="SELECT COUNT(*) From "+AUT_RECORDING_MET_TABLE_NAME
           +" WHERE source='livevox' " +
           "AND debtor_id= %s AND " +
           "recording_file_name=%s AND " +
           "recording_file_size='0' AND " +
           "initiated_date= %s AND " +
           "connected_date=%s AND " +
           "terminated_date=%s";
}
