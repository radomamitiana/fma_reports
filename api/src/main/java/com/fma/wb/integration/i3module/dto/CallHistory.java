package com.fma.wb.integration.i3module.dto;

import java.io.Serializable;

/**
 * @author Tiana Andriamanalina
 * @version 1.0
 * CallHistory class is used for the mapping
 */

public class CallHistory implements Serializable {
    private String campaignname;
    private String siteid;
    private int i3_identity;
    private String i3_rowId;
    private String callid;
    private String callIdKey;
    private String wrapupcategory;
    private String wrapupcode;
    private int callingmode;
    private String phonenumber;
    private String phonenumbertype;
    private String agentid;
    private int calledpartyoffset;
    private int odsOffset;
    private String previewpoptimeUTC;
    private String callplacedtimeUTC;
    private String callansweredtimeUTC;
    private String messageplaytimeUTC;
    private String callconnectedtimeUTC;
    private String calldisconnectedtimeUTC;
    private int length;
    private int caresult;
    private String cadetail;
    private boolean isabandoned;
    private boolean iscontact;
    private boolean isdetect;
    private boolean isrpc;
    private boolean issuccess;
    private float successresult;
    private String customdata1;
    private String customdata2;
    private int campaignfilterid;
    private int previewtimerinit;

    public String getCampaignname() {
        return campaignname;
    }

    public void setCampaignname(String campaignname) {
        this.campaignname = campaignname;
    }

    public String getSiteid() {
        return siteid;
    }

    public void setSiteid(String siteid) {
        this.siteid = siteid;
    }

    public int getI3_identity() {
        return i3_identity;
    }

    public void setI3_identity(int i3_identity) {
        this.i3_identity = i3_identity;
    }

    public String getI3_rowId() {
        return i3_rowId;
    }

    public void setI3_rowId(String i3_rowId) {
        this.i3_rowId = i3_rowId;
    }

    public String getCallid() {
        return callid;
    }

    public void setCallid(String callid) {
        this.callid = callid;
    }

    public String getCallIdKey() {
        return callIdKey;
    }

    public void setCallIdKey(String callIdKey) {
        this.callIdKey = callIdKey;
    }

    public String getWrapupcategory() {
        return wrapupcategory;
    }

    public void setWrapupcategory(String wrapupcategory) {
        this.wrapupcategory = wrapupcategory;
    }

    public String getWrapupcode() {
        return wrapupcode;
    }

    public void setWrapupcode(String wrapupcode) {
        this.wrapupcode = wrapupcode;
    }

    public int getCallingmode() {
        return callingmode;
    }

    public void setCallingmode(int callingmode) {
        this.callingmode = callingmode;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getPhonenumbertype() {
        return phonenumbertype;
    }

    public void setPhonenumbertype(String phonenumbertype) {
        this.phonenumbertype = phonenumbertype;
    }

    public String getAgentid() {
        return agentid;
    }

    public void setAgentid(String agentid) {
        this.agentid = agentid;
    }

    public int getCalledpartyoffset() {
        return calledpartyoffset;
    }

    public void setCalledpartyoffset(int calledpartyoffset) {
        this.calledpartyoffset = calledpartyoffset;
    }

    public int getOdsOffset() {
        return odsOffset;
    }

    public void setOdsOffset(int odsOffset) {
        this.odsOffset = odsOffset;
    }

    public String getPreviewpoptimeUTC() {
        return previewpoptimeUTC;
    }

    public void setPreviewpoptimeUTC(String previewpoptimeUTC) {
        this.previewpoptimeUTC = previewpoptimeUTC;
    }

    public String getCallplacedtimeUTC() {
        return callplacedtimeUTC;
    }

    public void setCallplacedtimeUTC(String callplacedtimeUTC) {
        this.callplacedtimeUTC = callplacedtimeUTC;
    }

    public String getCallansweredtimeUTC() {
        return callansweredtimeUTC;
    }

    public void setCallansweredtimeUTC(String callansweredtimeUTC) {
        this.callansweredtimeUTC = callansweredtimeUTC;
    }

    public String getMessageplaytimeUTC() {
        return messageplaytimeUTC;
    }

    public void setMessageplaytimeUTC(String messageplaytimeUTC) {
        this.messageplaytimeUTC = messageplaytimeUTC;
    }

    public String getCallconnectedtimeUTC() {
        return callconnectedtimeUTC;
    }

    public void setCallconnectedtimeUTC(String callconnectedtimeUTC) {
        this.callconnectedtimeUTC = callconnectedtimeUTC;
    }

    public String getCalldisconnectedtimeUTC() {
        return calldisconnectedtimeUTC;
    }

    public void setCalldisconnectedtimeUTC(String calldisconnectedtimeUTC) {
        this.calldisconnectedtimeUTC = calldisconnectedtimeUTC;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getCaresult() {
        return caresult;
    }

    public void setCaresult(int caresult) {
        this.caresult = caresult;
    }

    public String getCadetail() {
        return cadetail;
    }

    public void setCadetail(String cadetail) {
        this.cadetail = cadetail;
    }

    public boolean isIsabandoned() {
        return isabandoned;
    }

    public void setIsabandoned(boolean isabandoned) {
        this.isabandoned = isabandoned;
    }

    public boolean isIscontact() {
        return iscontact;
    }

    public void setIscontact(boolean iscontact) {
        this.iscontact = iscontact;
    }

    public boolean isIsdetect() {
        return isdetect;
    }

    public void setIsdetect(boolean isdetect) {
        this.isdetect = isdetect;
    }

    public boolean isIsrpc() {
        return isrpc;
    }

    public void setIsrpc(boolean isrpc) {
        this.isrpc = isrpc;
    }

    public boolean isIssuccess() {
        return issuccess;
    }

    public void setIssuccess(boolean issuccess) {
        this.issuccess = issuccess;
    }

    public float getSuccessresult() {
        return successresult;
    }

    public void setSuccessresult(float successresult) {
        this.successresult = successresult;
    }

    public String getCustomdata1() {
        return customdata1;
    }

    public void setCustomdata1(String customdata1) {
        this.customdata1 = customdata1;
    }

    public String getCustomdata2() {
        return customdata2;
    }

    public void setCustomdata2(String customdata2) {
        this.customdata2 = customdata2;
    }

    public int getCampaignfilterid() {
        return campaignfilterid;
    }

    public void setCampaignfilterid(int campaignfilterid) {
        this.campaignfilterid = campaignfilterid;
    }

    public int getPreviewtimerinit() {
        return previewtimerinit;
    }

    public void setPreviewtimerinit(int previewtimerinit) {
        this.previewtimerinit = previewtimerinit;
    }

    public CallHistory() {
    }

    @Override
    public String toString() {
        return "CallHistory{" +
                "campaignname='" + campaignname + '\'' +
                ", siteid='" + siteid + '\'' +
                ", i3_identity=" + i3_identity +
                ", i3_rowId='" + i3_rowId + '\'' +
                ", callid='" + callid + '\'' +
                ", callIdKey='" + callIdKey + '\'' +
                ", wrapupcategory='" + wrapupcategory + '\'' +
                ", wrapupcode='" + wrapupcode + '\'' +
                ", callingmode=" + callingmode +
                ", phonenumber='" + phonenumber + '\'' +
                ", phonenumbertype='" + phonenumbertype + '\'' +
                ", agentid='" + agentid + '\'' +
                ", calledpartyoffset=" + calledpartyoffset +
                ", odsOffset=" + odsOffset +
                ", previewpoptimeUTC=" + previewpoptimeUTC +
                ", callplacedtimeUTC=" + callplacedtimeUTC +
                ", callansweredtimeUTC=" + callansweredtimeUTC +
                ", messageplaytimeUTC=" + messageplaytimeUTC +
                ", callconnectedtimeUTC=" + callconnectedtimeUTC +
                ", calldisconnectedtimeUTC=" + calldisconnectedtimeUTC +
                ", length=" + length +
                ", caresult=" + caresult +
                ", cadetail='" + cadetail + '\'' +
                ", isabandoned=" + isabandoned +
                ", iscontact=" + iscontact +
                ", isdetect=" + isdetect +
                ", isrpc=" + isrpc +
                ", issuccess=" + issuccess +
                ", successresult=" + successresult +
                ", customdata1='" + customdata1 + '\'' +
                ", customdata2='" + customdata2 + '\'' +
                ", campaignfilterid=" + campaignfilterid +
                ", previewtimerinit='" + previewtimerinit + '\'' +
                '}';
    }
}