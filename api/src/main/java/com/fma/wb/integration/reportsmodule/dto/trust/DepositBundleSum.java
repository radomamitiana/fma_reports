package com.fma.wb.integration.reportsmodule.dto.trust;

import java.io.Serializable;

/**
 * This model is going to be used for doing the mapping with the Deposit_Bundle_Sum table within Infinity database
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class DepositBundleSum implements Serializable {
    private String procDate;
    private String pocketGroup;
    private int numChecks;
    private float ttlCks;

    public String getProcDate() {
        return procDate;
    }

    public void setProcDate(String procDate) {
        this.procDate = procDate;
    }

    public String getPocketGroup() {
        return pocketGroup;
    }

    public void setPocketGroup(String pocketGroup) {
        this.pocketGroup = pocketGroup;
    }

    public int getNumChecks() {
        return numChecks;
    }

    public void setNumChecks(int numChecks) {
        this.numChecks = numChecks;
    }

    public float getTtlCks() {
        return ttlCks;
    }

    public void setTtlCks(float ttlCks) {
        this.ttlCks = ttlCks;
    }

    /**
     * Constructor without parameter
     */
    public DepositBundleSum() {
    }

    @Override
    public String toString() {
        return "DepositBundleSum{" +
                "procDate='" + procDate + '\'' +
                ", pocketGroup='" + pocketGroup + '\'' +
                ", numChecks=" + numChecks +
                ", ttlCks=" + ttlCks +
                '}';
    }
}