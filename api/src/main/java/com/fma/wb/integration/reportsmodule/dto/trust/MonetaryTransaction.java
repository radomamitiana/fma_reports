package com.fma.wb.integration.reportsmodule.dto.trust;

import java.io.Serializable;

/**
 * This model is going to be used for doing the mapping with the Monetary_transaction table within Infinity database
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class MonetaryTransaction implements Serializable {
    private String date;
    private String debtorId;
    private String amount;
    private String transaction_type;
    //amount is type numeric(11,2)
    private int monetary_transaction_id;
    private int reversing_id;
    private char active;
    private String client_id;
    private int invoice_id;
    private int payment_type_id;
    private int payment_application_id;

    //new fields added for healthcare
    private String status;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getMonetary_transaction_id() {
        return monetary_transaction_id;
    }

    public void setMonetary_transaction_id(int monetary_transaction_id) {
        this.monetary_transaction_id = monetary_transaction_id;
    }

    public int getReversing_id() {
        return reversing_id;
    }

    public void setReversing_id(int reversing_id) {
        this.reversing_id = reversing_id;
    }

    public String getDebtorId() {
        return debtorId;
    }

    public void setDebtorId(String debtorId) {
        this.debtorId = debtorId;
    }

    public int getActive() {
        return active;
    }

    public void setActive(char active) {
        this.active = active;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public int getInvoice_id() {
        return invoice_id;
    }

    public void setInvoice_id(int invoice_id) {
        this.invoice_id = invoice_id;
    }

    public int getPayment_type_id() {
        return payment_type_id;
    }

    public void setPayment_type_id(int payment_type_id) {
        this.payment_type_id = payment_type_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Constructor without parameter
     */
    public MonetaryTransaction() {
    }

    @Override
    public String toString() {
        return "MonetaryTransaction{" +
                "date='" + date + '\'' +
                ", debtorId='" + debtorId + '\'' +
                ", amount=" + amount +
                ", transaction_type='" + transaction_type + '\'' +
                ", monetary_transaction_id=" + monetary_transaction_id +
                ", reversing_id=" + reversing_id +
                ", active=" + active +
                ", client_id='" + client_id + '\'' +
                ", invoice_id=" + invoice_id +
                ", payment_type_id=" + payment_type_id +
                '}';
    }

    public int getPayment_application_id() {
        return payment_application_id;
    }

    public void setPayment_application_id(int payment_application_id) {
        this.payment_application_id = payment_application_id;
    }
}