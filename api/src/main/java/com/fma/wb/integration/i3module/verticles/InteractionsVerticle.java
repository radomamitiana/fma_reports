package com.fma.wb.integration.i3module.verticles;

import com.fma.wb.integration.i3module.services.I3WService;
import com.fma.wb.integration.i3module.dto.CompositeType;
import com.fma.wb.integration.common.dto.api.ApiResponse;
import com.fma.wb.integration.common.dto.enums.WsResponseType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import common.Logger;
import common.LoggerTag;
import io.vertx.core.*;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import static com.fma.wb.integration.common.utils.UrlConfig.*;
import static utils.Configuration.RESPONSE_STATUS_CODE_OK;
import static utils.Configuration.STATUS_CODE_500;

/**
 * A verticle is a component of the application. We can deploy verticles to run the components.
 * The start method will be called when verticle is deployed. And notice this start method takes a parameter typed Future<Void>,
 * which means this is asynchronous start method. The Future indicates whether your actions have been done.
 * After done, you can call complete on the Future (or fail) to notify that you are done (success or failure).
 *
 * This represents the Interaction verticle. This will contain all resource calls directly related to the interaction.
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class InteractionsVerticle extends AbstractVerticle{

    private Context context;
    private Vertx vertx;
    private I3WService i3WService;
    private Gson gson;

    /**
     * init method
     * @param vertx
     * @param context
     */
    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
        this.context = context;
        this.vertx = vertx;
        i3WService = I3WService.getInstance(vertx,null);
        
        gson = new GsonBuilder().create();
    }

    /**
     * start method
     * @param fut
     */
    @Override
    public void start(Future<Void> fut)  {

        System.out.println("starting interaction verticle...");

        final Router router = Router.router(vertx);
        //router.route().handler(BodyHandler.create());

        router.route("/").handler(routingContext-> {
            HttpServerResponse response = routingContext.response();
            response
                    .putHeader("content-type", "text/html")
                    .end("<h1>Interaction verticle is running</h1>");
        });

        //Settings routes
        router.post(SET_I3AVAILABLE_STATUS)
                .produces("application/json")
                .consumes("application/json")
                .handler(this::setI3AvailableStatus);

        router.post(GET_AVAILABLE_CAMPAIGN)
                .produces("application/json")
                .consumes("application/json")
                .handler(InteractionsVerticle.this::getAvailableCampaign);

        router.post(MARK_DIALER_CALL_COMPLETE)
                .produces("application/json")
                .consumes("application/json")
                .handler(this::markDialerCallComplete);

        router.post(GET_INCOMING_CALL_INFO)
                .produces("application/json")
                .consumes("application/json")
                .handler(this::getIncomingCallInfo);

        router.post(BEGIN_PLAY_AUDIO)
                .produces("application/json")
                .consumes("application/json")
                .handler(this::beginPlayAudio);

        router.post(RECORDING_CALL)
                .produces("application/json")
                .consumes("application/json")
                .handler(this::recordingCall);

        router.post(RECORDING_SWITCH+":onOff")
                .produces("application/json")
                .consumes("application/json")
                .handler(this::recordingSwitch);

        router.post(HAS_INCOMING_CALL)
                .produces("application/json")
                .consumes("application/json")
                .handler(this::hasIncoming);

        router.post(MAKE_PHONE_CALL)
                .produces("application/json")
                .consumes("application/json")
                .handler(this::makePhoneCall);

        router.post(MAKE_CONFERENCE_CALL)
                .produces("application/json")
                .consumes("application/json")
                .handler(this::makeConferenceCall);

        router.post(MAKE_TRANSFER_CALL)
                .produces("application/json")
                .consumes("application/json")
                .handler(this::makeTransferCall);

        router.post(CONNECT_CONF_CALL)
                .produces("application/json")
                .consumes("application/json")
                .handler(this::connectConfCall);

        router.post(NO_CONF_CALL)
                .produces("application/json")
                .consumes("application/json")
                .handler(this::noConfCall);

        router.post(CONNECT_TRANSFER_CALL)
                .produces("application/json")
                .consumes("application/json")
                .handler(this::connectTransferCall);

        router.post(DISCONNECT_TRANSFER_CALL)
                .produces("application/json")
                .consumes("application/json")
                .handler(this::disconnectTransferCall);

        router.post(DISCONNECT_CALL)
                .produces("application/json")
                .consumes("application/json")
                .handler(this::disconnectCall);

        router.post(AGENT_LESS_CALL_COMPLETE)
                .produces("application/json")
                .consumes("application/json")
                .handler(this::agentLessCallComplete);

        HttpServerOptions options = new HttpServerOptions().setLogActivity(true);
        vertx.createHttpServer(options).requestHandler(router::accept).listen(config().getInteger("http.port", 9092));
    }

    /**
     * setI3AvailableStatus method
     * @param routingContext
     */
    private void setI3AvailableStatus(RoutingContext routingContext) {

        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);
            Logger.getInstance("").log(LoggerTag.DEBUG,"CParam2, [" +cParam.toString()+ "]","");

                    vertx.<Void>executeBlocking(future -> {

                        this.i3WService.setI3AvailableStatus(cParam);
                        future.complete();
                    }, asyncResult -> {

                        if (asyncResult.succeeded()) {
                            routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end();
                        } else {
                            asyncResult.cause().printStackTrace();
                            ApiResponse errorResponse = new ApiResponse();
                            errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                            errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                            routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                            Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
                        }
                    });


        });
        //this request may block event loop, so we use executeBlocking for handle it

    }

    /**
     * getAvailableCampaign method
     * @param routingContext
     */
    private void getAvailableCampaign(RoutingContext routingContext) {

        routingContext.request().bodyHandler(body -> {
            String reqBody=body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody,CompositeType.class);
            Logger.getInstance("").log(LoggerTag.DEBUG,"CParam2, [" +cParam.toString()+ "]","");
            //this request may block event loop, so we use executeBlocking for handle it
            vertx.<String[]>executeBlocking(future -> {

                String[] responseResult = this.i3WService.getAvailableCampaign(cParam);
                future.complete(responseResult);
            }, asyncResult -> {

                if (asyncResult.succeeded()) {

                    ApiResponse response = new ApiResponse();
                    response.setResponseType(WsResponseType.TYPE_REQUEST);
                    response.setData(this.gson.toJson(asyncResult.result()));

                    routingContext.response()
                            .setStatusCode(RESPONSE_STATUS_CODE_OK)
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .end(this.gson.toJson(response));
                } else {
                    ApiResponse errorResponse = new ApiResponse();
                    errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                    errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                    routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                    Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
                }
            });

        });

    }

    /**
     * markDialerCallComplete method
     * @param routingContext
     */
    private void markDialerCallComplete(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);
            //this request may block event loop, so we use executeBlocking for handle it
            vertx.<Boolean>executeBlocking(future -> {

                boolean responseResult = this.i3WService.markDialerCallComplete(cParam);
                future.complete(responseResult);
            }, asyncResult -> {

                if (asyncResult.succeeded()) {
                    ApiResponse response = new ApiResponse();
                    response.setResponseType(WsResponseType.TYPE_REQUEST);
                    response.setData(asyncResult.result().toString());

                    routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end(this.gson.toJson(response));
                } else {
                    ApiResponse errorResponse = new ApiResponse();
                    errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                    errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                    routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                    Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
                }
            });
        });

    }

    /**
     * getIncomingCallInfo method
     * @param routingContext
     */
    private void getIncomingCallInfo(RoutingContext routingContext) {

        routingContext.request().bodyHandler(body -> {
                    String reqBody = body.toString();
                    CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);


        //this request may block event loop, so we use executeBlocking for handle it
        vertx.<String[]>executeBlocking(future -> {

            String[] responseResult = this.i3WService.getIncomingCallInfo(cParam);
            future.complete(responseResult);
        }, asyncResult -> {

            if (asyncResult.succeeded()) {

                ApiResponse response = new ApiResponse();
                response.setResponseType(WsResponseType.TYPE_REQUEST);
                response.setData(this.gson.toJson(asyncResult.result()));

                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                ApiResponse errorResponse=new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * beginPlayAudio method
     * @param routingContext
     */
    private void beginPlayAudio(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
                    String reqBody = body.toString();
                    CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);
        //this request may block event loop, so we use executeBlocking for handle it
        vertx.<Integer>executeBlocking(future -> {

            int responseResult = this.i3WService.beginPlayAudio(cParam);
            future.complete(responseResult);
        }, asyncResult -> {

            if (asyncResult.succeeded()) {
                ApiResponse response = new ApiResponse();
                response.setResponseType(WsResponseType.TYPE_REQUEST);
                response.setData(""+asyncResult.result());

                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * recordingCall method
     * @param routingContext
     */
    private void recordingCall(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);
        //this request may block event loop, so we use executeBlocking for handle it
        vertx.<Boolean>executeBlocking(future -> {

            boolean responseResult = this.i3WService.recordingCall(cParam);
            future.complete(responseResult);
        }, asyncResult -> {

            if (asyncResult.succeeded()) {
                ApiResponse response = new ApiResponse();
                response.setResponseType(WsResponseType.TYPE_REQUEST);
                response.setData(""+asyncResult.result());

                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * recordingSwitch method
     * @param routingContext
     */
    private void recordingSwitch(RoutingContext routingContext) {

        routingContext.request().bodyHandler(body -> {
                    String reqBody = body.toString();
                    CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);

        //boolean onOff = this.gson.fromJson(routingContext.getBodyAsString(),Boolean.class);
        boolean onOff = Boolean.valueOf(routingContext.request().getParam("onOff"));
        //this request may block event loop, so we use executeBlocking for handle it
        vertx.<Boolean>executeBlocking(future -> {

            boolean responseResult = this.i3WService.recordingSwitch(cParam, onOff);
            //Once the recordingSwitch call which can be a blocking call is done, we need to send the returned result (responseResult)
            future.complete(responseResult);
        }, asyncResult -> {
            //Then we execute this block
            if (asyncResult.succeeded()) {
                ApiResponse response = new ApiResponse();
                //TYPE_REQUEST: for telling this is the result of a request
                response.setResponseType(WsResponseType.TYPE_REQUEST);
                response.setData(""+asyncResult.result());

                //We send the responseResult to the (icallFmars or alertui), both apps are our clients here
                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * hasIncoming method
     * @param routingContext
     */
    private void hasIncoming(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
                    String reqBody = body.toString();
                    CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);
        //this request may block event loop, so we use executeBlocking for handle it
        vertx.<String[]>executeBlocking(future -> {

            String[] responseResult = this.i3WService.hasIncoming(cParam);
            future.complete(responseResult);
        }, asyncResult -> {

            if (asyncResult.succeeded()) {
                ApiResponse response = new ApiResponse();
                response.setResponseType(WsResponseType.TYPE_REQUEST);
                response.setData(this.gson.toJson(asyncResult.result()));

                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * makePhoneCall method
     * @param routingContext
     */
    private void makePhoneCall(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);
        //this request may block event loop, so we use executeBlocking for handle it
        vertx.<String>executeBlocking(future -> {

            String responseResult = this.i3WService.makePhoneCall(cParam);
            future.complete(responseResult);
        }, asyncResult -> {

            if (asyncResult.succeeded()) {
                ApiResponse response = new ApiResponse();
                response.setResponseType(WsResponseType.TYPE_REQUEST);
                response.setData(asyncResult.result());

                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * makeConferenceCall method
     * @param routingContext
     */
    private void makeConferenceCall(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);

        vertx.<Integer>executeBlocking(future -> {

            int responseResult = this.i3WService.makeConferenceCall(cParam);
            future.complete(responseResult);
        }, asyncResult -> {

            if (asyncResult.succeeded()) {
                ApiResponse response = new ApiResponse();
                response.setResponseType(WsResponseType.TYPE_REQUEST);
                response.setData(""+asyncResult.result());

                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                asyncResult.cause().printStackTrace();
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * connectConfCall method
     * @param routingContext
     */
    private void connectConfCall(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);

        vertx.<Integer>executeBlocking(future -> {

            int responseResult = this.i3WService.connectConfCall(cParam);
            future.complete(responseResult);
        }, asyncResult -> {

            if (asyncResult.succeeded()) {
                ApiResponse response = new ApiResponse();
                response.setResponseType(WsResponseType.TYPE_REQUEST);
                response.setData(""+asyncResult.result());

                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * noConfCall method
     * @param routingContext
     */
    private void noConfCall(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);
        vertx.<Void>executeBlocking(future -> {

            this.i3WService.noConfCall(cParam);
            future.complete();
        }, asyncResult -> {

            if (asyncResult.succeeded()) {
                routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end();

            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * makeTransferCall method
     * @param routingContext
     */
    private void makeTransferCall(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);
        //this request may block event loop, so we use executeBlocking for handle it
        vertx.<Integer>executeBlocking(future -> {

            int responseResult = this.i3WService.makeTransferCall(cParam);
            future.complete(responseResult);
        }, asyncResult -> {

            if (asyncResult.succeeded()) {

                ApiResponse response = new ApiResponse();
                response.setResponseType(WsResponseType.TYPE_REQUEST);
                response.setData(""+asyncResult.result());

                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                asyncResult.cause().printStackTrace();
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * connectTransferCall method
     * @param routingContext
     */
    private void connectTransferCall(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);
        vertx.<Integer>executeBlocking(future -> {

            int responseResult = this.i3WService.connectTransferCall(cParam);
            future.complete(responseResult);
        }, asyncResult -> {

            if (asyncResult.succeeded()) {

                ApiResponse response = new ApiResponse();
                response.setResponseType(WsResponseType.TYPE_REQUEST);
                response.setData(""+asyncResult.result());

                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * disconnectTransferCall method
     * @param routingContext
     */
    private void disconnectTransferCall(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);

        vertx.<Void>executeBlocking(future -> {

            this.i3WService.disconnectTransferCall(cParam);
            future.complete();
        }, asyncResult -> {

            if (asyncResult.succeeded()) {
                routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end();

            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * disconnectCall method
     * @param routingContext
     */
    private void disconnectCall(RoutingContext routingContext) {
        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);
        vertx.<Void>executeBlocking(future -> {

            this.i3WService.disconnectCall(cParam);
            future.complete();
        }, asyncResult -> {

            if (asyncResult.succeeded()) {
                routingContext.response().setStatusCode(RESPONSE_STATUS_CODE_OK).end();

            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    /**
     * agentLessCallComplete / disposition method
     * @param routingContext
     */
    private void agentLessCallComplete(RoutingContext routingContext) {

        routingContext.request().bodyHandler(body -> {
            String reqBody = body.toString();
            CompositeType cParam = this.gson.fromJson(reqBody, CompositeType.class);

        vertx.<Integer>executeBlocking(future -> {

            int responseResult = this.i3WService.agentLessCallComplete(cParam);
            future.complete(responseResult);
        }, asyncResult -> {

            if (asyncResult.succeeded()) {
                ApiResponse response = new ApiResponse();
                response.setResponseType(WsResponseType.TYPE_REQUEST);
                response.setData(""+asyncResult.result());

                routingContext.response()
                        .setStatusCode(RESPONSE_STATUS_CODE_OK)
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(this.gson.toJson(response));
            } else {
                ApiResponse errorResponse = new ApiResponse();
                errorResponse.setResponseType(WsResponseType.TYPE_ERROR);
                errorResponse.setData(this.gson.toJson(asyncResult.cause()));
                routingContext.response().setStatusCode(STATUS_CODE_500).end(this.gson.toJson(errorResponse));
                Logger.getInstance("").log(LoggerTag.ERROR,asyncResult.cause().getMessage(),"");
            }
        });
        });
    }

    @Override
    public void stop() {
        System.out.println("Stopping interaction verticle");

    }
}