package com.fma.wb.integration;

import com.fma.wb.integration.common.dto.api.ApiResponse;
import com.fma.wb.integration.common.dto.enums.WsResponseType;
import com.fma.wb.integration.i3module.services.I3WService;
import com.fma.wb.integration.i3module.verticles.AutonomyMaintainerVerticle;
import com.fma.wb.integration.i3module.verticles.DialerVerticle;
import com.fma.wb.integration.i3module.verticles.InteractionsVerticle;
import com.fma.wb.integration.i3module.verticles.SessionVerticle;
import com.fma.wb.integration.reportsmodule.verticles.MainReportVerticle;
import com.google.gson.Gson;
import common.Logger;
import common.LoggerTag;
import io.vertx.core.*;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.StaticHandler;

import static com.fma.wb.integration.common.utils.UrlConfig.GET_ACTIVE_USERS_LINK;

/**
 * This class represents our main verticle. This is the main entrance / entry point.
 * This is the main verticle that will start the other verticles for a performance ease.
 * This is listening to the port 9091
 * This will contain the common methods that is not directly related to the dialer or Interaction or Session
 *
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class MainVerticle extends AbstractVerticle implements I3WService.ServerEventMessenger {

    private Context context;
    private Vertx vertx;
    private I3WService i3WService;
    ServerWebSocket webSocket;
    /*public static SessionFactory fmaSessionFactory;
    public static SessionFactory avdsSessionFactory;
    public static SessionFactory liveVoxSessionFactory;*/
    static MainVerticle mainVerticle;
    /**
     * This is just for a testing purpose
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("Vert.x web service");
        Vertx vertx = Vertx.vertx();
        mainVerticle=new MainVerticle();
        vertx.deployVerticle(mainVerticle);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Stopping Vert.x web service");

            // close  connection on report module
            try {
                MainReportVerticle.mainReportWebSocket.end();
            }catch (Exception ignored){}
            vertx.deploymentIDs().forEach(vertx::undeploy);
            vertx.undeploy(mainVerticle.deploymentID());
            mainVerticle.stop();
        }));

        //new HealthExcelAndPPTFileHelper().generatePPTFile("111AAA-Dhkk");

        /*vertx.fileSystem().open("test_data.json", new OpenOptions(), ar -> {
            if (ar.succeeded()) {
                System.out.println("Read...");
                AsyncFile asyncFile = ar.result();
                JsonParser jsonParser = JsonParser.newParser(asyncFile);
                jsonParser.arrayValueMode()
                        .exceptionHandler(t -> {
                            t.printStackTrace();
                            asyncFile.close();
                        })
                        .endHandler(v -> {
                            asyncFile.close();
                        }).handler(event -> {
                    if (event.type() == VALUE) {

                        ArrayList<TrustBalancingOutputData> dataPoint = new Gson().fromJson(event.arrayValue().encode(), TypeToken.getParameterized(ArrayList.class, TrustBalancingOutputData.class).getType());
                        String str= new TrustExcelFileGenerator(dataPoint).getBase64GeneratedFile();



                    }
                });
            } else {
                ar.cause().printStackTrace();
            }
        });*/

    }

    /**
     * init method
     * @param vertx
     * @param context
     */
    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
        this.context = context;
        this.vertx = vertx;
        i3WService = I3WService.getInstance(vertx,this);
    }

    /**
     * start method
     * @param future
     */
    @Override
    public void start(Future<Void> future)  {

        //System.out.println( "Starting main verticle..." );
        Logger.getInstance("").log(LoggerTag.INFORMATION,"Starting main verticle...","");
        Router router = Router.router(vertx);
        router.route("/").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response
                    .putHeader("content-type", "text/html")
                    .end("<h1>Webservice is running</h1>");
        });

        //createServer: I instanciate the server
        //Vert.x has multiple handler: web socket, http,etc...
        //Here we setting 2 connection handler for our http server: one for web socket connection and one for a classic http request
        //So vert.x uses the web socket handler
        //Otherwise it uses the request handler
        //A handler is mean to process an event
        HttpServerOptions options = new HttpServerOptions().setLogActivity(true);
        vertx.createHttpServer(options)
                .websocketHandler(websocket -> {
                    //this.mainReportWebSocket is the mainReportWebSocket of the MainVerticle class
                    MainVerticle.this.webSocket = websocket;
                    //handle messages.
                    //message represents the message sent. It is a string
                    webSocket.textMessageHandler(message -> {
                        Logger.getInstance("").log(LoggerTag.DEBUG,"Websocket message: [ "+message+" ]","");
                        switch (message.split("-")[0]){
                            case "NEW_CALL_DEBTOR_ID":
                                //Here, message looks like this: NEW_CALL_DEBTOR_ID-interactionId:debtorId
                                vertx.eventBus().send("calls_getting",message.split("-")[1]);
                                break;


                            case "NEW_DOCUMENTED_CALL":
                                //Here, message looks like this: NEW_CALL_DEBTOR_ID-debtorId
                                vertx.eventBus().send("invoice_info_available",message.split("-")[1]);
                                break;

                            default:
                                break;
                        }
                    });


                    System.out.println("WS REQUESTED");
                    websocket.writeTextMessage("CONNECTION ACCEPTED");

                    if (websocket.path().equals("/client_event_bus/")) {
                        System.out.println("WS connexion: " + websocket.path());

                    } else {
                        websocket.reject();
                    }
                    websocket.accept();
                })
                .requestHandler(router::accept)
                .listen(
                        config().getInteger("http.port", 9090),
                        result -> {
                            if (result.succeeded()) {
                                future.complete();
                            } else {
                                future.fail(result.cause());
                            }
                        }
                );
        //Adding all others verticles to this main verticle. At this point, we are starting all the other verticles
        CompositeFuture.all(deployHelper(AutonomyMaintainerVerticle.class.getName()),deployHelper(DialerVerticle.class.getName()),
                deployHelper(InteractionsVerticle.class.getName()),deployHelper(MainReportVerticle.class.getName()),deployHelper(SessionVerticle.class.getName())).setHandler(result -> {
            if(result.succeeded()){
                future.complete();
            } else {
                future.fail(result.cause());
            }
        });

/*        vertx.<Void>executeBlocking(endConfig -> {
            configureHibernate();
            endConfig.complete();
        }, asyncResult -> {

            if (asyncResult.succeeded()) {
                System.out.println("Hibernate configuration ended");
            } else {
                System.out.println("Error while setting hibernate");

            }
        });*/

        //assests handling
        router.route("/assets/*").handler(StaticHandler.create("assets"));
        //Settings routes
        router.get(GET_ACTIVE_USERS_LINK)
                .produces("application/json")
		.handler(this::getAllActive);

    }

    /**
     * stop server method
     */

    @Override
    public void stop() {
            Logger.getInstance("").log(LoggerTag.INFORMATION,"Shutting down application","");
    }

    /**
     * getAllActive method
     * @param routingContext
     */
    private void getAllActive(RoutingContext routingContext) {

        String[] users = this.i3WService.showUsers();
        ApiResponse response = new ApiResponse();
        response.setResponseType(WsResponseType.TYPE_REQUEST);
        response.setData(new Gson().toJson(users));
        routingContext.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(new Gson().toJson(response));
    }


    /**
     * deployHelper method
     * @param name
     * @return
     */
    private Future<Void> deployHelper(String name){

        final Future<Void> future = Future.future();
        final DeploymentOptions options = new DeploymentOptions().setWorker(true)
                .setWorkerPoolSize(1000);
        vertx.deployVerticle(name,options,res -> {
            if(res.failed()){
                Logger.getInstance("").log(LoggerTag.ERROR,"Failed to deploy verticle " + name,"");
                future.fail(res.cause());
            } else {
                System.out.println("Deployed: " + name);
                future.complete();
            }
        });

        return future;
    }

    /**
     * sendMessage method: send the message/notification from the phone system server to the client caller
     * @param o
     */
    @Override
    public void sendMessage(ApiResponse o){
        //EventArgs eventArgs=Json.decodeValue(o.getData(),EventArgs.class);
        //String curSessionId=eventArgs.getInteraction().getSessionId();
        //vertx.eventBus().send("/server_event_bus/"+curSessionId, Json.encode(o));
        webSocket.writeTextMessage(Json.encodePrettily(o));
    }



    /*public void configureHibernate()
    {
        // Configure Hibernate logging to only log SEVERE errors
        @SuppressWarnings("unused")
        org.jboss.logging.Logger logger = org.jboss.logging.Logger.getLogger("org.hibernate");
        java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.SEVERE);

        try {

            Configuration cfgFma = createHibernateConfiguration(ConfigurationUtils.INF_DB_HOST,ConfigurationUtils.INF_DB_USER,ConfigurationUtils.INF_DB_PSSWD);
            Configuration cfgAVDS = createHibernateConfiguration(ConfigurationUtils.IC_DB_HOST,ConfigurationUtils.IC_DB_USER,ConfigurationUtils.IC_DB_PSSWD);
            Configuration cfgLiveVox = createHibernateConfiguration(ConfigurationUtils.LIVE_DB_HOST,ConfigurationUtils.LIVE_DB_USER,ConfigurationUtils.LIVE_DB_PSSWD);

            cfgFma.addAnnotatedClass(AutonomyRecordingsMetadata.class);
            cfgFma.addAnnotatedClass(CallResponseLog.class);
            cfgFma.addAnnotatedClass(InvoiceActivity.class);

            cfgAVDS.addAnnotatedClass(CallHistory.class);
            cfgAVDS.addAnnotatedClass(InteractionSummary.class);
            cfgAVDS.addAnnotatedClass(IrRecordingMedia.class);


            fmaSessionFactory=cfgFma.buildSessionFactory();
            avdsSessionFactory=cfgAVDS.buildSessionFactory();
            liveVoxSessionFactory=cfgLiveVox.buildSessionFactory();


        } catch (Exception e) {
            System.out.println();
            e.printStackTrace();
        }
    }



    private Configuration createHibernateConfiguration(String host,String user,String psswd) {
        String url = host+ ";databaseName=" + "FMA";
        Configuration cfg = new Configuration()
                .setProperty("hibernate.connection.driver_class", "com.microsoft.sqlserver.jdbc.SQLServerDriver")
                .setProperty("hibernate.connection.url", url)
                .setProperty("hibernate.connection.username", user)
                .setProperty("hibernate.connection.password", psswd)
                .setProperty("hibernate.connection.autocommit", "true")
                .setProperty("hibernate.show_sql", "false");

        // Tell Hibernate to use the 'SQL Server' dialect when dynamically
        // generating SQL queries
        cfg.setProperty("hibernate.dialect", "org.hibernate.dialect.SQLServerDialect");
        // Tell Hibernate to show the generated T-SQL
        cfg.setProperty("hibernate.show_sql", "false");
        // Tremove in production
        cfg.setProperty("hibernate.hbm2ddl.auto", "update");
        return cfg;
    }*/
}
