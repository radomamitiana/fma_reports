package com.fma.wb.integration.i3module.dto;

import java.io.Serializable;

/**
 * @author Tiana Andriamanalina
 * @version 1.0
 * CallResponseLog class is used for the mapping
 */


public class CallResponseLog implements Serializable {
    private String callidkey;
    private long i3_identity;
    private String table_name;
    private String debtor_id;
    private String workflowname;
    private String campaignname;
    private String callplacedtime;
    private String finishcode;
    private String debtor_comment_written;
    private String note;
    private String active;
    private String created_on;
    private int created_by;
    private String modified_on;
    private int modified_by;
    private double amount_payment;
    private String phonenumber;
    private String agentid;
    private int length;

    public String getCallidkey() {
        return callidkey;
    }

    public void setCallidkey(String callidkey) {
        this.callidkey = callidkey;
    }

    public long getI3_identity() {
        return i3_identity;
    }

    public void setI3_identity(long i3_identity) {
        this.i3_identity = i3_identity;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getDebtor_id() {
        return debtor_id;
    }

    public void setDebtor_id(String debtor_id) {
        this.debtor_id = debtor_id;
    }

    public String getWorkflowname() {
        return workflowname;
    }

    public void setWorkflowname(String workflowname) {
        this.workflowname = workflowname;
    }

    public String getCampaignname() {
        return campaignname;
    }

    public void setCampaignname(String campaignname) {
        this.campaignname = campaignname;
    }

    public String getCallplacedtime() {
        return callplacedtime;
    }

    public void setCallplacedtime(String callplacedtime) {
        this.callplacedtime = callplacedtime;
    }

    public String getFinishcode() {
        return finishcode;
    }

    public void setFinishcode(String finishcode) {
        this.finishcode = finishcode;
    }

    public String getDebtor_comment_written() {
        return debtor_comment_written;
    }

    public void setDebtor_comment_written(String debtor_comment_written) {
        this.debtor_comment_written = debtor_comment_written;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public int getCreated_by() {
        return created_by;
    }

    public void setCreated_by(int created_by) {
        this.created_by = created_by;
    }

    public String getModified_on() {
        return modified_on;
    }

    public void setModified_on(String modified_on) {
        this.modified_on = modified_on;
    }

    public int getModified_by() {
        return modified_by;
    }

    public void setModified_by(int modified_by) {
        this.modified_by = modified_by;
    }

    public double getAmount_payment() {
        return amount_payment;
    }

    public void setAmount_payment(double amount_payment) {
        this.amount_payment = amount_payment;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getAgentid() {
        return agentid;
    }

    public void setAgentid(String agentid) {
        this.agentid = agentid;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public CallResponseLog() {
    }

    @Override
    public String toString() {
        return "CallResponseLog{" +
                "callidkey='" + callidkey + '\'' +
                ", i3_identity=" + i3_identity +
                ", table_name='" + table_name + '\'' +
                ", debtor_id='" + debtor_id + '\'' +
                ", workflowname='" + workflowname + '\'' +
                ", campaignname='" + campaignname + '\'' +
                ", callplacedtime=" + callplacedtime +
                ", finishcode='" + finishcode + '\'' +
                ", debtor_comment_written='" + debtor_comment_written + '\'' +
                ", note='" + note + '\'' +
                ", active='" + active + '\'' +
                ", created_on=" + created_on +
                ", created_by=" + created_by +
                ", modified_on=" + modified_on +
                ", modified_by=" + modified_by +
                ", amount_payment=" + amount_payment +
                ", phonenumber='" + phonenumber + '\'' +
                ", agentid='" + agentid + '\'' +
                ", length=" + length +
                '}';
    }
}