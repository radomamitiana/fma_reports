package com.fma.wb.integration.reportsmodule.utils;

import com.fma.wb.integration.common.dto.User;
import com.fma.wb.integration.reportsmodule.dto.Option;
import java.util.ArrayList;
import java.util.Arrays;

public class AvOptions {
    private  ArrayList<Option> optionsList= new ArrayList<>();


    public AvOptions(){
        this.optionsList.add(new Option("./assets/img/hospital.svg", "Healthcare", new ArrayList<>(Arrays.asList("1", "2", "3","MMCFARLANE", "PKOENIG", "CEO", "DBURKETT","TYOUNG","MKELLEHER", "ALANS", "TBIRNER", "PLEWIS", "KHOLLINGSW", "SGLANVILLE", "DSIERRA"))));
        this.optionsList.add(new Option("./assets/img/analysis.svg", "Trust Balancing", new ArrayList<>(Arrays.asList("1","MMCFARLANE", "PKOENIG", "SGLANVILLE", "ALANS", "CEO", "DBURKETT","TYOUNG","MKELLEHER", "TBIRNER", "PLEWIS", "KHOLLINGSW", "SKOEHN", "SPAISLEY", "JCROSS","JCABRERA"))));
        this.optionsList.add(new Option("./assets/img/time.svg", "Split time", new ArrayList<>(Arrays.asList("1", "2", "3","CMONNIER", "MMCFARLANE", "PKOENIG", "MJANAKES","SGLANVILLE", "RPOND","MCHAMBERS", "DSIERRA", "ALANS", "CEO", "DBURKETT","TYOUNG","MKELLEHER", "TBIRNER", "PLEWIS", "KHOLLINGSW"))));
        this.optionsList.add(new Option("./assets/img/compare.svg", "Collectors results", new ArrayList<>(Arrays.asList("1", "2", "3", "MMCFARLANE", "PKOENIG", "SGLANVILLE", "RPOND","MCHAMBERS", "DSIERRA", "ALANS", "CEO", "DBURKETT","TYOUNG","MKELLEHER", "TBIRNER", "PLEWIS", "KHOLLINGSW", "SKOEHN", "SPAISLEY", "JCROSS","JCABRERA"))));
        this.optionsList.add(new Option("./assets/img/communication.svg", "Client services", new ArrayList<>(Arrays.asList("1", "2", "3","MMCFARLANE", "PKOENIG", "SGLANVILLE", "RPOND","MCHAMBERS", "DSIERRA", "ALANS", "CEO", "DBURKETT","TYOUNG","MKELLEHER", "TBIRNER", "PLEWIS", "KHOLLINGSW", "SKOEHN", "SPAISLEY", "JCROSS","JCABRERA"))));
        this.optionsList.add(new Option("./assets/img/collaboration.svg", "Request service", new ArrayList<>(Arrays.asList("1", "2", "3","MMCFARLANE", "PKOENIG", "SGLANVILLE", "RPOND","MCHAMBERS", "DSIERRA", "ALANS", "CEO", "DBURKETT","TYOUNG","MKELLEHER", "TBIRNER", "PLEWIS", "KHOLLINGSW", "SKOEHN", "SPAISLEY", "JCROSS","JCABRERA"))));
        this.optionsList.add(new Option("./assets/img/change.svg", "Collections", new ArrayList<>(Arrays.asList("1", "2", "3","MMCFARLANE", "PKOENIG", "SGLANVILLE", "RPOND","MCHAMBERS", "DSIERRA", "ALANS", "CEO", "DBURKETT","TYOUNG","MKELLEHER", "TBIRNER", "PLEWIS", "KHOLLINGSW", "SKOEHN", "SPAISLEY", "JCROSS","JCABRERA"))));
        this.optionsList.add(new Option("./assets/img/growth.svg", "Collector score card", new ArrayList<>(Arrays.asList("1", "2", "3","MMCFARLANE", "PKOENIG", "SGLANVILLE", "RPOND","MCHAMBERS", "DSIERRA", "ALANS", "CEO", "DBURKETT","TYOUNG","MKELLEHER", "TBIRNER", "PLEWIS", "KHOLLINGSW", "SKOEHN", "SPAISLEY", "JCROSS","JCABRERA"))));
        this.optionsList.add(new Option("./assets/img/folder.svg", "New client setup", new ArrayList<>(Arrays.asList("1", "2", "3","MMCFARLANE", "PKOENIG", "SGLANVILLE", "RPOND","MCHAMBERS", "DSIERRA", "ALANS", "CEO", "DBURKETT","TYOUNG","MKELLEHER", "TBIRNER", "PLEWIS", "KHOLLINGSW", "SKOEHN", "SPAISLEY", "JCROSS","JCABRERA"))));
        this.optionsList.add(new Option("./assets/img/users-group.svg", "Absences points", new ArrayList<>(Arrays.asList("1", "2","MMCFARLANE", "PKOENIG", "SGLANVILLE", "RPOND","MCHAMBERS", "DSIERRA", "ALANS", "CEO", "DBURKETT","TYOUNG","MKELLEHER", "TBIRNER", "PLEWIS", "KHOLLINGSW", "SKOEHN", "SPAISLEY", "JCROSS","JCABRERA"))));
        this.optionsList.add(new Option("./assets/img/locked.svg", "Security Access", new ArrayList<>(Arrays.asList("1", "2", "3","MMCFARLANE", "PKOENIG", "ALANS", "CEO", "DBURKETT","TYOUNG","MKELLEHER", "TBIRNER", "PLEWIS", "KHOLLINGSW", "SKOEHN", "SPAISLEY", "JCROSS","JCABRERA"))));
        this.optionsList.add(new Option("./assets/img/forbidden.svg", "Non conformity", new ArrayList<>(Arrays.asList("1", "2", "3","MMCFARLANE", "PKOENIG", "MJANAKES","SGLANVILLE", "RPOND","MCHAMBERS", "DSIERRA", "ALANS", "CEO", "DBURKETT","TYOUNG","MKELLEHER", "TBIRNER", "PLEWIS", "KHOLLINGSW", "SKOEHN", "SPAISLEY", "JCROSS","JCABRERA"))));
        this.optionsList.add(new Option("./assets/img/analysis.svg", "Quickbook Balancing", new ArrayList<>(Arrays.asList("1","MMCFARLANE", "PKOENIG", "SGLANVILLE", "ALANS", "CEO", "DBURKETT","TYOUNG","MKELLEHER", "TBIRNER", "PLEWIS", "KHOLLINGSW", "SKOEHN", "SPAISLEY", "JCROSS","JCABRERA"))));
    }


    public ArrayList<Option> getUserOptions(User user){
        ArrayList<Option> rolesRetour=new ArrayList<>();
        if(user.getRoles()==null || user.getRoles().isEmpty())
            return new ArrayList<>();
        else{
            user.getRoles().forEach(s -> {
                for (Option option : this.optionsList) {
                    if (option.getRoles().contains(String.valueOf(s)) || option.getRoles().contains(user.getUsername())){
                        if(!rolesRetour.contains(option))
                            rolesRetour.add(option);
                    }
                }
            });

            return rolesRetour;
        }
    }
}
