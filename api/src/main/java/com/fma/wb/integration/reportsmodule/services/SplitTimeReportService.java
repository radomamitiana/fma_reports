package com.fma.wb.integration.reportsmodule.services;

import com.fma.wb.integration.common.dao.DbConnexionHelper;
import com.fma.wb.integration.common.dto.User;
import com.fma.wb.integration.common.dto.api.InternalResponse;
import com.fma.wb.integration.common.dto.enums.DBType;
import com.fma.wb.integration.reportsmodule.dto.splittime.SplitTimeDto;
import com.fma.wb.integration.reportsmodule.dto.splittime.SplitTimeMapping;
import com.fma.wb.integration.reportsmodule.dto.splittime.SplitTimeReportInputData;
import com.fma.wb.integration.reportsmodule.dto.splittime.SplitTimeReportOutputData;
import common.Logger;
import common.LoggerTag;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

public class SplitTimeReportService {
    private static SplitTimeReportService splitTimeReportService;
    private Vertx vertx;
    private static DbConnexionHelper dbConnexionHelper;
    private  HashMap<String,String> credentials = new HashMap<>();
    private static HashMap<String, User> activeSessions = new HashMap<>();

    public DbConnexionHelper getDbConnexionHelper() {
        return dbConnexionHelper;
    }

    private void setDbConnexionHelper(DbConnexionHelper db) {
        dbConnexionHelper = db;
    }

    public static HashMap<String, User> getActiveSessions() {
        return activeSessions;
    }

    public void setActiveSessions(HashMap<String, User> activeSessionsG) {
        activeSessions = activeSessionsG;
    }

    public SplitTimeReportService(Vertx vertx) {
        this.vertx = vertx;
        this.setDbConnexionHelper(new DbConnexionHelper(vertx,credentials));

    }

    public static SplitTimeReportService getInstance(Vertx vertx) {
        if (splitTimeReportService == null) {
            splitTimeReportService = new SplitTimeReportService(vertx);
        }
        return splitTimeReportService;
    }

    /**
     *
     * @param sessionId
     * @param splitTimeReportInputData
     * @param handler
     */
    public void getSplitTimeData(String sessionId, SplitTimeReportInputData splitTimeReportInputData, Future<InternalResponse> handler){
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: getSplitTime data ~~~~~~~~~~~~~~~~~~~~~~","");
        System.out.println("SESS: "+ Json.encodePrettily(SplitTimeReportService.getActiveSessions()));
        System.out.println("Id: "+ sessionId);
        System.out.println("splitTimeReportInputData.getInitiatedDateTimeBegin.: "+ splitTimeReportInputData.getInitiatedDateTimeBegin());
        System.out.println("splitTimeReportInputData.getInitiatedDateTimeEnd: "+ splitTimeReportInputData.getInitiatedDateTimeEnd());

        SplitTimeReportOutputData splitTimeReportOutputData = new SplitTimeReportOutputData();

        //Need to convert dates from and to criteria to GMT Date Time before querying the database
        //What we receive from the UI 2019-07-21T00:00:00-05:00
        //2019-07-15 20:14:25.000
        String dataBegin = splitTimeReportInputData.getInitiatedDateTimeBegin().substring(0, 10).trim();
        //String dataEnd = dataBegin.substring(0, 10).trim();
        System.out.println("dataBegin: "+ dataBegin);

        //set request date
        splitTimeReportOutputData.setDateRequest(dataBegin);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localEndDate = LocalDate.parse(dataBegin.substring(0, 10).trim(),formatter).plusDays(1);
        splitTimeReportInputData.setInitiatedDateTimeEnd(localEndDate.format(formatter));

        //splitTimeReportInputData.setInitiatedDateTimeBegin(Utils.toGMTDate(splitTimeReportInputData.getInitiatedDateTimeBegin()));
        //splitTimeReportInputData.setInitiatedDateTimeEnd(Utils.toGMTDate(splitTimeReportInputData.getInitiatedDateTimeEnd()));

        splitTimeReportInputData.setInitiatedDateTimeBegin(dataBegin);
        System.out.println("dataEnd: "+ splitTimeReportInputData.getInitiatedDateTimeEnd());

        if (!TrustBalancingService.getActiveSessions().containsKey(sessionId)){
            InternalResponse resp = new InternalResponse();
            resp.setCause(-1);
            if (!handler.isComplete()) {
                handler.complete(resp);
            }
        } else {

            if (dbConnexionHelper.getFmaConnection() == null) {
                try {


                    TrustBalancingService.connectToDb(DBType.Infinity,event ->{
                        if (event.succeeded()) {
                            dbConnexionHelper.setFmaConnection(event.result());

                            //Begin check phone system connection
                            if (dbConnexionHelper.getGenesysConnection() == null) {
                                try {
                                    TrustBalancingService.connectToDb(DBType.Phone_Sys, eventPhoneSystem -> {

                                        if (eventPhoneSystem.succeeded()) {
                                            dbConnexionHelper.setGenesysConnection(eventPhoneSystem.result());

                                            //Start split time calculation data retrieval
                                            this.processSplitTimeCalculation(splitTimeReportInputData,handler, splitTimeReportOutputData);

                                        } else {
                                            InternalResponse response1 = new InternalResponse();
                                            response1.setCause(0);
                                            response1.setData(eventPhoneSystem.cause().getMessage());
                                            if (!handler.isComplete()) {
                                                handler.complete(response1);
                                            }
                                        }
                                    },dbConnexionHelper);
                                } catch (SQLException | ClassNotFoundException e) {
                                    e.printStackTrace();
                                    InternalResponse response1 = new InternalResponse();
                                    response1.setCause(0);
                                    response1.setData(e.getMessage());
                                    if (!handler.isComplete()) {
                                        handler.complete(response1);
                                    }
                                }
                            } else {
                                //Start split time calculation data retrieval
                                this.processSplitTimeCalculation(splitTimeReportInputData,handler, splitTimeReportOutputData);
                            }

                            //End check phone system connection

                        } else {
                            InternalResponse resp = new InternalResponse();
                            resp.setCause(0);
                            resp.setData(event.cause().getMessage());
                            if (!handler.isComplete()) {
                                handler.complete(resp);
                            }
                        }
                    },dbConnexionHelper);

                } catch (SQLException | ClassNotFoundException e) {
                    e.printStackTrace();
                    InternalResponse resp = new InternalResponse();
                    resp.setCause(0);
                    resp.setData(e.getMessage());
                    if (!handler.isComplete()) {
                        handler.complete(resp);
                    }
                }
            } else {
                //Check phone system connection
                if (dbConnexionHelper.getGenesysConnection() == null) {
                    try {
                        TrustBalancingService.connectToDb(DBType.Phone_Sys, event -> {

                            if (event.succeeded()) {
                                dbConnexionHelper.setGenesysConnection(event.result());

                                //Start split time calculation data retrieval
                                this.processSplitTimeCalculation(splitTimeReportInputData,handler, splitTimeReportOutputData);

                            } else {
                                InternalResponse resp = new InternalResponse();
                                resp.setCause(0);
                                resp.setData(event.cause().getMessage());
                                if (!handler.isComplete()) {
                                    handler.complete(resp);
                                }
                            }

                        },dbConnexionHelper);
                    } catch (SQLException | ClassNotFoundException e) {
                        e.printStackTrace();
                        InternalResponse resp = new InternalResponse();
                        resp.setCause(0);
                        resp.setData(e.getMessage());
                        if (!handler.isComplete()) {
                            handler.complete(resp);
                        }
                    }
                } else {
                    //Check phone system connection
                    //Start Split time report data retrieval
                    this.processSplitTimeCalculation(splitTimeReportInputData,handler, splitTimeReportOutputData);
                }
            } // End big else
        }
    }

    /**
     *
     * @param splitTimeReportInputData
     * @param handler
     * @param splitTimeReportOutputData
     */
    private void processSplitTimeCalculation(SplitTimeReportInputData splitTimeReportInputData,
                                             Future<InternalResponse> handler, SplitTimeReportOutputData splitTimeReportOutputData){

        String initiatedDateTimeStart = splitTimeReportInputData.getInitiatedDateTimeBegin();
        String initiatedDateTimeEnd = splitTimeReportInputData.getInitiatedDateTimeBegin();

        String splitTimeQuery = "SELECT DISTINCT "+
                " ucdv.LocalUserId as localUserId, u.home_dept as homeDept, u.full_pt as fullPt, " +
                " CASE WHEN dt.DT_HOURS != 0 THEN dt.DT_HOURS*60 WHEN u.full_pt = 'Reg F/T' THEN 480 WHEN u.full_pt = 'Reg P/T' THEN 360 " +
                " ELSE 0 END as factor, "+
                " m1.last_name as manager, "+
                " sum(ucdv.CallDurationSeconds)/60 as durationMin, "+
                " count(ucdv.CallId) as numberOfCalls, "+
                " CONCAT(u.first_name, ' ', u.last_name) As fullName "+
                " FROM users u "+
                " LEFT JOIN [fmars102].[I3_IC_40].[dbo].[user_calldetail_viw] ucdv on ucdv.LocalUserId=u.username "+
                " LEFT JOIN collector c on c.user_id = u.user_id "+
                " LEFT JOIN manager m1 on m1.manager_id = c.manager_id "+
                " LEFT JOIN manager m2 on m2.user_id = u.user_id and m2.active = 't' "+
                " LEFT JOIN [fmars32].[EMPOWERTIME].[dbo].[E_DailyTotal] dt on dt.DT_EMPNO=u.soc_sec_num "+
                " WHERE u.active ='t' "+
                " AND u.home_dept >= 2000 "+
                " AND ucdv.InitiatedDateTimeGMT >= '"+ splitTimeReportInputData.getInitiatedDateTimeBegin()+"' AND ucdv.InitiatedDateTimeGMT < '"+ splitTimeReportInputData.getInitiatedDateTimeEnd()+"' AND ucdv.CallDurationSeconds != 0 "+
                " AND dt.DT_ADATE >= '"+ splitTimeReportInputData.getInitiatedDateTimeBegin()+"' and dt.DT_ADATE < '"+ splitTimeReportInputData.getInitiatedDateTimeEnd()+"' "+
                " AND m2.user_id is null " +
                " GROUP BY ucdv.LocalUserId, u.home_dept, m1.last_name, u.first_name, u.last_name, u.full_pt, dt.DT_HOURS";

        System.out.println("********************************      splitTimeQuery:   "+splitTimeQuery);

        dbConnexionHelper.getFmaConnection().query(String.format(splitTimeQuery, initiatedDateTimeStart, initiatedDateTimeEnd), result01 -> {
            if (result01.succeeded()) {
                List<JsonObject> results01 = result01.result().getRows(true);
                if (results01.size() == 0) {
                    InternalResponse resp = new InternalResponse();
                    resp.setCause(0);
                    resp.setData("No transaction found within infinity database for the selected period");
                    if (!handler.isComplete()) {
                        handler.complete(resp);
                    }
                } else {
                    final List<SplitTimeMapping> splitTimeDtoStream = DbConnexionHelper.returnQueryList(results01, SplitTimeMapping.class);
                    splitTimeDtoStream.forEach(splitTimeDtoStreamGot4 -> {
                        SplitTimeDto splitTimeDto = new SplitTimeDto();

                        //This allows to determine is a user collector is a full time (480) or part time employee (360)
                        splitTimeDto.setFactor(splitTimeDtoStreamGot4.getFactor());

                        splitTimeDto.setName(splitTimeDtoStreamGot4.getFullName());

                        splitTimeDto.setManager(splitTimeDtoStreamGot4.getManager());

                        splitTimeDto.setFullPt(splitTimeDtoStreamGot4.getFullPt());

                        splitTimeDto.setNumberOfCalls(Integer.valueOf(splitTimeDtoStreamGot4.getNumberOfCalls()));

                        splitTimeDto.setMinsDuration(splitTimeDtoStreamGot4.getDurationMin().toString());

                        //Calculate split time
                        BigDecimal factorInBigDecimal = new BigDecimal(splitTimeDtoStreamGot4.getFactor()).setScale(2, RoundingMode.HALF_UP);
                        BigDecimal nbOfCallsInBigDecimal = new BigDecimal(splitTimeDtoStreamGot4.getNumberOfCalls()).setScale(2, RoundingMode.HALF_UP);
                        BigDecimal durationMinInBigDecimal = BigDecimal.valueOf(splitTimeDtoStreamGot4.getDurationMin().longValue()).setScale(2, RoundingMode.HALF_UP);

                        MathContext mathContext = new MathContext(2, RoundingMode.HALF_UP);
                        BigDecimal diffSplitTime = (factorInBigDecimal.subtract(durationMinInBigDecimal)).divide(nbOfCallsInBigDecimal, mathContext);
                        splitTimeDto.setSplitTime(diffSplitTime.toString());

                        splitTimeDto.setFactor(factorInBigDecimal.toString());

                        System.out.println("splitTimeDto:    "+splitTimeDto.toString());
                        splitTimeReportOutputData.getSplitTimeDtoList().add(splitTimeDto);

                        System.out.println("splitTimeReportOutputData LIST SIZE:    "+splitTimeReportOutputData.getSplitTimeDtoList().size());
                    });
                    InternalResponse resp = new InternalResponse();
                    resp.setCause(1);
                    resp.setData(Json.encode(splitTimeReportOutputData));
                    if (!handler.isComplete()) {
                        handler.complete(resp);
                    }
                }
            } else {
                Logger.getInstance("").log(LoggerTag.ERROR, "Error getting split time data from the [fmars102].[I3_IC_40].[dbo].[user_calldetail_viw] view " + ExceptionUtils.exceptionStackTraceAsString(result01.cause()), "");
                InternalResponse resp = new InternalResponse();
                resp.setCause(0);
                resp.setData("An error happens while getting transactions from infinity database");
                if (!handler.isComplete()) {
                    handler.complete(resp);
                }
            }
        });
    }
}