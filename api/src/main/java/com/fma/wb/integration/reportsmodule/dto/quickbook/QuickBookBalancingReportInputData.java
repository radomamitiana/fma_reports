package com.fma.wb.integration.reportsmodule.dto.quickbook;

import java.io.Serializable;

public class QuickBookBalancingReportInputData implements Serializable {

    private int iWantTo;
    private String startDate;
    private String stopDate;
    private int dataSelectionRange;
    private int organizationId;
    private boolean allOrganizations;

    public int getiWantTo() {
        return iWantTo;
    }

    public void setiWantTo(int iWantTo) {
        this.iWantTo = iWantTo;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStopDate() {
        return stopDate;
    }

    public void setStopDate(String stopDate) {
        this.stopDate = stopDate;
    }

    public int getDataSelectionRange() {
        return dataSelectionRange;
    }

    public void setDataSelectionRange(int dataSelectionRange) {
        this.dataSelectionRange = dataSelectionRange;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public boolean isAllOrganizations() {
        return allOrganizations;
    }

    public void setAllOrganizations(boolean allOrganizations) {
        this.allOrganizations = allOrganizations;
    }
}
