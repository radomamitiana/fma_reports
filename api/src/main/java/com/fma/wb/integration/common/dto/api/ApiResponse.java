package com.fma.wb.integration.common.dto.api;

import com.fma.wb.integration.common.dto.enums.WsResponseType;
import java.io.Serializable;
import java.util.Date;

/**
 * Class for handle all webservice call responses
 */
public class ApiResponse implements Serializable {

    private WsResponseType responseType;
    private String data;
    private String comment;
    private String date;
    private int dataSize;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getDataSize() {
        return dataSize;
    }

    public void setDataSize(int dataSize) {
        this.dataSize = dataSize;
    }

    public ApiResponse() {
        //we set default response type to request
        this.responseType = WsResponseType.TYPE_REQUEST;
        this.date = new Date().toString();
    }

    public ApiResponse(String message, String date,String comment) {
        //we set default response type to request
        this.responseType = WsResponseType.TYPE_REQUEST;
        this.data = message;
        this.date = date;
        this.comment = comment;
    }


    public WsResponseType getResponseType() {
        return responseType;
    }

    public void setResponseType(WsResponseType responseType) {
        this.responseType = responseType;
    }


    @Override
    public String toString() {
        return "ApiResponse{" +
                "data='" + data + '\'' +
                ", date='" + date + '\'' +
                ", type='" + responseType.name() + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}

