package com.fma.wb.integration.reportsmodule.dto.trust;

import java.io.Serializable;

public class TrustOrganizationClient implements Serializable {
    private int trust_id;
    private String trustName;
    private String account_number;
    private int organization_id;
    private String orgName;
    private String client_id;
    private String client_identifier;
    private String clientName;

    public int getTrust_id() {
        return trust_id;
    }

    public void setTrust_id(int trust_id) {
        this.trust_id = trust_id;
    }

    public String getTrustName() {
        return trustName;
    }

    public void setTrustName(String trustName) {
        this.trustName = trustName;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public int getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(int organization_id) {
        this.organization_id = organization_id;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_identifier() {
        return client_identifier;
    }

    public void setClient_identifier(String client_identifier) {
        this.client_identifier = client_identifier;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public TrustOrganizationClient() {
    }

    @Override
    public String toString() {
        return "TrustOrganizationClient{" +
                "trust_id=" + trust_id +
                ", trustName='" + trustName + '\'' +
                ", account_number='" + account_number + '\'' +
                ", organization_id=" + organization_id +
                ", orgName='" + orgName + '\'' +
                ", client_id='" + client_id + '\'' +
                ", client_identifier='" + client_identifier + '\'' +
                ", clientName='" + clientName + '\'' +
                '}';
    }
}