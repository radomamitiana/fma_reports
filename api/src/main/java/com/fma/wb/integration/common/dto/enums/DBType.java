package com.fma.wb.integration.common.dto.enums;

public enum DBType {
    Phone_Sys,
    Infinity,
    Infinity2,
    Live_Vox,
    RP,
    Quick_Book
}
