package com.fma.wb.integration.i3module.dto;

import java.io.Serializable;

/**
 * @author Tiana Andriamanalina
 * @version 1.0
 * InvoiceActivity class is used for the mapping
 */


public class InvoiceActivity implements Serializable {
    private int invoice_activity_id;
    private int invoice_id;
    private String debtor_id;
    private String client_id;
    private int new_activity_code;
    private String result_code;
    private long comment_id;
    private String next_activity_date;
    private String i3_identity;
    private String next_activity_time;
    private String call_period;
    private String active;
    private String converted;
    private String status;
    private String created_on;
    private int created_by;
    private String modified_on;
    private int modified_by;
    private String source;
    private String result_direction;
    private int action_code;
    private int source_id;
    private String phone_number;
    private String callidkey;
    private String postal_code;



    public int getInvoice_activity_id() {
        return invoice_activity_id;
    }

    public void setInvoice_activity_id(int invoice_activity_id) {
        this.invoice_activity_id = invoice_activity_id;
    }

    public int getInvoice_id() {
        return invoice_id;
    }

    public void setInvoice_id(int invoice_id) {
        this.invoice_id = invoice_id;
    }

    public String getDebtor_id() {
        return debtor_id;
    }

    public void setDebtor_id(String debtor_id) {
        this.debtor_id = debtor_id;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public int getNew_activity_code() {
        return new_activity_code;
    }

    public void setNew_activity_code(int new_activity_code) {
        this.new_activity_code = new_activity_code;
    }

    public String getResult_code() {
        return result_code;
    }

    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

    public long getComment_id() {
        return comment_id;
    }

    public void setComment_id(long comment_id) {
        this.comment_id = comment_id;
    }

    public String getNext_activity_date() {
        return next_activity_date;
    }

    public void setNext_activity_date(String next_activity_date) {
        this.next_activity_date = next_activity_date;
    }

    public String getNext_activity_time() {
        return next_activity_time;
    }

    public void setNext_activity_time(String next_activity_time) {
        this.next_activity_time = next_activity_time;
    }

    public String getCall_period() {
        return call_period;
    }

    public void setCall_period(String call_period) {
        this.call_period = call_period;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getConverted() {
        return converted;
    }

    public void setConverted(String converted) {
        this.converted = converted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public int getCreated_by() {
        return created_by;
    }

    public void setCreated_by(int created_by) {
        this.created_by = created_by;
    }

    public String getModified_on() {
        return modified_on;
    }

    public void setModified_on(String modified_on) {
        this.modified_on = modified_on;
    }

    public int getModified_by() {
        return modified_by;
    }

    public void setModified_by(int modified_by) {
        this.modified_by = modified_by;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getResult_direction() {
        return result_direction;
    }

    public void setResult_direction(String result_direction) {
        this.result_direction = result_direction;
    }

    public int getAction_code() {
        return action_code;
    }

    public void setAction_code(int action_code) {
        this.action_code = action_code;
    }

    public int getSource_id() {
        return source_id;
    }

    public void setSource_id(int source_id) {
        this.source_id = source_id;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getCallidkey() {
        return callidkey;
    }

    public void setCallidkey(String callidkey) {
        this.callidkey = callidkey;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public InvoiceActivity() {
    }

    @Override
    public String toString() {
        return "InvoiceActivity{" +
                "invoice_activity_id=" + invoice_activity_id +
                ", invoice_id=" + invoice_id +
                ", debtor_id='" + debtor_id + '\'' +
                ", client_id='" + client_id + '\'' +
                ", new_activity_code=" + new_activity_code +
                ", result_code='" + result_code + '\'' +
                ", comment_id=" + comment_id +
                ", next_activity_date=" + next_activity_date +
                ", next_activity_time='" + next_activity_time + '\'' +
                ", call_period='" + call_period + '\'' +
                ", active='" + active + '\'' +
                ", converted='" + converted + '\'' +
                ", status='" + status + '\'' +
                ", created_on=" + created_on +
                ", created_by=" + created_by +
                ", modified_on=" + modified_on +
                ", modified_by=" + modified_by +
                ", source='" + source + '\'' +
                ", result_direction='" + result_direction + '\'' +
                ", action_code=" + action_code +
                ", source_id=" + source_id +
                ", phone_number='" + phone_number + '\'' +
                ", callidkey='" + callidkey + '\'' +
                ", postal_code='" + postal_code + '\'' +
                '}';
    }

    public String getI3_identity() {
        return i3_identity;
    }

    public void setI3_identity(String i3_identity) {
        this.i3_identity = i3_identity;
    }
}