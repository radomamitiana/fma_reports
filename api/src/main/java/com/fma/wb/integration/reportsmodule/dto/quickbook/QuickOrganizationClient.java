package com.fma.wb.integration.reportsmodule.dto.quickbook;

import java.io.Serializable;

public class QuickOrganizationClient implements Serializable {
    private int client_id;
    private String clientName;
    private int fee_grace_period;
    private String paid_agency;
    private String paid_direct;
    private String nsf_agency;
    private String nsf_direct;
    private String description;

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public int getFee_grace_period() {
        return fee_grace_period;
    }

    public void setFee_grace_period(int fee_grace_period) {
        this.fee_grace_period = fee_grace_period;
    }

    public String getPaid_agency() {
        return paid_agency;
    }

    public void setPaid_agency(String paid_agency) {
        this.paid_agency = paid_agency;
    }

    public String getPaid_direct() {
        return paid_direct;
    }

    public void setPaid_direct(String paid_direct) {
        this.paid_direct = paid_direct;
    }

    public String getNsf_agency() {
        return nsf_agency;
    }

    public void setNsf_agency(String nsf_agency) {
        this.nsf_agency = nsf_agency;
    }

    public String getNsf_direct() {
        return nsf_direct;
    }

    public void setNsf_direct(String nsf_direct) {
        this.nsf_direct = nsf_direct;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}