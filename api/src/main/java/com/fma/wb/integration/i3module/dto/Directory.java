package com.fma.wb.integration.i3module.dto;

import java.io.Serializable;

/**
 * @author Tiana Andriamanalina
 * @version 1.0
 * Directory class is used for the mapping
 */
public class Directory implements Serializable {
    private int directory_id;
    private String directory_name;
    private String cancel_status;

    public int getDirectory_id() {
        return directory_id;
    }

    public void setDirectory_id(int directory_id) {
        this.directory_id = directory_id;
    }

    public String getDirectory_name() {
        return directory_name;
    }

    public void setDirectory_name(String directory_name) {
        this.directory_name = directory_name;
    }

    public String getCancel_status() {
        return cancel_status;
    }

    public void setCancel_status(String cancel_status) {
        this.cancel_status = cancel_status;
    }

    public Directory() {
    }

    @Override
    public String toString() {
        return "Directory{" +
                "directory_id=" + directory_id +
                ", directory_name='" + directory_name + '\'' +
                ", cancel_status='" + cancel_status + '\'' +
                '}';
    }
}