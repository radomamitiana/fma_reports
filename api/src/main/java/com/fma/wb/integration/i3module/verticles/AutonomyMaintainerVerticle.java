package com.fma.wb.integration.i3module.verticles;

import com.fma.wb.integration.i3module.dto.AutonomyRecordingsMetadata;
import com.fma.wb.integration.common.dto.enums.DBType;
import com.fma.wb.integration.common.dao.DbConnexionHelper;
import com.google.gson.Gson;
import common.Logger;
import common.LoggerTag;
import interactions.InteractionController;
import io.vertx.core.*;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.web.Router;
import org.glassfish.jersey.internal.util.ExceptionUtils;
import rest_api_core.CustomOkHttpClient;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;
import static com.fma.wb.integration.i3module.utils.CommonDbQueries.*;
import static com.fma.wb.integration.common.utils.ConfigurationUtils.*;
import com.fma.wb.integration.common.utils.Utils;

/**
 *  AutonomyMaintainerVerticle class. This class handles all processes needed for recording calls metadata into autonomy recording metadata table.
 *  @author Tiana Andriamanalina
 *  @version 1.0
 */
public class AutonomyMaintainerVerticle extends AbstractVerticle {

    private Context context;
    private Vertx vertx;
    private DbConnexionHelper dbConnexionHelper;
    private  HashMap<String,String> credentials=new HashMap<>();
    private LocalMap<String, String> debtors_interactions ;

    /**
     * init method: the first method called when the verticle is deployed and runned
     * @param vertx
     * @param context
     */
    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
        this.context = context;
        this.vertx = vertx;
        SharedData sd = vertx.sharedData();
        debtors_interactions = sd.getLocalMap("debtors_interactions");

        /*HashMap<String,String > read=AutonomyPersistance.getInstance("").retrieveAutonomie();
        if(read!=null){
            debtors_interactions.putAll(AutonomyPersistance.getInstance("").retrieveAutonomie());

        }*/
    }

    /**
     * start method: is launched after init
     * @param fut
     */
    @Override
    public void start(Future<Void> fut)  {

        System.out.println( "Starting periodicals actions verticle ..." );
        Router router = Router.router(vertx);

        router.route("/").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response
                    .putHeader("content-type", "text/html")
                    .end("<h1>AutonomyMaintainerVerticle is running</h1>");
        });

        //we just start an application server
        HttpServerOptions options = new HttpServerOptions().setLogActivity(true);
        vertx.createHttpServer(options)
                .requestHandler(router)
                .listen(
                        config().getInteger("http.port", 9094),result -> {
                            if (result.succeeded()) {
                                fut.complete();

                            } else {
                                fut.fail(result.cause());
                            }
                        }
                );

        //Listen for event bus messages
        //Here we are retrieving all messages sent to calls_getting(calls_getting is like an endPoint)
        //This is for retrieving interactionId and debtorId and add it to the hashMap
        vertx.eventBus().consumer("calls_getting", message -> {
            String msg = message.body().toString();
            Logger.getInstance("").log(LoggerTag.INFORMATION,"Call message got = " + msg,"");
            this.debtors_interactions.putIfAbsent(msg.split(":")[0],msg.split(":")[1]);
            //message.reply("Debtor call info registered");

        });

        //Here we can start autonomy recording update once the call is ended
        vertx.eventBus().consumer("start_autonomie_after_call",message->{
            Logger.getInstance("").log(LoggerTag.INFORMATION,"GOT: "+message.body().toString(),"");
            this.startAutonomyRecording(message.body().toString(),end->System.out.println("......End doAutonomie....."));

            //message.reply("Starting autonomy recording Insert...");
        });

        //This happens when we document a call (after a call). We retrieve here the debtorId from infinity. We check if the combination debtorId/interactionId is in the debtors_interactions hashMap.
        //If Yes, it means the debtorId is not inserted yet into autonomy recording metadata table. Otherwise, it is already recorded into the  autonomy recording metadata table.
        //Once the debtorId is recorded in that table, we remove the combination iteractionId/debtorId from the hashMap
        //This method s goal is just to make sure that the debtoirId is added to the autonomy recording metadata table once the call is documented
        //we always check if the combination interactionId/debtorId is already present in the autonomy recording metadata table or not. If not, we add it
        vertx.eventBus().consumer("invoice_info_available",message->{
            final String[] interaction_id = {""};
            final String[] debtor_id = { message.body().toString() };
            Logger.getInstance("").log(LoggerTag.INFORMATION,"invoice infos av. "+message.body().toString(),"");
            debtors_interactions.forEach((s, s2) -> {
                if (s2.equals(debtor_id[0])) {
                    interaction_id[0] = s;
                }
                if (!interaction_id[0].isEmpty()){
                    this.doAutonomyInsert(debtor_id[0], interaction_id[0],end->System.out.println("---End doAutonomie: "+end));
                    //message.reply("Starting update autonomy recording with invoice infos...");
                }
            });
        });

        try {

            //Here we connect to the database
            dbConnexionHelper = DbConnexionHelper.getInstance(vertx,credentials);

            //Adding columnStore by default
            //addColumnStoreToFMATables();
            //Periodical HashMap Checking.
            setPeriodicByDay(10000, event -> {
                if(!debtors_interactions.isEmpty()) {
                    /*try{
                        AutonomyPersistance.getInstance("").persist(debtors_interactions);
                    }catch (Exception e){
                    }*/

                   String  message = new Gson().toJson(debtors_interactions);
                    Logger.getInstance("").log(LoggerTag.INFORMATION, "Periodic autonomy: { \n"+message+"\n}", "");

                    debtors_interactions.forEach((s, s2) -> {
                        if(s2.isEmpty() || s2.equals("null") || s2==null){
                            try {
                                if (this.dbConnexionHelper.getGenesysConnection() == null ) {
                                    connectToDb(DBType.Phone_Sys, avdsEvent -> {
                                        this.dbConnexionHelper.setGenesysConnection(avdsEvent.result());
                                        this.dbConnexionHelper.getGenesysConnection().query("USE " + IC_DB_NAME+" ;", inIc -> {
                                            if (inIc.succeeded()) {
                                                this.dbConnexionHelper.getGenesysConnection().query(String.format(GET_DEBTOR_FROM_CALL_HISTORY, s.trim()), resultDebt -> {
                                                    if (resultDebt.succeeded()) {
                                                        if (resultDebt.result().getNumRows() == 1) {
                                                            String csdata=resultDebt.result().toJson().getString("customData1");
                                                            doAutonomyInsert(csdata, s,end -> Logger.getInstance("").log(LoggerTag.INFORMATION,"---End doAutonomie: " + end,""));
                                                        } else {
                                                            Logger.getInstance("").log(LoggerTag.ERROR,"Error getting debtor Id for interaction: " + s,"");
                                                            debtors_interactions.remove(s);
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    });
                                } else {
                                    this.dbConnexionHelper.getGenesysConnection().query(String.format(GET_DEBTOR_FROM_CALL_HISTORY, s.trim()), resultDebt -> {
                                        if (resultDebt.succeeded()) {
                                            if (resultDebt.result().getNumRows() == 1) {
                                                doAutonomyInsert(resultDebt.result().toJson().getString("customData1"), s,end -> Logger.getInstance("").log(LoggerTag.INFORMATION,"---End doAutonomie: " + end,""));
                                            } else {
                                                Logger.getInstance("").log(LoggerTag.ERROR,"Error getting debtor Id for interaction: " + s,"");
                                                debtors_interactions.remove(s);                                            }
                                        } else {
                                            resultDebt.cause().printStackTrace();
                                        }
                                    });
                                }

                            } catch (SQLException | ClassNotFoundException e) {
                                debtors_interactions.remove(s);
                            }
                        }else{
                            doAutonomyInsert(s2, s, end ->Logger.getInstance("").log(LoggerTag.INFORMATION,"---End doAutonomie: " + end,""));
                        }
                    });
                }
            });


            //doAutonomyInsert("111111", "22222");


        } catch (Exception e) {
            Logger.getInstance("").log(LoggerTag.ERROR,ExceptionUtils.exceptionStackTraceAsString(e),"");
            //e.printStackTrace();
        }
    }



    /*private void startGettingInvoiceInfos(String debtorId){
      //timerId=vertx.setPeriodic(5000, aLong -> {
        //Check if debtors infos are available*
        if (fmaConnection!=null)
        fmaConnection.query("SELECT * FROM " + INVOICE_TABLE_NAME + " WHERE debtor_id= "+debtorId,event1 -> {
        });
        //});
   }*/

    private void stopPeriodic(int timerId){
        vertx.cancelTimer(timerId);
    }

    private void setPeriodicByDay(int period,Handler<Long> handler){
        vertx.setPeriodic(period,ee->{
            //Exec code only from 6 a.m to 10 p.m
            if (LocalDateTime.now().getHour() >= 6 && LocalDateTime.now().getHour() <= 22) {
                handler.handle(ee);
            }
        });
    }

    private void startAutonomyRecording(String msg, Handler<String> handler){
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~START: startAutonomyRecording ~~~~~~~~~~~~~~~~~~~~~~");

        ArrayList<String> data= Json.decodeValue(msg,ArrayList.class);
        String interactionId = data.get(0);
        String currentSessionId = data.get(1);
        String csrfToken = data.get(2);
        String cookie = data.get(3);
        String currentDebtor2 = debtors_interactions.get(interactionId);
        if ((debtors_interactions.containsKey(interactionId)) && (!("null".equals(currentDebtor2.toLowerCase()))) && (!currentDebtor2.isEmpty())){
            //We process autonomy updating directly
            //String currentDebtor = debtors_interactions.get(interactionId);
            doAutonomyInsert(currentDebtor2,interactionId,handler);
        } else {
            try {
                InteractionController controller = new InteractionController();
                CustomOkHttpClient client=controller.getCustomOkHttpClient();
                client.setConnexionCookie(cookie);
                client.setCsrfToken(csrfToken);
                controller.setCustomOkHttpClient(client);

                HashMap<String, String> attr = controller.getInteractionAttributes(currentSessionId,"debtor_id",interactionId,null);
                if (attr != null && !attr.get("debtor_id").isEmpty()){
                    //IMC call attributes got/received
                    String currentDebtor = attr.get("debtor_id");
                    doAutonomyInsert(currentDebtor,interactionId,handler);
                } else {
                    //Get debtor_id from callHistory JOINED to InteractionSummary
                    try {
                        if (this.dbConnexionHelper.getGenesysConnection() == null) {
                            connectToDb(DBType.Phone_Sys, avdsEvent -> {
                                this.dbConnexionHelper.setGenesysConnection(avdsEvent.result());
                                this.dbConnexionHelper.getGenesysConnection().query("USE " + IC_DB_NAME+" ;", inIc -> {
                                    if (inIc.succeeded()) {
                                        this.dbConnexionHelper.getGenesysConnection().query(String.format(GET_DEBTOR_FROM_CALL_HISTORY, interactionId.trim()), resultDebt -> {
                                            if (resultDebt.succeeded()) {
                                                if (resultDebt.result().getNumRows() == 1) {
                                                    doAutonomyInsert(resultDebt.result().toJson().getString("customData1"), interactionId,handler);
                                                } else {
                                                    Logger.getInstance("").log(LoggerTag.ERROR, "Error getting debtor Id for interaction: " + interactionId, "");
                                                }
                                            }
                                        });
                                    }
                                });
                            });
                        } else {
                            this.dbConnexionHelper.getGenesysConnection().query(String.format(GET_DEBTOR_FROM_CALL_HISTORY, interactionId.trim()), resultDebt -> {
                                if (resultDebt.succeeded()) {
                                    if (resultDebt.result().getNumRows() == 1) {
                                        doAutonomyInsert(resultDebt.result().toJson().getString("customData1"), interactionId,handler);
                                    } else {
                                        Logger.getInstance("").log(LoggerTag.ERROR, "Error getting debtor Id for interaction: " + interactionId, "");
                                    }
                                }
                            });
                        }

                    } catch (SQLException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void doAutonomyInsert(String debtor_id,String interactionId,Handler<String> handler) {
       /* System.out.println("~~~~~~~~~~~~~~~~~~~~~~START: doAutonomyInsert ~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~ debtor_id: "+debtor_id+"      interactionId: "+interactionId+"  ~~~~~~~~~~~~~~~~~~~~~~");
*/
        if (debtor_id.isEmpty() || debtor_id==null || debtor_id.equals("null")
                || interactionId.isEmpty() || interactionId==null || interactionId.equals("null")) {
//            System.out.print(" Imcomplete values, will retry later");
            return;
        }

        try {
            if(this.dbConnexionHelper.getGenesysConnection()==null)
                connectToDb(DBType.Phone_Sys,avdsEvent -> {
                    if(avdsEvent.succeeded()){

                        this.dbConnexionHelper.setGenesysConnection(avdsEvent.result());
                        this.dbConnexionHelper.getGenesysConnection().execute("USE "+IC_DB_NAME,inIc->{
                            if (inIc.succeeded()){
                                String query = String.format(GET_I3_CALL_METRIC, interactionId.trim());
                                this.dbConnexionHelper.getGenesysConnection().query(query, resultAvds -> {
                                    if (!resultAvds.succeeded()) {
                                        Logger.getInstance("").log(LoggerTag.ERROR,"doAutonomie: " + ExceptionUtils.exceptionStackTraceAsString(resultAvds.cause()),"");
                                    } else {
                                        List<JsonObject>resultats=resultAvds.result().getRows(true);
                                        try {

                                            switch (resultats.size()){
                                                case 0:
                                                    handler.handle("Empty rows");
                                                    break;

                                                default:

                                                    if(this.dbConnexionHelper.getFmaConnection()==null)
                                                        connectToDb(DBType.Infinity, infEvent -> {
                                                            if (infEvent.succeeded()) {
                                                                this.dbConnexionHelper.setFmaConnection( infEvent.result());
                                                                final Stream<AutonomyRecordingsMetadata> autonomyStream = DbConnexionHelper.returnQueryStream(resultats,AutonomyRecordingsMetadata.class);
                                                                autonomyStream
                                                                        .peek(AutonomyRecordingsMetadata::synchCorrectDateFields)
                                                                        .forEach(x-> {
                                                                            x.setDebtor_id(debtor_id);
                                                                            processInsert(x,handler,this.dbConnexionHelper.getFmaConnection(),interactionId);
                                                                        });


                                                            } else {
                                                                Logger.getInstance("").log(LoggerTag.ERROR,"---End doAutonomie: " + ExceptionUtils.exceptionStackTraceAsString(infEvent.cause()),"");
                                                            }

                                                        });
                                                    else{
                                                        final Stream<AutonomyRecordingsMetadata> autonomyStream = DbConnexionHelper.returnQueryStream(resultats,AutonomyRecordingsMetadata.class);
                                                        autonomyStream
                                                                .peek(AutonomyRecordingsMetadata::synchCorrectDateFields)
                                                                .forEach(x-> {
                                                                    x.setDebtor_id(debtor_id);
                                                                    processInsert(x,handler,this.dbConnexionHelper.getFmaConnection(),interactionId);
                                                                });
                                                    }
                                                    break;

                                            }

                                        } catch (SQLException | ClassNotFoundException e) {
                                            e.printStackTrace();
                                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e),"");

                                        }
                                    }
                                });


                            } else {
                                Logger.getInstance("").log(LoggerTag.ERROR,"inIc UNSUCCEEDED: " + ExceptionUtils.exceptionStackTraceAsString(inIc.cause()),"");
                            }

                        });

                    }else{
                        Logger.getInstance("").log(LoggerTag.ERROR,"IC connection UNSUCCEEDED","");

                    }

                });
                //to reduce later
            else{
                this.dbConnexionHelper.getGenesysConnection().execute("USE "+IC_DB_NAME,inIc->{
                    if (inIc.succeeded()){
                        //System.out.println(inIc.result().toJson());
//                            System.out.println("TEST USE QUERRY SUCCESS");
                        String query = String.format(GET_I3_CALL_METRIC, interactionId.trim());
                        //System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++  query:   "+query);
                        //genesysConnection.query("select insu.RemoteNumberCallId as remote_number, DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), insu.InitiatedDateTimeUTC) as initiated_date, DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), insu.ConnectedDateTimeUTC) as connected_date, DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), insu.TerminatedDateTimeUTC) as terminated_date, CASE WHEN (ConnectedDateTimeUTC <= '1970-01-02' OR TerminatedDateTimeUTC <= '1970-01-02') THEN 0 WHEN (ConnectedDateTimeUTC > TerminatedDateTimeUTC) THEN 0 ELSE DATEDIFF(SECOND,  ConnectedDateTimeUTC, TerminatedDateTimeUTC) END AS call_duration, CAST(ROUND(insu.tHeld / 1000., 0) AS INTEGER) AS hold_duration, insu.LastLocalUserId as voice_user_id,insu.LastLocalName as voice_full_name, irrm.MediaURI as recording_file_name, irrm.FileSize as recording_file_size,  CAST(CASE insu.Direction  WHEN 0 THEN 'Unknown'  WHEN 1 THEN 'I'  WHEN 2 THEN 'O'  WHEN 3 THEN 'Intercom'  ELSE 'Invalid value' END AS VARCHAR(20))AS direction, insu.InteractionIDKey as recorded_call_id_key, REPLACE(REPLACE(insu.RemoteNumberCallId,'+',''),'/','') as ani, insu.DNIS_LocalID as dnis,  insu.LastLocalNumber as local_number, insu.RemoteNumberCallId as remote_number from InteractionSummary insu left join ir_recordingMedia irrm on insu.InteractionIDKey=irrm.queueobjectidkey  WHERE insu.InteractionID = '"+interactionId+"' and irrm.MediaURI not like '%fmars94%' and irrm.MediaURI not like '%fmars95%' AND irrm.MediaURI like '%.sasf'", resultAvds -> {
                        this.dbConnexionHelper.getGenesysConnection().query(query, resultAvds -> {
                            if (!resultAvds.succeeded()) {
                                Logger.getInstance("").log(LoggerTag.ERROR,"doAutonomie: " + ExceptionUtils.exceptionStackTraceAsString(resultAvds.cause()),"");
                            } else {
                                List<JsonObject>resultats=resultAvds.result().getRows(true);
                                try {

                                    switch (resultats.size()){
                                        case 0:
                                            handler.handle("Empty rows");
                                            break;

                                        default:

                                            if(this.dbConnexionHelper.getFmaConnection()==null)
                                                connectToDb(DBType.Infinity, infEvent -> {
                                                    if (infEvent.succeeded()) {
                                                        this.dbConnexionHelper.setFmaConnection( infEvent.result());
                                                        final Stream<AutonomyRecordingsMetadata> autonomyStream = DbConnexionHelper.returnQueryStream(resultats,AutonomyRecordingsMetadata.class);
                                                        autonomyStream
                                                                .peek(AutonomyRecordingsMetadata::synchCorrectDateFields)
                                                                .forEach(x-> {
                                                                    x.setDebtor_id(debtor_id);
                                                                    processInsert(x,handler,this.dbConnexionHelper.getFmaConnection(),interactionId);
                                                                });


                                                    } else {
                                                        Logger.getInstance("").log(LoggerTag.ERROR,"---End doAutonomie: " + ExceptionUtils.exceptionStackTraceAsString(infEvent.cause()),"");
                                                    }

                                                });
                                            else{
                                                final Stream<AutonomyRecordingsMetadata> autonomyStream = DbConnexionHelper.returnQueryStream(resultats,AutonomyRecordingsMetadata.class);
                                                autonomyStream
                                                        .peek(AutonomyRecordingsMetadata::synchCorrectDateFields)
                                                        .forEach(x-> {
                                                            x.setDebtor_id(debtor_id);
                                                            processInsert(x,handler,this.dbConnexionHelper.getFmaConnection(),interactionId);
                                                        });
                                            }
                                            break;

                                    }

                                } catch (SQLException | ClassNotFoundException e) {
                                    e.printStackTrace();
                                    Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e),"");

                                }
                            }
                        });


                    } else {
                        Logger.getInstance("").log(LoggerTag.ERROR,"inIc UNSUCCEEDED: " + ExceptionUtils.exceptionStackTraceAsString(inIc.cause()),"");
                    }

                });
            }

        } catch (SQLException | ClassNotFoundException e) {
            Logger.getInstance("").log(LoggerTag.ERROR,"" + ExceptionUtils.exceptionStackTraceAsString(e),"");

        }

    }

    private void processInsert(AutonomyRecordingsMetadata currentObject, Handler<String> handler, SQLConnection fmaConnection,String interactionId){
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~START: processInsert ~~~~~~~~~~~~~~~~~~~~~~");

        if (currentObject.getAni() != null && !currentObject.getAni().isEmpty() && currentObject.getRecord_id() == -1) {

            //check debtorId format if it starts with 00
            currentObject.setDebtor_id(this.writeDebtorIdInformationInRightFormat(currentObject.getDebtor_id()));

            currentObject.setRemote_number(Utils.applyFormatPhoneNumberCorrect(currentObject.getRemote_number()));
            currentObject.setDnis(Utils.applyFormatPhoneNumberCorrect(currentObject.getDnis()));
            currentObject.setAni(Utils.applyFormatPhoneNumberCorrect(currentObject.getAni()));

            String currentRecordingFileName = currentObject.getRecording_file_name();
            currentObject.setRecording_file_name(currentRecordingFileName.substring(0, (currentRecordingFileName.length()-5)).concat(".wav"));

            String sql = "INSERT INTO autonomy_recordings_metadata (" +
                    "recorded_call_id_key, direction, initiated_date, " +
                    "connected_date, terminated_date, call_duration, hold_duration, " +
                    "recording_file_name, recording_file_size, voice_user_id, voice_full_name, " +
                    "ani, dnis, dialer_call, manual_call, " +
                    "local_number, remote_number, cancel_status, debtor_id, source ) VALUES (" +
                    currentObject.getRecorded_call_id_key() + ",'" + currentObject.getDirection() + "','" +
                    currentObject.getInitiated_date() + "','" + currentObject.getConnected_date() + "','" +
                    currentObject.getTerminated_date() + "'," + currentObject.getCall_duration() + "," +
                    currentObject.getHold_duration() + ",'" + currentObject.getRecording_file_name() +
                    "'," + currentObject.getRecording_file_size() + ",'" + currentObject.getVoice_user_id() +
                    "','" + currentObject.getVoice_full_name() + "'," + currentObject.getAni() + "," + currentObject.getDnis() +
                    "," + currentObject.getDialer_call() + "," + currentObject.getManual_call() + "," + currentObject.getLocal_number() +
                    ",'" + currentObject.getRemote_number() + "'," + currentObject.getCancel_status() + ",'" + currentObject.getDebtor_id() +
                    "'," + currentObject.getSource()
                    + ")";
            String sql2="SELECT COUNT(*) FROM autonomy_recordings_metadata WHERE recorded_call_id_key='"+currentObject.getRecorded_call_id_key()+"'";

            fmaConnection.query(sql2, resultSelect -> {
                if (!resultSelect.succeeded()) {
                    Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(resultSelect.cause()),"");

                } else {
                    if(resultSelect.result().getRows().get(0).getInteger("").equals(0)){

                        fmaConnection.query(sql, resultInsert -> {
                            if (!resultInsert.succeeded()) {
                                Logger.getInstance("").log(LoggerTag.ERROR,ExceptionUtils.exceptionStackTraceAsString(resultInsert.cause()),"");

                            } else {
                                try {
                                    //debtors_interactions.remove(interactionId,currentObject.getDebtor_id());
                                    debtors_interactions.remove(interactionId);
                                    handler.handle("Inserted..");

                                } catch (Exception e){
                                    Logger.getInstance("").log(LoggerTag.ERROR,ExceptionUtils.exceptionStackTraceAsString(e),"");

                                    handler.handle("Inserted: Error while removing..");
                                }
                            }
                        });
                    }  else {

                        try {
                            //debtors_interactions.remove(interactionId,currentObject.getDebtor_id());
                            debtors_interactions.remove(interactionId);
                            handler.handle("Existing Data: removed...");
                        } catch (Exception e){
                            Logger.getInstance("").log(LoggerTag.ERROR,ExceptionUtils.exceptionStackTraceAsString(e),"");
                            handler.handle("Existing Data: Error while removing..");
                        }

                    }


                }
            });

        } else {
            handler.handle("values are empty or null");
        }

    }

    private void addColumnStoreIndexToTable(SQLConnection connection,String tableName,Handler<AsyncResult> handler){
        connection.execute( String.format(COLUMN_MULTISTORE_Q,tableName),result->{
            if (result.succeeded()){
                System.out.println("Column index added successful");
            } else {
                System.out.println("Existing Index");
            }
            handler.handle(result);
        });
    }


    /**
     * It allows to add indexes multi columun store to a specific table
     */
    private void addColumnStoreToFMATables(){

        //Add to infinity_Tables
        if (this.dbConnexionHelper.getFmaConnection() != null){
            addColumnStoreIndexToTable(this.dbConnexionHelper.getFmaConnection(), INVOICE_TABLE_NAME, event2 -> {
            });
            addColumnStoreIndexToTable(this.dbConnexionHelper.getFmaConnection(), AUT_RECORDING_MET_TABLE_NAME, event2 -> {
            });
            addColumnStoreIndexToTable(this.dbConnexionHelper.getFmaConnection(), INVOICE_TABLE_NAME, event2 -> {
            });
        } else {
            try {
                connectToDb(DBType.Infinity, res->{
                    if (res.succeeded()) {
                        this.dbConnexionHelper.setFmaConnection(res.result());
                        addColumnStoreIndexToTable(this.dbConnexionHelper.getFmaConnection(), INVOICE_TABLE_NAME, event2 -> {
                        });
                        addColumnStoreIndexToTable(this.dbConnexionHelper.getFmaConnection(), AUT_RECORDING_MET_TABLE_NAME, event2 -> {
                        });
                        addColumnStoreIndexToTable(this.dbConnexionHelper.getFmaConnection(), INVOICE_TABLE_NAME, event2 -> {
                        });
                    }
                });
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }

            /*getConnections(res->{
                addColumnStoreIndexToTable(fmaConnection, INVOICE_TABLE_NAME, event2 -> {
                });
                addColumnStoreIndexToTable(fmaConnection, AUT_RECORDING_MET_TABLE_NAME, event2 -> {
                });
                addColumnStoreIndexToTable(fmaConnection, INVOICE_TABLE_NAME, event2 -> {
                });
            },null,null);*/
        }

        //Add to live_vox tables
    }

    public  void connectToDb(DBType dbNumber, Handler<AsyncResult<SQLConnection>> handler) throws SQLException, ClassNotFoundException {
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~START: connectToDb ~~~~~~~~~~~~~~~~~~~~~~");

        credentials = new HashMap<>();
        switch (dbNumber){
            case Phone_Sys:
                if(this.dbConnexionHelper.getClientIC()==null){
                    credentials.put("host",IC_DB_HOST);
                    credentials.put("user",IC_DB_USER);
                    credentials.put("psswd",IC_DB_PSSWD);
                    credentials.put("db",IC_DB_NAME);
                    dbConnexionHelper.setCredentials(credentials);

                    this.dbConnexionHelper.setClientIC(dbConnexionHelper.getDbConnection(credentials));
                    this.dbConnexionHelper.getClientIC().getConnection(event -> {
                        if (event.succeeded()) {
                            System.out.println("Connection succeeded");
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()),"");

                        }
                    });
                }else{
                    this.dbConnexionHelper.getClientIC().getConnection(event -> {
                        if (event.succeeded()) {
                            System.out.println("Connection succeeded");
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()),"");

                        }
                    });

                }

                break;

            case Infinity:
                if(this.dbConnexionHelper.getClientFma()==null){
                    credentials.put("host",INF_DB_HOST);
                    credentials.put("user",INF_DB_USER);
                    credentials.put("psswd",INF_DB_PSSWD);
                    credentials.put("db",INFINITY_DB_NAME);
                    dbConnexionHelper.setCredentials(credentials);
                    this.dbConnexionHelper.setClientFma(dbConnexionHelper.getDbConnection(credentials));
                    this.dbConnexionHelper.getClientFma().getConnection(event -> {
                        if (event.succeeded()) {
                            System.out.println("Connection succeeded");
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()),"");

                        }
                    });
                }else{
                    this.dbConnexionHelper.getClientFma().getConnection(event -> {
                        if (event.succeeded()) {
                            System.out.println("Connection succeeded");
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()),"");
                        }
                    });

                }

                break;

            default:
                if(this.dbConnexionHelper.getClientVox()==null){
                    credentials.put("host",LIVE_DB_HOST);
                    credentials.put("user",LIVE_DB_USER);
                    credentials.put("psswd",LIVE_DB_PSSWD);
                    credentials.put("db",LIVE_VOX_DB_NAME);
                    dbConnexionHelper.setCredentials(credentials);
                    this.dbConnexionHelper.setClientVox(dbConnexionHelper.getDbConnection(credentials));
                    this.dbConnexionHelper.getClientVox().getConnection(event -> {
                        if (event.succeeded()) {
                            System.out.println("Connection succeeded");
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()),"");
                        }
                    });
                }else{
                    this.dbConnexionHelper.getClientVox().getConnection(event -> {
                        if (event.succeeded()) {
                            System.out.println("Connection succeeded");
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()),"");
                        }
                    });

                }

                break;
        }

/*        credentials.put("db",databaseName);

        dbConnexionHelper.setCredentials(credentials);
        //Once the connection is established, we call the handler passed as a parameter to this method and execute the codes for that section handler
        dbConnexionHelper.getDbConnection(credentials).getConnection(event -> {
            if (event.succeeded()) {
                System.out.println("Connection succeeded");
                credentials = new HashMap<>();
                handler.handle(event);
            } else {
                Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()),"");

            }
        });*/
    }



    /*private void putOrReplaceToMap(Map<String,String> map,String key,String value){
        if (map.containsKey(key)) {
            credentials.replace(key, value);
        } else {
            credentials.put(key,value);
        }
    }*/


    private String writeDebtorIdInformationInRightFormat(String debtor) {
        if (debtor != null && !debtor.isEmpty())
            if (!(debtor.substring(0, 2)).equals("00"))
                debtor="00".concat(debtor);
        return debtor;
    }

    public void closeConnections(){

        try{
            if(this.dbConnexionHelper.getGenesysConnection()!=null)
                this.dbConnexionHelper.getGenesysConnection().close();
            if(this.dbConnexionHelper.getFmaConnection()!=null)
                this.dbConnexionHelper.getFmaConnection().close();
            this.dbConnexionHelper.setGenesysConnection(null);
            this.dbConnexionHelper.setFmaConnection(null);
        }catch (Exception ignored) {
        }
    }


    public void closeConnection(JDBCClient client){
        try{
            client.close();
        }catch (Exception ignored){

        }
    }


    /**
     * This method is called when we stop the AutonomyMaintainerVerticle. Here, we are also closing database connection
     * @throws Exception
     */

    @Override
    public void stop() {
        System.out.println("Stopping aut. verticle");

            /*try{
                AutonomyPersistance.getInstance("").persist(debtors_interactions);
            }catch (Exception e){
            }*/
        try {
            if(this.dbConnexionHelper.getClientIC()!=null)
                this.dbConnexionHelper.getClientIC().close();
            if(this.dbConnexionHelper.getClientFma()!=null)
                this.dbConnexionHelper.getClientFma().close();
            if(this.dbConnexionHelper.getClientVox()!=null)
                this.dbConnexionHelper.getClientVox().close();
            if(dbConnexionHelper !=null)
                this.dbConnexionHelper.closeConnection();
        } catch (Exception e){
            //Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e),"");
        }

    }
}