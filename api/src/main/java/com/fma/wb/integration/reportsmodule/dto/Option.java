package com.fma.wb.integration.reportsmodule.dto;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class Option implements Serializable {
    private String text;
    private String icon;
    private ArrayList<String> roles = new ArrayList<>();

    public String getText() {
        return text;
    }
    
    /**
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @param icon the icon to set
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the roles
     */
    public ArrayList<String> getRoles() {
        return roles;
    }

    /**
     * @param roles the roles to set
     */
    public void setRoles(ArrayList<String> roles) {
        this.roles = roles;
    }




    
    /**
     * Constructor without parameter
     */
    public Option() {
    }

    /**
     * Constructor with parameters
     * @param icon
     * @param text
     */
    public Option(String icon, String text, ArrayList<String> roles) {
        this.setIcon(icon);
        this.text = text;
        this.roles=roles;
    }
    

}