package com.fma.wb.integration.reportsmodule.dto.trust;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This model represents the input data send from the trust balancing form interface
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class TrustBalancingInputData implements Serializable {
    private List<String> organizationList = new ArrayList<>();
    private boolean displayRecovererCodeTotals = false;
    private boolean matchData = false;
    private String beginDateRange = "";
    private String endDateRange = "";
    private boolean showClientAccountNums = false;
    private boolean disableMatchPossibleDebtors = false;
    private int findAmounts = 0;

    public List<String> getOrganizationList() {
        return organizationList;
    }

    public void setOrganizationList(List<String> organizationList) {
        this.organizationList = organizationList;
    }

    public boolean isDisplayRecovererCodeTotals() {
        return displayRecovererCodeTotals;
    }

    public void setDisplayRecovererCodeTotals(boolean displayRecovererCodeTotals) {
        this.displayRecovererCodeTotals = displayRecovererCodeTotals;
    }

    public boolean isMatchData() {
        return matchData;
    }

    public void setMatchData(boolean matchData) {
        this.matchData = matchData;
    }

    public String getBeginDateRange() {
        return beginDateRange;
    }

    public void setBeginDateRange(String beginDateRange) {
        this.beginDateRange = beginDateRange;
    }

    public String getEndDateRange() {
        return endDateRange;
    }

    public void setEndDateRange(String endDateRange) {
        this.endDateRange = endDateRange;
    }

    public boolean isShowClientAccountNums() {
        return showClientAccountNums;
    }

    public void setShowClientAccountNums(boolean showClientAccountNums) {
        this.showClientAccountNums = showClientAccountNums;
    }

    public boolean isDisableMatchPossibleDebtors() {
        return disableMatchPossibleDebtors;
    }

    public void setDisableMatchPossibleDebtors(boolean disableMatchPossibleDebtors) {
        this.disableMatchPossibleDebtors = disableMatchPossibleDebtors;
    }

    public int getFindAmounts() {
        return findAmounts;
    }

    public void setFindAmounts(int findAmounts) {
        this.findAmounts = findAmounts;
    }

    /**
     * Constructor without parameter
     */
    public TrustBalancingInputData() {
    }

    @Override
    public String toString() {
        return "TrustBalancingInputData{" +
                "organizationList=" + organizationList +
                ", displayRecovererCodeTotals=" + displayRecovererCodeTotals +
                ", matchData=" + matchData +
                ", beginDateRange='" + beginDateRange + '\'' +
                ", endDateRange='" + endDateRange + '\'' +
                ", showClientAccountNums=" + showClientAccountNums +
                ", disableMatchPossibleDebtors=" + disableMatchPossibleDebtors +
                ", findAmounts='" + findAmounts + '\'' +
                '}';
    }
}