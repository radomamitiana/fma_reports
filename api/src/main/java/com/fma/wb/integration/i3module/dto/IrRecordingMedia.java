package com.fma.wb.integration.i3module.dto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class IrRecordingMedia implements Serializable {
    @JsonProperty("RecordingId")
    private String recordingId;
    @JsonProperty("RelatedRecordingId")
    private int relatedRecordingId;
    @JsonProperty("MediaUri")
    private String mediaUri;
    @JsonProperty("MediaKey")
    private String mediaKey;
    @JsonProperty("MediaType")
    private int mediaType;
    @JsonProperty("FileSize")
    private int fileSize;
    @JsonProperty("RecordingDate")
    private String recordingDate;
    @JsonProperty("RecordingDateOffSet")
    private int recordingDateOffSet;
    @JsonProperty("ExpirationDate")
    private String expirationDate;
    @JsonProperty("StartEventCode")
    private int startEventCode;
    @JsonProperty("Duration")
    private int duration;
    @JsonProperty("FromConnValue")
    private String fromConnValue;
    @JsonProperty("ToConnValue")
    private String toConnValue;
    @JsonProperty("MediaStatus")
    private String mediaStatus;
    @JsonProperty("Direction")
    private int direction;
    @JsonProperty("QueueObjectIdKey")
    private String queueObjectIdKey;
    @JsonProperty("InitiationPolicyName")
    private String initiationPolicyName;
    @JsonProperty("ScreenRecordedHostName")
    private String screenRecordedHostName;
    @JsonProperty("LineName")
    private String lineName;
    @JsonProperty("NumAttachments")
    private int numAttachments;
    @JsonProperty("CallType")
    private int callType;
    @JsonProperty("Version")
    private int version;
    @JsonProperty("KeywordCustomerScorePositive")
    private int keywordCustomerScorePositive;
    @JsonProperty("KeywordCustomerScoreNegative")
    private int keywordCustomerScoreNegative;
    @JsonProperty("KeywordAgentScorePositive")
    private int keywordAgentScorePositive;
    @JsonProperty("KeywordAgentScoreNegative")
    private int keywordAgentScoreNegative;
    @JsonProperty("IsArchived")
    private boolean isArchived;

    public String getRecordingId() {
        return recordingId;
    }

    public void setRecordingId(String recordingId) {
        this.recordingId = recordingId;
    }

    public int getRelatedRecordingId() {
        return relatedRecordingId;
    }

    public void setRelatedRecordingId(int relatedRecordingId) {
        this.relatedRecordingId = relatedRecordingId;
    }

    public String getMediaUri() {
        return mediaUri;
    }

    public void setMediaUri(String mediaUri) {
        this.mediaUri = mediaUri;
    }

    public String getMediaKey() {
        return mediaKey;
    }

    public void setMediaKey(String mediaKey) {
        this.mediaKey = mediaKey;
    }

    public int getMediaType() {
        return mediaType;
    }

    public void setMediaType(int mediaType) {
        this.mediaType = mediaType;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public String getRecordingDate() {
        return recordingDate;
    }

    public void setRecordingDate(String recordingDate) {
        this.recordingDate = recordingDate;
    }

    public int getRecordingDateOffSet() {
        return recordingDateOffSet;
    }

    public void setRecordingDateOffSet(int recordingDateOffSet) {
        this.recordingDateOffSet = recordingDateOffSet;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public int getStartEventCode() {
        return startEventCode;
    }

    public void setStartEventCode(int startEventCode) {
        this.startEventCode = startEventCode;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getFromConnValue() {
        return fromConnValue;
    }

    public void setFromConnValue(String fromConnValue) {
        this.fromConnValue = fromConnValue;
    }

    public String getToConnValue() {
        return toConnValue;
    }

    public void setToConnValue(String toConnValue) {
        this.toConnValue = toConnValue;
    }

    public String getMediaStatus() {
        return mediaStatus;
    }

    public void setMediaStatus(String mediaStatus) {
        this.mediaStatus = mediaStatus;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public String getQueueObjectIdKey() {
        return queueObjectIdKey;
    }

    public void setQueueObjectIdKey(String queueObjectIdKey) {
        this.queueObjectIdKey = queueObjectIdKey;
    }

    public String getInitiationPolicyName() {
        return initiationPolicyName;
    }

    public void setInitiationPolicyName(String initiationPolicyName) {
        this.initiationPolicyName = initiationPolicyName;
    }

    public String getScreenRecordedHostName() {
        return screenRecordedHostName;
    }

    public void setScreenRecordedHostName(String screenRecordedHostName) {
        this.screenRecordedHostName = screenRecordedHostName;
    }

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public int getNumAttachments() {
        return numAttachments;
    }

    public void setNumAttachments(int numAttachments) {
        this.numAttachments = numAttachments;
    }

    public int getCallType() {
        return callType;
    }

    public void setCallType(int callType) {
        this.callType = callType;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getKeywordCustomerScorePositive() {
        return keywordCustomerScorePositive;
    }

    public void setKeywordCustomerScorePositive(int keywordCustomerScorePositive) {
        this.keywordCustomerScorePositive = keywordCustomerScorePositive;
    }

    public int getKeywordCustomerScoreNegative() {
        return keywordCustomerScoreNegative;
    }

    public void setKeywordCustomerScoreNegative(int keywordCustomerScoreNegative) {
        this.keywordCustomerScoreNegative = keywordCustomerScoreNegative;
    }

    public int getKeywordAgentScorePositive() {
        return keywordAgentScorePositive;
    }

    public void setKeywordAgentScorePositive(int keywordAgentScorePositive) {
        this.keywordAgentScorePositive = keywordAgentScorePositive;
    }

    public int getKeywordAgentScoreNegative() {
        return keywordAgentScoreNegative;
    }

    public void setKeywordAgentScoreNegative(int keywordAgentScoreNegative) {
        this.keywordAgentScoreNegative = keywordAgentScoreNegative;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    public IrRecordingMedia() {
    }

    @Override
    public String toString() {
        return "IrRecordingMedia{" +
                "recordingId=" + recordingId +
                ", relatedRecordingId=" + relatedRecordingId +
                ", mediaUri='" + mediaUri + '\'' +
                ", mediaKey='" + mediaKey + '\'' +
                ", mediaType=" + mediaType +
                ", fileSize=" + fileSize +
                ", recordingDate=" + recordingDate +
                ", recordingDateOffSet=" + recordingDateOffSet +
                ", expirationDate=" + expirationDate +
                ", startEventCode=" + startEventCode +
                ", duration=" + duration +
                ", fromConnValue='" + fromConnValue + '\'' +
                ", toConnValue='" + toConnValue + '\'' +
                ", mediaStatus='" + mediaStatus + '\'' +
                ", direction=" + direction +
                ", queueObjectIdKey='" + queueObjectIdKey + '\'' +
                ", initiationPolicyName='" + initiationPolicyName + '\'' +
                ", screenRecordedHostName='" + screenRecordedHostName + '\'' +
                ", lineName='" + lineName + '\'' +
                ", numAttachments=" + numAttachments +
                ", callType=" + callType +
                ", version=" + version +
                ", keywordCustomerScorePositive=" + keywordCustomerScorePositive +
                ", keywordCustomerScoreNegative=" + keywordCustomerScoreNegative +
                ", keywordAgentScorePositive=" + keywordAgentScorePositive +
                ", keywordAgentScoreNegative=" + keywordAgentScoreNegative +
                ", isArchived=" + isArchived +
                '}';
    }
}