package com.fma.wb.integration.common.dto;

import io.vertx.ext.auth.jwt.impl.JWTUser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This model represents the User entity
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class User implements Serializable  {
    private int user_id;
    private String username = "";
    private String password = "";
    private List<Integer> roles = new ArrayList<>();
    private boolean isConnected = false;
    private String sessionId;
    private String last_name = "";
    private String  first_name = "";
    private String email;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Integer> getRoles() {
        return roles;
    }

    public void setRoles(List<Integer> roles) {
        this.roles = roles;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void addRole(int role){
        this.roles.add(role);
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Constructor without parameter
     */
    public User() {
    }

    /**
     * Constructor with parameters
     * @param username
     * @param password
     * @param roles
     * @param isConnected
     * @param sessionId
     */
    public User(String username, String password, List<Integer> roles, boolean isConnected, String sessionId) {
        this.username = username;
        this.password = password;
        this.roles = roles;
        this.isConnected = isConnected;
        this.sessionId = sessionId;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}