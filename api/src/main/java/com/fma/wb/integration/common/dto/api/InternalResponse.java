package com.fma.wb.integration.common.dto.api;

import java.io.Serializable;

/**
 * Class for handle all internal responses
 */
public class InternalResponse implements Serializable {
  private String data;
  private int cause=0;/*
  *
  * -1 =no active session
  *  0 error
  *  1 sucess
  */

  public InternalResponse() {
  }

  public InternalResponse(String data, int cause) {
    this.data = data;
    this.cause = cause;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public int getCause() {
    return cause;
  }

  public void setCause(int cause) {
    this.cause = cause;
  }
}
