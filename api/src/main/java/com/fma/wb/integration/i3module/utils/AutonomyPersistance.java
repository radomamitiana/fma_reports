package com.fma.wb.integration.i3module.utils;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import common.Logger;
import common.LoggerTag;
import dto.annotations.Nullable;
import io.vertx.core.shareddata.LocalMap;
import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.io.*;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.HashMap;

/**
 * Logger file handling errors and put all traces in a file under logs directory
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class AutonomyPersistance {

    private static final String AUTONOMIE_FILE_NAME = "autonomykeep";
    private String log_dir_path;
    private File newLogFile;
    private static AutonomyPersistance logger;

    public URI getLogFileURI(){
        return this.newLogFile.toURI();
    }

    public String getLog_dir_path() {
        return log_dir_path;
    }

    public void setLog_dir_path(String log_dir_path) {
        this.log_dir_path = log_dir_path;
    }

    public File getNewLogFile() {
        return newLogFile;
    }

    public void setNewLogFile(File newLogFile) {
        this.newLogFile = newLogFile;
    }

    public AutonomyPersistance(String path) {
        this.log_dir_path = path;
        if (log_dir_path.isEmpty()) {
            log_dir_path = System.getProperty("user.dir")+ File.separator + "persistance";
        }

        logger = this;
    }

    public static AutonomyPersistance getInstance(@Nullable String log_dir_path){
        if(log_dir_path.isEmpty()) {
            log_dir_path = System.getProperty("user.dir")+ File.separator + "persistance";
        }

        if (logger == null) {
            return logger = new AutonomyPersistance(log_dir_path);
        }
        else {
            return logger;
        }
    }

    private boolean createFile(){

        this.newLogFile = new File(this.log_dir_path,AUTONOMIE_FILE_NAME+".txt");

        try {
            return this.newLogFile.createNewFile();
        } catch (IOException e) {
            System.out.println(e.getMessage()+": "+newLogFile.getAbsolutePath());
            return false;
        }
    }

    private void writeStringToFile(String toWrite, File file) {
        try (FileWriter fw = new FileWriter(file.getAbsolutePath(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw)) {
            out.println(toWrite);
            //more code
        } catch (Exception e) {
            //exception handling left. We can add codes here to enrich it
            System.out.println(e.getMessage());
        }
    }

    public void persist(LocalMap<String,String> infos) {

        Gson gson = new Gson();
        String message = "";
        System.out.println("persisting");

        try {
            message = gson.toJson(infos);
            Logger.getInstance("").log(LoggerTag.INFORMATION, "autonomy data to persist: { "+message+" }", "");

        } catch (Exception e) {
            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e), "");
        }

        boolean created=false;
        if (this.newLogFile == null || !this.newLogFile.exists())
             created = createFile();

            if (created) {

                try {
                    writeStringToFile(message, this.newLogFile);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }



    }



    public HashMap<String,String> retrieveAutonomie(){

        File ff=new File(this.log_dir_path,AUTONOMIE_FILE_NAME+".txt");
        if(ff.exists()){
            try {

                BufferedReader reader = new BufferedReader(new FileReader(ff));
                String line;
                StringBuilder read= new StringBuilder();
                while ((line = reader.readLine()) != null)
                {
                    read.append(line);
                }
                reader.close();

                //We convert file content into hashmap if possible
                if(read.length() > 0){
                    Gson gson=new Gson();
                    Type typeOfHashMap = new TypeToken<HashMap<String, String>>() { }.getType();
                    //ff.delete();
                    return gson.fromJson(read.toString(), typeOfHashMap);

                }

            } catch (Exception e) {
                Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e),"");
            }
        }


        return null;

    }
}