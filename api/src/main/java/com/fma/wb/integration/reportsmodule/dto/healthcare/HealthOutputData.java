package com.fma.wb.integration.reportsmodule.dto.healthcare;

import io.vertx.core.json.JsonArray;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HealthOutputData implements Serializable {

    //FinClass fields
    private List<String> finiancialClass=new ArrayList<>();
    private List<String> clientsNames=new ArrayList<>();
    private List<Integer> clientsIds=new ArrayList<>();
    private Map<String, List<String>> financialClassActiveInventorySharp = new HashMap<>();
    private Map<String, List<String>> financialClassActiveInventoryDollar = new HashMap<>();
    private Map<String, List<String[]>> financialClassActiveInventoryAverageAge = new HashMap<>();
    //Agging fields
    private List<String[]> agingAccountsInventory = new ArrayList<>();
    private Map<String, List<String>> financialClassAging = new HashMap<>();

    //Overall results fields
    private Map<String, List<String>> placementsAndChanges = new HashMap<>(); //This will hold the overall results monthly placements and changes
    private Map<String, List<String>> collectionsAndChanges = new HashMap<>(); //This will hold the overall results monthly collections and changes
    private List<String> finiancialClassOverall=new ArrayList<>();

    //BATCH TRACK fields
    private Map<String, List<String>> batchTrackAlldatas=new HashMap<>();

    //Monthly reports fields
    // Key: client, val: monthly datas
    private Map<String,Map<String, List<String>>> monthlyList=new HashMap<>();

    private Map<String, List<String>> reportDetails=new HashMap<>();


    //Global fields
    private String startDateSelected ="";
    private String endDateSelected ="";
    private String sessionId ="";
    private String shortOrganizationName="Test test test3";
    private List<String> activCodeList=new ArrayList<>();
    private List<String> activDescList=new ArrayList<>();
    private boolean showClients;


    public HealthOutputData() {

    }

    public List<String> getFiniancialClass() {
        return finiancialClass;
    }

    public void setFiniancialClass(List<String> finiancialClass) {
        this.finiancialClass = finiancialClass;
    }

    public List<String> getClientsNames() {
        return clientsNames;
    }

    public void setClientsNames(List<String> clientsNames) {
        this.clientsNames = clientsNames;

    }


    public void addActivCode(String code) {
        this.activCodeList.add(code);
    }

    public void addActivDesc(String desc) {
        this.activDescList.add(desc);
    }

    public Map<String, List<String>> getFinancialClassActiveInventorySharp() {
        return financialClassActiveInventorySharp;
    }


    public void addMonthlyList(String key, Map<String, List<String>> value) {
        this.monthlyList.put(key,value);
    }


    public void addFinClassDatas1(String key, List<String> value) {
        this.financialClassActiveInventorySharp.put(key,value);
    }

    public void addFinClassDatas2(String key, List<String> value) {
        this.financialClassActiveInventoryDollar.put(key,value);
    }


    public void addFinClassDatas3(String key, List<String[]> value) {
        this.financialClassActiveInventoryAverageAge.put(key,value);
    }

    public void addPlacementsAndChanges(String key, List<String> value) {
        this.placementsAndChanges.put(key,value);
    }


    public void addCollectionsAndChanges(String key, List<String> value) {
        this.collectionsAndChanges.put(key,value);
    }




    public void addAgingDatas2(String key,List<String> value) {
        this.financialClassAging.put(key,value);
    }


    public void addAggingDatas1(String[] value) {
        this.agingAccountsInventory.add(value);
    }


    public void addBatchTrackData(String key, List<String> value) {
        this.batchTrackAlldatas.put(key,value);
    }

    public Map<String, List<String>> getFinancialClassActiveInventoryDollar() {
        return financialClassActiveInventoryDollar;
    }



    public Map<String, List<String[]>> getFinancialClassActiveInventoryAverageAge() {
        return financialClassActiveInventoryAverageAge;
    }



    public List<String[]> getAgingAccountsInventory() {
        return agingAccountsInventory;
    }

    public void setAgingAccountsInventory(List<String[]> agingAccountsInventory) {
        this.agingAccountsInventory = agingAccountsInventory;
    }

    public List<String> getActivCodeList() {
        return activCodeList;
    }

    public void setActivCodeList(List<String> activeCodeList) {
        this.activCodeList = activeCodeList;
    }

    public List<String> getActivDescList() {
        return activDescList;
    }

    public void setActivDescList(List<String> activDescList) {
        this.activDescList = activDescList;
    }

    public String getShortOrganizationName() {
        return shortOrganizationName;
    }

    public void setShortOrganizationName(String shortOrganizationName) {
        this.shortOrganizationName = shortOrganizationName;
    }

    public Map<String, List<String>> getFinancialClassAging() {
        return financialClassAging;
    }


    public String getEndDateSelected() {
        return endDateSelected;
    }

    public void setEndDateSelected(String endDateSelected) {
        this.endDateSelected = endDateSelected;
    }


    public void setFinancialClassActiveInventorySharp(Map<String, List<String>> financialClassActiveInventorySharp) {
        this.financialClassActiveInventorySharp = financialClassActiveInventorySharp;
    }


    public void setFinancialClassActiveInventoryDollar(Map<String, List<String>> financialClassActiveInventoryDollar) {
        this.financialClassActiveInventoryDollar = financialClassActiveInventoryDollar;
    }

    public void setFinancialClassActiveInventoryAverageAge(Map<String, List<String[]>> financialClassActiveInventoryAverageAge) {
        this.financialClassActiveInventoryAverageAge = financialClassActiveInventoryAverageAge;
    }

    public void setFinancialClassAging(Map<String, List<String>> financialClassAging) {
        this.financialClassAging = financialClassAging;
    }

    public Map<String, List<String>> getPlacementsAndChanges() {
        return placementsAndChanges;
    }

    public void setPlacementsAndChanges(Map<String, List<String>> placementsAndChanges) {
        this.placementsAndChanges = placementsAndChanges;
    }


    public Map<String, List<String>> getCollectionsAndChanges() {
        return collectionsAndChanges;
    }

    public void setCollectionsAndChanges(Map<String, List<String>> collectionsAndChanges) {
        this.collectionsAndChanges = collectionsAndChanges;
    }


    public Map<String, List<String>> getBatchTrackAlldatas() {
        return batchTrackAlldatas;
    }

    public void setBatchTrackAlldatas(Map<String, List<String>> batchTrackAlldatas) {
        this.batchTrackAlldatas = batchTrackAlldatas;
    }

    public List<String> getFiniancialClassOverall() {
        return finiancialClassOverall;
    }

    public void setFiniancialClassOverall(List<String> finiancialClassOverall) {
        this.finiancialClassOverall = finiancialClassOverall;
    }

    public Map<String, Map<String, List<String>>> getMonthlyList() {
        return monthlyList;
    }

    public void setMonthlyList(Map<String, Map<String, List<String>>> monthlyList) {
        this.monthlyList = monthlyList;
    }

    public void setMonthlyList(String client,Map<String, List<String>> monthlyList) {
        this.monthlyList.put(client,monthlyList);
    }

    public List<Integer> getClientsIds() {
        return clientsIds;
    }

    public void setClientsIds(List<Integer> clientsIds) {
        this.clientsIds = clientsIds;
    }

    public Map<String, List<String>> getReportDetails() {
        return reportDetails;
    }

    public void setReportDetails(Map<String, List<String>> reportDetails) {
        this.reportDetails = reportDetails;
    }


    public void addReportDetails(String key, List<String> val) {
        this.reportDetails.put(key, val);
    }

    public boolean isShowClients() {
        return showClients;
    }

    public void setShowClients(boolean showClients) {
        this.showClients = showClients;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getStartDateSelected() {
        return startDateSelected;
    }

    public void setStartDateSelected(String startDateSelected) {
        this.startDateSelected = startDateSelected;
    }
}
