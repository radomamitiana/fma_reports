package com.fma.wb.integration.i3module.dto;

import session.Session;

public class PeopleManager {

    private Session session;

    private static PeopleManager peopleManager;

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public PeopleManager() {
    }

    public PeopleManager(Session session) {
        this.session = session;
    }

    public static PeopleManager getInstance(Session session) {
        return (peopleManager == null)? new PeopleManager(session) : peopleManager;
    }

    @Override
    public String toString() {
        return "PeopleManager{" +
                "session=" + session +
                '}';
    }
}
