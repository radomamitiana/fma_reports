package com.fma.wb.integration.reportsmodule.utils.io;

import com.fma.wb.integration.reportsmodule.dto.healthcare.HealthOutputData;
import com.fma.wb.integration.reportsmodule.utils.DateUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.vertx.core.Vertx;
import io.vertx.core.file.AsyncFile;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.json.Json;
import io.vertx.core.parsetools.JsonParser;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.Years;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.vertx.core.parsetools.JsonEventType.VALUE;
import static java.util.Map.Entry.comparingByKey;
import static java.util.stream.Collectors.toMap;
import static org.apache.poi.ss.usermodel.CellType.*;

public class HealthCareExcelFileGenerator {

    private HealthOutputData data ;
    private Workbook workbook;
    private DataFormat customFormat;
    private DataFormat percentStyle;
    private DataFormat styleNumberCustomFormat;
    private String[] columnsFinancialClass = {"Financial Class", "0 - 30", "31 - 60", "61 - 90", "91 - 120", "121 - 150", "151 - 180", ">180", "Totals"};
    private String[] columnsFinClAging = {"Financial Class", "0 - 30", "31 - 60", "61 - 90", "91 - 120", "121 - 150", "151 - 180", ">180", "Grand Total"};
    private String[] columnsAging = {" ", "Active Accounts #", "Active Accounts $", "% of Inventory"};
    //Aging sheet: rows names
    private String[] rowsAging = {"Active < 180 days", "Active > 180 days", "No Date", "Grand Total"};
    private String[] rowC1 = {
            "",
            "Account Listed",
            "",
            "",
            "",
            "",
            "Returns",
            "",
            "Client recalls",
            "",
            "",
            "",
            "Claims Denied",
            "",
            "",
            "Current Month",
            "",
            "Life to Date",
            "",
            "Active Accounts",
            "",
            "",
            ""
    };


    private String[] rowC2= {
            "Date",
            "#",
            "$",
            "Avg Bal",
            "Adjustments $",
            "Paid In Full #",
            "#",
            "$",
            "Curr Month #",
            "Current Month $",
            "LTD #",
            "LTD $",
            "#",
            "$",
            "Total Net Assignments",
            "$",
            "%",
            "Collections",
            "Recovery %",
            "#",
            "$",
            "%",
            "AVG Age"
    };

    private CellStyle headerCellStyle;
    private CellStyle yellowCellStyle;
    private CellStyle yellowCellStylePercent;
    private CellStyle yellowCellStyleCurrency;
    private CellStyle blankStyle;
    private CellStyle blueStyle;
    private CellStyle blueStyleBold;
    private CellStyle styleCurrencyNonBold;
    private CellStyle styleCurrencyBold;

    private CellStyle styleNonBold;
    private CellStyle styleBold;

    private CellStyle styleNumeric;
    private CellStyle styleNumericBold;
    private CellStyle styleDataValues2;
    private CellStyle styleFont1;
    private CellStyle styleFont2;
    private CellStyle defaultStyle;
    private CellStyle signStyle;
    private CellStyle styleFont4;
    private CellStyle stylePercentageData;
    private Font fontNonBold;
    private Font fontBold;
    private Font whiteFont;
    private short csFormat;
    private short percentFormat;
    private short csStyleNumberCustomFormat;
    private Map<String, List<String>> batchTrackTotal=new HashMap<>();


    public HealthCareExcelFileGenerator() {
    }

    public HealthCareExcelFileGenerator(HealthOutputData data) {
        this.data = data;
        this.workbook = new XSSFWorkbook();


        //NEW STYLES
        this.customFormat = workbook.createDataFormat();
        csFormat = this.customFormat.getFormat("$###,##0.00");

        // create percent style
        percentStyle = workbook.createDataFormat();
        percentFormat = percentStyle.getFormat("0.00%");

        styleNumberCustomFormat=workbook.createDataFormat();
        csStyleNumberCustomFormat = styleNumberCustomFormat.getFormat("#,##0");


        Font headerFont = workbook.createFont();
        whiteFont = workbook.createFont();

        headerFont.setFontHeightInPoints((short) 27);
        headerFont.setBold(true);
        headerFont.setColor(IndexedColors.WHITE.getIndex());
        headerFont.setFontHeightInPoints((short) 27);


        whiteFont.setColor(IndexedColors.WHITE.getIndex());
        //whiteFont.setFontHeightInPoints((short) 12);

        //Style used for all currency amount
        fontNonBold = workbook.createFont();
        //fontNonBold.setFontHeightInPoints((short) 12);

        fontBold = workbook.createFont();
        //fontBold.setFontHeightInPoints((short) 12);
        fontBold.setBold(true);

        /*Font font = workbook.createFont();
        //font.setFontHeightInPoints((short) 12);

        Font font1 = workbook.createFont();
        //font1.setFontHeightInPoints((short) 12);
        font1.setBold(true);

        Font font2 = workbook.createFont();
        //font2.setFontHeightInPoints((short) 12);
        font2.setBold(false);

        Font font3 = workbook.createFont();
        //font3.setFontHeightInPoints((short) 12);
        font3.setBold(true);

        Font font4 = workbook.createFont();
        //font4.setFontHeightInPoints((short) 12);
        font4.setBold(false);*/

        Font font3 = workbook.createFont();
        //font3.setFontHeightInPoints((short) 12);
        font3.setBold(false);

        defaultStyle = workbook.createCellStyle();
        defaultStyle.setFont(font3);
        defaultStyle.setAlignment(HorizontalAlignment.CENTER);


        signStyle = workbook.createCellStyle();
        Font fontSign = workbook.createFont();
        fontSign.setBold(false);
        fontSign.setFontHeightInPoints((short)10);
        fontSign.setColor(IndexedColors.GREY_80_PERCENT.getIndex());
        signStyle.setFont(font3);
        signStyle.setAlignment(HorizontalAlignment.CENTER);


        // Create a CellStyle with the font
        headerCellStyle = workbook.createCellStyle();
        yellowCellStyle = workbook.createCellStyle();
        yellowCellStyleCurrency = workbook.createCellStyle();
        yellowCellStylePercent = workbook.createCellStyle();
        blankStyle = workbook.createCellStyle();
        blueStyle = workbook.createCellStyle();
        blueStyleBold = workbook.createCellStyle();

        headerCellStyle.setFont(headerFont);
        yellowCellStyle.setFont(fontNonBold);
        yellowCellStyleCurrency.setFont(fontNonBold);
        yellowCellStylePercent.setFont(fontNonBold);
        blankStyle.setFont(headerFont);
        blueStyle.setFont(whiteFont);
        blueStyleBold.setFont(whiteFont);

        blankStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        blankStyle.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        blankStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        blueStyle.setFillForegroundColor(IndexedColors.ROYAL_BLUE.getIndex());
        blueStyle.setFillBackgroundColor(IndexedColors.ROYAL_BLUE.getIndex());
        blueStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        blueStyle.setAlignment(HorizontalAlignment.CENTER);
        blueStyle.setBorderTop(BorderStyle.THIN);
        blueStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
        blueStyle.setBorderBottom(BorderStyle.THIN);
        blueStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        blueStyle.setBorderLeft(BorderStyle.THIN);
        blueStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        blueStyle.setBorderRight(BorderStyle.THIN);
        blueStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());


        blueStyleBold.setFillForegroundColor(IndexedColors.ROYAL_BLUE.getIndex());
        blueStyleBold.setFillBackgroundColor(IndexedColors.ROYAL_BLUE.getIndex());
        blueStyleBold.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        blueStyleBold.setAlignment(HorizontalAlignment.CENTER);
        blueStyleBold.setBorderTop(BorderStyle.valueOf((short)2));
        blueStyleBold.setTopBorderColor(IndexedColors.BLACK.getIndex());
        blueStyleBold.setBorderBottom(BorderStyle.valueOf((short)2));
        blueStyleBold.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        blueStyleBold.setBorderLeft(BorderStyle.valueOf((short)2));
        blueStyleBold.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        blueStyleBold.setBorderRight(BorderStyle.valueOf((short)2));
        blueStyleBold.setRightBorderColor(IndexedColors.BLACK.getIndex());

        headerCellStyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
        headerCellStyle.setFillBackgroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
        headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerCellStyle.setAlignment(HorizontalAlignment.CENTER);

        yellowCellStyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
        yellowCellStyle.setFillBackgroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
        yellowCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        yellowCellStyle.setAlignment(HorizontalAlignment.RIGHT);
        yellowCellStyleCurrency.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
        yellowCellStyleCurrency.setFillBackgroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
        yellowCellStyleCurrency.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        yellowCellStyleCurrency.setAlignment(HorizontalAlignment.RIGHT);
        yellowCellStylePercent.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
        yellowCellStylePercent.setFillBackgroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
        yellowCellStylePercent.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        yellowCellStylePercent.setAlignment(HorizontalAlignment.RIGHT);

        yellowCellStyle.setBorderTop(BorderStyle.valueOf((short)1));
        yellowCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
        yellowCellStyle.setBorderBottom(BorderStyle.valueOf((short)1));
        yellowCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        yellowCellStyle.setBorderLeft(BorderStyle.valueOf((short)1));
        yellowCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        yellowCellStyle.setBorderRight(BorderStyle.valueOf((short)1));
        yellowCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());

        yellowCellStyleCurrency.setBorderTop(BorderStyle.valueOf((short)1));
        yellowCellStyleCurrency.setTopBorderColor(IndexedColors.BLACK.getIndex());
        yellowCellStyleCurrency.setBorderBottom(BorderStyle.valueOf((short)1));
        yellowCellStyleCurrency.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        yellowCellStyleCurrency.setBorderLeft(BorderStyle.valueOf((short)1));
        yellowCellStyleCurrency.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        yellowCellStyleCurrency.setBorderRight(BorderStyle.valueOf((short)1));
        yellowCellStyleCurrency.setRightBorderColor(IndexedColors.BLACK.getIndex());

        yellowCellStylePercent.setBorderTop(BorderStyle.valueOf((short)1));
        yellowCellStylePercent.setTopBorderColor(IndexedColors.BLACK.getIndex());
        yellowCellStylePercent.setBorderBottom(BorderStyle.valueOf((short)1));
        yellowCellStylePercent.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        yellowCellStylePercent.setBorderLeft(BorderStyle.valueOf((short)1));
        yellowCellStylePercent.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        yellowCellStylePercent.setBorderRight(BorderStyle.valueOf((short)1));
        yellowCellStylePercent.setRightBorderColor(IndexedColors.BLACK.getIndex());





        styleCurrencyNonBold = workbook.createCellStyle();
        styleCurrencyNonBold.setFont(fontNonBold);
        styleCurrencyNonBold.setDataFormat(csFormat);
        styleCurrencyNonBold.setAlignment(HorizontalAlignment.RIGHT);

        yellowCellStyleCurrency.setDataFormat(csFormat);
        yellowCellStylePercent.setDataFormat(percentFormat);

        styleNonBold = workbook.createCellStyle();
        styleNonBold.setFont(fontNonBold);
        styleNonBold.setAlignment(HorizontalAlignment.RIGHT);

        styleBold = workbook.createCellStyle();
        styleBold.setFont(fontBold);
        styleBold.setAlignment(HorizontalAlignment.RIGHT);

        styleCurrencyBold = workbook.createCellStyle();
        styleCurrencyBold.setFont(fontBold);
        styleCurrencyBold.setDataFormat(csFormat);
        styleCurrencyBold.setAlignment(HorizontalAlignment.RIGHT);

        styleNumeric = workbook.createCellStyle();
        styleNumeric.setFont(fontNonBold);
        styleNumeric.setAlignment(HorizontalAlignment.RIGHT);
        styleNumeric.setDataFormat(csStyleNumberCustomFormat);

        styleNumericBold = workbook.createCellStyle();
        styleNumericBold.setFont(fontBold);
        styleNumericBold.setAlignment(HorizontalAlignment.RIGHT);
        styleNumericBold.setDataFormat(csStyleNumberCustomFormat);

        styleDataValues2 = workbook.createCellStyle();
        styleDataValues2.setFont(fontNonBold);
        styleDataValues2.setAlignment(HorizontalAlignment.LEFT);

        styleFont1 = workbook.createCellStyle();
        styleFont1.setFont(fontBold);
        styleFont1.setAlignment(HorizontalAlignment.LEFT);


        stylePercentageData = workbook.createCellStyle();
        stylePercentageData.setDataFormat(percentFormat);
        stylePercentageData.setFont(fontNonBold);
        stylePercentageData.setAlignment(HorizontalAlignment.RIGHT);


    }


    private HealthOutputData calculateBatchTrack(HealthOutputData outputData){


        Map<String, List<String>> mergedDatas=new HashMap<>();


        Stream<Map<String, List<String>>> ddt=outputData.getMonthlyList().values().stream().filter(stringListMap -> !stringListMap.isEmpty());

        ddt.collect(Collectors.toList()).forEach(stringListMap -> {

            stringListMap.forEach((s, strings) -> {
                if(!strings.isEmpty()){
                    System.out.println("mergedDatas: "+Json.encodePrettily(mergedDatas));

                    if(!mergedDatas.containsKey(s)){
                        mergedDatas.put(s,strings);
                    }else{

                        double s1;
                        int s2;
                        List<String> ss=new ArrayList<>();
                        for (int i = 0; i < strings.size(); i++) {

                            if(!strings.get(i).trim().contains(".") && !mergedDatas.get(s).get(i).trim().contains(".")){
                                s2=Integer.valueOf((strings.get(i).trim().isEmpty()?"0":strings.get(i)))+Integer.valueOf((mergedDatas.get(s).get(i).trim().isEmpty()?"0":mergedDatas.get(s).get(i)));
                                ss.add(i,""+s2);
                            }else{
                                s1=Double.valueOf((strings.get(i).trim().isEmpty()?"0.0":strings.get(i)))+Double.valueOf((mergedDatas.get(s).get(i).trim().isEmpty()?"0.0":mergedDatas.get(s).get(i)));
                                ss.add(i,""+s1);
                            }


                        }
                        mergedDatas.put(s,ss);



                    }
                }

            });

        });


        outputData.setBatchTrackAlldatas(mergedDatas);
        return outputData;

    }

    private Sheet createClientDataOcc(String sheetName, String title, Map<String, List<String>> vals,boolean total){
        System.out.println("START CDATA....");
        Sheet sheet=this.workbook.createSheet(sheetName);

        Map<String, List<String>> mergedDatas=new HashMap<>();

        DateTime date1 = new DateTime().withDate(Integer.valueOf(DateUtils.getDateYear(this.data.getEndDateSelected())), Integer.valueOf(DateUtils.getDateMonth(this.data.getEndDateSelected())), Integer.valueOf(DateUtils.getDateDay(this.data.getEndDateSelected()))).withTime(0, 0, 0, 0);
        DateTime date2 = new DateTime().withDate(Integer.valueOf(DateUtils.getDateYear(this.data.getStartDateSelected())), Integer.valueOf(DateUtils.getDateMonth(this.data.getStartDateSelected())), Integer.valueOf(DateUtils.getDateDay(this.data.getStartDateSelected()))).withTime(0, 0, 0, 0);
        int monDiff = Months.monthsBetween(date2, date1).getMonths();
        int yearDiff = Years.yearsBetween(date2, date1).getYears();

        System.out.println("DIFF: "+monDiff);
        System.out.println("DIFF2: "+yearDiff);
        String enddt=this.data.getEndDateSelected();
        //String enddt=DateUtils.minusMonthsFromDate(this.data.getEndDateSelected(),1);

        if(yearDiff==0 && monDiff==0) {
            String key = DateUtils.getDateMonthInt(enddt) + "-"
                    + DateUtils.getDateYear(enddt);
            //System.out.println("K: " + key);

            List<String> empty = new ArrayList<>();
            for (int j = 0; j < 22; j++) {
                empty.add(" ");
            }
            mergedDatas.put(key,empty);

        }
        else{
            for (int i = 0; i <= monDiff; i++) {

                String key= DateUtils.getDateMonthInt(enddt) + "-"
                        + DateUtils.getDateYear(enddt);
                System.out.println("K: "+key);

                List<String> empty=new ArrayList<>();
                for (int j = 0; j < 22; j++) {
                    empty.add(" ");
                }
                mergedDatas.put(key,empty);
                enddt= DateUtils.minusMonthsFromDate(enddt,1);

            }
        }


        System.out.println("MMDT: "+Json.encodePrettily(mergedDatas));


        TreeMap<String, List<String>> sorted=new TreeMap<>((o1, o2) -> {
            String[] one = o1.split("-");
            String[] two = o2.split("-");

            if(Integer.valueOf(one[1])<Integer.valueOf(two[1])){
                return 1;
            }
            else if(Integer.valueOf(one[1])>Integer.valueOf(two[1])){
                return -1;
            }
            else{
                return Integer.valueOf(two[0]).compareTo(Integer.valueOf(one[0]));
            }
        });


        mergedDatas.forEach((s, strings) -> vals.forEach((s1, strings1) -> {
            if (s.equals(s1)){
                mergedDatas.put(s,strings1);
            }
        }));


        sorted.putAll(mergedDatas);



        AtomicInteger rowsOcc = new AtomicInteger();
        Row firstRow = sheet.createRow(rowsOcc.getAndIncrement());
        Cell cellFusion = firstRow.createCell(0, STRING);
        cellFusion.setCellValue(title);
        sheet.addMergedRegion(new CellRangeAddress(0,0,0,22));
        cellFusion.setCellStyle(headerCellStyle);


        addColumnNamesInBlue(sheet, sheet.createRow(rowsOcc.getAndIncrement()), rowC1, blueStyleBold);
        addColumnNamesInBlue(sheet, sheet.createRow(rowsOcc.getAndIncrement()), rowC2, blueStyle);

        sheet.addMergedRegion(new CellRangeAddress(1,1,1,2));
        sheet.addMergedRegion(new CellRangeAddress(1,1,3,5));
        sheet.addMergedRegion(new CellRangeAddress(1,1,6,7));
        sheet.addMergedRegion(new CellRangeAddress(1,1,8,11));
        sheet.addMergedRegion(new CellRangeAddress(1,1,12,13));
        sheet.addMergedRegion(new CellRangeAddress(1,1,15,16));
        sheet.addMergedRegion(new CellRangeAddress(1,1,17,18));
        sheet.addMergedRegion(new CellRangeAddress(1,1,19,22));

        //if(!vals.isEmpty()){

        final int[] taille = {0};
        this.data.getMonthlyList().forEach((s, stringListMap) -> {
            if(!stringListMap.isEmpty()) taille[0]++;

        });

        if(taille[0]==0)
            taille[0]++;

        sorted.forEach((date, datas) -> {

            Row row=sheet.createRow(rowsOcc.getAndIncrement());
            Cell cell0=row.createCell(0);
            cell0.setCellValue(date);
            for (int i = 1; i < 23; i++) {
                switch (i) {
                    case 16: {
                        Cell cell = row.createCell(i, FORMULA);
                        cell.setCellStyle(stylePercentageData);
                        String first = "P" + (row.getRowNum() + 1);
                        String second = "C" + (row.getRowNum() + 1);

                        cell.setCellFormula("IF(" + second + "<>0," + first + "/" + second + ",0)");

                        break;
                    }
                    case 18: {
                        Cell cell = row.createCell(i, FORMULA);
                        cell.setCellStyle(stylePercentageData);
                        String first = "R" + (row.getRowNum() + 1);
                        String second = "C" + (row.getRowNum() + 1);
                        cell.setCellFormula("IF(" + second + "<>0," + first + "/" + second + ",0)");

                        break;
                    }
                    case 21: {
                        Cell cell = row.createCell(i, FORMULA);
                        cell.setCellStyle(stylePercentageData);
                        String first = "T" + (row.getRowNum() + 1);
                        String second = "B" + (row.getRowNum() + 1);
                        cell.setCellFormula("IF(" + second + "<>0," + first + "/" + second + ",0)");

                        break;
                    }


                    case 3:{
                        Cell cell = row.createCell(i, NUMERIC);
                        double vv = datas.get(i - 1).trim().isEmpty() ? 0.0 : Double.valueOf(datas.get(i - 1).trim());
                        if(total) {

                            cell.setCellValue(vv/ taille[0]);
                        }else{
                            cell.setCellValue(vv);
                        }

                        cell.setCellStyle(styleCurrencyNonBold);

                        break;
                    }

                    case 22: {
                        Cell cell = row.createCell(i, NUMERIC);
                        double vv = datas.get(i - 1).trim().isEmpty() ? 0.0 : Double.valueOf(datas.get(i - 1).trim());
                        if(total) {

                            cell.setCellValue(Math.round(vv/ taille[0]));
                        }else{
                            cell.setCellValue(vv);
                        }

                        break;
                    }
                    case 20: {
                        Cell cell = row.createCell(i, FORMULA);
                        cell.setCellStyle(styleCurrencyNonBold);
                        String first = "C" + (row.getRowNum() + 1);
                        String second = "E" + (row.getRowNum() + 1);
                        String trhee = "R" + (row.getRowNum() + 1);
                        String four = "H" + (row.getRowNum() + 1);
                        cell.setCellFormula(first + "+(" + second + "-" + trhee + "-" + four + ")");

                        break;
                    }


                    case 19:{

                        Cell cell = row.createCell(i, FORMULA);
                        cell.setCellStyle(styleNonBold);
                        String first = "B" + (row.getRowNum() + 1);
                        String second = "G" + (row.getRowNum() + 1);
                        String trhee = "K" + (row.getRowNum() + 1);
                        cell.setCellFormula(first + "-" + second + "-" + trhee );

                        break;


                    }

                    case 14:{

                        Cell cell = row.createCell(i, FORMULA);
                        cell.setCellStyle(styleCurrencyNonBold);
                        String first = "C" + (row.getRowNum() + 1);
                        String second = "E" + (row.getRowNum() + 1);
                        String trhee = "L" + (row.getRowNum() + 1);
                        String four = "H" + (row.getRowNum() + 1);
                        cell.setCellFormula(first + "-" + second + "-" + trhee + "-" + four + "");

                        break;


                    }



                    default: {
                        Cell cell = row.createCell(i, NUMERIC);
                        double vv = datas.get(i - 1).trim().isEmpty() ? 0.0 : Double.valueOf(datas.get(i - 1).trim());
                        cell.setCellValue(vv);

                        if (i == 2 || i == 4 || i == 7 || i == 9 || i == 11 || i == 13 || i == 15 || i ==17) {
                            cell.setCellStyle(styleCurrencyNonBold);
                        }else{
                            cell.setCellStyle(styleNumeric);
                        }

                        break;
                    }
                }

            }
        });


        Row gTotalRow2=sheet.createRow(rowsOcc.getAndIncrement());
        Cell tot2=gTotalRow2.createCell(0, STRING);
        CellStyle style=workbook.createCellStyle();
        style.setFont(fontBold);

        style.setFillForegroundColor(IndexedColors.ROYAL_BLUE.getIndex());
        style.setFillBackgroundColor(IndexedColors.ROYAL_BLUE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        tot2.setCellStyle(style);
        tot2.setCellValue("Total");
        for (int i = 1; i < 23; i++) {

            switch (i) {
                case 18: {
                    Cell cell = gTotalRow2.createCell(i, FORMULA);
                    cell.setCellStyle(yellowCellStylePercent);
                    String first = "R" + (gTotalRow2.getRowNum() + 1);
                    String second = "C" + (gTotalRow2.getRowNum() + 1);
                    cell.setCellFormula("IF(" + second + "<>0," + first + "/" + second + ",0)");

                    break;
                }
                case 16: {
                    Cell cell = gTotalRow2.createCell(i, FORMULA);
                    cell.setCellStyle(yellowCellStylePercent);
                    String first = "P" + (gTotalRow2.getRowNum() + 1);
                    String second = "C" + (gTotalRow2.getRowNum() + 1);

                    cell.setCellFormula("IF(" + second + "<>0," + first + "/" + second + ",0)");

                    break;
                }
                case 21: {
                    Cell cell = gTotalRow2.createCell(i, FORMULA);
                    cell.setCellStyle(yellowCellStylePercent);
                    String first = "T" + (gTotalRow2.getRowNum() + 1);
                    String second = "B" + (gTotalRow2.getRowNum() + 1);
                    cell.setCellFormula("IF(" + second + "<>0," + first + "/" + second + ",0)");

                    break;
                }
                case 20: {
                    Cell cell = gTotalRow2.createCell(i, FORMULA);
                    cell.setCellStyle(yellowCellStyleCurrency);
                    String first = "C" + (gTotalRow2.getRowNum() + 1);
                    String second = "E" + (gTotalRow2.getRowNum() + 1);
                    String trhee = "R" + (gTotalRow2.getRowNum() + 1);
                    String four = "H" + (gTotalRow2.getRowNum() + 1);
                    cell.setCellFormula(first + "+(" + second + "-" + trhee + "-" + four + ")");

                    break;
                }


                case 3: {

                    Cell cell = gTotalRow2.createCell(i, FORMULA);
                    cell.setCellStyle(yellowCellStyleCurrency);
                    cell.setCellFormula("SUM(" + CellReference.convertNumToColString(cell.getColumnIndex()) + (cell.getAddress().getRow() - sorted.keySet().size() + 1) + ":" + CellReference.convertNumToColString(cell.getColumnIndex()) + cell.getAddress().getRow() + ")/COUNTIF("+CellReference.convertNumToColString(cell.getColumnIndex()) + (cell.getAddress().getRow() - sorted.keySet().size() + 1) + ":" + CellReference.convertNumToColString(cell.getColumnIndex()) + cell.getAddress().getRow()+",\"<>0\")");

                    break;
                }

                case 22: {
                    Cell cell = gTotalRow2.createCell(i, FORMULA);
                    cell.setCellStyle(yellowCellStyle);
                    cell.setCellFormula("ROUND(SUM(" + CellReference.convertNumToColString(cell.getColumnIndex()) + (cell.getAddress().getRow() - sorted.keySet().size() + 1) + ":" + CellReference.convertNumToColString(cell.getColumnIndex()) + cell.getAddress().getRow() + ")/COUNTIF("+CellReference.convertNumToColString(cell.getColumnIndex()) + (cell.getAddress().getRow() - sorted.keySet().size() + 1) + ":" + CellReference.convertNumToColString(cell.getColumnIndex()) + cell.getAddress().getRow() +",\"<>0\"), 0)");

                    break;
                }

                default: {
                    Cell cell = gTotalRow2.createCell(i, FORMULA);

                    cell.setCellFormula("sum(" + CellReference.convertNumToColString(cell.getColumnIndex()) + (cell.getAddress().getRow() - sorted.keySet().size() + 1) + ":" + CellReference.convertNumToColString(cell.getColumnIndex()) + cell.getAddress().getRow() + ")");
                    if (i == 2  || i == 4 || i == 7 || i == 9 || i == 11 || i == 13 || i == 14 || i == 15 || i==17) {
                        cell.setCellStyle(yellowCellStyleCurrency);
                    } else {
                        yellowCellStyle.setDataFormat(csStyleNumberCustomFormat);
                        cell.setCellStyle(yellowCellStyle);
                    }
                    break;
                }
            }
            }


        sheet.getRow(2).setHeight((short)600);
        sheet.forEach(cells -> cells.forEach(cell -> {

            //sheet.autoSizeColumn(cell.getAddress().getColumn());

            sheet.setColumnWidth(cell.getColumnIndex(), 4500);
            sheet.setDefaultColumnStyle(cell.getColumnIndex(), defaultStyle);
            sheet.setColumnWidth(0, 3000);

        }));

        /*if(total){
            deleteColumn(sheet,5);
        }*/

    //}


        addSignPicture(sheet,rowsOcc,true);



        return sheet;

    }

    private Sheet createOverallSheet(){
        System.out.println("START Overall....");
        Sheet ovSheet=this.workbook.createSheet("Overall Results");
        AtomicInteger rowsOcc = new AtomicInteger();
        //GETTING DATES
        String toDt=DateUtils.addDaysToDate(this.data.getEndDateSelected(),1);
        //1
        String staMoDt=DateUtils.minusMonthsFromDate(DateUtils.setDateTo1fstDay(toDt),1);
        //2
        String prevmoDt1 = DateUtils.minusMonthsFromDate(DateUtils.setDateTo1fstDay(staMoDt),1);
        //
        String prevmoDt2 = DateUtils.minusMonthsFromDate(DateUtils.setDateTo1fstDay(staMoDt),2);
        //3
        String threeMoDt = DateUtils.minusMonthsFromDate(DateUtils.setDateTo1fstDay(staMoDt),3);

        String[] dates = {

                DateUtils.getDateMonthShortName(staMoDt) + "-"
                        +DateUtils.getDateYear(staMoDt),
                DateUtils.getDateMonthShortName(prevmoDt1) + "-"
                        + DateUtils.getDateYear(prevmoDt1),
                DateUtils.getDateMonthShortName(prevmoDt2) + "-"
                        + DateUtils.getDateYear(prevmoDt2),
                DateUtils.getDateMonthShortName(threeMoDt) + "-"
                        + DateUtils.getDateYear(threeMoDt)};


        Row firstRow = ovSheet.createRow(rowsOcc.getAndIncrement());
        Cell cellFusion = firstRow.createCell(0, STRING);
        cellFusion.setCellValue("Overall Results");
        //Merging cells by providing cell index
        cellFusion.setCellStyle(headerCellStyle);
        ovSheet.addMergedRegion(new CellRangeAddress(0,0,0,13));


        //PLACEMENTS AND CHANGES
        Row rowOverallResults1 = ovSheet.createRow(rowsOcc.getAndIncrement());
        //rowOverallResults1.setHeight((short) 505);
        Cell cellOverallRes1 = rowOverallResults1.createCell(0);
        cellOverallRes1.setCellValue("MONTHLY");
        cellOverallRes1.setCellStyle(blueStyle);

        Cell cellOverallRes2 = rowOverallResults1.createCell(1);
        cellOverallRes2.setCellValue("Placements");
        cellOverallRes2.setCellStyle(blueStyle);
        ovSheet.addMergedRegion(new CellRangeAddress(1,1,1,5));

        Cell cellOverallRes3 = rowOverallResults1.createCell(6);
        cellOverallRes3.setCellValue("Changes");
        cellOverallRes3.setCellStyle(blueStyle);
        ovSheet.addMergedRegion(new CellRangeAddress(1,1,6,7));

        Cell cellOverallRes4 = rowOverallResults1.createCell(8);
        cellOverallRes4.setCellValue("");
        cellOverallRes4.setCellStyle(blankStyle);

        Cell cellOverallRes5 = rowOverallResults1.createCell(9);
        cellOverallRes5.setCellValue("YEARLY");
        cellOverallRes5.setCellStyle(blueStyle);

        Cell cellOverallRes7 = rowOverallResults1.createCell(10);
        cellOverallRes7.setCellValue("Placements");
        cellOverallRes7.setCellStyle(blueStyle);
        ovSheet.addMergedRegion(new CellRangeAddress(1,1,10,11));

        Cell cellOverallRes8 = rowOverallResults1.createCell(12);
        cellOverallRes8.setCellValue("Change");
        ovSheet.addMergedRegion(new CellRangeAddress(1,1,12,13));
        cellOverallRes8.setCellStyle(blueStyle);
        //rowOverallResults1.setHeight((short)600);


        Row rowOverallResults2 = ovSheet.createRow(rowsOcc.getAndIncrement());

        AtomicInteger cellOcc1=new AtomicInteger();
        Cell blk=rowOverallResults2.createCell(cellOcc1.getAndIncrement());
        blk.setCellValue("");


        for (String date : dates) {
            Cell cellMonthlyPlaceMnt0 = rowOverallResults2.createCell(cellOcc1.getAndIncrement());
            cellMonthlyPlaceMnt0.setCellValue(date);
            cellMonthlyPlaceMnt0.setCellStyle(blueStyle);
        }


        Cell cellMonthlyPlaceMnt1 = rowOverallResults2.createCell(cellOcc1.getAndIncrement());
        cellMonthlyPlaceMnt1.setCellValue("Prior 3 Mo Avg");
        cellMonthlyPlaceMnt1.setCellStyle(blueStyle);

        Cell cellMonthlyPlaceMnt2 = rowOverallResults2.createCell(cellOcc1.getAndIncrement());
        cellMonthlyPlaceMnt2.setCellValue("$");
        cellMonthlyPlaceMnt2.setCellStyle(blueStyle);

        Cell cellMonthlyPlaceMnt3 = rowOverallResults2.createCell(cellOcc1.getAndIncrement());
        cellMonthlyPlaceMnt3.setCellValue("%");
        cellMonthlyPlaceMnt3.setCellStyle(blueStyle);

        rowOverallResults2.createCell(cellOcc1.getAndIncrement()).setCellValue("");
        rowOverallResults2.createCell(cellOcc1.getAndIncrement()).setCellValue("");

        Cell cellMonthlyPlaceMnt4 = rowOverallResults2.createCell(cellOcc1.getAndIncrement());
        cellMonthlyPlaceMnt4.setCellValue("YTD");
        cellMonthlyPlaceMnt4.setCellStyle(blueStyle);

        Cell cellMonthlyPlaceMnt5 = rowOverallResults2.createCell(cellOcc1.getAndIncrement());
        cellMonthlyPlaceMnt5.setCellValue("Previous YTD");
        cellMonthlyPlaceMnt5.setCellStyle(blueStyle);

        Cell cellMonthlyPlaceMnt6 = rowOverallResults2.createCell(cellOcc1.getAndIncrement());
        cellMonthlyPlaceMnt6.setCellValue("$");
        cellMonthlyPlaceMnt6.setCellStyle(blueStyle);

        Cell cellMonthlyPlaceMnt7 = rowOverallResults2.createCell(cellOcc1.getAndIncrement());
        cellMonthlyPlaceMnt7.setCellValue("%");
        cellMonthlyPlaceMnt7.setCellStyle(blueStyle);

        this.data.getPlacementsAndChanges().forEach((fClass, vals) -> {
            Row rowOverallResults3 = ovSheet.createRow(rowsOcc.getAndIncrement());
            Cell cell0 = rowOverallResults3.createCell(0);
            cell0.setCellValue(fClass);
            cell0.setCellStyle(styleDataValues2);

            AtomicInteger cellOcc = new AtomicInteger(1);

            for (int i = cellOcc.get(); i < 9; i++) {
                if(i==6){
                    Cell cell = rowOverallResults3.createCell(i, FORMULA);
                    cell.setCellStyle(styleCurrencyNonBold);
                    cell.setCellFormula("B"+(rowOverallResults3.getRowNum()+1)+"-F"+(rowOverallResults3.getRowNum()+1));
                }else if(i==7){
                    Cell cell = rowOverallResults3.createCell(i, FORMULA);
                    cell.setCellStyle(stylePercentageData);
                    String first="B"+(rowOverallResults3.getRowNum()+1);
                    String second="F"+(rowOverallResults3.getRowNum()+1);

                    cell.setCellFormula("IF("+first+"<>0,("+first+"-"+second+")/"+first+",0)");



                }else if(i==8){
                    rowOverallResults3.createCell(8).setBlank();
                }else{
                    Cell cell = rowOverallResults3.createCell(i, NUMERIC);
                    cell.setCellStyle(styleCurrencyNonBold);
                    cell.setCellValue(Double.valueOf((vals.size()==0)?"0.0":vals.get(i-1)));

                }
                cellOcc.incrementAndGet();
            }


            Cell cell00 = rowOverallResults3.createCell(cellOcc.getAndIncrement());
            cell00.setCellValue(fClass);
            cell00.setCellStyle(styleDataValues2);

            for (int i = 10; i < 14; i++) {
                if(i==12){
                    Cell cell = rowOverallResults3.createCell(i, FORMULA);
                    cell.setCellStyle(styleCurrencyNonBold);
                    cell.setCellFormula("K"+(rowOverallResults3.getRowNum()+1)+"-L"+(rowOverallResults3.getRowNum()+1));
                }else if(i==13){
                    Cell cell = rowOverallResults3.createCell(i, FORMULA);
                    cell.setCellStyle(stylePercentageData);
                    String first="K"+(rowOverallResults3.getRowNum()+1);
                    String second="L"+(rowOverallResults3.getRowNum()+1);

                    cell.setCellFormula("IF("+first+"<>0,("+first+"-"+second+")/"+first+",0)");


                }else{
                    Cell cell = rowOverallResults3.createCell(i, NUMERIC);
                    cell.setCellStyle(styleCurrencyNonBold);
                    cell.setCellValue(Double.valueOf((vals.size()==0)?"0.0":vals.get(i-5)));

                }

                cellOcc.incrementAndGet();

            }



        });

        Row totRow=ovSheet.createRow(rowsOcc.getAndIncrement());
        for (int i = 0; i < 13; i++) {
            if(i==0 || i==9){
                Cell cell=totRow.createCell(i);
                CellStyle style=workbook.createCellStyle();
                style.setFont(fontBold);
                cell.setCellStyle(style);
                cell.setCellValue("Total");

            }else if (i==7 || i==8){

                totRow.createCell(i).setCellValue("");
                totRow.createCell(i).setCellValue("");
            } else{
                Cell cell=totRow.createCell(i, FORMULA);
                cell.setCellStyle(styleCurrencyBold);
                cell.setCellFormula("sum("+ CellReference.convertNumToColString(cell.getColumnIndex()) + (cell.getAddress().getRow()-this.data.getFiniancialClass().size()+1) + ":" + CellReference.convertNumToColString(cell.getColumnIndex())+ cell.getAddress().getRow() + ")");

            }

        }


        //Blank rows
        ovSheet.createRow(rowsOcc.getAndIncrement());
        ovSheet.createRow(rowsOcc.getAndIncrement());


        //COLLECTIONS AND CHANGES
        Row rowOverallResults11 = ovSheet.createRow(rowsOcc.getAndIncrement());
        //rowOverallResults1.setHeight((short) 505);
        Cell cellOverallRes11 = rowOverallResults11.createCell(0);
        cellOverallRes11.setCellValue("");
        cellOverallRes11.setCellStyle(styleNonBold);

        Cell cellOverallRes22 = rowOverallResults11.createCell(1);
        cellOverallRes22.setCellValue("Collections");
        cellOverallRes22.setCellStyle(blueStyle);
        ovSheet.addMergedRegion(new CellRangeAddress(rowOverallResults11.getRowNum(),rowOverallResults11.getRowNum(),1,5));

        Cell cellOverallRes33 = rowOverallResults11.createCell(6);
        cellOverallRes33.setCellValue("Changes");
        cellOverallRes33.setCellStyle(blueStyle);
        ovSheet.addMergedRegion(new CellRangeAddress(rowOverallResults11.getRowNum(),rowOverallResults11.getRowNum(),6,7));

        Cell cellOverallRes44 = rowOverallResults11.createCell(8);
        cellOverallRes44.setCellValue("");
        cellOverallRes44.setCellStyle(blankStyle);

        Cell cellOverallRes55 = rowOverallResults11.createCell(9);
        cellOverallRes55.setCellValue("");
        cellOverallRes55.setCellStyle(styleNonBold);

        Cell cellOverallRes77 = rowOverallResults11.createCell(10);
        cellOverallRes77.setCellValue("Collections");
        cellOverallRes77.setCellStyle(blueStyle);
        ovSheet.addMergedRegion(new CellRangeAddress(rowOverallResults11.getRowNum(),rowOverallResults11.getRowNum(),10,11));

        Cell cellOverallRes88 = rowOverallResults11.createCell(12);
        cellOverallRes88.setCellValue("Change");
        ovSheet.addMergedRegion(new CellRangeAddress(rowOverallResults11.getRowNum(),rowOverallResults11.getRowNum(),12,13));
        cellOverallRes88.setCellStyle(blueStyle);
        //rowOverallResults1.setHeight((short)600);


        Row rowOverallResults22 = ovSheet.createRow(rowsOcc.getAndIncrement());

        AtomicInteger cellOcc11=new AtomicInteger();
        Cell blk1=rowOverallResults22.createCell(cellOcc11.getAndIncrement());
        blk1.setBlank();


        for (String date : dates) {
            Cell cellMonthlyPlaceMnt0 = rowOverallResults22.createCell(cellOcc11.getAndIncrement());
            cellMonthlyPlaceMnt0.setCellValue(date);
            cellMonthlyPlaceMnt0.setCellStyle(blueStyle);
        }


        Cell cellMonthlyPlaceMnt11 = rowOverallResults22.createCell(cellOcc11.getAndIncrement());
        cellMonthlyPlaceMnt11.setCellValue("Prior 3 Mo Avg");
        cellMonthlyPlaceMnt11.setCellStyle(blueStyle);

        Cell cellMonthlyPlaceMnt22 = rowOverallResults22.createCell(cellOcc11.getAndIncrement());
        cellMonthlyPlaceMnt22.setCellValue("$");
        cellMonthlyPlaceMnt22.setCellStyle(blueStyle);

        Cell cellMonthlyPlaceMnt33 = rowOverallResults22.createCell(cellOcc11.getAndIncrement());
        cellMonthlyPlaceMnt33.setCellValue("%");
        cellMonthlyPlaceMnt33.setCellStyle(blueStyle);

        rowOverallResults22.createCell(cellOcc11.getAndIncrement()).setCellValue("");
        rowOverallResults22.createCell(cellOcc11.getAndIncrement()).setCellValue("");


        Cell cellMonthlyPlaceMnt44 = rowOverallResults22.createCell(cellOcc11.getAndIncrement());
        cellMonthlyPlaceMnt44.setCellValue("YTD");
        cellMonthlyPlaceMnt44.setCellStyle(blueStyle);

        Cell cellMonthlyPlaceMnt55 = rowOverallResults22.createCell(cellOcc11.getAndIncrement());
        cellMonthlyPlaceMnt55.setCellValue("Previous YTD");
        cellMonthlyPlaceMnt55.setCellStyle(blueStyle);

        Cell cellMonthlyPlaceMnt66 = rowOverallResults22.createCell(cellOcc11.getAndIncrement());
        cellMonthlyPlaceMnt66.setCellValue("$");
        cellMonthlyPlaceMnt66.setCellStyle(blueStyle);

        Cell cellMonthlyPlaceMnt77 = rowOverallResults22.createCell(cellOcc11.getAndIncrement());
        cellMonthlyPlaceMnt77.setCellValue("%");
        cellMonthlyPlaceMnt77.setCellStyle(blueStyle);


        this.data.getCollectionsAndChanges().forEach((fClass, vals) -> {
            Row rowOverallResults3 = ovSheet.createRow(rowsOcc.getAndIncrement());
            Cell cell0 = rowOverallResults3.createCell(0);
            cell0.setCellValue(fClass);
            cell0.setCellStyle(styleDataValues2);

            AtomicInteger cellOcc = new AtomicInteger(1);

            for (int i = cellOcc.get(); i < 9; i++) {
                if(i==6){
                    Cell cell = rowOverallResults3.createCell(i, FORMULA);
                    cell.setCellStyle(styleCurrencyNonBold);
                    cell.setCellFormula("B"+(rowOverallResults3.getRowNum()+1)+"-F"+(rowOverallResults3.getRowNum()+1));
                }else if(i==7){
                    Cell cell = rowOverallResults3.createCell(i, FORMULA);
                    cell.setCellStyle(stylePercentageData);
                    String first="B"+(rowOverallResults3.getRowNum()+1);
                    String second="F"+(rowOverallResults3.getRowNum()+1);

                    cell.setCellFormula("IF("+first+"<>0,("+first+"-"+second+")/"+first+",0)");



                }else if(i==8){
                    rowOverallResults3.createCell(8).setBlank();
                }else{
                    Cell cell = rowOverallResults3.createCell(i, NUMERIC);
                    cell.setCellStyle(styleCurrencyNonBold);
                    cell.setCellValue(Double.valueOf((vals.size()==0)?"0.0":vals.get(i-1)));
                }
                cellOcc.incrementAndGet();
            }


            Cell cell00 = rowOverallResults3.createCell(cellOcc.getAndIncrement());
            cell00.setCellValue(fClass);
            cell00.setCellStyle(styleDataValues2);

            for (int i = 10; i < 14; i++) {
                if(i==12){
                    Cell cell = rowOverallResults3.createCell(i, FORMULA);
                    cell.setCellStyle(styleCurrencyNonBold);
                    cell.setCellFormula("K"+(rowOverallResults3.getRowNum()+1)+"-L"+(rowOverallResults3.getRowNum()+1));
                }else if(i==13){
                    Cell cell = rowOverallResults3.createCell(i, FORMULA);
                    cell.setCellStyle(stylePercentageData);
                    String first="K"+(rowOverallResults3.getRowNum()+1);
                    String second="L"+(rowOverallResults3.getRowNum()+1);

                    cell.setCellFormula("IF("+first+"<>0,("+first+"-"+second+")/"+first+",0)");


                }else{
                    Cell cell = rowOverallResults3.createCell(i, NUMERIC);
                    cell.setCellStyle(styleCurrencyNonBold);
                    cell.setCellValue(Double.valueOf((vals.size()==0)?"0.0":vals.get(i-5)));

                }

                cellOcc.incrementAndGet();

            }



        });

        Row totRow2=ovSheet.createRow(rowsOcc.getAndIncrement());
        for (int i = 0; i < 13; i++) {
            if(i==0 || i==9){
                Cell cell=totRow2.createCell(i);
                CellStyle style=workbook.createCellStyle();
                style.setFont(fontBold);
                cell.setCellStyle(style);
                cell.setCellValue("Total");

            }else if (i==7 || i==8){

                totRow2.createCell(i).setCellValue("");
                totRow2.createCell(i).setCellValue("");
            } else{
                Cell cell=totRow2.createCell(i, FORMULA);
                cell.setCellStyle(styleCurrencyBold);
                cell.setCellFormula("sum("+ CellReference.convertNumToColString(cell.getColumnIndex()) + (cell.getAddress().getRow()-this.data.getFiniancialClass().size()+1) + ":" + CellReference.convertNumToColString(cell.getColumnIndex())+ cell.getAddress().getRow() + ")");


            }

        }

        addSignPicture(ovSheet,rowsOcc,false);

        return ovSheet;
    }

    private Sheet createAgingSheet(){
        System.out.println("START Aging....");

        //Fin class #
        Sheet agingSheet=this.workbook.createSheet("Aging");
        AtomicInteger rowsOcc = new AtomicInteger();

        Row firstRow = agingSheet.createRow(rowsOcc.getAndIncrement());
        Cell cellFusion = firstRow.createCell(0, STRING);
        cellFusion.setCellValue("Aging Summary");
        //Merging cells by providing cell index
        agingSheet.addMergedRegion(new CellRangeAddress(0,0,0,9));
        cellFusion.setCellStyle(headerCellStyle);
        addColumnNamesInBlue(agingSheet, agingSheet.createRow(rowsOcc.getAndIncrement()), columnsAging, blueStyle);

        Row one =agingSheet.createRow(rowsOcc.getAndIncrement());
        Cell oo=one.createCell(0);
        agingSheet.setColumnWidth(oo.getColumnIndex(),4000);
        oo.setCellValue(rowsAging[0]);
        oo.setCellStyle(styleDataValues2);
        Cell cell0=one.createCell(1, NUMERIC);
        cell0.setCellStyle(styleNumeric);
        cell0.setCellValue(Integer.valueOf(this.data.getAgingAccountsInventory().get(0)[0]));
        Cell cell1=one.createCell(2, NUMERIC);
        cell1.setCellStyle(styleCurrencyNonBold);
        cell1.setCellValue(Double.valueOf((this.data.getAgingAccountsInventory().get(0)[1].trim().equals("null"))?"0.0":this.data.getAgingAccountsInventory().get(0)[1]));
        Cell cell2=one.createCell(3, NUMERIC);
        cell2.setCellStyle(stylePercentageData);
        double percent=Double.valueOf(this.data.getAgingAccountsInventory().get(0)[0])/Integer.valueOf(this.data.getAgingAccountsInventory().get(0)[2]);
        cell2.setCellValue(percent);


        Row two =agingSheet.createRow(rowsOcc.getAndIncrement());
        Cell oo2=two.createCell(0);
        agingSheet.setColumnWidth(oo2.getColumnIndex(),4000);
        oo2.setCellValue(rowsAging[1]);
        oo2.setCellStyle(styleDataValues2);
        Cell cell01=two.createCell(1, NUMERIC);
        cell01.setCellStyle(styleNumeric);
        cell01.setCellValue(Integer.valueOf(this.data.getAgingAccountsInventory().get(1)[0]));
        Cell cell11=two.createCell(2, NUMERIC);
        cell11.setCellStyle(styleCurrencyNonBold);
        cell11.setCellValue(Double.valueOf((this.data.getAgingAccountsInventory().get(1)[1].trim().equals("null"))?"0.0":this.data.getAgingAccountsInventory().get(1)[1]));
        Cell cell22=two.createCell(3, NUMERIC);
        cell22.setCellStyle(stylePercentageData);
        double percent2=Double.valueOf(this.data.getAgingAccountsInventory().get(1)[0])/Integer.valueOf(this.data.getAgingAccountsInventory().get(1)[2]);
        cell22.setCellValue(percent2);


        /*Row twob =agingSheet.createRow(rowsOcc.getAndIncrement());
        Cell oo22=twob.createCell(0);
        agingSheet.setColumnWidth(oo22.getColumnIndex(),4000);
        oo22.setCellValue(rowsAging[2]);
        oo22.setCellStyle(styleDataValues2);
        Cell cell012=twob.createCell(1, NUMERIC);
        cell012.setCellStyle(styleNumeric);
        cell012.setCellValue(Integer.valueOf(this.data.getAgingAccountsInventory().get(2)[0]));
        Cell cell112=twob.createCell(2, NUMERIC);
        cell112.setCellStyle(styleCurrencyNonBold);
        cell112.setCellValue(Double.valueOf((this.data.getAgingAccountsInventory().get(2)[1].trim().equals("null"))?"0.0":this.data.getAgingAccountsInventory().get(2)[1]));
        Cell cell222=twob.createCell(3, NUMERIC);
        cell222.setCellStyle(stylePercentageData);
        double percent3=Double.valueOf(this.data.getAgingAccountsInventory().get(2)[0])/Integer.valueOf(this.data.getAgingAccountsInventory().get(2)[2]);
        cell222.setCellValue(percent3);*/

        Row three=agingSheet.createRow(rowsOcc.getAndIncrement());
        Cell oo3=three.createCell(0);
        agingSheet.setColumnWidth(oo3.getColumnIndex(),4000);
        oo3.setCellValue(rowsAging[3]);
        oo3.setCellStyle(styleDataValues2);
        Cell celltr1=three.createCell(1, FORMULA);
        celltr1.setCellStyle(styleNumericBold);
        celltr1.setCellFormula("sum(B3:B4)");
        Cell celltr2=three.createCell(2, FORMULA);
        celltr2.setCellStyle(styleCurrencyBold);
        celltr2.setCellFormula("sum(C3:C4)");

        /*cell2.setCellFormula("C3/C6");
        cell22.setCellFormula("C4/C6");
        cell222.setCellFormula("C5/C6");*/

        agingSheet.createRow(rowsOcc.getAndIncrement());
        agingSheet.createRow(rowsOcc.getAndIncrement());


        //Fin class $
        // Create row and cells header for Active inventory $
        addColumnNamesInBlue(agingSheet, agingSheet.createRow(rowsOcc.getAndIncrement()), columnsFinClAging, blueStyle);
        this.data.getFinancialClassAging().forEach((finClass, vals) -> {
            //System.out.println("EE:"+vals.get(0));
            Row dtRow=agingSheet.createRow(rowsOcc.getAndIncrement());
            Cell c=dtRow.createCell(0, STRING);
            c.setCellValue(finClass);
            c.setCellStyle(styleDataValues2);
            AtomicInteger cellsOcc = new AtomicInteger(1);
            vals.forEach(val -> {
                Cell cell=dtRow.createCell(cellsOcc.getAndIncrement(), NUMERIC);
                cell.setCellStyle(styleCurrencyNonBold);
                cell.setCellValue(Double.valueOf(val));
            });
            Cell totalsCell=dtRow.createCell(cellsOcc.getAndIncrement(), FORMULA);
            totalsCell.setCellStyle(styleCurrencyBold);
            totalsCell.setCellFormula("sum(B" + (dtRow.getRowNum()+1) + ":H" + (dtRow.getRowNum()+1) + ")");

        });

        //Create grand total
        Row gTotalRow2=agingSheet.createRow(rowsOcc.getAndIncrement());
        Cell tot2=gTotalRow2.createCell(0, STRING);
        CellStyle style=workbook.createCellStyle();
        style.setFont(fontBold);
        tot2.setCellStyle(style);
        tot2.setCellValue("Grand Total");
        for (int i = 1; i < 9; i++) {
            Cell cell=gTotalRow2.createCell(i, FORMULA);
            cell.setCellStyle(styleCurrencyBold);
            cell.setCellFormula("sum("+ CellReference.convertNumToColString(cell.getColumnIndex()) + (cell.getAddress().getRow()-this.data.getFinancialClassAging().size()+1) + ":" + CellReference.convertNumToColString(cell.getColumnIndex())+ cell.getAddress().getRow() + ")");
        }




        addSignPicture(agingSheet,rowsOcc,true);

       /* Row signature=agingSheet.createRow(rowsOcc.getAndIncrement()+5);
        Cell fus = signature.createCell(0, STRING);
        fus.setCellStyle(signStyle);
        fus.setCellValue(SIGNATURE);
        agingSheet.addMergedRegion(new CellRangeAddress(signature.getRowNum(),signature.getRowNum(),0,8));
*/


        return agingSheet;
    }


    private Sheet createFinancialClassSheet(){

        System.out.println("START FINANCIAL CLASS....");

        //Fin class #
        Sheet finclassSheet=this.workbook.createSheet("Financial Class");
        AtomicInteger rowsOcc = new AtomicInteger();

        Row firstRow = finclassSheet.createRow(rowsOcc.getAndIncrement());
        Cell cellFusion = firstRow.createCell(0, STRING);
        cellFusion.setCellValue("Financial Class Summary");
        //Merging cells by providing cell index
        finclassSheet.addMergedRegion(new CellRangeAddress(0,0,0,8));
        cellFusion.setCellStyle(headerCellStyle);

        addSubtitle(finclassSheet, finclassSheet.createRow(rowsOcc.getAndIncrement()), "Active Inventory #", blueStyle);
        // Create row and cells header for Active inventory #
        addColumnNamesInBlue(finclassSheet, finclassSheet.createRow(rowsOcc.getAndIncrement()), columnsFinancialClass, blueStyle);

        this.data.getFinancialClassActiveInventorySharp().forEach((finClass, vals) -> {
            //System.out.println("EE:"+vals.get(0));
            Row dtRow=finclassSheet.createRow(rowsOcc.getAndIncrement());
            dtRow.createCell(0, STRING).setCellValue(finClass);
            AtomicInteger cellsOcc = new AtomicInteger(1);
            vals.forEach(val -> {
                Cell cell=dtRow.createCell(cellsOcc.getAndIncrement(), NUMERIC);
                cell.setCellStyle(styleNumeric);
                cell.setCellValue(Double.valueOf(val));
            });
            Cell totalsCell=dtRow.createCell(cellsOcc.getAndIncrement(), FORMULA);
            totalsCell.setCellStyle(styleNumericBold);
            totalsCell.setCellFormula("sum(B" + (dtRow.getRowNum()+1) + ":H" + (dtRow.getRowNum()+1) + ")");

        });

        //Create grand total
        Row gTotalRow=finclassSheet.createRow(rowsOcc.getAndIncrement());
        Cell tot=gTotalRow.createCell(0, STRING);
        CellStyle style=workbook.createCellStyle();
        style.setFont(fontBold);
        tot.setCellStyle(style);
        tot.setCellValue("Grand Total");
        for (int i = 1; i < 9; i++) {
            Cell cell=gTotalRow.createCell(i, FORMULA);
            cell.setCellStyle(styleNumericBold);
            cell.setCellFormula("sum("+ CellReference.convertNumToColString(cell.getColumnIndex()) + (cell.getAddress().getRow()-this.data.getFiniancialClass().size()+1) + ":" + CellReference.convertNumToColString(cell.getColumnIndex())+ cell.getAddress().getRow() + ")");
        }

        finclassSheet.createRow(rowsOcc.getAndIncrement());


        //Fin class $
        addSubtitle(finclassSheet, finclassSheet.createRow(rowsOcc.getAndIncrement()), "Active Inventory $", blueStyle);
        // Create row and cells header for Active inventory $
        addColumnNamesInBlue(finclassSheet, finclassSheet.createRow(rowsOcc.getAndIncrement()), columnsFinancialClass, blueStyle);

        this.data.getFinancialClassActiveInventoryDollar().forEach((finClass, vals) -> {
            //System.out.println("EE:"+vals.get(0));
            Row dtRow=finclassSheet.createRow(rowsOcc.getAndIncrement());
            dtRow.createCell(0, STRING).setCellValue(finClass);
            AtomicInteger cellsOcc = new AtomicInteger(1);
            vals.forEach(val -> {
                Cell cell=dtRow.createCell(cellsOcc.getAndIncrement(), NUMERIC);
                cell.setCellStyle(styleCurrencyNonBold);
                cell.setCellValue(Double.valueOf(val));
            });
            Cell totalsCell=dtRow.createCell(cellsOcc.getAndIncrement(), FORMULA);
            totalsCell.setCellStyle(styleCurrencyBold);
            totalsCell.setCellFormula("sum(B" + (dtRow.getRowNum()+1) + ":H" + (dtRow.getRowNum()+1) + ")");

        });

        //Create grand total
        Row gTotalRow2=finclassSheet.createRow(rowsOcc.getAndIncrement());
        Cell tot2=gTotalRow2.createCell(0, STRING);
        tot2.setCellStyle(style);
        tot2.setCellValue("Grand Total");
        for (int i = 1; i < 9; i++) {
            Cell cell=gTotalRow2.createCell(i, FORMULA);
            cell.setCellStyle(styleCurrencyBold);
            cell.setCellFormula("sum("+ CellReference.convertNumToColString(cell.getColumnIndex()) + (cell.getAddress().getRow()-this.data.getFiniancialClass().size()+1) + ":" + CellReference.convertNumToColString(cell.getColumnIndex())+ cell.getAddress().getRow() + ")");
        }

        finclassSheet.createRow(rowsOcc.getAndIncrement());


        //Fin class AGE
        String computeDate=DateUtils.minusMonthsFromDate(this.data.getEndDateSelected(),5);
        String[] agDates=new String[8];
        agDates[0]="Financial Class";
        for (int i = 1; i < 8; i++) {
            agDates[i]=DateUtils.getDateMonthShortName(computeDate)+" - "+DateUtils.getDateYear(computeDate);
             computeDate=DateUtils.addMonthsToDate(computeDate,1);
        }
        agDates[7]="Grand Total";



        addSubtitle(finclassSheet, finclassSheet.createRow(rowsOcc.getAndIncrement()), "Average Age", blueStyle);
        // Create row and cells header for Active inventory $
        addColumnNamesInBlue(finclassSheet, finclassSheet.createRow(rowsOcc.getAndIncrement()), agDates, blueStyle);

        this.data.getFinancialClassActiveInventoryAverageAge().forEach((finClass, vals) -> {
            //System.out.println("EEVALS:"+Json.encodePrettily(vals));
            Row dtRow=finclassSheet.createRow(rowsOcc.getAndIncrement());
            dtRow.createCell(0, STRING).setCellValue(finClass);
            AtomicInteger cellsOcc = new AtomicInteger(1);

            for (int i = 1; i < agDates.length-1; i++) {
                Cell cell=dtRow.createCell(cellsOcc.getAndIncrement(), NUMERIC);
                cell.setCellStyle(styleNumeric);

                int finalI = i;
                vals.forEach(val -> {

                    String nn="";
                    try {
                        nn= Month.of(Integer.parseInt(val[0].trim().split(" - ")[0])).getDisplayName(TextStyle.SHORT_STANDALONE, Locale.getDefault());

                    }catch (NumberFormatException nf){
                        nn= val[0].trim().split(" - ")[0];
                    }


                    //nn=(nn.length()>=3)?nn.substring(0,3):nn;
                    nn+=" - "+val[0].trim().split(" - ")[1];

                    System.out.println("EEVALS:"+agDates[finalI].trim()+" "+nn);

                    if(nn.trim().equals(agDates[finalI].trim())){
                        cell.setCellValue(Double.valueOf(val[1]));

                    }
                });
            }



            Cell totalsCell=dtRow.createCell(cellsOcc.getAndIncrement(), FORMULA);
            totalsCell.setCellStyle(styleNumericBold);
            String first="B" + (dtRow.getRowNum()+1);
            String second="G" + (dtRow.getRowNum()+1);

            totalsCell.setCellFormula("IF(COUNTIF("+first+":"+second+",\"<>0\")>0,ROUNDDOWN(SUM("+first+":"+second+")/COUNTIF("+first+":"+second+",\"<>0\"),0),0)");

        });

        //Create grand total
        Row gTotalRow3=finclassSheet.createRow(rowsOcc.getAndIncrement());
        Cell tot3=gTotalRow3.createCell(0, STRING);
        tot3.setCellStyle(style);
        tot3.setCellValue("Grand Total");
        for (int i = 1; i < 8; i++) {
            Cell cell=gTotalRow3.createCell(i, FORMULA);
            cell.setCellStyle(styleNumericBold);
            String first=CellReference.convertNumToColString(cell.getColumnIndex()) + (cell.getAddress().getRow()-this.data.getFiniancialClass().size()+1);
            String second=CellReference.convertNumToColString(cell.getColumnIndex())+ cell.getAddress().getRow();

            //cell.setCellFormula("IF(COUNT("+first+":"+second+")>0,ROUNDDOWN(AVERAGE("+first+":"+second+"),0),\"\")");
            cell.setCellFormula("IF(COUNTIF("+first+":"+second+",\"<>0\")>0,ROUNDDOWN(SUM("+first+":"+second+")/COUNTIF("+first+":"+second+",\"<>0\"),0),0)");


        }


        addSignPicture(finclassSheet,rowsOcc,true);

        //finclassSheet.setFitToPage(true);

        /*Row signature=finclassSheet.createRow(rowsOcc.getAndIncrement()+5);
        Cell fus = signature.createCell(0, STRING);
        fus.setCellStyle(signStyle);
        fus.setCellValue(SIGNATURE);
        finclassSheet.addMergedRegion(new CellRangeAddress(signature.getRowNum(),signature.getRowNum(),0,8));
*/
        return finclassSheet;

    }



    public String getBase64GeneratedFile(){

        Sheet sheetFinancialClass;
        Sheet sheetAging;
        Sheet sheetOv;
        Sheet sheetBt;

        // ******* START FINANCIAL CLASS *******

        sheetFinancialClass=this.createFinancialClassSheet();
        sheetAging=this.createAgingSheet();
        sheetOv=this.createOverallSheet();

        //Build Clients tables
        if(this.data.isShowClients())

        this.data.getMonthlyList().entrySet()
                .stream()
                .sorted(comparingByKey())
                .collect(
                        toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (e1, e2) -> e2, LinkedHashMap::new)).forEach((client, vals) -> {
            if(!vals.isEmpty()){
                String cli_name = client
                        .replaceAll("CHI St Lukes Health Memorial ","").trim()
                        .replaceAll("CHI St. Joseph Health ","")
                        .replaceAll("CHI St Lukes ","")
                        .replaceAll("CHI Brazosport ","")
                        .replaceAll(" (Bad Debt)","")
                        .replaceAll("Medical Center", "").replaceAll("-", " ");
                if (cli_name.length() > 28) {
                    cli_name = cli_name.substring(0, 28);
                }


                //int id=this.data.getClientsIds().get(this.data.getClientsNames().indexOf(client));
                this.createClientDataOcc(cli_name,cli_name,vals,false);
               // this.createClientDataOcc(id+" "+cli_name,cli_name,vals,false);

            }


        });


        //Build Batch Track table

        sheetBt= this.createClientDataOcc("Batch Track","Placement Summary - Overall",calculateBatchTrack(this.data).getBatchTrackAlldatas(),true);


        sheetFinancialClass.forEach(cells -> cells.forEach(cell -> {
            sheetFinancialClass.setColumnWidth(cell.getColumnIndex(), 4200);
            sheetFinancialClass.setDefaultColumnStyle(cell.getColumnIndex(), defaultStyle);
            sheetFinancialClass.setColumnWidth(0, 5500);

        }));


        /*sheetBt.forEach(cells -> cells.forEach(cell -> {
            sheetBt.setColumnWidth(cell.getColumnIndex(), 4200);
            sheetBt.setDefaultColumnStyle(cell.getColumnIndex(),defaultStyle);
            //sheetBt.setColumnWidth(0, 5500);

        }));*/

        sheetAging.forEach(cells -> cells.forEach(cell -> {
            sheetAging.setColumnWidth(cell.getColumnIndex(), 4200);
            sheetAging.setDefaultColumnStyle(cell.getColumnIndex(), defaultStyle);
            sheetAging.setColumnWidth(0, 5500);


        }));

        sheetOv.forEach(cells -> cells.forEach(cell -> {
            sheetOv.setColumnWidth(cell.getColumnIndex(), 3500);
            sheetOv.setDefaultColumnStyle(cell.getColumnIndex(), defaultStyle);
            sheetOv.setColumnWidth(0, 5000);
            sheetOv.setColumnWidth(8, 800);

        }));



        OutputStream fileOut;
        String fileName = Paths.get("assets","healthCareFiles") +File.separator+this.data.getSessionId().replace("-", "")+".xlsx";

        try {


            fileOut = new FileOutputStream(fileName);

            workbook.write(fileOut);

            fileOut.close();
            workbook.close();
            System.out.println("END......");

            return encodeFileToBase64Binary(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("ERRR......");

        return "";
    }


    /**
     * This method allows to resize automatically the column size
     * @param wb this parameter represents the workbook
     */
  /*  private void autoSizeColumns(Workbook wb) {
        int numberOfSheets = wb.getNumberOfSheets();
        for (int sheetNum = 0; sheetNum < numberOfSheets; sheetNum++) {
            Row row = wb.getSheetAt(sheetNum).getRow(0);
            for (int colNum = 0; colNum < row.getLastCellNum(); colNum++) {
                wb.getSheetAt(sheetNum).autoSizeColumn(colNum);
            }
        }
    }*/



    /**
     * This method allows to display subtitle such Active Inventory #, Active inventory $, Average Age within Financial class tab
     * @param row
     * @param subtitle
     * @param cellStyle
     */
    private static void addSubtitle(Sheet sheet, Row row, String subtitle, CellStyle cellStyle) {
        row.setHeight((short) 400);
        Cell cellTitle = row.createCell((short) 0);
        cellTitle.setCellValue(subtitle);
        cellTitle.setCellStyle(cellStyle);
        sheet.autoSizeColumn(0);
    }

    /**
     * This method allows to display all column names
     * @param row
     * @param columnHeaders
     * @param cellStyle
     */
    private static void addColumnNamesInBlue(Sheet sheet, Row row, String[] columnHeaders, CellStyle cellStyle) {
        row.setHeight((short) 305);

        for(int i = 0; i < columnHeaders.length; i++) {
            Cell cellTemp = row.createCell(i);
            //if (!columnHeaders[i].trim().isEmpty()){
                cellTemp.setCellValue(columnHeaders[i]);
                cellTemp.setCellStyle(cellStyle);
            //}
        }

        for(int i = 0; i < columnHeaders.length; i++) {
            sheet.autoSizeColumn(i);
        }
    }


    private static String encodeFileToBase64Binary(String fileName) throws IOException {
        File file = new File(fileName);
        byte[] encoded = Base64.encodeBase64(Files.readAllBytes(file.toPath()));
        return new String(encoded, StandardCharsets.US_ASCII);
    }

    public static void main(String[] args) {

        //


        Vertx.vertx().fileSystem().open("h_test.json", new OpenOptions(), ar -> {
            if (ar.succeeded()) {
                System.out.println("Read...");
                AsyncFile asyncFile = ar.result();
                JsonParser jsonParser = JsonParser.newParser(asyncFile);
                jsonParser.arrayValueMode()
                        .exceptionHandler(t -> {
                            t.printStackTrace();
                            asyncFile.close();
                        })
                        .endHandler(v -> {
                            asyncFile.close();
                        }).handler(event -> {
                    if (event.type() == VALUE) {


                        System.out.println(event.arrayValue());
                        ArrayList<HealthOutputData> dataPoint = new Gson().fromJson(event.arrayValue().encode(), TypeToken.getParameterized(ArrayList.class, HealthOutputData.class).getType());

                        HealthCareExcelFileGenerator generator=new HealthCareExcelFileGenerator(dataPoint.get(0));

                        //generator.getBase64GeneratedFile();

                        new HealthExcelAndPPTFileHelper(dataPoint.get(0)).generatePPTFile(dataPoint.get(0).getSessionId());



                    }
                });
            } else {
                ar.cause().printStackTrace();
            }
        });


    }



    public HealthOutputData getData() {
        return data;
    }

    public void setData(HealthOutputData data) {
        this.data = data;
    }

    public Workbook getWorkbook() {
        return workbook;
    }

    public void setWorkbook(Workbook workbook) {
        this.workbook = workbook;
    }

    public DataFormat getPercentStyle() {
        return percentStyle;
    }

    public void setPercentStyle(DataFormat percentStyle) {
        this.percentStyle = percentStyle;
    }

    public DataFormat getStyleNumberCustomFormat() {
        return styleNumberCustomFormat;
    }

    public void setStyleNumberCustomFormat(DataFormat styleNumberCustomFormat) {
        this.styleNumberCustomFormat = styleNumberCustomFormat;
    }

    public DataFormat getCustomFormat() {
        return customFormat;
    }

    public void setCustomFormat(DataFormat customFormat) {
        this.customFormat = customFormat;
    }

    public CellStyle getHeaderCellStyle() {
        return headerCellStyle;
    }

    public void setHeaderCellStyle(CellStyle headerCellStyle) {
        this.headerCellStyle = headerCellStyle;
    }

    public CellStyle getBlankStyle() {
        return blankStyle;
    }

    public void setBlankStyle(CellStyle blankStyle) {
        this.blankStyle = blankStyle;
    }

    public CellStyle getBlueStyle() {
        return blueStyle;
    }

    public void setBlueStyle(CellStyle blueStyle) {
        this.blueStyle = blueStyle;
    }

    public CellStyle getStyleCurrencyNonBold() {
        return styleCurrencyNonBold;
    }

    public void setStyleCurrencyNonBold(CellStyle styleCurrencyNonBold) {
        this.styleCurrencyNonBold = styleCurrencyNonBold;
    }

    public CellStyle getStyleCurrencyBold() {
        return styleCurrencyBold;
    }

    public void setStyleCurrencyBold(CellStyle styleCurrencyBold) {
        this.styleCurrencyBold = styleCurrencyBold;
    }

    private void addSignPicture(Sheet sheet, AtomicInteger rowsOcc, boolean rszPict){
        try {
            String logo=Paths.get("assets")+File.separator+"sign_logo.png";
            /*InputStream img = new FileInputStream(logo);
            byte[] bytes = IOUtils.toByteArray(img);
            workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);*/

            final FileInputStream stream = new FileInputStream(logo);
            final CreationHelper helper = workbook.getCreationHelper();
            final Drawing drawing = sheet.createDrawingPatriarch();

            final ClientAnchor anchor = helper.createClientAnchor();
            anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_AND_RESIZE);


            final int pictureIndex = workbook.addPicture(IOUtils.toByteArray(stream), Workbook.PICTURE_TYPE_PNG);

            anchor.setCol1(0);
            anchor.setRow1(rowsOcc.getAndIncrement()+4);
            anchor.setRow2(rowsOcc.getAndIncrement()+8);
            anchor.setCol2(2);
            final Picture pict = drawing.createPicture( anchor, pictureIndex );

            if(rszPict) pict.resize(0.5);
            else pict.resize(0.6);
            //sheet.addMergedRegion(new CellRangeAddress(rowsOcc.getAndIncrement()+2,rowsOcc.getAndIncrement()+4,0,0));



        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
