package com.fma.wb.integration.i3module.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fma.wb.integration.common.utils.Utils;

import java.io.Serializable;

/**
 * @author Tiana Andriamanalina
 * @version 1.0
 * AutonomyRecordingsMetadata class is used for the mapping
 */


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AutonomyRecordingsMetadata implements Serializable {
    private int record_id=-1;
    private String recorded_call_id_key;
    private String direction;
    private String initiated_date;
    private String connected_date;
    private String terminated_date;
    private int call_duration;
    private int hold_duration;
    private String recording_file_name;
    private int recording_file_size;
    private String voice_user_id;
    private String voice_full_name;
    private String ani;
    private String dnis;
    private String dialer_call;
    private String manual_call;
    private String local_number;
    private String remote_number;
    private String cancel_status;
    private String debtor_id;
    private String source;

    public void synchCorrectDateFields(){
        try {
            this.connected_date = Utils.toUsCentralTimeFormat(this.connected_date);
            this.terminated_date = Utils.toUsCentralTimeFormat(this.terminated_date);
            this.initiated_date = Utils.toUsCentralTimeFormat(this.initiated_date);
        } catch(Exception exception) {

        }
    }
    public int getRecord_id() {
        return record_id;
    }

    public void setRecord_id(int record_id) {
        this.record_id = record_id;
    }

    public String getRecorded_call_id_key() {
        return recorded_call_id_key;
    }

    public void setRecorded_call_id_key(String recorded_call_id_key) {
        this.recorded_call_id_key = recorded_call_id_key;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getInitiated_date() {
        return initiated_date;
    }

    public void setInitiated_date(String initiated_date) {
        this.initiated_date = initiated_date;
    }

    public String getConnected_date() {
        return connected_date;
    }

    public void setConnected_date(String connected_date) {
        this.connected_date = connected_date;
    }

    public String getTerminated_date() {
        return terminated_date;
    }

    public void setTerminated_date(String terminated_date) {
        this.terminated_date = terminated_date;
    }

    public int getCall_duration() {
        return call_duration;
    }

    public void setCall_duration(int call_duration) {
        this.call_duration = call_duration;
    }

    public int getHold_duration() {
        return hold_duration;
    }

    public void setHold_duration(int hold_duration) {
        this.hold_duration = hold_duration;
    }

    public String getRecording_file_name() {
        return recording_file_name;
    }

    public void setRecording_file_name(String recording_file_name) {
        this.recording_file_name = recording_file_name;
    }

    public int getRecording_file_size() {
        return recording_file_size;
    }

    public void setRecording_file_size(int recording_file_size) {
        this.recording_file_size = recording_file_size;
    }

    public String getVoice_user_id() {
        return voice_user_id;
    }

    public void setVoice_user_id(String voice_user_id) {
        this.voice_user_id = voice_user_id;
    }

    public String getVoice_full_name() {
        return voice_full_name;
    }

    public void setVoice_full_name(String voice_full_name) {
        this.voice_full_name = voice_full_name;
    }

    public String getAni() {
        return ani;
    }

    public void setAni(String ani) {
        this.ani = ani;
    }

    public String getDnis() {
        return dnis;
    }

    public void setDnis(String dnis) {
        this.dnis = dnis;
    }

    public String getDialer_call() {
        return dialer_call;
    }

    public void setDialer_call(String dialer_call) {
        this.dialer_call = dialer_call;
    }

    public String getManual_call() {
        return manual_call;
    }

    public void setManual_call(String manual_call) {
        this.manual_call = manual_call;
    }

    public String getLocal_number() {
        return local_number;
    }

    public void setLocal_number(String local_number) {
        this.local_number = local_number;
    }

    public String getRemote_number() {
        return remote_number;
    }

    public void setRemote_number(String remote_number) {
        this.remote_number = remote_number;
    }

    public String getCancel_status() {
        return cancel_status;
    }

    public void setCancel_status(String cancel_status) {
        this.cancel_status = cancel_status;
    }

    public String getDebtor_id() {
        return debtor_id;
    }

    public void setDebtor_id(String debtor_id) {
        this.debtor_id = debtor_id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public AutonomyRecordingsMetadata() {
    }

    @Override
    public String toString() {
        return "AutonomyRecordingsMetadata{" +
                "record_id=" + record_id +
                ", recorded_call_id_key='" + recorded_call_id_key + '\'' +
                ", direction='" + direction + '\'' +
                ", initiated_date=" + initiated_date +
                ", connected_date=" + connected_date +
                ", terminated_date=" + terminated_date +
                ", call_duration=" + call_duration +
                ", hold_duration=" + hold_duration +
                ", recording_file_name='" + recording_file_name + '\'' +
                ", recording_file_size=" + recording_file_size +
                ", voice_user_id='" + voice_user_id + '\'' +
                ", voice_full_name='" + voice_full_name + '\'' +
                ", ani='" + ani + '\'' +
                ", dnis='" + dnis + '\'' +
                ", dialer_call='" + dialer_call + '\'' +
                ", manual_call='" + manual_call + '\'' +
                ", local_number='" + local_number + '\'' +
                ", remote_number='" + remote_number + '\'' +
                ", cancel_status='" + cancel_status + '\'' +
                ", debtor_id='" + debtor_id + '\'' +
                ", source='" + source + '\'' +
                '}';
    }
}