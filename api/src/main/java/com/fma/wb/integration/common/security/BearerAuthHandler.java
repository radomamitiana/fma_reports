package com.fma.wb.integration.common.security;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.impl.AuthHandlerImpl;

class BearerAuthHandler extends AuthHandlerImpl {

    public BearerAuthHandler(AuthProvider authProvider) {
        super(authProvider);
    }

    @Override
    public void handle(RoutingContext context) {

    }

    @Override
    public void parseCredentials(RoutingContext context, Handler<AsyncResult<JsonObject>> handler) {
        HttpServerRequest request = context.request();
        String authorization = request.headers().get(HttpHeaders.AUTHORIZATION);

        if (authorization == null) {
            context.fail(401);
        } else {
            String token;
            String scheme;

            try {
                String[] parts = authorization.split(" ");
                scheme = parts[0];
                token = parts[1];
            } catch (ArrayIndexOutOfBoundsException e) {
                context.fail(401);
                return;
            } catch (IllegalArgumentException | NullPointerException e) {
                context.fail(e);
                return;
            }

            if (scheme.equalsIgnoreCase("bearer")) {
                JsonObject creds = new JsonObject();
                creds.put("token", token);

                authProvider.authenticate(creds,  res ->{
                    if (res.succeeded()) {
                        context.next();
                    } else {
                        context.fail(401);
                    }
                });

            } else {
                context.fail(401);
            }
        }
    }
}