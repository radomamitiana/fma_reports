package com.fma.wb.integration.common.dto.enums;

public enum WsResponseType {
    TYPE_REQUEST,
    TYPE_EVENT,
    TYPE_ERROR
}
