package com.fma.wb.integration.i3module.dto;

import java.io.Serializable;

public class LiveVox implements Serializable{
    private int agent_name_id;
    private int agent_result_id;
    private String call_date;
    private int call_result_id;
    private int campaign_file_name_id;
    private int client_id;
    private int day;
    private long debtor;
    private int directory_id;
    private int duration;
    private int hour;
    private int live_vox_id;
    private int min;
    private int month;
    private String phone_dialed;
    private int sec;
    private String session_id;
    private String wave_file_name;
    private int year;
    private int zip_file_id;

    public LiveVox() {
    }

    public int getAgent_name_id() {
        return agent_name_id;
    }

    public void setAgent_name_id(int agent_name_id) {
        this.agent_name_id = agent_name_id;
    }

    public int getAgent_result_id() {
        return agent_result_id;
    }

    public void setAgent_result_id(int agent_result_id) {
        this.agent_result_id = agent_result_id;
    }

    public String getCall_date() {
        return call_date;
    }

    public void setCall_date(String call_date) {
        this.call_date = call_date;
    }

    public int getCall_result_id() {
        return call_result_id;
    }

    public void setCall_result_id(int call_result_id) {
        this.call_result_id = call_result_id;
    }

    public int getCampaign_file_name_id() {
        return campaign_file_name_id;
    }

    public void setCampaign_file_name_id(int campaign_file_name_id) {
        this.campaign_file_name_id = campaign_file_name_id;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public long getDebtor() {
        return debtor;
    }

    public void setDebtor(long debtor) {
        this.debtor = debtor;
    }

    public int getDirectory_id() {
        return directory_id;
    }

    public void setDirectory_id(int directory_id) {
        this.directory_id = directory_id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getLive_vox_id() {
        return live_vox_id;
    }

    public void setLive_vox_id(int live_vox_id) {
        this.live_vox_id = live_vox_id;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getPhone_dialed() {
        return phone_dialed;
    }

    public void setPhone_dialed(String phone_dialed) {
        this.phone_dialed = phone_dialed;
    }

    public int getSec() {
        return sec;
    }

    public void setSec(int sec) {
        this.sec = sec;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getWave_file_name() {
        return wave_file_name;
    }

    public void setWave_file_name(String wave_file_name) {
        this.wave_file_name = wave_file_name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getZip_file_id() {
        return zip_file_id;
    }

    public void setZip_file_id(int zip_file_id) {
        this.zip_file_id = zip_file_id;
    }
}