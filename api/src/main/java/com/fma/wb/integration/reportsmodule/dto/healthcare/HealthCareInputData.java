package com.fma.wb.integration.reportsmodule.dto.healthcare;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HealthCareInputData implements Serializable {

    private int organizationId ;
    private String beginDateRange = "";
    private String endDateRange = "";
    private boolean showClients;

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int org) {
        this.organizationId = org;
    }

    public String getBeginDateRange() {
        return beginDateRange;
    }

    public void setBeginDateRange(String beginDateRange) {
        this.beginDateRange = beginDateRange;
    }

    public String getEndDateRange() {
        return endDateRange;
    }

    public void setEndDateRange(String endDateRange) {
        this.endDateRange = endDateRange;
    }

    public HealthCareInputData() {
    }


    public boolean isShowClients() {
        return showClients;
    }

    public void setShowClients(boolean showClients) {
        this.showClients = showClients;
    }
}
