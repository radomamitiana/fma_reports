package com.fma.wb.integration.reportsmodule.dto.splittime;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SplitTimeReportOutputData implements Serializable {
    private String dateRequest = "";
    private List<SplitTimeDto> splitTimeDtoList = new ArrayList<>();
    private List<SplitTimeDto> splitTimeDtoNewFactorList = new ArrayList<>();

    public String getDateRequest() {
        return dateRequest;
    }

    public void setDateRequest(String dateRequest) {
        this.dateRequest = dateRequest;
    }

    public List<SplitTimeDto> getSplitTimeDtoList() {
        return splitTimeDtoList;
    }

    public void setSplitTimeDtoList(List<SplitTimeDto> splitTimeDtoList) {
        this.splitTimeDtoList = splitTimeDtoList;
    }

    public List<SplitTimeDto> getSplitTimeDtoNewFactorList() {
        return splitTimeDtoNewFactorList;
    }

    public void setSplitTimeDtoNewFactorList(List<SplitTimeDto> splitTimeDtoNewFactorList) {
        this.splitTimeDtoNewFactorList = splitTimeDtoNewFactorList;
    }

    public SplitTimeReportOutputData() {
    }

    @Override
    public String toString() {
        return "SplitTimeReportOutputData{" +
                "dateRequest='" + dateRequest + '\'' +
                ", splitTimeDtoList=" + splitTimeDtoList +
                ", splitTimeDtoNewFactorList=" + splitTimeDtoNewFactorList +
                '}';
    }
}