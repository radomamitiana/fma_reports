package com.fma.wb.integration.i3module.services;
import com.fma.wb.integration.i3module.dto.CompositeType;
import com.fma.wb.integration.i3module.dto.SessionInteraction;
import com.fma.wb.integration.i3module.dto.SessionSettings;
import com.fma.wb.integration.common.dto.api.ApiResponse;
import com.fma.wb.integration.common.dto.enums.WsResponseType;
import com.fma.wb.integration.common.utils.ConfigurationUtils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import common.Logger;
import common.LoggerTag;
import dto.*;
import dto.enums.*;
import dto.handlers.*;
import dto.statusMessagesDto.QueueSubscriptionParameters;
import dto.statusMessagesDto.UserStatus;
import dto.statusMessagesDto.UserStatusUpdate;
import interactions.*;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import messaging.MessagingController;
import org.glassfish.jersey.internal.util.ExceptionUtils;
import queues.InteractionQueue;
import queues.InteractionQueueController;
import rest_api_core.ConnexionResponse;
import session.DialerSession;
import session.Session;
import status.Status;
import java.util.*;

import static com.fma.wb.integration.common.utils.ConfigurationUtils.*;

/**
 * This class is going to be my verticle and should extend AbstractVerticle
 * The start method should be created in that class when implementing the vert.x part of this implementation
 *
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class I3WService  implements II3WService, DialerSession.EventHandler, MessagingController.EventHandler {

    //private static final Logger log = Logger.getLogger(I3WService.class);
    private HashMap<String, SessionInteraction> sessionMap;
    private HashMap<String, MessagingController> messagingList;
    private List<String> activeUserList;
    private static I3WService i3WService;
    private Vertx vertx;
    private ServerEventMessenger serverEventMessenger;
    private HashMap<String, List<AvailableCampaign>> sessionsAvailableCapaigns = new HashMap<>();
    public static final String managerRole = "29";

    /**
     * This constructor accepts no parameters
     * and will initialize the sessionMap and ActiveUserList properties.
     */
    public I3WService() {
        this.sessionMap = new HashMap<>();
        this.activeUserList = new ArrayList<>();
        this.messagingList=new HashMap<>();
        this.vertx = Vertx.vertx();
    }

    /**
     * This constructor with parameter
     *
     * @param vertx
     */
    public I3WService(Vertx vertx) {
        this.sessionMap = new HashMap<>();
        this.activeUserList = new ArrayList<>();
        this.messagingList=new HashMap<>();
        this.vertx = vertx;
    }

    /**
     * This method returns an instance of I3WService
     *
     * @param vertx
     * @param eventHandler
     * @return I3WService instance
     */
    public static I3WService getInstance(Vertx vertx, ServerEventMessenger eventHandler) {
        if (i3WService == null) {
            i3WService = new I3WService(vertx);
            if (eventHandler != null)
                i3WService.setServerEventMessenger(eventHandler);
        } else {
            if (eventHandler != null)
                i3WService.setServerEventMessenger(eventHandler);
        }

        return i3WService;
    }

    /**
     * This is the event handler related to interactionQueue_InteractionAdded.
     *
     * @param o
     * @param e
     */
    @Override
    public void interactionQueue_InteractionAdded(Object o, InteractionAttributesEventArgs e) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: interactionQueue_InteractionAdded ~~~~~~~~~~~~~~~~~~~~~~","");
        //inbound call capture here
        if (e.getInteraction() != null) {

            ApiResponse apiResponse = new ApiResponse();
            apiResponse.setResponseType(WsResponseType.TYPE_EVENT);
            //we retrieve the sessionId and pass it to the clientCaller
            //String actualSessionId = e.getInteraction().getSessionId();
            //TO CHECK

            InteractionController cInteractionController = e.getInteraction().getController(e.getSession());


            String actualSessionId = e.getSession().getSessionId();
            apiResponse.setComment("Interaction added to queue." + actualSessionId);
            apiResponse.setData(e.getMessage().toString());
            i3WService.getServerEventMessenger().sendMessage(apiResponse);

            try {
                //if (! e.getInteraction().isWatching(ConfigurationUtils.EIC_CALL_DIRECTION)) {
                InteractionSubscription subscription = new InteractionSubscription();
                ArrayList<String> paramsList = new ArrayList<>();
                paramsList.add(ConfigurationUtils.EIC_CALL_DIRECTION);
                paramsList.add(ConfigurationUtils.EIC_REMOTE_ADDRESS);
                paramsList.add(ConfigurationUtils.CALL_ID_KEY);
                subscription.setAttributeNames(paramsList);
                //e.Interaction.StartWatching(new string[] { "Eic_CallDirection", "Eic_RemoteAddress", InteractionAttributeName.CallIdKey});
                //StatusMessage: UserStatus getSpecifiedUserStatusByUserId(String sessionId, String userId, UserStatusUpdate userStatusUpdate, User user)
                e.getInteraction().getController(e.getSession()).createOrUpdateWatchSubscriptionOnInteraction(e.getSession().getSessionId(), e.getInteraction().getInteractionId(), subscription);
                //}

                Logger.getInstance("").log(LoggerTag.INFORMATION, "InteractionQueue_InteractionAdded...[" + e.getSession().getSessionInformation().getMachineName() +
                        "] callDirection: " + getInteractionAttribute(actualSessionId, e.getInteraction(), ConfigurationUtils.EIC_CALL_DIRECTION) + " [" +
                        getInteractionAttribute(actualSessionId, e.getInteraction(), ConfigurationUtils.CALL_ID_KEY) + "]", "");
            } catch (Exception ex) {
                ex.printStackTrace();
                Logger.getInstance("").log(LoggerTag.ERROR, "InteractionQueue_InteractionAdded... Interaction " + e.getInteraction().getInteractionId() + " " + ex.getMessage(), "");
            }

            //SessionSettings sessionSettings = null;
            String currentMachine = e.getSession().getSessionInformation().getMachineName();
            SessionInteraction sess=sessionMap.get(currentMachine);

            if ("I".equals(getInteractionAttribute(actualSessionId, e.getInteraction(), ConfigurationUtils.EIC_CALL_DIRECTION))) {
                //inbound call
                //sessionSettings = (SessionSettings)e.getInteraction().getController().getSession().getSessionInformation();
                //sessionSettings = (SessionSettings)e.getSession().getSessionInformation();
                if (sessionMap.containsKey(currentMachine)) {
                    if (sessionMap.get(currentMachine).getInbound() == null ||
                            !getInteractionAttribute(actualSessionId,sessionMap.get(currentMachine).getInbound(),EIC_STATE).equals("C") ) {
                        //*****add an inbound call interaction -
                        sess.setIod(1);

                        Logger.getInstance("").log(LoggerTag.INFORMATION, "InteractionQueue_InteractionAdded: add inbound call - on [" + currentMachine + "] " +
                                getInteractionAttribute(actualSessionId, e.getInteraction(), ConfigurationUtils.CALL_ID_KEY), "");

                        sess.setInbound(e.getInteraction());
                        sessionMap.replace(currentMachine,sess);
                    } else {
                        Logger.getInstance("").log(LoggerTag.INFORMATION, "InteractionQueue_InteractionAdded: has connected inbound - skip add inbound call - on [" + currentMachine + "] " +
                                getInteractionAttribute(actualSessionId, e.getInteraction(), ConfigurationUtils.CALL_ID_KEY), "");
                    }
                } else {
                    Logger.getInstance("").log(LoggerTag.ERROR, "InteractionQueue_InteractionAdded: Couldn't add inbound call-[" + currentMachine + "] not in session map.  " +
                            getInteractionAttribute(actualSessionId, e.getInteraction(), ConfigurationUtils.CALL_ID_KEY), "");
                }
            } else if (getInteractionAttribute(actualSessionId, e.getInteraction(), ConfigurationUtils.EIC_CALL_DIRECTION).equals(ConfigurationUtils.EIC_CALL_DIRECTION_VALUE_O)) {

                //need to capture the agentless campaign IVR call
                //sessionSettings = (SessionSettings) e.getInteraction().getController().getSession().getSessionInformation();

                //sessionSettings = (SessionSettings) e.getSession().getSessionInformation();

                if (sessionMap.containsKey(currentMachine)) {
                    //exclude the transfer call added (the make transfer call already added the call interaction in TransferToInteraction)
                    if (!sessionMap.get(currentMachine).isTransfer()) {
                        try {
                            //add the new call interaction
                            if (sessionMap.get(currentMachine).getIod() != ConfigurationUtils.IOD_VALUE_2) {
                                //exclude the manual call

                                sess=sessionMap.get(currentMachine);
                                sess.setIod(ConfigurationUtils.IOD_VALUE_4); //possible ivr - dialer call will be changed to 3
                                sess.setInteraction(e.getInteraction());
                                sessionMap.replace(currentMachine,sess);
                                Logger.getInstance("").log(LoggerTag.INFORMATION, "InteractionQueue_InteractionAdded: add to [" + currentMachine + "] call interaction campaign - "
                                        + getInteractionAttribute(actualSessionId, e.getInteraction(), "is_attr_campaignid"), "");
                            } else {
                                //the manual call still in the processing of remove and the ivr call coming, let check if the manual is done
                                if (!getInteractionAttribute(actualSessionId, e.getInteraction(), "is_attr_campaignid").isEmpty()) {
                                    //the current interaction is marked as manual and the call is not in connecting state - timing
                                    if (sessionMap.get(currentMachine).getInteraction() != null && !getInteractionAttribute(actualSessionId,sessionMap.get(currentMachine).getInteraction(),EIC_STATE).equals("C")) {
                                        Logger.getInstance("").log(LoggerTag.INFORMATION, "InteractionQueue_InteractionAdded: interaction not connect add to [" + currentMachine + "] call interaction " +
                                                "campaign - " + getInteractionAttribute(actualSessionId, e.getInteraction(), "is_attr_campaignid"), "");

                                        sess=sessionMap.get(currentMachine);
                                        sess.setIod(ConfigurationUtils.IOD_VALUE_4); //possible ivr
                                        sess.setInteraction(e.getInteraction());
                                        sessionMap.replace(currentMachine,sess);
                                    } else {
                                        //the current interaction
                                        if (sessionMap.get(currentMachine).getInteraction() != null) {
                                            Logger.getInstance("").log(LoggerTag.INFORMATION, "InteractionQueue_InteractionAdded: [" + currentMachine + "] current interaction still active - " +
                                                    sessionMap.get(currentMachine).getInteraction().getInteractionId(), "");
                                        } else {
                                            Logger.getInstance("").log(LoggerTag.INFORMATION, "InteractionQueue_InteractionAdded: interaction null add new call to [" + currentMachine + "] call interaction " +
                                                    "campaign - " + getInteractionAttribute(actualSessionId, e.getInteraction(), "is_attr_campaignid"), "");

                                            sess=sessionMap.get(currentMachine);
                                            sess.setIod(ConfigurationUtils.IOD_VALUE_4); //possible ivr
                                            sess.setInteraction(e.getInteraction());
                                            sessionMap.replace(currentMachine,sess);
                                        }
                                    }
                                }
                            }
                        } catch (Exception iex) {
                            Logger.getInstance("").log(LoggerTag.ERROR, "InteractionQueue_InteractionAdded: " + iex.getMessage(), "");
                        }
                    } else {
                    /* if( sessionMap.get(sesset.getMachineName()).getTransferToInteraction() != null)
                         Logger.getInstance("").log(LoggerTag.INFORMATION,"InteractionQueue_InteractionAdded: outbound transfer call already captured on [" + sesset.getMachineName() + "] " + sessionMap.get(sesset.getMachineName()).getTransferToInteraction().InteractionId );
                     else
                         Logger.getInstance("").log(LoggerTag.ERROR,"InteractionQueue_InteractionAdded: outbound transfer call doesn't exist on [" + sesset.getMachineName() + "] and isTransfer marked as true ");
                     */
                    }
                } else {
                    //may
                    Logger.getInstance("").log(LoggerTag.ERROR, "InteractionQueue_InteractionAdded: not in sessionMap [" + currentMachine + "]. Couldn't add outbound call " +
                            getInteractionAttribute(actualSessionId, e.getInteraction(), ConfigurationUtils.CALL_ID_KEY), "");
                }
            } else {
                //otherwise is the intercom
                try {
                    //sessionSettings = (SessionSettings)e.getInteraction().getController().getSession().getSessionInformation();
                    //sessionSettings = (SessionSettings)e.getSession().getSessionInformation();

                    if (sessionMap.containsKey(currentMachine)) {
                        if (!sessionMap.get(currentMachine).isCanDialOut()) {
                            Logger.getInstance("").log(LoggerTag.INFORMATION, "InteractionQueue_InteractionAdded: call on [" + currentMachine + "] [" +
                                    getInteractionAttribute(actualSessionId, e.getInteraction(), ConfigurationUtils.CALL_ID_KEY) +
                                    "] disconnect not from infinity and none dialer call.", "");

                            //e.getInteraction().getController().disconnectInteraction(sessionMap.get(sessionSettings.getMachineName()).getSession().getSessionId(),e.getInteraction().getInteractionId(),null);
                            cInteractionController.disconnectInteraction(sessionMap.get(currentMachine).getSession().getSessionId(), e.getInteraction().getInteractionId(), null);
                        } else {
                            if (sessionMap.get(currentMachine).getInteraction() == null ||
                                    !getInteractionAttribute(actualSessionId,sessionMap.get(currentMachine).getInteraction(),EIC_STATE).equals("C") ||
                                    getInteractionAttribute(actualSessionId,e.getInteraction(),EIC_STATE).equals("C")) {
                                Logger.getInstance("").log(LoggerTag.INFORMATION, "InteractionQueue_InteractionAdded: call [" + getInteractionAttribute(e.getSession().getSessionId(), e.getInteraction(), ConfigurationUtils.CALL_ID_KEY) +
                                        "] allowed and added to interaction ", "");

                                sess=sessionMap.get(currentMachine);
                                sess.setInteraction(e.getInteraction());
                                sess.setIod(2);
                                sessionMap.replace(currentMachine,sess);
                            }
                        }
                    }
                } catch (Exception iex) {
                    Logger.getInstance("").log(LoggerTag.ERROR, "InteractionQueue_InteractionAdded: disconnect [" + getInteractionAttribute(e.getSession().getSessionId(), e.getInteraction(), ConfigurationUtils.EIC_REMOTE_ADDRESS) + "] " +
                            getInteractionAttribute(actualSessionId, e.getInteraction(), ConfigurationUtils.CALL_ID_KEY) + iex.getMessage(), "");
                }
            }
        }
    }

    /**
     * This is the event handler related to interactionQueue_InteractionChanged;
     *
     * @param o
     * @param e
     */
    @Override
    public void interactionQueue_InteractionChanged(Object o, InteractionAttributesEventArgs e) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: interactionQueue_InteractionChanged ~~~~~~~~~~~~~~~~~~~~~~","");
        //interactionQueue_InteractionAdded(o,e);

        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setResponseType(WsResponseType.TYPE_EVENT);
        //we retrieve the sessionId and pass it to the clientCaller
        //String actualSessionId = e.getInteraction().getSessionId();
        //TO CHECK
        String actualSessionId = e.getSession().getSessionId();
        apiResponse.setComment("Interaction changed." + actualSessionId);
        apiResponse.setData(e.getMessage().toString());
        i3WService.getServerEventMessenger().sendMessage(apiResponse);


    }

    /**
     * This is the event related to interactionQueue_InteractionRemoved.
     *
     * @param sender
     * @param e
     */
    @Override
    public void interactionQueue_InteractionRemoved(Object sender, InteractionEventArgs e) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: interactionQueue_InteractionRemoved ~~~~~~~~~~~~~~~~~~~~~~","");


        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setResponseType(WsResponseType.TYPE_EVENT);
        //we retrieve the sessionId and pass it to the clientCaller
        //TO CHECK
        //String actualSessionId = e.getInteraction().getSessionId();
        String actualSessionId = e.getSession().getSessionId();
        apiResponse.setComment("Interaction removed." + actualSessionId);
        apiResponse.setData(e.getMessage().toString());
        i3WService.getServerEventMessenger().sendMessage(apiResponse);
        Session session = e.getSession();
        SessionInformation sessionSettings = session.getSessionInformation();
        if (sessionMap.containsKey(sessionSettings.getMachineName())) {
            //added the inbound interaction
            if ((sessionMap.get(sessionSettings.getMachineName())).getInbound() != null &&
                    (sessionMap.get(sessionSettings.getMachineName())).getInbound().getInteractionId().equals(e.getInteraction().getInteractionId())) {
                Logger.getInstance("").log(LoggerTag.INFORMATION, "InteractionRemoved: inbound call is - on [ " + sessionSettings.getMachineName() + "]", "");
                SessionInteraction ss= (sessionMap.get(sessionSettings.getMachineName()));
                ss.setInbound(null);
                sessionMap.replace(sessionSettings.getMachineName(),ss);
            } else if (sessionMap.get(sessionSettings.getMachineName()).getInteraction() != null &&
                    sessionMap.get(sessionSettings.getMachineName()).getInteraction().getInteractionId().equals(e.getInteraction().getInteractionId())) {
                //check out bound
                Logger.getInstance("").log(LoggerTag.INFORMATION, "InteractionRemoved: call is - on [ " + sessionSettings.getMachineName() + "]", "");
                SessionInteraction ss= (sessionMap.get(sessionSettings.getMachineName()));
                ss.setInteraction(null);
                sessionMap.replace(sessionSettings.getMachineName(),ss);
            } else if (sessionMap.get(sessionSettings.getMachineName()).getTransferToInteraction() != null &&
                    sessionMap.get(sessionSettings.getMachineName()).getTransferToInteraction().getInteractionId().equals(e.getInteraction().getInteractionId())) {
                Logger.getInstance("").log(LoggerTag.INFORMATION, "InteractionRemoved: TransferToCall is - on [ " + sessionSettings.getMachineName() + "]", "");
                SessionInteraction ss= (sessionMap.get(sessionSettings.getMachineName()));
                ss.setTransferToInteraction(null);
                sessionMap.replace(sessionSettings.getMachineName(),ss);
            } else {
                //try the dialer call interaction
                /*
                if (sessionMap.get(sesset.getMachineName()).getDialerCallInteraction() != null &&
                    sessionMap.get(sesset.getMachineName()).getDialerCallInteraction().InteractionId == e.getInteraction().getInteractionId())
                {
                    Logger.getInstance("").log(LoggerTag.INFORMATION,"InteractionRemoved: dailer call disconnect- on [" + sesset.getMachineName() + "] interactionId-" + e.getInteraction().getInteractionId());
                }
                else
                    Logger.getInstance("").log(LoggerTag.INFORMATION,"InteractionRemoved: not - on [" + sesset.getMachineName() + "] interactionId-" + e.getInteraction().getInteractionId());
                */
            }
        }


    }

    /**
     * This is the event handler related to connectCompleted
     *
     * @param o
     * @param e
     */
    @Override
    public void connectCompleted(Object o, AsyncCompletedEventArgs e) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: connectCompleted ~~~~~~~~~~~~~~~~~~~~~~","");

        if (e.getError() != null) {
            try {
                throw e.getError();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setResponseType(WsResponseType.TYPE_EVENT);
        //we retrieve the sessionId and pass it to the clientCaller
        //TO CHECK
        //String actualSessionId = e.getInteraction().getSessionId();
        Session session = e.getSession();

        apiResponse.setComment("Connection established." + session.getSessionId());
        apiResponse.setData(e.getMessage().toString());
        i3WService.getServerEventMessenger().sendMessage(apiResponse);


    }


    private boolean connectSession(Session session, CompositeType cParam) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: connectSession ~~~~~~~~~~~~~~~~~~~~~~","");

        Logger.getInstance("").log(LoggerTag.INFORMATION, " in ConnectSession", "");
        //create new session settings
        //SessionSettings sessionsettings = new SessionSettings();
        //sessionsettings.setClassOfService(ClassOfService.General);
        //sessionsettings.setDeviceType(DeviceType.Web);

        //sessionsettings.setApplicationName("I3WService");
        //sessionsettings.setApplicationName(ConfigurationUtils.DEFAULT_APPLICATION_NAME);
        //sessionsettings.setMachineName(cParam.getMachine());

        try {

            if (session != null && session.getConnectionSettings().getConnectionState() != ConnectionState.Up) {
                session.getConnection(cParam.toUser(), ConfigurationUtils.DEFAULT_LANGUAGE);
            }

            // if the user trying to connect to the server does NOT have role 29 (is not a manager), we do need make a station connection request to the server, otherwise no need make station connection
            if (!doesUserHaveManagerRole(cParam.getUserRoles())) {

                //create station settings
                //StationSettings stationSettings = new WorkstationSettings(cParam.getMachine(), SupportedMediaType.call);
                WorkstationSettings stationSettings = new WorkstationSettings();
                stationSettings.setWorkstation(cParam.getMachine());
                stationSettings.setSupportedMediaTypes(new SupportedMediaType[]{ SupportedMediaType.call });
                stationSettings.setReadyForInteractions(true);

                try {
                    ConnexionResponse resp = session.createOrChangeOrReplaceStation(session.getSessionId(), new Gson().toJson(stationSettings), "");
                    if (resp == null || resp.getRequestResponse().code() != 200) {
                        //We stop heartBeats for this user
                        try {
                            MessagingController msg = this.messagingList.get(session.getSessionId());
                            msg.setSwitchHeartBeat(false);
                            this.messagingList.replace(session.getSessionId(), msg);
                        } catch (Exception e) {
                            Logger.getInstance("").log(LoggerTag.ERROR, "Error stopping heartbeats on user  [ " + cParam.getUserID() + "] " + ExceptionUtils.exceptionStackTraceAsString(e), "");

                        }
                    }
                } catch (Exception exception) {
                    Logger.getInstance("").log(LoggerTag.ERROR, "ConnectSession  createOrChangeOrReplaceStation....Error -  [ " + cParam.getUserID() + "]  " + exception.getMessage(), "");
                }
            }

            session.setAutoReconnectEnable(true);
            session.setAutoReconnectInterval(30); //30 seconds
            //add watch
            //addInteractionQueue(session);
        } catch (Exception ex) {
            //Logger.getInstance("").log(LoggerTag.ERROR,"ConnectSession....Error - " + cparam.getMachine() + " " + cparam.Userid() + " " + ex.ToString());
            Logger.getInstance("").log(LoggerTag.ERROR, "ConnectSession....Error -  [ " + cParam.getUserID() + "]  " + ex.getMessage(), "");
            return false;
        }

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~END: connectSession ~~~~~~~~~~~~~~~~~~~~~~");

        return true;


    }
    /**
     * This method is used to get a session
     *
     * @param cParam
     * @return a session object
     */
    private Session getSession(CompositeType cParam) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: getSession ~~~~~~~~~~~~~~~~~~~~~~","");

        SessionInteraction sessionInteraction;
        String language = "en-us";


        if (sessionMap.containsKey(cParam.getMachine())) {
            Logger.getInstance("").log(LoggerTag.WARNING, " sessionMap.containsKey(cParam.getMachine(): " + cParam.getMachine(), "");
            sessionInteraction = sessionMap.get(cParam.getMachine());
            //session exist
            if (sessionInteraction.getSession() != null) {
                Logger.getInstance("").log(LoggerTag.WARNING, " sessionInteraction.getSession() != null ", "");
                //check if different user id request
                if (sessionInteraction.getSession().getConnexionResponse().getUserID() != null && !sessionInteraction.getSession().getConnexionResponse().getUserID().isEmpty()
                        && !sessionInteraction.getSession().getConnexionResponse().getUserID().toLowerCase().equals(cParam.getUserID().toLowerCase())) {

                    Session currentSession = sessionInteraction.getSession();

                    Logger.getInstance("").log(LoggerTag.WARNING, " sessionInteraction.getSession().getConnexionResponse().getUserID() != null && !sessionInteraction.getSession().getConnexionResponse().getUserID().isEmpty() && sessionInteraction.getSession().getConnexionResponse().getUserID().toLowerCase() != cparam.getUserID().toLowerCase() " + cParam.getMachine(), "");
                    //invalid the previous user id session like the same as in infinity
                    Logger.getInstance("").log(LoggerTag.INFORMATION, " getSession.... - exist userId [ " + sessionInteraction.getSession().getConnexionResponse().getUserID().toLowerCase() + "] session disconnect " + " new userId[" + cParam.getUserID().toLowerCase() + "] session on [" + cParam.getMachine() + "]", "");
                    try {
                        //logoutInvalidCall only remove the dialer related interaction using the Machine name
                        Logger.getInstance("").log(LoggerTag.WARNING, " TRYING ", "");
                        logoutInvalidCall(cParam); //invalid the previous different user on the same machine,so we could create the new session for the current user

                        if (activeUserList.contains(currentSession.getConnectionResponse().getUserID().toLowerCase())) {
                            activeUserList.remove(currentSession.getConnectionResponse().getUserID().toLowerCase());
                        }
                        //removing connection ends session
                        //sessionInteraction.getSession().Disconnect();
                        currentSession.removeStationConnection(currentSession.getSessionId(),language,cParam.toUser());

                        currentSession.removeConnection(currentSession.getSessionId(), language);
                    } catch (Exception ex) {
                        Logger.getInstance("").log(LoggerTag.ERROR, "getSession.... - disconnect exist userid [ " + currentSession.getConnectionResponse().getUserID().toLowerCase() + "] exception " + ex.getMessage(), "");
                    }
                    sessionInteraction.setSession(null);
                }
                //the same user id - no change
                Logger.getInstance("").log(LoggerTag.WARNING, "Same User ID-no change", "");
            }

            //if( sessionInteraction.getSession() == null) {
            if (sessionInteraction.getSession() == null) {
                Logger.getInstance("").log(LoggerTag.WARNING, "sessionInteraction is Null ", "");

                Session session = new Session();
                session.getConnection(cParam.toUser(), "");

                sessionInteraction.setSession(session);
                sessionInteraction.setDialerSession(null);
                sessionInteraction.setDialerCallInteraction(null);
            }
        } else {
            sessionInteraction = new SessionInteraction();
            Session session = new Session();
            session.getConnection(cParam.toUser(), "");
            sessionInteraction.setSession(session);
            sessionInteraction.setUser(cParam.toUser());
            sessionMap.put(cParam.getMachine(), sessionInteraction);
            Logger.getInstance("").log(LoggerTag.INFORMATION, " getSession...not in sessionmap put[ " + cParam.getUserID() + " ] on [" + cParam.getMachine() + "]", "");
        }

        Session session = sessionInteraction.getSession();

        if (session.getConnectionSettings().getConnectionState() == ConnectionState.Up) {
            if (!this.connectSession(session, cParam)) {
                Logger.getInstance("").log(LoggerTag.WARNING, " no ic session remove the user from the active list", "");
                //no ic session remove the user from the active list
                session = null;
                if (activeUserList.contains(cParam.getUserID().toLowerCase())) {
                    activeUserList.remove(cParam.getUserID().toLowerCase());
                }
            } else {
                //add the user to the active list
                Logger.getInstance("").log(LoggerTag.WARNING, " Adding user to list. ", "");
                if (!activeUserList.contains(cParam.getUserID().toLowerCase())) {
                    activeUserList.add(cParam.getUserID().toLowerCase());
                }

                sessionInteraction.setIod(0);
                sessionMap.replace(cParam.getMachine(),sessionInteraction);
            }
        }

        try {
            if (session != null) {
                //Logon without parameters
                //When a session connexion is initiated, also initialize messaging for events handling

                SessionInformation sessionInformation = new SessionInformation();
                sessionInformation.setMachineName(cParam.getMachine());
                session.setSessionInformation(sessionInformation);


                this.messagingList.put(session.getSessionId(),new MessagingController(session).setEventHandler(this));

                InteractionQueue currInteractionQueue = new InteractionQueue();

                QueueID queueID = new QueueID();
                queueID.setQueueType(QueueType.user);
                //queueID.setQueueType(QueueType.workgroup);
                queueID.setQueueName(cParam.getUserID());
                //queueID.setQueueName("");
                ArrayList<QueueID> listQueue = new ArrayList<>();
                listQueue.add(queueID);
                QueueSubscriptionParameters parameters = new QueueSubscriptionParameters();
                parameters.setQueueIds(listQueue);
                //parameters.setAttributeNames(new ArrayList<>());
                parameters.setAttributeNames(Arrays.asList("Eic_State"));
                InteractionQueueController controller = new InteractionQueueController(session);

                currInteractionQueue.setInteractionQueueController(controller);
                SessionInteraction cSessionInteraction = sessionMap.get(cParam.getMachine());
                cSessionInteraction.setInteractionQueue(currInteractionQueue);
                sessionMap.replace(cParam.getMachine(), cSessionInteraction);


                controller.createOrUpdateSubscriptionInteractionQueue(session.getSessionId(), cParam.getUserID(), parameters, "");

                DialerCallInteractionController dialerManager = new DialerCallInteractionController(session);
                dialerManager.getDialerSession().logon(null, "" + session.getSessionId(), this.getUser(cParam));


                //Try Set periodic deconnections

                try{

                    vertx.setPeriodic(480000,res->{

                        Logger.getInstance("").log(LoggerTag.INFORMATION,"=============== periodical checking ===================","");
                        this.sessionMap.forEach((s, sessionInteraction1) -> {
                            try {
                                Session curSess = sessionInteraction1.getSession();
                                ConnectionSettings sessInf = curSess.reconnectSession(curSess.getSessionId(),language);
                                if(!sessInf.getConnectionState().equals(ConnectionState.Up) && activeUserList.contains(sessionInteraction1.getUser().getUserID().toLowerCase())){
                                    CompositeType ctype = new CompositeType();
                                    ctype.setUserID(sessionInteraction1.getUser().getUserID());
                                    ctype.setPassword(sessionInteraction1.getUser().getPassWord());
                                    ctype.setMachine(s);
                                    ctype.setHost("http://fmars100:8018");
                                    logoff(ctype);

                                }
                            }catch (Exception e){
                                Logger.getInstance("").log(LoggerTag.ERROR,""+ExceptionUtils.exceptionStackTraceAsString(e),"");
                            }

                        });

                    });

                }catch (Exception ign){
                    ign.printStackTrace();
                }

            } else {
                Logger.getInstance("").log(LoggerTag.WARNING,"session is null","");
            }
        } catch (Exception e) {
            Logger.getInstance("").log(LoggerTag.ERROR, e.getMessage(), "");
        }

        return session;

    }

    /**
     * This method allows to get the current user status
     *
     * @param session
     * @return current user status as a string
     */
    private String getUserCurrentStatus(Session session, String userId) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: getUserCurrentStatus ~~~~~~~~~~~~~~~~~~~~~~","");

        //If the session is connected, get the status of the user in the session
        ConnectionSettings connectionSettings = session.reconnectSession(session.getSessionId(), "en-us");
        //if (session != null && session.getSessionsMessage().getConnectionState() == ConnectionState.Up) {
        /*if (session != null && connectionSettings.getConnectionState() == ConnectionState.Up) {
            return session.getConnectionSettings().getConnectionState().name();
            //Status statusMessage = new Status(session);
            //UserStatusUpdate userStatusUpdate = new UserStatusUpdate();
            //userStatusUpdate.setStatusId(callStatus);
            //userStatusUpdate.setUntil(null);
        } else {
            return "";
        }*/
        if (session != null && connectionSettings.getConnectionState() == ConnectionState.Up) {
            Status statusMessage = new Status(session);
            UserStatus status = statusMessage.getSpecifiedUserStatusByUserId(session.getSessionId(), userId);
            return status.getStatusId();
            //return session.getConnectionSettings().getConnectionState().name();
        } else {
            return "NOT CONNECTED";
        }
    }

    /**
     * This method allows to get the session and try to reconnect in case of session lost
     *
     * @param cParam
     * @return a long value
     */
    @Override
    public long getSessionManagerConnectedSession(CompositeType cParam) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: getSessionManagerConnectedSession ~~~~~~~~~~~~~~~~~~~~~~","");

        Logger.getInstance("").log(LoggerTag.WARNING, "getSessionManagerConnectedSession...CParam, [" + Json.encodePrettily(cParam) + "]", "");

        //make the machine key all lower case when added in the session map
        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty()) {
            return -1;
        }

        if (!sessionMap.containsKey(cParam.getMachine()) && activeUserList.contains(cParam.getUserID().toLowerCase())) {
            //don't allow ic session in this computer for the same user which the user active logged in from other computer in infinity
            Logger.getInstance("").log(LoggerTag.ERROR, "getSessionManagerConnectedSession.....couldn't create session, [" + cParam.getMachine() + "] user " + cParam.getUserID() + " active on other workstation ", "");

            return -2;
        }

        Session session = getSession(cParam);
        ConnectionSettings connectionSettings = session.reconnectSession(session.getSessionId(), "en-us");

        if (session != null && connectionSettings.getConnectionState() == ConnectionState.Up) {
            if ((cParam.getNoRestrict() != null) && ("yes".equals(cParam.getNoRestrict()))) {
                sessionMap.get(cParam.getMachine()).setCanDialOut(true);
            } else {
                sessionMap.get(cParam.getMachine()).setCanDialOut(false);
            }

            if (cParam.getI3Status() != null && ("yes".equals(cParam.getI3Status()))) {
                this.updateUserCallingStatus(session, cParam.getUserID(),ConfigurationUtils.I3STATUS_AVAILABLE);
                sessionMap.get(cParam.getMachine()).setUpdateI3Status(true);
            } else {
                sessionMap.get(cParam.getMachine()).setUpdateI3Status(false);
            }
            cParam.setSessionID(Long.valueOf(session.getSessionId()));

        } else {
            cParam.setSessionID(0);
            sessionMap.remove(cParam.getMachine());
            Logger.getInstance("").log(LoggerTag.ERROR, "getSessionManagerConnectedSession.....removed [" + cParam.getMachine() + "] [" + cParam.getUserID() + "] couldn't create active session or session state is down.", "");
        }

        return cParam.getSessionID();
    }

    /**
     * This method returns an array of available campaign
     *
     * @param cParam
     * @return an array of string
     */
    @Override
    public String[] getAvailableCampaign(CompositeType cParam) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: getAvailableCampaign ~~~~~~~~~~~~~~~~~~~~~~","");

        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty()) {
            Logger.getInstance("").log(LoggerTag.ERROR, "Null 1", "");
            return null;
        }

        if (!sessionMap.containsKey(cParam.getMachine())) {
            this.getSession(cParam);
        }

        Session session = sessionMap.get(cParam.getMachine()).getSession();
        if (session != null) {
            try {


                //DialerCallInteractionController dialerManager = new DialerCallInteractionController(session);
                ArrayList<String> cNames =new ArrayList<>();
                List<AvailableCampaign> campaigns = this.sessionsAvailableCapaigns.get(""+session.getSessionId());
                if (campaigns!=null)
                    for (AvailableCampaign c : campaigns) {
                        if(c!=null)
                            cNames.add(c.getName());
                    }
                /*else{

                    //Attempt to relog to Dialer for get availableCampaignsMessage
                    try{
                        DialerCallInteractionController dialerManager = new DialerCallInteractionController(session);
                        dialerManager.getDialerSession().logon(null, "" + session.getSessionId(), this.getUser(cParam));
                        Thread.sleep(2000);
                        //vertx.wait(2000);
                        campaigns = this.sessionsAvailableCapaigns.get(""+session.getSessionId());
                        if (campaigns!=null)
                            for (AvailableCampaign c : campaigns) {
                                if(c!=null)
                                    cNames.add(c.getName());
                            }

                    }catch (Exception e){
                        e.printStackTrace();
                    }


                }*/

                Logger.getInstance("").log(LoggerTag.INFORMATION, "Avalaible campaigns { \n" + Json.encodePrettily(campaigns) + "\n } for session : " + session.getSessionId(), "");

                String[] workFlows = cNames.toArray(new String[cNames.size()]);
                if (workFlows.length == 0 || workFlows[0] == null) {
                    return new String[]{""};
                } else {
                    return workFlows;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                Logger.getInstance("").log(LoggerTag.ERROR, "getAvailableCampaigns exception [" + cParam.getMachine() + "] " + ex.getMessage(), "");
                return new String[]{""};
            }
        } else {
            Logger.getInstance("").log(LoggerTag.ERROR, "Null 2", "");
            return new String[]{""};
        }
    }

    /**
     * This method is called to logon to campaign
     *
     * @param cParam
     * @return an integer
     */
    @Override
    public int logonCampaign(CompositeType cParam) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: logonCampaign ~~~~~~~~~~~~~~~~~~~~~~","");

        String currentCampaignId="";
        for (AvailableCampaign c : this.sessionsAvailableCapaigns.get(sessionMap.get(cParam.getMachine()).getSession().getSessionId())) {
            if(c.getName().equals(cParam.getCampaign()))
                currentCampaignId=c.getId();
        }

        int returnedValue = -1;
        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty() || cParam.getCampaign().isEmpty() || currentCampaignId.isEmpty()) {
            return returnedValue;
        }

        if (!sessionMap.containsKey(cParam.getMachine())) {
            getSession(cParam);
        }

        Session session = sessionMap.get(cParam.getMachine()).getSession();
        try {
            if (sessionMap.get(cParam.getMachine()).getDialerCallInteraction() != null) {
                //when the user accidently closed the browser, they can still pickup to finish the same call
                Logger.getInstance("").log(LoggerTag.ERROR, "logonCampaign....exited and relog on [" + cParam.getMachine() + "] Session id " + session.getSessionId() + " continue campaign", "");
                return 0;
            }
            if (sessionMap.get(cParam.getMachine()).getDialerSession() != null) {
                Logger.getInstance("").log(LoggerTag.ERROR, "logonCampaign....[" + cParam.getMachine() + "] No dialer call interaction,but exist dialer session ", "");
            }
            DialerCallInteractionController dialerManager = new DialerCallInteractionController(session);
            this.updateUserCallingStatus(session, cParam.getUserID(), ConfigurationUtils.I3STATUS_AVAILABLE);
            sessionMap.get(cParam.getMachine()).setMarkedComplete(true);
            DialerSession cDialerSessionController = dialerManager.getDialerSession();

            LogonResult result = dialerManager.getDialerSession().logon(new LogInParameters(currentCampaignId), "" + cParam.getSessionID(), this.getUser(cParam));
            DialerSession dialerSession = result.getDialerSession();

            if (dialerSession != null) {
                dialerSession.setEventHandler(this);
                Logger.getInstance("").log(LoggerTag.INFORMATION, "logonCampaign successful " + cParam.getCampaign() + " Session id " + session.getSessionId(), "");
                //dialerSession.StartReceivingCalls();
                DialerCallInteractionController dialerCallInteractionController = new DialerCallInteractionController(session);
                ArrayList<String> campaignIds = new ArrayList<>();
                campaignIds.add(currentCampaignId);
                dialerCallInteractionController.readyForCalls(session.getSessionId(), campaignIds, "en-us", this.getUser(cParam));

                //created the successful dialer user session
                returnedValue = 0;
            } else {
                Logger.getInstance("").log(LoggerTag.ERROR, "logonCampaign failed [" + cParam.getMachine() + "] " + cParam.getCampaign() + " Session id " + session.getSessionId(), "");
                returnedValue = 1;
            }
        } catch (Exception ex) {
            Logger.getInstance("").log(LoggerTag.ERROR, "logonCampaign exception [" + cParam.getMachine() + "] " + ex.toString(), "");
            returnedValue = -1;
        }
        return returnedValue;
    }

    /**
     * This is the log off method. This is used to disconnect a session
     *
     * @param cParam
     * @return an integer value
     */
    @Override
    public int logoff(CompositeType cParam) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: logoff ~~~~~~~~~~~~~~~~~~~~~~","");

        int returnedValue = -1;
        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty()) {
            return returnedValue;
        }

        if (sessionMap.containsKey(cParam.getMachine())) {
            //sessionInteraction exist
            Session sess = sessionMap.get(cParam.getMachine()).getSession();
            if (sess != null) {
                try {
                    this.logoutInvalidCall(cParam);
                    if (cParam.getUserID().equals("any") && activeUserList.contains(sess.getConnectionResponse().getUserID().toLowerCase())) {
                        activeUserList.remove(sess.getConnectionResponse().getUserID().toLowerCase());
                    }
                    if (sessionMap.get(cParam.getMachine()).isUpdateI3Status()) {
                        this.updateUserCallingStatus(sess,cParam.getUserID(),ConfigurationUtils.I3STATUS_GONE_HOME);
                    }

                    try{
                        this.messagingList.get(sess.getSessionId()).setSwitchHeartBeat(false);
                        this.messagingList.remove(sess.getSessionId());

                    }catch (Exception e){
                        Logger.getInstance("").log(LoggerTag.WARNING, "Error removing messaging on user  [ " + cParam.getUserID() + "] " + ExceptionUtils.exceptionStackTraceAsString(e), "");
                    }


                    try {
                        sess.removeStationConnection(sess.getSessionId(),"",cParam.toUser());
                    } catch (Exception exp) {
                        Logger.getInstance("").log(LoggerTag.WARNING, "logoff..... exception removing station connection " + ExceptionUtils.exceptionStackTraceAsString(exp), "");
                    }

                    try {
                        sess.removeConnection(sess.getSessionId(), "");
                    } catch (Exception exp) {
                        Logger.getInstance("").log(LoggerTag.WARNING, "logoff..... exception removing connection " + ExceptionUtils.exceptionStackTraceAsString(exp), "");
                    }


                    sess = null;
                } catch (Exception ex) {
                    Logger.getInstance("").log(LoggerTag.WARNING, "logoff..... exception " + ex.getMessage(), "");
                }

                returnedValue = 0;
            } else {
                returnedValue = 1;
            }

            if ((sessionMap.get(cParam.getMachine()).getInteractionQueue()) != null && (sess != null)) {
                try {
                    //sessionMap.get(cParam.getMachine()).getInteractionQueue().stopWatching();
                    sessionMap.get(cParam.getMachine()).getInteractionQueue().getInteractionQueueController().deleteInteractionQueueBySubscriptionId(sess.getSessionId(),cParam.getUserID());
                }
                catch (Exception ex) {
                    Logger.getInstance("").log(LoggerTag.ERROR,"logoff... [" + cParam.getMachine() + "] stopwatch queue exception " + ex.getMessage(),"");
                }
                sessionMap.get(cParam.getMachine()).setInteractionQueue(null);
            }

            //remove the user from session map
            sessionMap.remove(cParam.getMachine());

            //remove the user id from the active list
            if (activeUserList.contains(cParam.getUserID().toLowerCase())) {
                activeUserList.remove(cParam.getUserID().toLowerCase());
            }
        }
        return returnedValue;
    }


    private void logoff(Session sess) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: logoff ~~~~~~~~~~~~~~~~~~~~~~","");

        if (sess==null) {
            return ;
        }

        final SessionInteraction[] curSS = {null};
        final String[] curMachine = {null};
        Session finalSess = sess;
        String userId=sess.getConnectionResponse().getUserID();
        sessionMap.forEach((s, sessionInteraction) -> {
            if(sessionInteraction.getSession().equals(finalSess))
                curSS[0] =sessionInteraction;
            curMachine[0] =s;
        });

        if (curSS[0]!=null && curMachine[0] !=null) {
            //sessionInteraction exist

            try {
                //this.logoutInvalidCall(cParam);
                if (activeUserList.contains(userId.toLowerCase())) {
                    activeUserList.remove(userId.toLowerCase());
                }
                if (sessionMap.get(userId).isUpdateI3Status()) {
                    this.updateUserCallingStatus(sess,userId,ConfigurationUtils.I3STATUS_GONE_HOME);
                }

                try{
                    this.messagingList.replace(sess.getSessionId(),null);
                    this.messagingList.remove(sess.getSessionId());

                }catch (Exception e){
                    Logger.getInstance("").log(LoggerTag.WARNING, "Error removing messaging on user  [ " + userId+ "] " + ExceptionUtils.exceptionStackTraceAsString(e), "");

                }


                try {
                    sess.removeStationConnection(sess.getSessionId(),"",null);
                } catch (Exception exp) {
                    Logger.getInstance("").log(LoggerTag.WARNING, "logoff..... exception removing station connection " + ExceptionUtils.exceptionStackTraceAsString(exp), "");

                }

                try {
                    sess.removeConnection(sess.getSessionId(), "");
                } catch (Exception exp) {
                    Logger.getInstance("").log(LoggerTag.WARNING, "logoff..... exception removing connection " + ExceptionUtils.exceptionStackTraceAsString(exp), "");

                }




                sess = null;
            } catch (Exception ex) {
                Logger.getInstance("").log(LoggerTag.WARNING, "logoff..... exception " + ex.getMessage(), "");
            }


            if ((curSS[0].getInteractionQueue()) != null && (sess != null)) {
                try {
                    //sessionMap.get(cParam.getMachine()).getInteractionQueue().stopWatching();
                    sessionMap.get(curMachine[0]).getInteractionQueue().getInteractionQueueController().deleteInteractionQueueBySubscriptionId(sess.getSessionId(),userId);
                }
                catch (Exception ex) {
                    Logger.getInstance("").log(LoggerTag.ERROR,"logoff... [" + curMachine[0] + "] stopwatch queue exception " + ex.getMessage(),"");
                }
                sessionMap.get(curMachine[0]).setInteractionQueue(null);
            }

            //remove the user from session map
            sessionMap.remove(curMachine[0]);

            //remove the user id from the active list
            if (activeUserList.contains(userId.toLowerCase())) {
                activeUserList.remove(userId.toLowerCase());
            }
        }
    }

    /**
     * This is the method called to mark a dialer call complete method.
     *
     * @param cParam
     * @return boolean value
     */
    @Override
    public boolean markDialerCallComplete(CompositeType cParam) {
        //function called to disposition the dialer call interaction to get the next call interaction
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: markDialerCallComplete ~~~~~~~~~~~~~~~~~~~~~~","");


        if (sessionMap.containsKey(cParam.getMachine())) {

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

            if (sessionMap.get(cParam.getMachine()).getDialerCallInteraction() != null) {
                try {
                    DialerCallInteraction dialerCallInteraction = sessionMap.get(cParam.getMachine()).getDialerCallInteraction();
                    if (!sessionMap.get(cParam.getMachine()).isMarkedComplete()) {
                        SessionInteraction cSessionInteraction = sessionMap.get(cParam.getMachine());
                        Session sessionCurrent = cSessionInteraction.getSession();

                        //sessionMap.get(cParam.getMachine()).setMarkedComplete(true);
                        cSessionInteraction.setMarkedComplete(true);
                        sessionMap.replace(cParam.getMachine(),cSessionInteraction);
                        Logger.getInstance("").log(LoggerTag.INFORMATION, "markDialerCallComplete......[" + cParam.getMachine() + "] dialer call " + dialerCallInteraction.getInteractionId() +
                                " mode " + dialerCallInteraction.getDialingMode(), "");

                        DialerCallInteractionController cDialerCallInteractionController = dialerCallInteraction.getDialerCallInteractionController(sessionCurrent);

                        DispositionParameters dispositionParameters = new DispositionParameters();
                        if (dialerCallInteraction.getDialingMode() == DialingMode.Preview) {
                            dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_SKIPPED);
                            //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                            cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                            //Tracing.TraceAlways("markDialerCallComplete......preview call disposition");
                        } else {
                            switch (cParam.getResult()) {
                                case ConfigurationUtils.FAILURE_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_FAILURE);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.WRONG_PARTY_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_WRONGPARTY);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.SUCCESS_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_SUCCESS);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.MACHINE_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_MACHINE);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.NO_ANSWER_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_NOANSWER);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.REMOTE_HANGUP_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_REMOTEHANGUP);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.TRANSFERRED_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_TRANSFERRED);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.SKIPPED_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_SKIPPED);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.PHONE_NUMBER_SUCCESS_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_PHONENUMBERSUCCESS);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.PHONE_NUMBER_DELETED_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_PHONENUMBERDELETED);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.FAX_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_FAX);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.SCHEDULED_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_SCHEDULED);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.BUSY_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_BUSY);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.SIT_UNCALLABLE_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_SITUNCALLABLE);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.SIT_CALLABLE_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_SITCALLABLE);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.NO_LINES_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_NOLINES);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.DELETED_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_DELETED);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                case ConfigurationUtils.SIT_LABEL:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_SIT);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                                default:
                                    dispositionParameters.setLabel(ConfigurationUtils.DISPOSITION_CALL_PARAM_AMBIGIOUS);
                                    //dialerCallInteraction.getDialerCallInteractionController().disposition(Long.toString(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    cDialerCallInteractionController.disposition(sessionCurrent.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                                    break;
                            }
                            //Tracing.TraceAlways("markDialerCallComplete......regular call disposition");
                        }
                    } //else the call already marked complete
                    return true;
                } catch (Exception e) {
                    Logger.getInstance("").log(LoggerTag.ERROR, "markDialerCallComplete...[" + cParam.getMachine() + "] mark call completion exception " + e.getMessage(), "");
                }
            } else {
                Logger.getInstance("").log(LoggerTag.INFORMATION, "markDialerCallComplete.....[" + cParam.getMachine() + "] Dialer call interaction is null", "");
                sessionMap.get(cParam.getMachine()).setMarkedComplete(true);
            }
        }
        Logger.getInstance("").log(LoggerTag.INFORMATION, "SessionMap may be empty", "");
        return false;
    }

    /**
     * This method allows to build a user object from cParam object
     *
     * @param cParam
     * @return user object
     */
    private User getUser(CompositeType cParam) {
        User user = new User();
        user.setUserID(cParam.getUserID());
        user.setPassWord(cParam.getPassword());
        user.setApplicationName(ConfigurationUtils.APPLICATION_NAME);

        return user;
    }

    /**
     * This method is called when receiving a new dialer call
     *
     * @param cParam
     * @return an integer
     */
    @Override
    public int receiveNewDialerCall(CompositeType cParam) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: receiveNewDialerCall ~~~~~~~~~~~~~~~~~~~~~~","");

        if ((sessionMap.containsKey(cParam.getMachine())) && (sessionMap.get(cParam.getMachine()).getSession() != null)) {

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

            this.updateUserCallingStatus(sessionMap.get(cParam.getMachine()).getSession(),cParam.getUserID(), ConfigurationUtils.I3STATUS_AVAILABLE);

            if (sessionMap.get(cParam.getMachine()).getDialerSession() != null) {
                SessionInteraction cSessionInteraction = sessionMap.get(cParam.getMachine());

                if (cSessionInteraction.isMarkedComplete()) {
                    DialerCallInteractionController dialerCallInteractionController = new DialerCallInteractionController(cSessionInteraction.getSession());
                    dialerCallInteractionController.setDialerSession(sessionMap.get(cParam.getMachine()).getDialerSession());
                    //On ICWS web site, Once an agent has logged in to one or more campaigns, the agent does not immediately start receiving calls. Calling ready-for-calls indicates that the agent is ready to receive calls
                    dialerCallInteractionController.readyForCalls(sessionMap.get(cParam.getMachine()).getSession().getSessionId(), null, "", cParam.toUser());
                    //Logger.getInstance("").log(LoggerTag.INFORMATION,"receiveNewDialerCall.....start new dialer call interaction ","");
                    return 0;
                } else {
                    Logger.getInstance("").log(LoggerTag.INFORMATION, "receiveNewDialerCall.....[" + cParam.getMachine() + "] previous dialer call haven't marked complete! ", "");
                }
            } else {
                return -1;
            }
        }
        return -2;
    }

    /**
     * The information about the logged user session for admin purpose
     *
     * @return users information as an array
     */
    @Override
    public String[] getUsersInfo() {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: getUsersInfo ~~~~~~~~~~~~~~~~~~~~~~","");

        List<String> users = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry<String, SessionInteraction> entry : sessionMap.entrySet()) {
            String key = entry.getKey();
            SessionInteraction value = entry.getValue();

            stringBuilder.append("Workstation:").append(key).append(" User:" + value.getSession().getConnectionResponse().getUserID());
            //session info
            if (value.getSession() != null) {
                stringBuilder.append(" Session:").append(value.getSession().getSessionId());
            } else {
                stringBuilder.append(" Session:-1");
            }
            //dialer Campaign
            if (value.getDialerSession() != null) {
                stringBuilder.append(" Campaign:" + value.getDialerSession().getCampaignId());
            } else {
                stringBuilder.append(" Campaign:");
            }
            users.add(stringBuilder.toString());
        }

        return users.toArray(new String[users.size()]);
    }

    /**
     * This method allows to logout connected user
     *
     * @param cParam
     * @return boolean value
     */
    @Override
    public boolean adminLogoutUser(CompositeType cParam) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: adminLogoutUser ~~~~~~~~~~~~~~~~~~~~~~","");

        if (cParam == null) {
            return false;
        }
        //add tracing log info
        Logger.getInstance("").log(LoggerTag.INFORMATION, "adminLogoutUser...............................host-[" + cParam.getMachine() + "] userid-" + cParam.getUserID(), "");

        //remove the user from active list and check if session exist on workstation
        if (("".equals(cParam.getMachine())) && (!"".equals(cParam.getUserID()))) {

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));


            if (activeUserList.contains(cParam.getUserID().toLowerCase())) {
                for (Map.Entry<String, SessionInteraction> entry : sessionMap.entrySet()) {
                    String key = entry.getKey();
                    SessionInteraction value = entry.getValue();

                    if (value.getSession() != null && value.getSession().getConnectionResponse().getUserID().toLowerCase().equals(cParam.getUserID().toLowerCase())) {
                        cParam.setMachine(key);
                        return logoff(cParam) >= 0;
                    }
                }
                activeUserList.remove(cParam.getUserID().toLowerCase());
                return true; //removed successful
            } else {
                return false;
            }
        }

        if ("".equals(cParam.getUserID())) {
            cParam.setUserID("any");
        }

        return logoff(cParam) >= 0;
    }

    /**
     * This function returns the Active Users List
     *
     * @return active users list in an array
     */
    @Override
    public String[] showUsers() {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: showUsers ~~~~~~~~~~~~~~~~~~~~~~","");
        return activeUserList.toArray(new String[activeUserList.size()]);
    }

    /**
     * This method is called to get an incoming call information
     *
     * @param cParam
     * @return an array of string
     */
    @Override
    public String[] getIncomingCallInfo(CompositeType cParam) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: getIncomingCallInfo ~~~~~~~~~~~~~~~~~~~~~~","");

        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty()) {
            return null;
        }

        String[] ret = new String[5];
        ret[0] = "";
        ret[1] = "";
        ret[2] = ""; //for distinguish the inbound and outbound routed
        ret[3] = ""; //interaction id
        ret[4] = ""; //reserved

        //sessionInteraction exist
        if (sessionMap.containsKey(cParam.getMachine())) {

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

            SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());
            Interaction currentInteraction = currentSessionInteraction.getInteraction();
            Session cSession = currentSessionInteraction.getSession();

            if (currentSessionInteraction.getIod() != ConfigurationUtils.IOD_VALUE_2 && currentSessionInteraction.getIod() != ConfigurationUtils.IOD_VALUE_3 && currentInteraction != null) {
                InteractionController cInteractionController = currentInteraction.getController(cSession);

                try {
                    if (getInteractionAttribute(cSession.getSessionId(),currentInteraction,EIC_STATE).equals("A")
                            ||getInteractionAttribute(cSession.getSessionId(),currentInteraction,EIC_STATE).equals("X")
                            || getInteractionAttribute(cSession.getSessionId(),currentInteraction,EIC_STATE).equals("R")
                            || getInteractionAttribute(cSession.getSessionId(),currentInteraction,EIC_STATE).equals("H")
                            || getInteractionAttribute(cSession.getSessionId(),currentInteraction,EIC_STATE).equals("O")
                            || getInteractionAttribute(cSession.getSessionId(),currentInteraction,EIC_STATE).equals("P")) {
                        try {
                            //currentInteraction.getController().pickUpInteraction(currentSessionInteraction.getSession().getSessionId(),currentInteraction.getInteractionId(),cParam.toUser());
                            cInteractionController.pickUpInteraction(cSession.getSessionId(), currentInteraction.getInteractionId(), cParam.toUser());
                        } catch (Exception icx) {
                            Logger.getInstance("").log(LoggerTag.ERROR, "getIncomingCallInfo exception outbound from [" + cParam.getMachine() + "] pick up " + " callstate " +
                                    getInteractionAttribute(cSession.getSessionId(),currentInteraction,EIC_STATE) + " " + icx.getMessage(), "");
                        }
                        //mark all of the agentless call complete to Success
                        //String campaign = getInteractionAttribute(currentInteraction.getController().getSession().getSessionId(),currentInteraction,"is_attr_campaignid");
                        String campaign = getInteractionAttribute(cSession.getSessionId(), currentInteraction, "is_attr_campaignid");
                        if (campaign == null || campaign.isEmpty()) {
                            //skip the mark of call complete for no campaign
                            Logger.getInstance("").log(LoggerTag.INFORMATION, "getIncomingCallInfo......outbound pickup no call complete [" + cParam.getMachine() + "] " + " [" + cParam.getUserID() + "] " +
                                    " campaign " + campaign + " interactionId " + currentInteraction.getInteractionId(), "");
                        } else {
                            DialerCallInteractionController dialerManager = new DialerCallInteractionController(cSession);
                            //dialerManager.setDialerSession((DialerSession)currentSessionInteraction.getSession());
                            //dialerManager.setDialerSession((DialerSession) cSession);
                            //dialerManager.setDialerSession(currentSessionInteraction.getDialerSession());

                            //For this we need to call: DiallerCallInteractionController.disposition method
                            // String sessionId & DispositionParameters (label, callAttributes) are required
                            // "label" is The label of the disposition choice. For a previewPop with an allowSkip value of true, pass in a label of "Skipped - Agent Skip" to skip the call.
                            // "callAttributes" is a collection of call contact attributes and their corresponding values.
                            //dialerManager.AgentlessCallComplete(new AgentlessCallCompletionParameters(campaign, currentInteraction.getInteractionId(),  "Success"));

                            Map<String, String> callAttributes = new HashMap<>();
                            callAttributes.put(ConfigurationUtils.IS_ATTR_CAMPAIGN_ID, campaign);
                            DispositionParameters dispositionParameters = new DispositionParameters(ConfigurationUtils.SUCCESS_LABEL, callAttributes);
                            User user = new User(ConfigurationUtils.DEFAULT_APPLICATION_NAME, cParam.getUserID(), cParam.getPassword());

                            dialerManager.disposition(cSession.getSessionId(), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);

                            Logger.getInstance("").log(LoggerTag.INFORMATION, "getIncomingCallInfo......outbound pickup on [" + cParam.getMachine() + "] " + " [" + cParam.getUserID() + "] " +
                                    " campaign " + campaign + " interactionId " + currentInteraction.getInteractionId(), "");
                        }
                    } else {


                        Logger.getInstance("").log(LoggerTag.INFORMATION, "getIncomingCallInfo......outbound on [" + cParam.getMachine() + "] " + " [" + cParam.getUserID() + "] "
                                + " callstate " + getInteractionAttribute(cSession.getSessionId(),currentInteraction,EIC_STATE), "");
                    }

                    //for IVR, try to retrieve the debtor id here
                    try {
                        //ret[0] = getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(),currentInteraction,ConfigurationUtils.EIC_REMOTE_ADDRESS);
                        ret[0] = getInteractionAttribute(cSession.getSessionId(), currentInteraction, ConfigurationUtils.EIC_REMOTE_ADDRESS);
                    } catch (Exception icx) {
                        Logger.getInstance("").log(LoggerTag.ERROR, "getIncomingCallInfo exception from [" + cParam.getMachine() + "] retrieve phone number " + icx.getMessage(), "");
                    }

                    try {
                        //ret[4] = getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(),currentInteraction,ConfigurationUtils.IS_ATTR_DEBTOR_ID);
                        ret[4] = getInteractionAttribute(cSession.getSessionId(), currentInteraction, IS_ATTR_DEBTOR_ID);
                        //We can send debtor_id here to PeriodicActionsVerticle
                        if (!ret[4].isEmpty())
                            vertx.eventBus().send("calls_getting", currentInteraction.getInteractionId() + ":" + ret[4]);

                    } catch (Exception icx) {
                        Logger.getInstance("").log(LoggerTag.ERROR, "getIncomingCallInfo exception from [" + cParam.getMachine() + "] retrieve debtorid for phone " + ret[0] + " " + icx.getMessage(), "");
                    }

                    try {
                        ret[3] = currentInteraction.getInteractionId();
                        //ret[2] = getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(),currentInteraction,ConfigurationUtils.EIC_CALL_DIRECTION);
                        ret[2] = getInteractionAttribute(cSession.getSessionId(), currentInteraction, ConfigurationUtils.EIC_CALL_DIRECTION);
                        //ret[1] = getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(),currentInteraction,ConfigurationUtils.CALL_ID_KEY);
                        ret[1] = getInteractionAttribute(cSession.getSessionId(), currentInteraction, ConfigurationUtils.CALL_ID_KEY);
                    } catch (Exception icx) {
                        Logger.getInstance("").log(LoggerTag.ERROR, "getIncomingCallInfo exception from [" + cParam.getMachine() + "] retrieve callid " + ret[1] + " " + icx.getMessage(), "");
                    }
                } catch (Exception ex) {
                    Logger.getInstance("").log(LoggerTag.ERROR, "getIncomingCallInfo exception outbound from [" + cParam.getMachine() + "] " + " [" + cParam.getUserID() + "]" + ex.getMessage(), "");
                }
            }

            if (currentSessionInteraction.getInbound() != null && (currentInteraction == null || (currentInteraction != null && !getInteractionAttribute(cSession.getSessionId(),currentInteraction,EIC_STATE).equals("C")))) {
                try {
                    Interaction currentInboundInteraction = currentSessionInteraction.getInbound();
                    ret[3] = currentInboundInteraction.getInteractionId();
                    //ret[0] = getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(),currentInboundInteraction,ConfigurationUtils.EIC_REMOTE_ADDRESS);
                    ret[0] = getInteractionAttribute(cSession.getSessionId(), currentInboundInteraction, ConfigurationUtils.EIC_REMOTE_ADDRESS);
                    //ret[1] = getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(),currentInboundInteraction,ConfigurationUtils.CALL_ID_KEY);
                    ret[1] = getInteractionAttribute(cSession.getSessionId(), currentInboundInteraction, ConfigurationUtils.CALL_ID_KEY);
                    //ret[2] = getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(),currentInboundInteraction,ConfigurationUtils.EIC_CALL_DIRECTION);
                    ret[2] = getInteractionAttribute(cSession.getSessionId(), currentInboundInteraction, ConfigurationUtils.EIC_CALL_DIRECTION);

                    try {
                        //ret[4] = getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(),currentInteraction,ConfigurationUtils.IS_ATTR_DEBTOR_ID);
                        ret[4] = getInteractionAttribute(cSession.getSessionId(), currentInboundInteraction, IS_ATTR_DEBTOR_ID);
                        //We can send debtor_id here to PeriodicActionsVerticle
                        if (!ret[4].isEmpty())
                            vertx.eventBus().send("calls_getting", currentInboundInteraction.getInteractionId() + ":" + ret[4]);

                    } catch (Exception icx) {
                        Logger.getInstance("").log(LoggerTag.ERROR, "getIncomingCallInfo exception from [" + cParam.getMachine() + "] retrieve debtorid for phone " + ret[0] + " " + icx.getMessage(), "");
                    }

                    String currentInteractionState = getInteractionAttribute(cSession.getSessionId(),currentInboundInteraction,EIC_STATE);

                    if (currentInteractionState.equals("A") || currentInteractionState.equals("X") ||
                            currentInteractionState.equals("R") || currentInteractionState.equals("H") ||
                            currentInteractionState.equals("O") || currentInteractionState.equals("P")) {
                        Logger.getInstance("").log(LoggerTag.INFORMATION, "getIncomingCallInfo......inbound pickup on [" + cParam.getMachine() + "] " + " [" + cParam.getUserID() + "] phone " + ret[0] +
                                " callid " + ret[1] + " callstate " + currentInteractionState, "");

                        //currentInboundInteraction.getController().pickUpInteraction(currentSessionInteraction.getSession().getSessionId(),currentInboundInteraction.getInteractionId(),cParam.toUser());
                        InteractionController cInboundInteractionController = currentInboundInteraction.getController(cSession);
                        cInboundInteractionController.pickUpInteraction(currentSessionInteraction.getSession().getSessionId(), currentInboundInteraction.getInteractionId(), cParam.toUser());
                    } else {
                        Logger.getInstance("").log(LoggerTag.INFORMATION, "getIncomingCallInfo......inbound on [" + cParam.getMachine() + "] " + " [" + cParam.getUserID() + "] phone " + ret[0] +
                                " callid " + ret[1] + " callstate " + currentInteractionState, "");
                    }
                } catch (Exception ex) {
                    Logger.getInstance("").log(LoggerTag.ERROR, "getIncomingCallInfo exception inbound from [" + cParam.getMachine() + "] " + " [" + cParam.getUserID() + "]" + ex.getMessage(), "");
                }
            } else {

            }
        }
        return ret;
    }

    /**
     * This method is called to begin audio play
     *
     * @param cParam
     * @return an integer
     */
    @Override
    public int beginPlayAudio(CompositeType cParam) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: beginPlayAudio ~~~~~~~~~~~~~~~~~~~~~~","");

        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty() || cParam.getDisclosureID().isEmpty()) {
            return 1;
        }

        if (sessionMap.containsKey(cParam.getMachine())) {

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

            Session session = sessionMap.get(cParam.getMachine()).getSession();
            if (session.getConnectionSettings().getConnectionState() == ConnectionState.Up) {
                try {
                    if ((sessionMap.get(cParam.getMachine()).getInbound() != null) && (
                            getInteractionAttribute(session.getSessionId(),sessionMap.get(cParam.getMachine()).getInbound(),EIC_STATE).equals("C"))) {
                        //check inbound
                        if ((sessionMap.get(cParam.getMachine()).getCallInteraction() != null) &&
                                (sessionMap.get(cParam.getMachine()).getCallInteraction().getInteractionId().equals(sessionMap.get(cParam.getMachine()).getInbound().getInteractionId()))) {
                            // PathPlayWave audio: \\\\fmars64\\InfinityShare\\productionResources\\audio\\
                            /*sessionMap.get(cParam.getMachine()).getCallInteraction().getCallInteractionController().playWav(""+cParam.getSessionID(),
                                    sessionMap.get(cParam.getMachine()).getInbound().getInteractionId(),
                                    ConfigurationUtils.PATH_PLAY_WAVE_AUDIO + cParam.getDisclosureID(),cParam.toUser());
                            */
                            CallInteractionController currCallInteractionController1 = sessionMap.get(cParam.getMachine()).getCallInteraction().getCallInteractionController(session);
                            currCallInteractionController1.playWav("" + cParam.getSessionID(),
                                    sessionMap.get(cParam.getMachine()).getInbound().getInteractionId(),
                                    ConfigurationUtils.PATH_PLAY_WAVE_AUDIO + cParam.getDisclosureID(), cParam.toUser());

                            Logger.getInstance("").log(LoggerTag.INFORMATION, "beginPlayAudio......[" + cParam.getMachine() + "] inbound not first time wave file... " + ConfigurationUtils.PATH_PLAY_WAVE_AUDIO + cParam.getDisclosureID(), "");
                        } else {
                            CallInteraction callInteraction = new CallInteraction();
                            callInteraction.setCallInteractionController((CallInteractionController) new InteractionController(session));
                            callInteraction.setInteractionId(sessionMap.get(cParam.getMachine()).getInbound().getInteractionId());
                            sessionMap.get(cParam.getMachine()).setCallInteraction(callInteraction);

                            /*sessionMap.get(cParam.getMachine()).getCallInteraction().getCallInteractionController().playWav(""+cParam.getSessionID(),
                                    sessionMap.get(cParam.getMachine()).getInbound().getInteractionId(),
                                    ConfigurationUtils.PATH_PLAY_WAVE_AUDIO + cParam.getDisclosureID(),cParam.toUser());
                                    */
                            CallInteractionController currCallInteractionController2 = sessionMap.get(cParam.getMachine()).getCallInteraction().getCallInteractionController(session);
                            currCallInteractionController2.playWav("" + cParam.getSessionID(),
                                    sessionMap.get(cParam.getMachine()).getInbound().getInteractionId(),
                                    ConfigurationUtils.PATH_PLAY_WAVE_AUDIO + cParam.getDisclosureID(), cParam.toUser());


                            Logger.getInstance("").log(LoggerTag.INFORMATION, "beginPlayAudio......[" + cParam.getMachine() + "] inbound not first time wave file... " + ConfigurationUtils.PATH_PLAY_WAVE_AUDIO + cParam.getDisclosureID(), "");
                        }
                    } else if ((sessionMap.get(cParam.getMachine()).getInteraction() != null) && (

                            getInteractionAttribute(session.getSessionId(),sessionMap.get(cParam.getMachine()).getInteraction(),EIC_STATE).equals("C"))) {
                        //outbound or ivr
                        if (sessionMap.get(cParam.getMachine()).getCallInteraction() != null &&
                                sessionMap.get(cParam.getMachine()).getCallInteraction().getInteractionId().equals(sessionMap.get(cParam.getMachine()).getInteraction().getInteractionId())) {

                            /*sessionMap.get(cParam.getMachine()).getCallInteraction().getCallInteractionController().playWav(""+cParam.getSessionID(),
                                    sessionMap.get(cParam.getMachine()).getInbound().getInteractionId(),
                                    ConfigurationUtils.PATH_PLAY_WAVE_AUDIO + cParam.getDisclosureID(),cParam.toUser());
                            */
                            CallInteractionController currCallInteractionController3 = sessionMap.get(cParam.getMachine()).getCallInteraction().getCallInteractionController(session);
                            currCallInteractionController3.playWav("" + cParam.getSessionID(),
                                    sessionMap.get(cParam.getMachine()).getInbound().getInteractionId(),
                                    ConfigurationUtils.PATH_PLAY_WAVE_AUDIO + cParam.getDisclosureID(), cParam.toUser());

                            Logger.getInstance("").log(LoggerTag.INFORMATION, "beginPlayAudio......[" + cParam.getMachine() + "] inbound not first time wave file... " + ConfigurationUtils.PATH_PLAY_WAVE_AUDIO + cParam.getDisclosureID(), "");
                        } else {
                            /*sessionMap.get(cParam.getMachine()).getCallInteraction().getCallInteractionController().playWav(""+cParam.getSessionID(),
                                    sessionMap.get(cParam.getMachine()).getInbound().getInteractionId(),
                                    ConfigurationUtils.PATH_PLAY_WAVE_AUDIO + cParam.getDisclosureID(),cParam.toUser());
                            */
                            CallInteractionController currCallInteractionController4 = sessionMap.get(cParam.getMachine()).getCallInteraction().getCallInteractionController(session);
                            currCallInteractionController4.playWav("" + cParam.getSessionID(),
                                    sessionMap.get(cParam.getMachine()).getInbound().getInteractionId(),
                                    ConfigurationUtils.PATH_PLAY_WAVE_AUDIO + cParam.getDisclosureID(), cParam.toUser());

                            Logger.getInstance("").log(LoggerTag.INFORMATION, "beginPlayAudio......[" + cParam.getMachine() + "] inbound not first time wave file... " + ConfigurationUtils.PATH_PLAY_WAVE_AUDIO + cParam.getDisclosureID(), "");
                        }
                    } else if (sessionMap.get(cParam.getMachine()).getDialerCallInteraction() != null && sessionMap.get(cParam.getMachine()).getDialerCallInteraction().getDialingMode() != DialingMode.Preview) {
                        //check the dialer call first
                        /*sessionMap.get(cParam.getMachine()).getCallInteraction().getCallInteractionController().playWav(""+cParam.getSessionID(),
                                sessionMap.get(cParam.getMachine()).getInbound().getInteractionId(),
                                ConfigurationUtils.PATH_PLAY_WAVE_AUDIO + cParam.getDisclosureID(),cParam.toUser());
                        */
                        CallInteractionController currCallInteractionController5 = sessionMap.get(cParam.getMachine()).getCallInteraction().getCallInteractionController(session);
                        currCallInteractionController5.playWav("" + cParam.getSessionID(),
                                sessionMap.get(cParam.getMachine()).getInbound().getInteractionId(),
                                ConfigurationUtils.PATH_PLAY_WAVE_AUDIO + cParam.getDisclosureID(), cParam.toUser());

                        Logger.getInstance("").log(LoggerTag.INFORMATION, "beginPlayAudio......[" + cParam.getMachine() + "] inbound not first time wave file... " + ConfigurationUtils.PATH_PLAY_WAVE_AUDIO + cParam.getDisclosureID(), "");
                    } else {
                        return 4;
                    }

                    return 0;
                } catch (Exception icEx) {
                    Logger.getInstance("").log(LoggerTag.ERROR, "beginPlayAudio......[" + cParam.getMachine() + "] error " + icEx.getMessage(), "");
                    return -1;
                }
            } else {
                //no session
                return 2;
            }
        }

        //not in the session map
        return 3;
    }

    /**
     * This method is used to do a call recording
     *
     * @param cParam
     * @return a boolean value
     */
    @Override
    public boolean recordingCall(CompositeType cParam) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: recordingCall ~~~~~~~~~~~~~~~~~~~~~~","");

        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty()) {
            return false;
        }

        cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
        cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

        if (!sessionMap.containsKey(cParam.getMachine())) {
            this.getSession(cParam);
        }

        SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());

        //either old call interaction or inbound exist, do not allow outbound call, dialer session exist do not allow
        if (currentSessionInteraction.getInteraction() != null) {
            Logger.getInstance("").log(LoggerTag.ERROR, "recordingCall......call out bound interaction exist on [" + cParam.getMachine() + "] " + sessionMap.get(cParam.getMachine()).getInteraction().getInteractionId(), "");
            return false;
        }

        if (currentSessionInteraction.getInbound() != null) {
            Logger.getInstance("").log(LoggerTag.ERROR, "recordingCall......call in bound interaction exist on [" + cParam.getMachine() + "] " + sessionMap.get(cParam.getMachine()).getInbound().getInteractionId(), "");
            return false;
        }

        if (currentSessionInteraction.getDialerSession() != null) {
            Logger.getInstance("").log(LoggerTag.ERROR, "recordingCall......dialer call session exist on [" + cParam.getMachine() + "] ", "");
            return false;
        }

        Session session = currentSessionInteraction.getSession();
        //make sure the session is still ok
        if (session != null) {
            if (session.getConnectionSettings().getConnectionState() != ConnectionState.Up) {
                Logger.getInstance("").log(LoggerTag.ERROR, "recordingCall......session on [" + cParam.getMachine() + "] is down. Reconnect ", "");
                this.connectSession(session, cParam);
            }

            if (session.getConnectionSettings().getConnectionState() == ConnectionState.Up) {
                currentSessionInteraction.setIod(2);
                sessionMap.replace(cParam.getMachine(),currentSessionInteraction);
                InteractionController interactionsManager = new InteractionController(session);
                if (currentSessionInteraction.isUpdateI3Status()) {
                    //this.UpdateUserCallingStatus(session, "Do Not Disturb");
                    this.updateUserCallingStatus(session,cParam.getUserID(), ConfigurationUtils.USER_CALLING_STATUS);
                }

                CreateCallParameters parameters = new CreateCallParameters();
                parameters.setTarget(cParam.getPhoneID());
                parameters.setCallMadeStage(CallMadeStorageTypes.Allocated);
                Interaction interaction = interactionsManager.createInteraction(session.getSessionId(), parameters, cParam.toUser());

                if (interaction != null) {
                    try {
                        currentSessionInteraction.setInteraction(interaction);
                        sessionMap.replace(cParam.getMachine(),currentSessionInteraction);
                        Logger.getInstance("").log(LoggerTag.INFORMATION, "recordingCall......[" + cParam.getMachine() + "] interaction " + interaction.getInteractionId(), "");
                        return true;
                    } catch (Exception e) {
                        Logger.getInstance("").log(LoggerTag.ERROR, e.getMessage(), "");
                        return false;
                    }
                } else {
                    Logger.getInstance("").log(LoggerTag.ERROR, "recordingCall......[" + cParam.getMachine() + "] Interaction null Object ", "");
                    return false;
                }
            } else {
                Logger.getInstance("").log(LoggerTag.ERROR, "recordingCall......[" + cParam.getMachine() + "] Session connection state down ", "");
                return false;
            }
        } else {
            Logger.getInstance("").log(LoggerTag.ERROR, "recordingCall......[" + cParam.getMachine() + "] Session Object is null ", "");
            return false;
        }
    }

    /**
     * This method is called to do a recording switch
     *
     * @param cParam
     * @param onOff
     * @return a boolean value
     */
    @Override
    public boolean recordingSwitch(CompositeType cParam, boolean onOff) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: recordingSwitch ~~~~~~~~~~~~~~~~~~~~~~","");

        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty() || cParam.getDisclosureID().isEmpty()) {
            return false;
        }

        if (sessionMap.containsKey(cParam.getMachine())) {

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

            SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());

            Session session = currentSessionInteraction.getSession();
            if ((session.getConnectionSettings().getConnectionState() == ConnectionState.Up) && (currentSessionInteraction.getInteraction() != null)) {
                try {
                    //SessionMap[cparam.Machine].GetInteraction().Record(onOff, false);
                    CommonInteractionParameters parameters = new CommonInteractionParameters();
                    parameters.setOn(onOff);
                    parameters.setSupervisor(false);
                    InteractionController controller = new InteractionController(session);
                    controller.recordInteraction(session.getSessionId(), currentSessionInteraction.getInteraction().getInteractionId(), parameters, cParam.toUser());
                    Logger.getInstance("").log(LoggerTag.INFORMATION, "recordingSwitch......[" + cParam.getMachine() + "] record the call " + onOff, "");
                } catch (Exception exception) {
                    Logger.getInstance("").log(LoggerTag.ERROR, "recordingSwitch......[" + cParam.getMachine() + "] error " + exception.getMessage(), "");
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * This method is called with the usage of the web socket for checking if there is an incoming call
     *
     * @param cParam
     * @return an array of string
     */
    @Override
    public String[] hasIncoming(CompositeType cParam) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: hasIncoming ~~~~~~~~~~~~~~~~~~~~~~","");


        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty()) {
            return null;
        }

        String[] ret = new String[3];
        ret[0] = ""; //status
        ret[1] = ""; //phone number
        ret[2] = ""; //not in the session if empty

        /*try {
            //status
            ret[0] = getUserCurrentStatus(sessionMap.get(cParam.getMachine()).getSession(), cParam.getUserID());
        } catch (Exception ex) {
            //Logger.getInstance("").log(LoggerTag.ERROR, "ringIncoming: exception on [" + cParam.getMachine() + "] " + iex.getMessage(), "")
        }*/

        if (sessionMap.containsKey(cParam.getMachine())) {
            //sessionInteraction exist

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));
            System.out.println(Json.encodePrettily(cParam));

            try {

                SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());
                //if (ret[0].isEmpty()) {
                //status
                ret[0] = getUserCurrentStatus(currentSessionInteraction.getSession(), cParam.getUserID());
                //}
                //for inbound
                if (currentSessionInteraction.getInbound() != null) {

                    System.out.println(".........................Inbound.........................;");
                    String instate = getInteractionAttribute(sessionMap.get(cParam.getMachine()).getSession().getSessionId(), currentSessionInteraction.getInbound(), EIC_STATE);
                    if (instate.equals("R") || instate.equals("X") ||
                            instate.equals("A") || instate.equals("O") ||
                            instate.equals("P")) {

                        if(ret[0].toLowerCase().equals(I3STATUS_AVAILABLE.toLowerCase())){
                            try{
                                this.updateUserCallingStatus(sessionMap.get(cParam.getMachine()).getSession(),cParam.getUserID(),I3STATUS_INBOUND_CALL);
                            }catch (Exception e){
                                Logger.getInstance("").log(LoggerTag.ERROR," error changing status on inbound: "+ExceptionUtils.exceptionStackTraceAsString(e),"");
                            }
                        }
                        Logger.getInstance("").log(LoggerTag.INFORMATION, "ringIncoming: Inbound Interaction state in " + instate, "");
                        ret[2] = "0";
                    } else {
                        if (instate.equals("M")) {
                            currentSessionInteraction.setInbound(null);
                            /*if(ret[0].toLowerCase().equals(I3STATUS_INBOUND_CALL.toLowerCase())){
                                try{
                                    this.updateUserCallingStatus(sessionMap.get(cParam.getMachine()).getSession(),cParam.getUserID(),I3STATUS_AVAILABLE);
                                }catch (Exception e){
                                    Logger.getInstance("").log(LoggerTag.ERROR," error changing status: "+ExceptionUtils.exceptionStackTraceAsString(e),"");
                                }
                            }*/
                        }
                        ret[2] = "1";

                    }
                    //if (currentSessionInteraction.getInbound() != null) {
                    ret[1] = getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(), currentSessionInteraction.getInbound(), ConfigurationUtils.EIC_REMOTE_ADDRESS);
                    System.out.println("ret[1]: "+ret[1]);
                    //}
                } else if (currentSessionInteraction.getIod() != 2 &&
                        currentSessionInteraction.getInteraction() != null) {

                    System.out.println("............................IOD or interaction............................");
                    String instate =
                            getInteractionAttribute(sessionMap.get(cParam.getMachine()).getSession().getSessionId(), currentSessionInteraction.getInteraction(), EIC_STATE);
                    if (instate.equals("R") || instate.equals("X") ||
                            instate.equals("A") || instate.equals("O") ||
                            instate.equals("P")) {
                        Logger.getInstance("").log(LoggerTag.INFORMATION, "ringIncoming: Dialer Interaction state in " + instate, "");
                        ret[2] = "0";
                    } else {
                        ret[2] = "1";
                        /*if(!ret[0].toLowerCase().equals(I3STATUS_AVAILABLE.toLowerCase())){
                            try{
                                this.updateUserCallingStatus(sessionMap.get(cParam.getMachine()).getSession(),cParam.getUserID(),I3STATUS_AVAILABLE);
                            }catch (Exception e){
                                Logger.getInstance("").log(LoggerTag.ERROR," error changing status: "+ExceptionUtils.exceptionStackTraceAsString(e),"");
                            }
                        }*/
                    }
                    ret[1] =
                            getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(), currentSessionInteraction.getInteraction(), ConfigurationUtils.EIC_REMOTE_ADDRESS);
                } else if
                (currentSessionInteraction.getDialerCallInteraction() != null) {

                    System.out.println("............................Dialer............................");

                    String instate =
                            getInteractionAttribute(sessionMap.get(cParam.getMachine()).getSession().getSessionId(), currentSessionInteraction.getDialerCallInteraction(), EIC_STATE);
                    if (instate.equals("R") || instate.equals("X") ||
                            instate.equals("A") || instate.equals("O") ||
                            instate.equals("P")) {
                        ret[2] = "0";
                    } else {
                        ret[2] = "1";

                        /*if(!ret[0].toLowerCase().equals(I3STATUS_AVAILABLE.toLowerCase())){
                            try{
                                this.updateUserCallingStatus(sessionMap.get(cParam.getMachine()).getSession(),cParam.getUserID(),I3STATUS_AVAILABLE);
                            }catch (Exception e){
                                Logger.getInstance("").log(LoggerTag.ERROR," error changing status: "+ExceptionUtils.exceptionStackTraceAsString(e),"");
                            }
                        }*/
                    }
                    ret[1] =
                            getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(), currentSessionInteraction.getDialerCallInteraction(), ConfigurationUtils.EIC_REMOTE_ADDRESS);
                } else {

                    System.out.println("............................nothing............................");

                    if(ret[0].toLowerCase().equals(I3STATUS_INBOUND_CALL.toLowerCase())){
                        try{
                            this.updateUserCallingStatus(sessionMap.get(cParam.getMachine()).getSession(),cParam.getUserID(),I3STATUS_AVAILABLE);
                        }catch (Exception e){
                            Logger.getInstance("").log(LoggerTag.ERROR," error changing status: "+ExceptionUtils.exceptionStackTraceAsString(e),"");
                        }
                    }

                    ret[2] = "-1";

                }
            } catch (Exception iex) {
                //when dialer call disconnected, access the call state will throw exception

                Logger.getInstance("").log(LoggerTag.WARNING, "ringIncoming: exception on [" + cParam.getMachine() + "] " + iex.getMessage(), "");
                ret[1] = "-1"; //exception
            }
        } else {
            Logger.getInstance("").log(LoggerTag.INFORMATION, "ringIncoming: not in the session map [" + cParam.getMachine() + "]", "");
        }


        return ret;
    }

    /**
     * This method is called for making a phone call
     *
     * @param cParam
     * @return a string
     */
    public String makePhoneCall(CompositeType cParam) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: makePhoneCall ~~~~~~~~~~~~~~~~~~~~~~","");

        System.out.println("CPARAM [ \n " + Json.encodePrettily(cParam) + " \n ]");
        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty()) {
            return null;
        }

        if (!sessionMap.containsKey(cParam.getMachine())) {
            this.getSession(cParam);
        }

        cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
        cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

        SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());

        //either old call interaction or inbound exist, do not allow outbound call, dialer session exist do not allow
        if (currentSessionInteraction.getInteraction() != null) {
            Logger.getInstance("").log(LoggerTag.INFORMATION, "makePhoneCall......call out bound interaction exist on [" + cParam.getMachine() + "] " + sessionMap.get(cParam.getMachine()).getInteraction().getInteractionId(), "");
            return null;
        }

        if (currentSessionInteraction.getInbound() != null) {
            Logger.getInstance("").log(LoggerTag.INFORMATION, "makePhoneCall......call in bound interaction exist on [" + cParam.getMachine() + "] " + sessionMap.get(cParam.getMachine()).getInbound().getInteractionId(), "");
            return null;
        }

        Session session = currentSessionInteraction.getSession();

        if (session != null) {
            //make sure the session is still ok
            if (session.getConnectionSettings().getConnectionState() != ConnectionState.Up) {
                Logger.getInstance("").log(LoggerTag.INFORMATION, "makePhoneCall......session on [" + cParam.getMachine() + "] is down. Reconnect ", "");
                connectSession(session, cParam);
            }

            if (session.getConnectionSettings().getConnectionState() == ConnectionState.Up) {
                //manual out bound
                currentSessionInteraction.setIod(2);
                InteractionController interactionsManager = new InteractionController(session);
                if (currentSessionInteraction.isUpdateI3Status()) {
                    //this.UpdateUserCallingStatus(session, "Manual Dialer Call");
                    this.updateUserCallingStatus(session,cParam.getUserID(),ConfigurationUtils.CALL_TYPE_MANUAL_DIALER_CALL);
                }
                currentSessionInteraction.setTransfer(false);
                currentSessionInteraction.setTransferToInteraction(null);
                currentSessionInteraction.setInbound(null);
                sessionMap.replace(cParam.getMachine(),currentSessionInteraction);
                //As Genesys said, makeCall request is equivalent to our createInteraction function
                CreateCallParameters parameters = new CreateCallParameters();
                parameters.setTarget(cParam.getPhoneID());
                if (!cParam.getDebtor_id().isEmpty()) {
                    Map<String, String> addAttr = new HashMap<>();
                    addAttr.put("debtor_id", cParam.getDebtor_id());
                    parameters.setAdditionalAttributes(addAttr);
                }
                Interaction interaction = interactionsManager.createInteraction(session.getSessionId(), parameters, cParam.toUser());

                if (interaction != null) {

                    try {
                        currentSessionInteraction.setInteraction(interaction);
                        sessionMap.replace(cParam.getMachine(),currentSessionInteraction);

                        //Tracing.TraceAlways("makePhoneCall......[" + cparam.Machine + "] interaction " + interaction.InteractionId);
                        InteractionController cInteractionController = interaction.getController(session);
                        String currCallIdKey = cInteractionController.getInteractionAttributes(session.getSessionId(), "Eic_RemoteName,Eic_State," + ConfigurationUtils.CALL_ID_KEY, interaction.getInteractionId(), cParam.toUser()).get(ConfigurationUtils.CALL_ID_KEY);
                        System.out.println("currCallIdKey:  " + currCallIdKey);

                        if (!cParam.getDebtor_id().isEmpty()) {
                            vertx.eventBus().send("calls_getting", interaction.getInteractionId() + ":" + cParam.getDebtor_id());
                        }
                        return currCallIdKey;
                    } catch (Exception ex) {
                        Logger.getInstance("").log(LoggerTag.ERROR, "makePhoneCall......[" + cParam.getMachine() + "] couldn't retrieve call id key " + ex.getMessage(), "");
                        return "-1";
                    }
                } else {
                    Logger.getInstance("").log(LoggerTag.ERROR, "makePhoneCall......[" + cParam.getMachine() + "] Interaction null Object ", "");
                    return null; //no interaction Object
                }
            } else {
                Logger.getInstance("").log(LoggerTag.ERROR, "makePhoneCall......[" + cParam.getMachine() + "]Session connection state down ", "");
                return null;
            }
        } else {
            Logger.getInstance("").log(LoggerTag.ERROR, "makePhoneCall......[" + cParam.getMachine() + "] Session Object is null ", "");
            return null;
        }
    }

    /**
     * This method is called in order to make a conference call
     *
     * @param cParam
     * @return an integer
     */
    public int makeConferenceCall(CompositeType cParam) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: makeConferenceCall ~~~~~~~~~~~~~~~~~~~~~~","");
        System.out.println("CPARAM [ \n " + Json.encodePrettily(cParam) + " \n ]");

        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty() || cParam.getPhoneID().isEmpty()) {
            return -1;
        }

        //sessionInteraction exist
        if (sessionMap.containsKey(cParam.getMachine())) {

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

            SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());

            if (currentSessionInteraction.getIod() == 1 && currentSessionInteraction.getInbound() != null) {
                //inbound consult transfer
                //currentSessionInteraction.setInteractionConference(
                //InteractionsManager.getInstance(currentSessionInteraction.getSession()).MakeNewConference(new Interaction[] { currentSessionInteraction.getInbound() }));
                currentSessionInteraction.setInteraction(currentSessionInteraction.getInbound());
                currentSessionInteraction.setTransfer(true);
                currentSessionInteraction.setTransferToInteraction(makeTransferCall(currentSessionInteraction.getSession(), cParam.getPhoneID(),currentSessionInteraction.getInbound()));
                Logger.getInstance("").log(LoggerTag.INFORMATION, "makeConferenceCall......make conf inbound call to phone id " + cParam.getPhoneID() + " Interaction id " +
                        currentSessionInteraction.getTransferToInteraction().getInteractionId() + " on [" + cParam.getMachine() + "]", "");
                sessionMap.replace(cParam.getMachine(),currentSessionInteraction);
                return 0;
            } else if (currentSessionInteraction.getIod() == 2 && currentSessionInteraction.getInteraction() != null) {
                //manual outbound consult transfer
                //currentSessionInteraction.setInteractionConference(
                // InteractionsManager.getInstance(currentSessionInteraction.getSession()).MakeNewConference(new Interaction[] { currentSessionInteraction.getInteraction() }));
                currentSessionInteraction.setTransfer(true);
                currentSessionInteraction.setTransferToInteraction(makeTransferCall(currentSessionInteraction.getSession(), cParam.getPhoneID(),currentSessionInteraction.getInteraction()));
                Logger.getInstance("").log(LoggerTag.INFORMATION, "makeConferenceCall......make conf manual call to phone id " + cParam.getPhoneID() + " Interaction id " + currentSessionInteraction.getTransferToInteraction().getInteractionId() + " on [" + cParam.getMachine() + "]", "");
                sessionMap.replace(cParam.getMachine(),currentSessionInteraction);
                return 0;
            } else if (currentSessionInteraction.getIod() == 4 && currentSessionInteraction.getInteraction() != null) {
                //blind transfer IVR call
                //currentSessionInteraction.setInteractionConference(
                //InteractionsManager.getInstance(currentSessionInteraction.getSession()).MakeNewConference(new Interaction[] { currentSessionInteraction.getInteraction() }));
                currentSessionInteraction.setTransfer(true);
                Logger.getInstance("").log(LoggerTag.INFORMATION, "makeConferenceCall......make conf unknown call to phone id " + cParam.getPhoneID() + " on [" + cParam.getMachine() + "]", "");
                currentSessionInteraction.setTransferToInteraction(makeTransferCall(currentSessionInteraction.getSession(), cParam.getPhoneID(),currentSessionInteraction.getInteraction()));
                sessionMap.replace(cParam.getMachine(),currentSessionInteraction);
                return 0;
            } else if (currentSessionInteraction.getIod() == 3 && currentSessionInteraction.getDialerCallInteraction() != null) {
                //the user is on the dialer interaction
                //currentSessionInteraction.setInteractionConference(
                // InteractionsManager.getInstance(currentSessionInteraction.getSession()).MakeNewConference(new Interaction[] { currentSessionInteraction.getDialerCallInteraction() }));
                currentSessionInteraction.setInteraction(currentSessionInteraction.getDialerCallInteraction());
                currentSessionInteraction.setTransfer(true);
                Logger.getInstance("").log(LoggerTag.INFORMATION, "makeConferenceCall......make conf dialer call to phone id " + cParam.getPhoneID() + " on [" + cParam.getMachine() + "]", "");
                currentSessionInteraction.setTransferToInteraction(makeTransferCall(currentSessionInteraction.getSession(), cParam.getPhoneID(),currentSessionInteraction.getDialerCallInteraction()));
                sessionMap.replace(cParam.getMachine(),currentSessionInteraction);
                return 0;
            } else {
                return -1;
            }
        }
        return -1;
    }

    /**
     * This method is used to connect a conference call
     *
     * @param cParam
     * @return an integer
     */
    public int connectConfCall(CompositeType cParam) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: connectConfCall ~~~~~~~~~~~~~~~~~~~~~~","");

        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty()) {
            return -1;
        }

        //sessionInteraction exist
        if (sessionMap.containsKey(cParam.getMachine())) {
            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

            SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());

            currentSessionInteraction.setTransfer(false);
            sessionMap.replace(cParam.getMachine(),currentSessionInteraction);


            if (currentSessionInteraction.getInteraction() != null) {
                try{
                    List<String> interactions = new ArrayList<>();
                    interactions.add(currentSessionInteraction.getInteraction().getInteractionId());
                    interactions.add(currentSessionInteraction.getTransferToInteraction().getInteractionId());
                    System.out.println("PARTIES: "+Json.encodePrettily(interactions));

                    //TO CHECK
                    //ConferenceCreationResult result = currentSessionInteraction.getInteractionConference().getInteractionConferenceController().createInteractionConference(String.valueOf(cParam.getSessionID()), interactions, ConfigurationUtils.DEFAULT_LANGUAGE,this.getUser(cParam));
                    CreateConferenceParameters createConferenceParameters = new CreateConferenceParameters();
                    createConferenceParameters.setInteractions(interactions);

                    InteractionConferenceController controller=new InteractionConferenceController(currentSessionInteraction.getSession());
                    ConferenceCreationResult result = controller.createInteractionConference(currentSessionInteraction.getSession().getSessionId(), createConferenceParameters, ConfigurationUtils.DEFAULT_LANGUAGE, this.getUser(cParam));
                    if(result!=null){
                        System.out.println("RESSS: "+Json.encodePrettily(result));
                        InteractionConference interactionConference = new InteractionConference();
                        interactionConference.setConferenceId(result.getConferenceId());
                        interactionConference.addParty(currentSessionInteraction.getInteraction());
                        interactionConference.setInteractionConferenceController(controller);
                        currentSessionInteraction.setInteractionConference(interactionConference);
                        sessionMap.replace(cParam.getMachine(),currentSessionInteraction);

                    }

                }catch (Exception e){
                    Logger.getInstance("").log(LoggerTag.ERROR," Create Conf: "+ ExceptionUtils.exceptionStackTraceAsString(e), "");
                    return -1;
                }

            }

            if (    currentSessionInteraction.getInteractionConference() != null
                    &&
                    currentSessionInteraction.getTransferToInteraction() != null) {
                try {

                    Logger.getInstance("").log(LoggerTag.INFORMATION, "connectConfCall......from [" + cParam.getMachine() + "] successful connected the existing call ", "");

                   /* currentSessionInteraction.getInteractionConference().getInteractionConferenceController(currentSessionInteraction.getSession())
                            .addInteractionAsParty(
                            currentSessionInteraction.getSession().getSessionId(),
                            currentSessionInteraction.getInteractionConference().getConferenceId(), iIds, cParam.toUser(),
                            "");*/

                    /*if(cParam.getPhoneID()!= null && !cParam.getPhoneID().isEmpty() && !currentSessionInteraction.getCurrentConferenceTargets().contains(cParam.getPhoneID())){
                        InteractionConferenceController controller = new InteractionConferenceController(currentSessionInteraction.getSession());
                        controller.createInviteParty(currentSessionInteraction.getSession().getSessionId(), currentSessionInteraction.getInteractionConference().getConferenceId(), cParam.getPhoneID(), null, "", cParam.toUser());
                    }*/

                    sessionMap.replace(cParam.getMachine(),currentSessionInteraction);
                    return 0;
                } catch (Exception iex) {
                    Logger.getInstance("").log(LoggerTag.DEBUG,"connectConfCall......from [" + cParam.getMachine() + "] call type " + currentSessionInteraction.getIod()+" "+ iex.getMessage(),"");
                }

            } else {
                Logger.getInstance("").log(LoggerTag.ERROR, "connectConfCall......from [" + cParam.getMachine() + "] call type " + currentSessionInteraction.getIod()
                        + " couldn't make conference call", "");
            }
        }
        return -1;
    }

    /**
     * This method is called to no conference call action
     *
     * @param cParam
     */
    public void noConfCall(CompositeType cParam) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: noConfCall ~~~~~~~~~~~~~~~~~~~~~~","");

        //sessionInteraction exist
        if (sessionMap.containsKey(cParam.getMachine())) {

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

            SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());
            Session cSession = currentSessionInteraction.getSession();

            currentSessionInteraction.setTransfer(false);
            currentSessionInteraction.setCurrentConferenceTargets(new ArrayList<>());
            sessionMap.replace(cParam.getMachine(),currentSessionInteraction);


            try {
                if (currentSessionInteraction.getTransferToInteraction() != null) {
                    Logger.getInstance("").log(LoggerTag.INFORMATION, "noConfCall.....disconnect third party conf call[" + cParam.getMachine() + "]", "");

                    //currentSessionInteraction.getTransferToInteraction().getController().disconnectInteraction(currentSessionInteraction.getSession().getSessionId(),currentSessionInteraction.getTransferToInteraction().getInteractionId(),cParam.toUser());

                    InteractionController cInteractionController1 = new InteractionController(cSession);
                    cInteractionController1.disconnectInteraction(cSession.getSessionId(), currentSessionInteraction.getTransferToInteraction().getInteractionId(), cParam.toUser());
                }
            } catch (Exception icx) {
                Logger.getInstance("").log(LoggerTag.ERROR, "noConfCall.....disconnect third party conf call exception " + icx.getMessage(), "");
            }

            if (currentSessionInteraction.getInteraction() != null) {
                Logger.getInstance("").log(LoggerTag.INFORMATION, "noConfCall.....the original interaction " + currentSessionInteraction.getInteraction().getInteractionId(), "");
                try {


                    if (getInteractionAttribute(cSession.getSessionId(),currentSessionInteraction.getInteraction(),EIC_STATE).equals("H")) {
                        Logger.getInstance("").log(LoggerTag.INFORMATION, "noConfCall.....pick up call[" + cParam.getMachine() + "]", "");

                        //currentSessionInteraction.getInteraction().getController().pickUpInteraction(currentSessionInteraction.getSession().getSessionId(),currentSessionInteraction.getTransferToInteraction().getInteractionId(),cParam.toUser());

                        InteractionController cInteractionController2 = new InteractionController(cSession);
                        cInteractionController2.pickUpInteraction(cSession.getSessionId(), currentSessionInteraction.getTransferToInteraction().getInteractionId(), cParam.toUser());
                    }
                } catch (Exception icx) {
                    Logger.getInstance("").log(LoggerTag.ERROR, "noConfCall.....pick up call exception " + icx.getMessage(), "");
                }
            } else {
                Logger.getInstance("").log(LoggerTag.INFORMATION, "noConfCall.....the original interaction is null ", "");
            }
        }
    }

    /**
     * This method is called by the public make transfer call method. It is called while making a transfer call.
     *
     * @param sessionParameter
     * @param phoneId
     * @return an interaction
     */
    private Interaction makeTransferCall(Session sessionParameter, String phoneId,Interaction interaction) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: INTERACTION[makeTransferCall] ~~~~~~~~~~~~~~~~~~~~~~","");

        InteractionController interactionsManager = new InteractionController(sessionParameter);
        CreateConsultParameters parameters = new CreateConsultParameters();
        //parameters.setCallMadeStage(CallMadeStorageTypes.Started);
        parameters.setTarget(phoneId);

        //if (interaction.getAttributes()!=null || !interaction.getAttributes().isEmpty())
        //parameters.setAdditionalAttributes(interaction.getAttributes());

        //Create the consult transfer
        ConsultTransferResult consultTransferResult = interactionsManager.initiateConsultTransferBetweenCallerAgentAndConsult(sessionParameter.getSessionId(),interaction.getInteractionId(), parameters, null);

        // Get the consultId and add it to the related sessionInteraction
        sessionMap.forEach((s, sessionInteraction) -> {
            if(sessionInteraction.getSession().getSessionId().equals(sessionParameter.getSessionId())){
                sessionInteraction.setCurConsultId(consultTransferResult.getConsultId());
                sessionInteraction.addTarget(phoneId);
                sessionMap.replace(s,sessionInteraction);
            }
        });

        //Change who the agent is speaking and listening to in the consult transfer
        ConsultTransferAudience consultTransferAudience = new ConsultTransferAudience();
        consultTransferAudience.setConsultedParty(true);
        consultTransferAudience.setOriginalParty(false);
        ChangeAudienceParameters changeAudienceParameters = new ChangeAudienceParameters(consultTransferAudience);
        interactionsManager.changeAudienceBySessionIdAndInteractionIdAndConsultId(sessionParameter.getSessionId(),interaction.getInteractionId(),consultTransferResult.getConsultId(),changeAudienceParameters,null);
        Interaction returnInt=new Interaction();
        returnInt.setInteractionId(consultTransferResult.getConsultInteractionId());
        return returnInt;
    }

    /**
     * This is the main method called when we make a transfer call
     *
     * @param cParam
     * @return an integer
     */
    public int makeTransferCall(CompositeType cParam) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: INT[makeTransferCall] ~~~~~~~~~~~~~~~~~~~~~~","");

        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty() || cParam.getPhoneID().isEmpty()) {
            return -1;
        }
        //sessionInteraction exist
        if (sessionMap.containsKey(cParam.getMachine())) {

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

            SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());

            try {
                if (currentSessionInteraction.getIod() == 1 && currentSessionInteraction.getInbound() != null) {
                    //inbound consult transfer
                    currentSessionInteraction.setTransfer(true);
                    currentSessionInteraction.setTransferToInteraction(makeTransferCall(currentSessionInteraction.getSession(), cParam.getPhoneID(),currentSessionInteraction.getInbound()));

                    Logger.getInstance("").log(LoggerTag.INFORMATION, "consultTransferCall......make transfer inbound call to phone id " + cParam.getPhoneID() + " Interaction id " +
                            currentSessionInteraction.getTransferToInteraction().getInteractionId() + " on [" + cParam.getMachine() + "]", "");
                    sessionMap.replace(cParam.getMachine(),currentSessionInteraction);

                    return 0;
                } else if (currentSessionInteraction.getIod() == 2 && currentSessionInteraction.getInteraction() != null) {
                    //manual outbound consult transfer
                    currentSessionInteraction.setTransfer(true);
                    currentSessionInteraction.setTransferToInteraction(makeTransferCall(currentSessionInteraction.getSession(), cParam.getPhoneID(),currentSessionInteraction.getInteraction()));
                    sessionMap.replace(cParam.getMachine(),currentSessionInteraction);


                    Logger.getInstance("").log(LoggerTag.INFORMATION, "consultTransferCall......make transfer manual call to phone id " + cParam.getPhoneID() + " Interaction id " +
                            currentSessionInteraction.getTransferToInteraction().getInteractionId() + " on [" + cParam.getMachine() + "]", "");

                    return 0;
                } else if (currentSessionInteraction.getIod() == 4 && currentSessionInteraction.getInteraction() != null) {
                    //blind transfer IVR call
                    currentSessionInteraction.setTransfer(true);

                    Logger.getInstance("").log(LoggerTag.INFORMATION, "consultTransferCall......make transfer unknown call to phone id " + cParam.getPhoneID() + " on [" + cParam.getMachine() + "]", "");

                    currentSessionInteraction.setTransferToInteraction(makeTransferCall(currentSessionInteraction.getSession(), cParam.getPhoneID(),currentSessionInteraction.getInteraction()));
                    sessionMap.replace(cParam.getMachine(),currentSessionInteraction);

                    return 0;
                } else if (currentSessionInteraction.getIod() == 3 && currentSessionInteraction.getDialerCallInteraction() != null) {
                    //the user is on the dialer interaction
                    currentSessionInteraction.setTransfer(true);
                    Logger.getInstance("").log(LoggerTag.INFORMATION, "consultTransferCall......make transfer dialer call to phone id " + cParam.getPhoneID() + " on [" + cParam.getMachine() + "]", "");
                    currentSessionInteraction.setTransferToInteraction(makeTransferCall(currentSessionInteraction.getSession(), cParam.getPhoneID(),currentSessionInteraction.getDialerCallInteraction()));
                    sessionMap.replace(cParam.getMachine(),currentSessionInteraction);

                    return 0;
                } else
                    return -1;
            } catch (Exception ex) {
                ex.printStackTrace();
                Logger.getInstance("").log(LoggerTag.ERROR, "consultTransferCall......from [" + cParam.getMachine() + "] call type " + currentSessionInteraction.getIod()
                        + " exception " + ex.getMessage(), "");
            }
        }
        return -1;
    }

    /**
     * This method is called for connect transfer call
     *
     * @param cParam
     * @return an integer
     */
    public int connectTransferCall(CompositeType cParam) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: connectTransferCall ~~~~~~~~~~~~~~~~~~~~~~","");

        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty()) {
            return -1;
        }
        //sessionInteraction exist
        if (sessionMap.containsKey(cParam.getMachine())) {

            Interaction currentTransferInteraction = null;
            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));
            SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());
            Session cSession = currentSessionInteraction.getSession();

            //check the both interactions did connect, then perform transfer connect
            try {
                currentSessionInteraction.setTransfer(false);

                currentTransferInteraction = currentSessionInteraction.getTransferToInteraction();

                if (currentSessionInteraction.getIod() == 1 &&
                        currentSessionInteraction.getInbound() != null && currentTransferInteraction != null) {
                    /*CreateConsultParameters createConsultParameters = new CreateConsultParameters();
                    if (!cParam.getDebtor_id().isEmpty()) {
                        Map<String, String> addAttr = new HashMap<>();
                        addAttr.put("debtor_id", cParam.getDebtor_id());
                        createConsultParameters.setAdditionalAttributes(addAttr);
                    }
                    createConsultParameters.setTarget(cParam.getPhoneID());
*/
                    Logger.getInstance("").log(LoggerTag.INFORMATION, "connectTransferCall......from [" + cParam.getMachine() + "] successful transferred the inbound call ", "");

                    //currentSessionInteraction.getTransferToInteraction().getController().initiateConsultTransferBetweenCallerAgentAndConsult(currentSessionInteraction.getSession().getSessionId(),currentSessionInteraction.getInbound().getInteractionId(),createConsultParameters,cParam.toUser());
                    InteractionController cInteractionController1 = currentTransferInteraction.getController(cSession);
                    //cInteractionController1.initiateConsultTransferBetweenCallerAgentAndConsult(cSession.getSessionId(), currentSessionInteraction.getInbound().getInteractionId(), createConsultParameters, cParam.toUser());
                    cInteractionController1.concludeConsultTransferByConsultId(cSession.getSessionId(), currentSessionInteraction.getInbound().getInteractionId(), currentSessionInteraction.getCurConsultId(), cParam.toUser());

                    currentSessionInteraction.setInbound(null);
                    currentSessionInteraction.setTransferToInteraction(null);
                    currentSessionInteraction.setCurConsultId(null);
                    sessionMap.replace(cParam.getMachine(),currentSessionInteraction);
                    return 0;
                } else if (currentSessionInteraction.getIod() == 2 && currentSessionInteraction.getInteraction() != null && currentTransferInteraction != null) {
                    /*CreateConsultParameters createConsultParameters = new CreateConsultParameters();
                    if (!cParam.getDebtor_id().isEmpty()) {
                        Map<String, String> addAttr = new HashMap<>();
                        addAttr.put("debtor_id", cParam.getDebtor_id());
                        createConsultParameters.setAdditionalAttributes(addAttr);
                    }
                    createConsultParameters.setTarget(cParam.getPhoneID());*/
                    //createConsultParameters.setCallMadeStage(CallMadeStorageTypes.Allocated);

                    Logger.getInstance("").log(LoggerTag.INFORMATION, "connectTransferCall......from [" + cParam.getMachine() + "] successful transferred the manual outbound call ", "");

                    //currentSessionInteraction.getTransferToInteraction().getController().initiateConsultTransferBetweenCallerAgentAndConsult(currentSessionInteraction.getSession().getSessionId(),currentSessionInteraction.getInbound().getInteractionId(),createConsultParameters,cParam.toUser());
                    InteractionController cInteractionController2 = currentTransferInteraction.getController(cSession);
                    //cInteractionController2.initiateConsultTransferBetweenCallerAgentAndConsult(cSession.getSessionId(), currentSessionInteraction.getInbound().getInteractionId(), createConsultParameters, cParam.toUser());
                    cInteractionController2.concludeConsultTransferByConsultId(cSession.getSessionId(), currentSessionInteraction.getInbound().getInteractionId(), currentSessionInteraction.getCurConsultId(), cParam.toUser());

                    currentSessionInteraction.setInteraction(null);
                    currentSessionInteraction.setTransferToInteraction(null);
                    currentSessionInteraction.setCurConsultId(null);
                    sessionMap.replace(cParam.getMachine(),currentSessionInteraction);
                    return 0;
                } else if (currentSessionInteraction.getIod() == 4 && currentSessionInteraction.getInteraction() != null
                        && currentTransferInteraction != null) {
                    Logger.getInstance("").log(LoggerTag.INFORMATION, "connectTransferCall......from [" + cParam.getMachine() + "] successful transferred unknown outbound call ", "");

                    CreateConsultParameters createConsultParameters = new CreateConsultParameters();

                    createConsultParameters.setTarget(cParam.getPhoneID());
                    //createConsultParameters.setCallMadeStage(CallMadeStorageTypes.Allocated);

                    //currentSessionInteraction.getTransferToInteraction().getController().initiateConsultTransferBetweenCallerAgentAndConsult(currentSessionInteraction.getSession().getSessionId(),currentSessionInteraction.getInbound().getInteractionId(),createConsultParameters,cParam.toUser());

                    InteractionController cInteractionController3 = currentTransferInteraction.getController(cSession);
                    //cInteractionController3.initiateConsultTransferBetweenCallerAgentAndConsult(cSession.getSessionId(), currentSessionInteraction.getInbound().getInteractionId(), createConsultParameters, cParam.toUser());
                    cInteractionController3.concludeConsultTransferByConsultId(cSession.getSessionId(), currentSessionInteraction.getInbound().getInteractionId(), currentSessionInteraction.getCurConsultId(), cParam.toUser());

                    currentSessionInteraction.setInteraction(null);
                    currentSessionInteraction.setTransferToInteraction(null);
                    currentSessionInteraction.setCurConsultId(null);
                    sessionMap.replace(cParam.getMachine(),currentSessionInteraction);
                    return 0;

                } else if (currentSessionInteraction.getIod() == 3 && currentSessionInteraction.getDialerCallInteraction() != null
                        && currentSessionInteraction.getDialerCallInteraction().getDialingMode() == DialingMode.Regular && currentTransferInteraction != null) {

                    Logger.getInstance("").log(LoggerTag.INFORMATION, "connectTransferCall......from [" + cParam.getMachine() + "] successful transfered the dailer regular outbound call ", "");

                    /*CreateConsultParameters createConsultParameters = new CreateConsultParameters();
                    if (!cParam.getDebtor_id().isEmpty()) {
                        Map<String, String> addAttr = new HashMap<>();
                        addAttr.put("debtor_id", cParam.getDebtor_id());
                        createConsultParameters.setAdditionalAttributes(addAttr);
                    }
                    createConsultParameters.setTarget(cParam.getPhoneID());*/
                    //createConsultParameters.setCallMadeStage(CallMadeStorageTypes.Allocated);
                    //currentSessionInteraction.getTransferToInteraction().getController().initiateConsultTransferBetweenCallerAgentAndConsult(currentSessionInteraction.getSession().getSessionId(),currentSessionInteraction.getInbound().getInteractionId(),createConsultParameters,cParam.toUser());

                    InteractionController cInteractionController4 = currentTransferInteraction.getController(cSession);
                    cInteractionController4.concludeConsultTransferByConsultId(cSession.getSessionId(), currentSessionInteraction.getInbound().getInteractionId(), currentSessionInteraction.getCurConsultId(), cParam.toUser());
                    //cInteractionController4.initiateConsultTransferBetweenCallerAgentAndConsult(cSession.getSessionId(), currentSessionInteraction.getInbound().getInteractionId(), createConsultParameters, cParam.toUser());
                    currentSessionInteraction.setInteraction(null);
                    currentSessionInteraction.setTransferToInteraction(null);
                    currentSessionInteraction.setCurConsultId(null);
                    sessionMap.replace(cParam.getMachine(),currentSessionInteraction);
                    return 0;
                } else {
                    //unsuccessful connect transfer call
                    Logger.getInstance("").log(LoggerTag.ERROR, "connectTransferCall......from [" + cParam.getMachine() + "] call type " + currentSessionInteraction.getIod()
                            + " couldn't send transfer ", "");
                }
            } catch (Exception exception) {
                Logger.getInstance("").log(LoggerTag.ERROR, "connectTransferCall......from [" + cParam.getMachine() + "] call type " + currentSessionInteraction.getIod()
                        + " exception " + exception.getMessage(), "");
            }
        }
        return -1;
    }

    /**
     * This method is called to disconnect a transfer call
     *
     * @param cParam
     */
    public void disconnectTransferCall(CompositeType cParam) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: disconnectTransferCall ~~~~~~~~~~~~~~~~~~~~~~","");

        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty())
            return;

        //sessionInteraction exist
        if (sessionMap.containsKey(cParam.getMachine())) {

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

            SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());

            currentSessionInteraction.setTransfer(false);
            sessionMap.replace(cParam.getMachine(),currentSessionInteraction);

            if (currentSessionInteraction.getTransferToInteraction() != null) {
                //disconnect the transferred to
                try {
                    currentSessionInteraction.getTransferToInteraction().getController(currentSessionInteraction.getSession()).disconnectInteraction(currentSessionInteraction.getSession().getSessionId(), currentSessionInteraction.getTransferToInteraction().getInteractionId(), cParam.toUser());
                } catch (Exception ex) {
                    Logger.getInstance("").log(LoggerTag.INFORMATION, "disconnectTransferCall.....call disconnect exception " + ex.getMessage(), "");
                }
            }
            currentSessionInteraction.setTransferToInteraction(null);
            currentSessionInteraction.setCurConsultId(null);
            sessionMap.replace(cParam.getMachine(),currentSessionInteraction);

            //transfer to interaction exist, the original call always set to held
            if ((currentSessionInteraction.getIod() == 2 || currentSessionInteraction.getIod() == 4)
                    && currentSessionInteraction.getInteraction() != null) {
                //manual or unknown type
                try {
                    if (getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(),currentSessionInteraction.getInteraction(),EIC_STATE).equals("H")) {
                        currentSessionInteraction.getInteraction().getController(currentSessionInteraction.getSession()).pickUpInteraction(currentSessionInteraction.getSession().getSessionId(), currentSessionInteraction.getTransferToInteraction().getInteractionId(), cParam.toUser());
                    }
                } catch (Exception ex) {
                    Logger.getInstance("").log(LoggerTag.INFORMATION, "disconnectTransferCall.....orig call pick exception " + ex.getMessage(), "");
                }
            } else if (currentSessionInteraction.getIod() == 1 && currentSessionInteraction.getInbound() != null) {
                //inbound
                try {
                    if (getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(),currentSessionInteraction.getInbound(),EIC_STATE).equals("H"))
                    {
                        currentSessionInteraction.getInbound().getController(currentSessionInteraction.getSession()).pickUpInteraction(currentSessionInteraction.getSession().getSessionId(), currentSessionInteraction.getInbound().getInteractionId(), cParam.toUser());
                    }
                } catch (Exception ex) {
                    Logger.getInstance("").log(LoggerTag.ERROR, "disconnectTransferCall.....orig call pick exception " + ex.getMessage(), "");
                }
            } else if (currentSessionInteraction.getIod() == 3 && currentSessionInteraction.getDialerCallInteraction() != null
                    && currentSessionInteraction.getDialerCallInteraction().getDialingMode() == DialingMode.Regular) {
                //dialer call
                try {
                    if (getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(),currentSessionInteraction.getDialerCallInteraction(),EIC_STATE).equals("H")) {
                        currentSessionInteraction.getDialerCallInteraction().getController(currentSessionInteraction.getSession()).pickUpInteraction(currentSessionInteraction.getSession().getSessionId(), currentSessionInteraction.getDialerCallInteraction().getInteractionId(), cParam.toUser());
                    }
                } catch (Exception ex) {
                    Logger.getInstance("").log(LoggerTag.ERROR, "disconnectTransferCall.....orig call pick exception " + ex.getMessage(), "");
                }
            }
        }
    }


    public void disconnectCall(CompositeType cParam) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: disconnectCall ~~~~~~~~~~~~~~~~~~~~~~","");

        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty()) {
            return;
        }
        //disconnect the manual outbound call or incoming call
        //sessionInteraction exist
        if (sessionMap.containsKey(cParam.getMachine())) {
            //Session session=sessionMap.get(cParam.getMachine()).getSession();

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

            SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());
            ArrayList<String> listToSend;
            if (currentSessionInteraction.getInbound() != null && !cParam.getResult().isEmpty() &&
                    currentSessionInteraction.getInbound().getInteractionId().equals(cParam.getResult())) {
                //disconnect inbound call
                try {
                    //TO CHECK
                    Interaction currentInboundInteraction1 = currentSessionInteraction.getInbound();
                    currentInboundInteraction1.getController(currentSessionInteraction.getSession()).disconnectInteraction(currentSessionInteraction.getSession().getSessionId(), currentSessionInteraction.getInbound().getInteractionId(), cParam.toUser());

                    listToSend = new ArrayList<>();
                    listToSend.add(currentInboundInteraction1.getInteractionId());
                    listToSend.add(currentSessionInteraction.getSession().getSessionId());
                    listToSend.add(currentSessionInteraction.getSession().getCustomOkHttpClient().getCsrfToken());
                    listToSend.add(currentSessionInteraction.getSession().getCustomOkHttpClient().getConnexionCookie());

                    vertx.eventBus().send("start_autonomie_after_call", Json.encode(listToSend));
                } catch (Exception ex) {
                    Logger.getInstance("").log(LoggerTag.ERROR, "disconnectCall.....Inbound call disconnect exception " + ex.getMessage(), "");
                }
                this.updateUserCallingStatus(sessionMap.get(cParam.getMachine()).getSession(),cParam.getUserID(),I3STATUS_AVAILABLE);
                currentSessionInteraction.setInbound(null);
                sessionMap.replace(cParam.getMachine(),currentSessionInteraction);


            } else if (currentSessionInteraction.getInteractionConference() != null) {
                try {
                    Collection<Interaction> conferenceInteractions = currentSessionInteraction.getInteractionConference().getParties();
                    for (Interaction conferenceInteraction : conferenceInteractions) {
                        //TO CHECK
                        conferenceInteraction.getController(currentSessionInteraction.getSession()).disconnectInteraction(currentSessionInteraction.getSession().getSessionId(), conferenceInteraction.getInteractionId(), cParam.toUser());

                        listToSend = new ArrayList<>();
                        listToSend.add(conferenceInteraction.getInteractionId());
                        listToSend.add(currentSessionInteraction.getSession().getSessionId());
                        listToSend.add(currentSessionInteraction.getSession().getCustomOkHttpClient().getCsrfToken());
                        listToSend.add(currentSessionInteraction.getSession().getCustomOkHttpClient().getConnexionCookie());

                        vertx.eventBus().send("start_autonomie_after_call", Json.encode(listToSend));
                    }
                } catch (Exception icx) {
                    Logger.getInstance("").log(LoggerTag.ERROR, "disconnectCall.....Conference call disconnect exception " + icx.getMessage(), "");
                } finally {
                    this.updateUserCallingStatus(sessionMap.get(cParam.getMachine()).getSession(),cParam.getUserID(),I3STATUS_AVAILABLE);

                    currentSessionInteraction.setInteractionConference(null);
                    sessionMap.replace(cParam.getMachine(),currentSessionInteraction);

                }
            } else if (currentSessionInteraction.getInteraction() != null) {
                //disconnect the manual call or IVR out bound call
                try {
                    //TO CHECK
                    //currentSessionInteraction.getInteraction().getController().disconnectInteraction(currentSessionInteraction.getInteraction().getSessionId(),currentSessionInteraction.getInteraction().getInteractionId(),cParam.toUser());
                    Interaction manualOrIvrInteraction = currentSessionInteraction.getInteraction();
                    currentSessionInteraction.getInteraction().getController(currentSessionInteraction.getSession()).disconnectInteraction(currentSessionInteraction.getSession().getSessionId(), manualOrIvrInteraction.getInteractionId(), cParam.toUser());

                    listToSend = new ArrayList<>();
                    listToSend.add(manualOrIvrInteraction.getInteractionId());
                    listToSend.add(currentSessionInteraction.getSession().getSessionId());
                    listToSend.add(currentSessionInteraction.getSession().getCustomOkHttpClient().getCsrfToken());
                    listToSend.add(currentSessionInteraction.getSession().getCustomOkHttpClient().getConnexionCookie());

                    vertx.eventBus().send("start_autonomie_after_call", Json.encode(listToSend));

                } catch (Exception ex) {
                    Logger.getInstance("").log(LoggerTag.INFORMATION, "disconnectCall.....Outbound call disconnect exception " + ex.getMessage(), "");
                }
                this.updateUserCallingStatus(sessionMap.get(cParam.getMachine()).getSession(),cParam.getUserID(),I3STATUS_AVAILABLE);
                currentSessionInteraction.setInteraction(null);
                sessionMap.replace(cParam.getMachine(),currentSessionInteraction);

            }

            //the user has dialer preview interaction - set the user status to manual dialer call
            if (currentSessionInteraction.getDialerCallInteraction() != null &&
                    currentSessionInteraction.getDialerCallInteraction().getDialingMode() == DialingMode.Preview) {
                this.updateUserCallingStatus(currentSessionInteraction.getSession(), cParam.getUserID(),I3STATUS_MANUAL_DIALER_CALL);
            }
        }
    }

    /**
     * This method is called to disconnect a dialer call
     *
     * @param cParam
     * @return an integer
     */
    @Override
    public int disconnectDialerCall(CompositeType cParam) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: disconnectDialerCall ~~~~~~~~~~~~~~~~~~~~~~","");

        int returnedValue = -1;
        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty()) {
            Logger.getInstance("").log(LoggerTag.WARNING, "...................Disconnect dialer call, Imcomplete args..............\nRETURN: " + returnedValue, "");

            return returnedValue;
        }

        SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());

        if (sessionMap.containsKey(cParam.getMachine())) {

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

            if (currentSessionInteraction.getDialerCallInteraction() != null) {
                //try if the predictive call the disconnect will throw exception
                if (currentSessionInteraction.getDialerCallInteraction().getDialingMode() != DialingMode.Preview) {
                    try {
                        //the dialer call disconnect removed from Queue, while the dialer call interaction Object still exist to do call disposition
                        currentSessionInteraction.getDialerCallInteraction().getController(currentSessionInteraction.getSession()).disconnectInteraction(currentSessionInteraction.getSession().getSessionId(), currentSessionInteraction.getDialerCallInteraction().getInteractionId(), cParam.toUser());

                        ArrayList<String> listToSend = new ArrayList<>();
                        listToSend.add(currentSessionInteraction.getDialerCallInteraction().getInteractionId());
                        listToSend.add(currentSessionInteraction.getSession().getSessionId());
                        listToSend.add(currentSessionInteraction.getSession().getCustomOkHttpClient().getCsrfToken());
                        listToSend.add(currentSessionInteraction.getSession().getCustomOkHttpClient().getConnexionCookie());

                        vertx.eventBus().send("start_autonomie_after_call", Json.encode(listToSend));

                        //Logger.getInstance("").log(LoggerTag.INFORMATION,"disconnectDialerCall.....dialer regular call disconnect");
                        returnedValue = 0;


                    } catch (Exception iex) {
                        //Logger.getInstance("").log("disconnectDialerCall.....dialer regular call exception " + iex.ToString());
                        Logger.getInstance("").log(LoggerTag.ERROR,iex.getMessage(),"");
                        returnedValue = 2;
                    }

                    this.updateUserCallingStatus(sessionMap.get(cParam.getMachine()).getSession(),cParam.getUserID(),I3STATUS_AVAILABLE);

                } else
                    returnedValue = 1;
            }
        } else if (currentSessionInteraction.getInteractionConference() != null) {
            try {
                Collection<Interaction> conferenceInteractions = currentSessionInteraction.getInteractionConference().getParties();
                conferenceInteractions = Collections.unmodifiableCollection(conferenceInteractions);

                for (Interaction conferenceInteraction : conferenceInteractions) {
                    conferenceInteraction.getController(currentSessionInteraction.getSession()).disconnectInteraction(currentSessionInteraction.getSession().getSessionId(), currentSessionInteraction.getDialerCallInteraction().getInteractionId(), cParam.toUser());

                    ArrayList<String> listToSend = new ArrayList<>();
                    listToSend.add(conferenceInteraction.getInteractionId());
                    listToSend.add(currentSessionInteraction.getSession().getSessionId());
                    listToSend.add(currentSessionInteraction.getSession().getCustomOkHttpClient().getCsrfToken());
                    listToSend.add(currentSessionInteraction.getSession().getCustomOkHttpClient().getConnexionCookie());

                    vertx.eventBus().send("start_autonomie_after_call", Json.encode(listToSend));

                }
            } catch (Exception icx) {
                Logger.getInstance("").log(LoggerTag.INFORMATION, "disconnectDialerCall.....Conference call disconnect exception " + icx.getMessage(), "");
            } finally {
                this.updateUserCallingStatus(sessionMap.get(cParam.getMachine()).getSession(),cParam.getUserID(),I3STATUS_AVAILABLE);

                currentSessionInteraction.setInteractionConference(null);
                sessionMap.replace(cParam.getMachine(),currentSessionInteraction);
            }
        }

        Logger.getInstance("").log(LoggerTag.WARNING, "...................Disconnect dialer call..............\nRETURN: " + returnedValue, "");

        return returnedValue;
    }

    /**
     * This method is called by the main method to update user calling status (this.setI3AvailableStatus)
     *
     * @param session
     * @param callStatus
     */
    private void updateUserCallingStatus(Session session, String userId, String callStatus) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: updateUserCallingStatus ~~~~~~~~~~~~~~~~~~~~~~","");

        if (session != null && session.getConnectionSettings().getConnectionState() == ConnectionState.Up) {

            Status statusMessage = new Status(session);
            UserStatusUpdate userStatusUpdate = new UserStatusUpdate();
            userStatusUpdate.setStatusId(callStatus);
            userStatusUpdate.setUntil(null);
            statusMessage.updateSpecifiedUserStatus(session.getSessionId(), userId, userStatusUpdate);
        }
    }

    /**
     * This method is called for updating user status
     *
     * @param cParam
     */
    public void setI3AvailableStatus(CompositeType cParam) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: setI3AvailableStatus ~~~~~~~~~~~~~~~~~~~~~~","");

        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty()) {
            return;
        }
        if (!sessionMap.containsKey(cParam.getMachine())) {
            this.getSession(cParam);
        }
        cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
        cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));
        Session session = sessionMap.get(cParam.getMachine()).getSession();
        if (cParam.getI3Status().equals(ConfigurationUtils.I3STATUS_AVAILABLE) || cParam.getI3Status().equals(ConfigurationUtils.I3STATUS_GONE_HOME) || cParam.getI3Status().equals(ConfigurationUtils.I3STATUS_MANUAL_DIALER_CALL) ||
                cParam.getI3Status().equals(ConfigurationUtils.I3STATUS_AWAY_FROM_DESK)
                || cParam.getI3Status().equals(ConfigurationUtils.I3STATUS_DO_NOT_DISTURB)
                || cParam.getI3Status().equals(ConfigurationUtils.I3STATUS_AT_LUNCH) ||
                cParam.getI3Status().equals(ConfigurationUtils.I3STATUS_IN_A_MEETING) ||
                cParam.getI3Status().equals(ConfigurationUtils.I3STATUS_INBOUND_CALL)

        ) {
            this.updateUserCallingStatus(session,cParam.getUserID(), cParam.getI3Status());
            //Logger.getInstance("").log(LoggerTag.INFORMATION,"setI3AvailableStatus......on [" + cparam.getMachine() + "] to " + cparam.I3status);
        }
    }

    /**
     * This method is called to log off campaign
     *
     * @param cParam
     * @return an integer
     */
    public int logoffCampaign(CompositeType cParam) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: logoffCampaign ~~~~~~~~~~~~~~~~~~~~~~","");

        int returnedValue = -1;
        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty()) {
            return returnedValue;
        }

        //sessionInteraction exist
        if (sessionMap.containsKey(cParam.getMachine())) {

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

            Session session = sessionMap.get(cParam.getMachine()).getSession();
            if (session != null) {
                logoutInvalidCall(cParam);
                //logoff Campaign status
                this.updateUserCallingStatus(session, cParam.getUserID(), ConfigurationUtils.I3STATUS_AWAY_FROM_DESK);
                returnedValue = 0;
            } else {
                Logger.getInstance("").log(LoggerTag.INFORMATION, "logoffCampaign... [" + cParam.getMachine() + "]", "");
            }
        }
        return returnedValue;
    }

    /**
     * This method is part of the main method called when we want to logout in case of an invalid call
     *
     * @param cParam
     */
    private void logoutInvalidCall(CompositeType cParam) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: logoutInvalidCall ~~~~~~~~~~~~~~~~~~~~~~","");


        SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());

        if (currentSessionInteraction.getDialerSession() != null) {
            DialerSession dialerSession = currentSessionInteraction.getDialerSession();
            String sessionId = dialerSession.getSessionId();
            try {
                dialerSession.logoffDialer(null, sessionId, null);
                Logger.getInstance("").log(LoggerTag.INFORMATION, "logoutInvalidCall... request logout[" + cParam.getMachine() + "]", "");
            } catch (Exception iex) {
                Logger.getInstance("").log(LoggerTag.ERROR, "logoutInvalidCall... [" + cParam.getMachine() + "] request logout exception " + iex.getMessage(), "");
            } finally {
                // Logger.getInstance("").log(LoggerTag.INFORMATION,"logoutInvalidCall... set the dialer session to null [" + cparam.getMachine() + "]");
                currentSessionInteraction.setDialerSession(null);
                sessionMap.replace(cParam.getMachine(),currentSessionInteraction);

            }
        }
        if (currentSessionInteraction.getDialerCallInteraction() != null) {
            currentSessionInteraction.setDialerCallInteraction(null);
           /* try{
                this.updateUserCallingStatus(sessionMap.get(cParam.getMachine()).getSession(),cParam.getUserID(),I3STATUS_AVAILABLE);

            }catch (Exception e){

            }*/
            //Logger.getInstance("").log(LoggerTag.INFORMATION,"logoutInvalidCall... set call interaction to null ");
        }
        currentSessionInteraction.setIod(0);
        sessionMap.replace(cParam.getMachine(),currentSessionInteraction);

    }

    /**
     * This method corresponds to the call disposition for ICWS library
     *
     * @param cParam
     * @return an integer
     */
    @Override
    public int agentLessCallComplete(CompositeType cParam) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: agentLessCallComplete ~~~~~~~~~~~~~~~~~~~~~~","");

        int returnedValue = -1;
        if (cParam == null || cParam.getHost().isEmpty() || cParam.getMachine().isEmpty() || cParam.getUserID().isEmpty()) {
            return returnedValue;
        }
        //sessionInteraction exist
        if (sessionMap.containsKey(cParam.getMachine())) {

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

            SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());
            Session session = currentSessionInteraction.getSession();
            if (session != null) {
                if (currentSessionInteraction.getInteraction() != null) {
                    try {
                        //mark the agentless call complete. With ICWS we are using disposition call here
                        Interaction interaction = currentSessionInteraction.getInteraction();
                        Map<String, String> callAttributes = new HashMap<>();
                        DispositionParameters dispositionParameters = null;
                        User user = new User(ConfigurationUtils.DEFAULT_APPLICATION_NAME, cParam.getUserID(), cParam.getPassword());
                        String campaign = getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(), interaction, ConfigurationUtils.IS_ATTR_CAMPAIGN_ID);
                        callAttributes.put(ConfigurationUtils.IS_ATTR_CAMPAIGN_ID, campaign);

                        Logger.getInstance("").log(LoggerTag.INFORMATION, "disposition or agentlessCallComplete........on [" + cParam.getMachine() + "] campaign:  " + campaign + " interaction:  " + interaction.getInteractionId(), "");

                        //DialerCallInteractionController dialerManager = new DialerCallInteractionController();
                        DialerCallInteractionController dialerManager = new DialerCallInteractionController(session);
                        //dialerManager.setDialerSession((DialerSession) currentSessionInteraction.getSession());

                        switch (cParam.getResult()) {
                            case ConfigurationUtils.FAILURE_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.FAILURE_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.WRONG_PARTY_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.WRONG_PARTY_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.SUCCESS_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.SUCCESS_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.MACHINE_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.MACHINE_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.NO_ANSWER_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.NO_ANSWER_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.REMOTE_HANGUP_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.REMOTE_HANGUP_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.TRANSFERRED_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.TRANSFERRED_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.SKIPPED_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.SKIPPED_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.PHONE_NUMBER_SUCCESS_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.PHONE_NUMBER_SUCCESS_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.PHONE_NUMBER_DELETED_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.PHONE_NUMBER_DELETED_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.FAX_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.FAX_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.SCHEDULED_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.SCHEDULED_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.BUSY_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.BUSY_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.SIT_UNCALLABLE_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.SIT_UNCALLABLE_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.SIT_CALLABLE_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.SIT_CALLABLE_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.NO_LINES_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.NO_LINES_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.DELETED_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.DELETED_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            case ConfigurationUtils.SIT_LABEL:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.SIT_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                            default:
                                dispositionParameters = new DispositionParameters(ConfigurationUtils.AMBIGUOUS_LABEL, callAttributes);
                                dialerManager.disposition(String.valueOf(cParam.getSessionID()), dispositionParameters, ConfigurationUtils.DEFAULT_LANGUAGE, user);
                                break;
                        }

                    } catch (Exception ex) {
                        Logger.getInstance("").log(LoggerTag.ERROR, "agentlessCallComplete........exception on [ " + cParam.getMachine() + " ] " + ex.toString(), "");
                    }

                    returnedValue = 0;
                } else {
                    Logger.getInstance("").log(LoggerTag.ERROR, "agentlessCallComplete........call interaction is null on [" + cParam.getMachine() + "]", "");
                }
            }
        } else {
            Logger.getInstance("").log(LoggerTag.ERROR, "agentlessCallComplete........not exist in sessionmap for [" + cParam.getMachine() + "]", "");
            returnedValue = 1;
        }

        return returnedValue;
    }

    /**
     * This method is implemented as such in the C# file
     *
     * @param sender
     * @param args
     */
    private void CampaignStarted(Object sender, CampaignStartedEventArgs args) {
    }

    /**
     * This method is implemented as such in the C# file
     *
     * @param sender
     * @param args
     */
    private void CampaignStopped(Object sender, CampaignStartedEventArgs args) {
    }

    /**
     * This represents the break granted event
     *
     * @param o
     * @param eventArgs
     */
    @Override
    public void breakGranted(Object o, EventArgs eventArgs) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: breakGranted ~~~~~~~~~~~~~~~~~~~~~~","");

        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setResponseType(WsResponseType.TYPE_EVENT);
        //we retrieve the sessionId and pass it to the clientCaller
        //TO CHECK
        //String actualSessionId = eventArgs.getInteraction().getSessionId();
        String actualSessionId = eventArgs.getSession().getSessionId();
        apiResponse.setComment("Break up granted." + actualSessionId);
        apiResponse.setData(eventArgs.getMessage().toString());
        i3WService.getServerEventMessenger().sendMessage(apiResponse);
    }

    /**
     * This represents the preview call added event
     *
     * @param o
     * @param e
     */
    @Override
    public void previewCallAdded(Object o, PreviewCallAddedEventArgs e) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: previewCallAdded ~~~~~~~~~~~~~~~~~~~~~~","");


        String actualSessionId = e.getSession().getSessionId();

        Session[] curSession = {null};
        sessionMap.forEach((s, sess) -> {
            if(sess.getSession().getSessionId().equals(actualSessionId)){
                curSession[0]=sess.getSession();
                SessionInformation sessionInformation=new SessionInformation();
                sessionInformation.setMachineName(s);
                curSession[0].setSessionInformation(sessionInformation);
            }
        });

        if(curSession[0]==null){
            Logger.getInstance("").log(LoggerTag.ERROR,"Session Not Found","");
            return;
        }

        DialerCallInteraction dialerCallInteraction=new DialerCallInteraction();
        dialerCallInteraction.setDialerCallInteractionController(new DialerCallInteractionController(curSession[0]));
        dialerCallInteraction.setInteractionId(e.getInteraction().getInteractionId());
        dialerCallInteraction.setDialingMode(DialingMode.Preview);

        addDialerInteractionToSession((DialerCallInteraction) e.getInteraction());
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setResponseType(WsResponseType.TYPE_EVENT);

        apiResponse.setComment("Preview call Added." + actualSessionId);
        apiResponse.setData(e.getMessage().toString());
        i3WService.getServerEventMessenger().sendMessage(apiResponse);
    }

    /**
     * This represents the data pop event
     *
     * @param o
     * @param e
     */
    @Override
    public void dataPop(Object o, DataPopEventArgs e) {

        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: dataPop ~~~~~~~~~~~~~~~~~~~~~~","");

        //predictive call coming


        //if the dialer call didn't change the status, the previous call maybe a problem
        //this.UpdateUserCallingStatus(e.getInteraction().InteractionsManager.Session, "Do Not Disturb");
        Gson gson = new Gson();
        //DataPopMessage dataPopMessage = gson.fromJson(e.getMessage(), DataPopMessage.class);
        JsonElement element = gson.fromJson(e.getMessage(), JsonElement.class);
        JsonObject object = element.getAsJsonObject();
        JsonObject call=object.get("call").getAsJsonObject();
        String actualSessionId = e.getSession().getSessionId();

        Logger.getInstance("").log(LoggerTag.INFORMATION, "DataPop......dialer interaction id[" + call.get("interactionId").getAsString() + "]", "");

        Session[] curSession = {null};
        sessionMap.forEach((s, sess) -> {
            if(sess.getSession().getSessionId().equals(actualSessionId)){
                curSession[0] = sess.getSession();
                SessionInformation sessionInformation = new SessionInformation();
                sessionInformation.setMachineName(s);
                curSession[0].setSessionInformation(sessionInformation);
            }
        });

        if(curSession[0] == null){
            Logger.getInstance("").log(LoggerTag.ERROR,"Session Not Found","");
            return;
        }

        DialerCallInteraction dialerCallInteraction = new DialerCallInteraction();
        dialerCallInteraction.setDialerCallInteractionController(new DialerCallInteractionController(curSession[0]));
        dialerCallInteraction.setController(new InteractionController(curSession[0]));
        dialerCallInteraction.setInteractionId(call.get("interactionId").getAsString());

        switch (call.get("dialingMode").getAsInt()){
            case 0:
                dialerCallInteraction.setDialingMode(DialingMode.Preview);
                break;

            case 1:
                dialerCallInteraction.setDialingMode(DialingMode.OwnAgentCallback_Preview);
                break;

            case 3:
                dialerCallInteraction.setDialingMode(DialingMode.OwnAgentCallback);
                break;

            case 4:
                dialerCallInteraction.setDialingMode(DialingMode.Agentless);
                break;

            case 5:
                dialerCallInteraction.setDialingMode(DialingMode.Precise);
                break;

            case 6:
                dialerCallInteraction.setDialingMode(DialingMode.Precise);
                break;

            default:
                dialerCallInteraction.setDialingMode(DialingMode.Regular);
                break;
        }


        //addDialerInteractionToSession(dialerCallInteraction);

        //Get debtor_id
        switch (call.get("__type").getAsString()) {
            case "urn:inin.com:dialer:dataPop":
                DataPop dataPop = gson.fromJson(call, DataPop.class);
                dialerCallInteraction.setAttributes(new HashMap<>(dataPop.getAttributes()));

                //Add campaign to list
                if(this.sessionsAvailableCapaigns.get(actualSessionId)==null){
                    AvailableCampaign campaign=new AvailableCampaign();
                    campaign.setId(call.get("campaignId").getAsString());
                    if(dataPop.getAttributes().containsKey(IS_ATTR_CAMPAIGN_NAME)){
                        campaign.setName(dataPop.getAttributes().get(IS_ATTR_CAMPAIGN_NAME));
                        //Always power
                        campaign.setCampaignType(CampaignType.Power);
                    }
                    ArrayList<AvailableCampaign> av=new ArrayList<>();
                    av.add(campaign);
                    this.sessionsAvailableCapaigns.put(actualSessionId,av);

                }


                if (dataPop.getAttributes().containsKey(IS_ATTR_DEBTOR_ID) && !dataPop.getAttributes().get(IS_ATTR_DEBTOR_ID).isEmpty())
                    vertx.eventBus().send("calls_getting", dataPop.getInteractionId() + ":" + dataPop.getAttributes().get(IS_ATTR_DEBTOR_ID));
                break;

            case "urn:inin.com:dialer:previewPop":
                PreviewPop previewPop = gson.fromJson(call, PreviewPop.class);
                dialerCallInteraction.setAttributes(new HashMap<>(previewPop.getAttributes()));
                if (previewPop.getAttributes().containsKey(IS_ATTR_DEBTOR_ID) && !previewPop.getAttributes().get(IS_ATTR_DEBTOR_ID).isEmpty())
                    vertx.eventBus().send("calls_getting", previewPop.getInteractionId() + ":" + previewPop.getAttributes().get(IS_ATTR_DEBTOR_ID));
                break;

            default:
                break;
        }

        addDialerInteractionToSession(dialerCallInteraction);

        //Send to Infinity
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setResponseType(WsResponseType.TYPE_EVENT);
        //we retrieve the sessionId and pass it to the clientCaller
        //TO CHECK
        //String actualSessionId = e.getInteraction().getSessionId();
        apiResponse.setComment("Data popped." + actualSessionId);
        apiResponse.setData(e.getMessage().toString());
        i3WService.getServerEventMessenger().sendMessage(apiResponse);


    }



    @Override
    public void availableCampaigns(Object o, EventArgs e) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: availableCampaigns ~~~~~~~~~~~~~~~~~~~~~~","");

        Session session = e.getSession();
        AvailableCampaignsMessage availableCampaignsMessage = session.getGson().fromJson(e.getMessage(), AvailableCampaignsMessage.class);
        Logger.getInstance("").log(LoggerTag.INFORMATION, "Avalaible campaigns Messages[" + e.getMessage() + "]", "");

        //this.sessionsAvailableCapaigns.put(session.getSessionId(), availableCampaignsMessage.getCampaignsAdded());
        putOrReplaceToMap(this.sessionsAvailableCapaigns,session.getSessionId(),availableCampaignsMessage.getCampaignsAdded());
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setResponseType(WsResponseType.TYPE_EVENT);
        apiResponse.setComment("Campaigns." + session.getSessionId());
        apiResponse.setData(e.getMessage().toString());
        //Here we say, call sendMessage implemented in MainVerticle
        i3WService.getServerEventMessenger().sendMessage(apiResponse);
    }


    /**
     * This method is called to add a dialer call interaction to a session
     *
     * @param dCallInteraction
     */
    private void addDialerInteractionToSession(DialerCallInteraction dCallInteraction) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: addDialerInteractionToSession ~~~~~~~~~~~~~~~~~~~~~~","");

        if (dCallInteraction != null) {
            SessionInformation setting = dCallInteraction.getDialerCallInteractionController().getSession().getSessionInformation();
            if (sessionMap.containsKey(setting.getMachineName())) {
                // session map has the user workstation info
                SessionInteraction sessionInteraction = sessionMap.get(setting.getMachineName());
                sessionInteraction.setIod(ConfigurationUtils.IOD_VALUE_3);  //dialer call
                sessionMap.replace(setting.getMachineName(),sessionInteraction);

                if (sessionMap.get(setting.getMachineName()).isMarkedComplete()) {
                    //no exist dialer call interaction
                    //put the new dialer call interaction
                    sessionInteraction.setMarkedComplete(false);
                    sessionInteraction.setDialerCallInteraction(dCallInteraction);
                    sessionInteraction.setDialerSession(dCallInteraction.getDialerCallInteractionController().getDialerSession());
                    Logger.getInstance("").log(LoggerTag.INFORMATION, "addDialerInteractionToSession dialer call add Interaction [" + dCallInteraction.getInteractionId() + "] mode[" + dCallInteraction.getDialingMode() + "] station[" + setting.getMachineName() + "]" + " sid " + dCallInteraction.getDialerCallInteractionController().getSession().getSessionId(), "");
                    if (dCallInteraction.getDialingMode() == DialingMode.Preview) {
                        //preview campaign - remove added from the queue to the interaction in sessionMap
                        sessionInteraction.setInteraction(null);
                        sessionMap.replace(setting.getMachineName(),sessionInteraction);
                    } else {
                        //*****regular call - pickup
                        try {
                            sessionMap.get(setting.getMachineName()).getDialerCallInteraction().getDialerCallInteractionController().pickUpInteraction(
                                    sessionInteraction.getSession().getSessionId(),
                                    dCallInteraction.getInteractionId(), null);
                        } catch (Exception ex) {
                            Logger.getInstance("").log(LoggerTag.ERROR, "addDialerInteractionToSession..... call pickup exception [" + setting.getMachineName() + "] "
                                    + dCallInteraction.getInteractionId() + " " + ex.getMessage(), "");
                        }
                    }
                } else {
                    //else do nothing (the dialer will automatically transfer the call to other available agent)
                    if (!dCallInteraction.getInteractionId().equals(sessionMap.get(setting.getMachineName()).getDialerCallInteraction().getInteractionId())) {
                        //fix for after IVR call, the agent might not mark the old dailer call complete issue - update 05/23/2012
                        Logger.getInstance("").log(LoggerTag.INFORMATION, "addDialerInteractionToSession dialer call new [" + dCallInteraction.getInteractionId() + "] mode[" + dCallInteraction.getDialingMode() +
                                "] station[" + setting.getMachineName() + "]" + " sid " + dCallInteraction.getDialerCallInteractionController().getSession().getSessionId() + " old one haven't marked complete[" + sessionMap.get(setting.getMachineName()).getDialerCallInteraction().getInteractionId()
                                + "] mode[" + sessionMap.get(setting.getMachineName()).getDialerCallInteraction().getDialingMode() + "] station [" + setting.getMachineName() + "]", "");
                        //mark the old dialer call interaction complete
                           /*
                            sessionMap.get(setting.getMachineName()).getDialerCallInteraction().CallComplete(new CallCompletionParameters(ReasonCode.Ambiguous, "Ambiguous"));
                            //put the new one in the session map
                            sessionMap.get(setting.getMachineName()).MarkedComplete = false;
                            sessionMap.get(setting.getMachineName()).setDialerCallInteraction(dcallInteraction);
                            sessionMap.get(setting.getMachineName()).setDialerSession(dcallInteraction.DialerSession);
                            Logger.getInstance("").log(LoggerTag.INFORMATION,"addDialerInteractionToSession dialer call add Interaction [" + dcallInteraction.InteractionId + "] mode[" + dcallInteraction.DialingMode + "] station[" + setting.getMachineName() + "]" + " sid " + dcallInteraction.InteractionsManager.Session.SessionId);
                            if (dcallInteraction.DialingMode == DialingMode.Preview)
                            {//preview campaign - remove added from the queue to the interaction in sessionMap
                                sessionMap.get(setting.getMachineName()).setInteraction(null);
                            }
                            else
                            {//*****regular call - pickup
                                try
                                {
                                    sessionMap.get(setting.getMachineName()).getDialerCallInteraction().Pickup();
                                }
                                catch (Exception ex)
                                {
                                }
                            }*/
                    }

                    //otherwise the same dialer interaction do not need to add to the session map
                }
            } else {
                Logger.getInstance("").log(LoggerTag.ERROR, "addDialerInteractionToSession- workstation [" + setting.getMachineName() + "] not in sessionMap skip dialer call id[" + dCallInteraction.getInteractionId() + "]", "");
            }
        }
    }

    /**
     * This methods represents the getDialerCallAcctIdAndMode method
     * This function called when the agent logged into the Campaign from infinity
     *
     * @param cParam
     * @return an array of string
     */
    @Override
    public String[] getDialerCallAcctIdAndMode(CompositeType cParam) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: getDialerCallAcctIdAndMode ~~~~~~~~~~~~~~~~~~~~~~","");

        //retrieve the dialer added call interaction account id to be displaced in infinity debtor screen
        String[] ret = new String[4];
        ret[0] = "";
        ret[1] = "";
        ret[2] = "";
        ret[3] = "";

        if (sessionMap.containsKey(cParam.getMachine())) {

            cParam.setUserID(sessionMap.get(cParam.getMachine()).getUser().getUserID());
            cParam.setSessionID(Long.valueOf(sessionMap.get(cParam.getMachine()).getSession().getSessionId()));

            SessionInteraction currentSessionInteraction = sessionMap.get(cParam.getMachine());

            // the call have marked as complete return the value
            if (currentSessionInteraction.getDialerCallInteraction() != null) {
                DialerCallInteraction dialerCallInteraction = currentSessionInteraction.getDialerCallInteraction();
                if (!currentSessionInteraction.isMarkedComplete()) {
                    //the dialer call hasn't marked complete

                    /*Replaced while by this foreach loop*/
                    for (Map.Entry<String, String> entry : dialerCallInteraction.getAttributes().entrySet()) {
                        //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue()); System.out.println("Key = " + entry.getKey());

                        String pairKey = entry.getKey();
                        String pairValue = entry.getValue();

                        if (IS_ATTR_DEBTOR_ID.equals(pairKey)) {
                            ret[0] = pairValue;
                            ret[1] = dialerCallInteraction.getDialingMode().name();
                            //try if phone number and callidkey retrieve here for the campaign
                            if (dialerCallInteraction.getDialingMode() != DialingMode.Preview) {
                                try {
                                    ret[3] = getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(), dialerCallInteraction, ConfigurationUtils.CALL_ID_KEY);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    Logger.getInstance("").log(LoggerTag.ERROR, "getDialerCallAcctIdAndMode....[" + cParam.getMachine() + "] debtor id " + ret[0] + " dialingmode " + ret[1] +
                                            " Error retrieve callid - " + ex.toString(), "");
                                    ret[3] = "";
                                }
                                try {
                                    ret[2] = getInteractionAttribute(currentSessionInteraction.getSession().getSessionId(), dialerCallInteraction, ConfigurationUtils.EIC_REMOTE_ADDRESS);
                                } catch (Exception ix) {
                                    ix.printStackTrace();
                                    Logger.getInstance("").log(LoggerTag.ERROR, "getDialerCallAcctIdAndMode....[" + cParam.getMachine() + "] debtor id " + ret[0] + " dialingmode " + ret[1] +
                                            " Error retrieve Eic_RemoteAddress - " + ix.toString(), "");
                                    ret[2] = "";
                                }
                            } else {
                                ret[2] = "";
                                ret[3] = "";
                            }
                            Logger.getInstance("").log(LoggerTag.INFORMATION, "getDialerCallAcctIdAndMode.....[" + cParam.getMachine() + "] debtor id " + ret[0] + " dialingmode " + ret[1] +
                                    " ---phone " + ret[2] + " callid " + ret[3], "");
                            break;
                        }
                    }
                }
            } else {
                ret[0] = "-1"; //no dialer call interaction
            }
        } else {
            ret[0] = "0";  //user doesn't exist in session map
        }
        return ret;
    }

    /**
     * This method is used to add an interaction to a queue
     *
     * @param session
     */
    private void addInteractionQueue(Session session) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: addInteractionQueue ~~~~~~~~~~~~~~~~~~~~~~","");

        SessionSettings sessionSettings = (SessionSettings) session.getSessionInformation();
        if (sessionMap.containsKey(sessionSettings.getMachineName())) {
            if (sessionMap.get(sessionSettings.getMachineName()).getInteractionQueue() != null) {
                //sessionMap.get(sessionSettings.getMachineName()).getInteractionQueue().stopWatching();
                sessionMap.get(sessionSettings.getMachineName()).setInteractionQueue(null);
            }
        }

        InteractionController controller = new InteractionController();
        controller.setSession(session);

        InteractionQueue interactionQueue = new InteractionQueue(controller, new QueueID(QueueType.user, session.getConnectionResponse().getUserID()));
        //InteractionQueue interactionQueue = new InteractionQueue(controller, new QueueID(QueueType.workgroup, session.getConnectionResponse().getUserID()));

        //interactionQueue.getInteractionQueueController().setMessaging(this.messaging);

        sessionMap.get(sessionSettings.getMachineName()).setInteractionQueue(interactionQueue);
        //Not needed for us
        //interactionQueue.startWatching(new String[] {""});

        //String attributeName = sessionMap.get(sesset.getMachineName()).toString();
    }

    /**
     * This is the interactionsManager_InteractionAutoAnswered event
     *
     * @param o
     * @param e
     */
    @Override
    public void interactionsManager_InteractionAutoAnswered(Object o, InteractionEventArgs e) {
        Logger.getInstance("").log(LoggerTag.DEBUG,"~~~~~~~~~~~~~~~~~~~~~~START: interactionsManager_InteractionAutoAnswered ~~~~~~~~~~~~~~~~~~~~~~","");

        //Send a message to main Verticle
        //We can also send eventArgs if we have specifics actions to do with it
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setResponseType(WsResponseType.TYPE_EVENT);
        //we retrieve the sessionId and pass it to the clientCaller
        //TO CHECK
        //String actualSessionId = e.getInteraction().getSessionId();
        String actualSessionId = e.getSession().getSessionId();
        apiResponse.setComment("Auto answer got." + actualSessionId);
        apiResponse.setData(e.getMessage().toString());
        i3WService.getServerEventMessenger().sendMessage(apiResponse);
    }

    /**
     * This is the logoutGranted event
     *
     * @param o
     * @param eventArgs
     */
    @Override
    public void logoutGranted(Object o, EventArgs eventArgs) {
        //Send a message to main Verticle
        //We can also send eventArgs if we have specifics actions to do with it
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setResponseType(WsResponseType.TYPE_EVENT);
        //we retrieve the sessionId and pass it to the clientCaller
        //TO CHECK
        //String actualSessionId = eventArgs.getInteraction().getSessionId();
        String actualSessionId = eventArgs.getSession().getSessionId();
        apiResponse.setComment("Logged out." + actualSessionId);
        apiResponse.setData(eventArgs.getMessage().toString());
        //Here we say, call sendMessage implemented in MainVerticle
        i3WService.getServerEventMessenger().sendMessage(apiResponse);
    }

    /**
     * This is the logoutGranted event
     *
     * @param o
     * @param eventArgs
     */
    @Override
    public void genericEvent(Object o, EventArgs eventArgs) {
        //Send a message to main Verticle
        //We can also send eventArgs if we have specifics actions to do with it
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setResponseType(WsResponseType.TYPE_EVENT);
        //we retrieve the sessionId and pass it to the clientCaller
        //TO CHECK
        //String actualSessionId = eventArgs.getInteraction().getSessionId();
        String actualSessionId = eventArgs.getSession().getSessionId();
        apiResponse.setComment("New Event." + actualSessionId);
        apiResponse.setData(eventArgs.getMessage().toString());
        //Here we say, call sendMessage implemented in MainVerticle
        i3WService.getServerEventMessenger().sendMessage(apiResponse);
    }

    @Override
    public void shouldDisconnectSession(Object o, Session session) {
        Logger.getInstance("").log(LoggerTag.INFORMATION,"Disconnecting user after 03 heartbeat failure","");
        this.logoff(session);
    }

    /**
     * This method is used for sending messages...
     *
     * @return ServerEventMessenger
     */
    public ServerEventMessenger getServerEventMessenger() {
        return serverEventMessenger;
    }

    /**
     * This is a setter for ServerEventMessenger attribute
     *
     * @param serverEventMessenger
     */
    public void setServerEventMessenger(ServerEventMessenger serverEventMessenger) {
        this.serverEventMessenger = serverEventMessenger;
    }

    public HashMap<String, List<AvailableCampaign>> getSessionsAvailableCapaigns() {
        return sessionsAvailableCapaigns;
    }

    public void setSessionsAvailableCapaigns(HashMap<String, List<AvailableCampaign>> sessionsAvailableCapaigns) {
        this.sessionsAvailableCapaigns = sessionsAvailableCapaigns;
    }

    /**
     * This represents the ServerEventMessenger interface
     */
    public interface ServerEventMessenger {
        void sendMessage(ApiResponse o);
    }


    /* Custom interaction's attributes getter*/

    private String getInteractionAttribute(String sessionId, Interaction interaction, String attributeName) {
        String attr = "";
        if (interaction.getAttributes() != null || !interaction.getAttributes().isEmpty()) {
            if (interaction.getAttributes().containsKey(attributeName))
                attr = interaction.getAttributes().get(attributeName);
        }

        if (attr.isEmpty()) {
            attr = interaction.getController().getInteractionAttributes(sessionId, attributeName, interaction.getInteractionId(), null).get(attributeName);
            if (attr.isEmpty() && attributeName.equals(IS_ATTR_DEBTOR_ID))
                attr = interaction.getController().getInteractionAttributes(sessionId, ConfigurationUtils.IMC_DEBTOR_ID, interaction.getInteractionId(), null).get(ConfigurationUtils.IMC_DEBTOR_ID);
        }

        //ensure we add new attribute got from server to
        //HashMap<String,String> attr=interaction.getAttributes();
        //attr.putIfAbsent(attributeName,curAttr);
        //interaction.setAttributes(attr);
        return attr;
    }


    private void putOrReplaceToMap(Map<String,List<AvailableCampaign>> map,String key,List<AvailableCampaign> value){
        if (map.containsKey(key)) {
            map.replace(key, value);
        } else {
            map.put(key,value);
        }
    }

    private boolean doesUserHaveManagerRole(List<String> userRolesList) {
        if (userRolesList == null || userRolesList.isEmpty()) {
            return false;
        }

        for (String currentRole : userRolesList) {
            if (currentRole.trim().toLowerCase().equals(managerRole)) {
                Logger.getInstance("").log(LoggerTag.DEBUG,"The user is a manager, has role: "+currentRole,"");
                return true;
            }
        }
        return false;
    }
}