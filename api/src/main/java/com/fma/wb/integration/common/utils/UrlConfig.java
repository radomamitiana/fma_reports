package com.fma.wb.integration.common.utils;

/**
 * This contains all URIs mapping (Infinity --> web service)
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class UrlConfig {
    public static final String GET_ACTIVE_USERS_LINK = "/ws/activeuserlist";

    //Mappings related to the dialer
    public static final String LOGON_CAMPAIGN = "/ws/logonCampaign";
    public static final String RECEIVE_NEW_DIALER_CALL = "/ws/receiveNewDialerCall";
    public static final String GET_USERS_INFO = "/ws/getUsersInfo";
    public static final String DISCONNECT_DIALER_CALL = "/ws/disconnectDialerCall";
    public static final String GET_DIALER_CALL_ACCT_ID_AND_MODE = "/ws/getDialerCallAcctIdAndMode";

    //Mappings related to the interactions
    public static final String GET_AVAILABLE_CAMPAIGN = "/ws/getAvailableCampaign";
    public static final String MARK_DIALER_CALL_COMPLETE = "/ws/markDialerCallComplete";
    public static final String GET_INCOMING_CALL_INFO = "/ws/getIncomingCallInfo";
    public static final String BEGIN_PLAY_AUDIO = "/ws/beginPlayAudio";
    public static final String RECORDING_CALL = "/ws/recordingCall";
    public static final String RECORDING_SWITCH = "/ws/recordingSwitch";
    public static final String HAS_INCOMING_CALL = "/ws/hasIncomingCall";
    public static final String MAKE_PHONE_CALL = "/ws/makePhoneCall";
    public static final String MAKE_CONFERENCE_CALL = "/ws/makeConferenceCall";
    public static final String CONNECT_CONF_CALL  = "/ws/connectConfCall";
    public static final String NO_CONF_CALL  = "/ws/noConfCall";
    public static final String MAKE_TRANSFER_CALL  = "/ws/makeTransferCall";
    public static final String CONNECT_TRANSFER_CALL  = "/ws/connectTransferCall";
    public static final String DISCONNECT_TRANSFER_CALL  = "/ws/disconnectTransferCall";
    public static final String DISCONNECT_CALL  = "/ws/disconnectCall";
    public static final String AGENT_LESS_CALL_COMPLETE  = "/ws/agentLessCallComplete";

    //Mappings related to the sessions
    public static final String SET_I3AVAILABLE_STATUS = "/ws/setI3AvailableStatus";
    public static final String LOG_OFF_CAMPAIGN = "/ws/logoffCampaign";
    public static final String GET_SESSION_MANAGER_CONNECTED_SESSION = "/ws/getSessionManagerConnectedSession";
    public static final String LOG_OFF = "/ws/logoff";
    public static final String ADMIN_LOGOUT_USER = "/ws/adminLogoutUser";

    //Mappings related to TrustBalancing report
    public static final String LOGIN_BACK_OFFICE_AND_REPORTS_APP = "/ws/loginBackOfficeAndReportsApp";
    public static final String LOGOUT_BACK_OFFICE_AND_REPORTS_APP = "/ws/auth/logoutBackOfficeAndReportsApp";
    public static final String GET_OPTIONS_LIST = "/ws/auth/getOptionsList";
    public static final String GET_ORGANIZATION_LIST = "/ws/auth/getOrganizationList";
    public static final String GET_ORGANIZATION_LIST2 = "/ws/auth/getHealthOrganizationList";
    public static final String GET_TRUST_BALANCING_FORM_DATA = "/ws/auth/getTrustBalancingData";

    //Mappings related to healthcare report
    public static final String GET_HEALTH_DATA = "/ws/auth/getHealthcareData";
    public static final String GET_COMMENTED_HEALTH_DATA = "/ws/auth/getCommentedHealthData";

    //Mappings related to Split time report
    public static final String GET_SPLIT_TIME_REPORT_DATA = "/ws/auth/getSplitTimeReportData";

    //Mappings related to quick book balancing report
    public static final String GET_ORGANIZATION_QUICKBOOK_LIST = "/ws/auth/getOrganizationQuickBookList";
    public static final String GET_CLIENT_ORGANIZATION_QUICK = "/ws/auth/getClientOrganizationQuick";
    public static final String SAVE_COLLECTIONS_PROCESSING_BY_ALL_CLIENT = "/ws/auth/saveCollectionsProcessingByAllClient";
    public static final String SAVE_COLLECTIONS_PROCESSING_BY_ONE_CLIENT = "/ws/auth/saveCollectionsProcessingByOneClient";
    public static final String GET_COLLECTIONS_PROCESSING = "/ws/auth/getCollectionsProcessing";
    public static final String SAVE_OFFSET = "/ws/auth/saveOffset";
    public static final String GET_OFFSET = "/ws/auth/getOffset";
    public static final String PRINT_PROC_INSTR = "/ws/auth/printProcInstr";
    public static final String QUICK_BOOK_BALANCING_REPORT_DATA = "/ws/auth/quickBookBalancingReportData";
}
