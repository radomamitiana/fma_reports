package com.fma.wb.integration.i3module.dto;

import java.io.Serializable;

/**
 * This model is going to be used for doing the mapping with the Invoice table within Infinity database
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class Invoice implements Serializable {
    private String client_account_number;
    private String invoice_id;

    public String getClient_account_number() {
        return client_account_number;
    }

    public void setClient_account_number(String client_account_number) {
        this.client_account_number = client_account_number;
    }

    public String getInvoice_id() {
        return invoice_id;
    }

    public void setInvoice_id(String invoice_id) {
        this.invoice_id = invoice_id;
    }

    /**
     * Constructor without parameter
     */
    public Invoice() {
    }

    /**
     * Constructor with parameters
     * @param client_account_number
     * @param invoice_id
     */
    public Invoice(String client_account_number, String invoice_id) {
        this.client_account_number = client_account_number;
        this.invoice_id = invoice_id;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "client_account_number='" + client_account_number + '\'' +
                ", invoice_id='" + invoice_id + '\'' +
                '}';
    }
}