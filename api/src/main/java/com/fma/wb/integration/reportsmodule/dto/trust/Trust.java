package com.fma.wb.integration.reportsmodule.dto.trust;

import java.io.Serializable;

/**
 * This model is going to be used for doing the mapping with the Trust table within Infinity database
 * @author Tiana Andriamanalina
 * @version 1.0
 */
public class Trust implements Serializable {
    private int trust_id;
    private String name;
    private String active;
    private String account_number;

    public int getTrust_id() {
        return trust_id;
    }

    public void setTrust_id(int trust_id) {
        this.trust_id = trust_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    /**
     * Constructor without parameter
     */
    public Trust() {
    }

    /**
     * Constructor with parameters
     * @param trust_id
     * @param name
     * @param active
     * @param account_number
     */
    public Trust(int trust_id, String name, String active, String account_number) {
        this.trust_id = trust_id;
        this.name = name;
        this.active = active;
        this.account_number = account_number;
    }

    @Override
    public String toString() {
        return "Trust{" +
                "trust_id=" + trust_id +
                ", name='" + name + '\'' +
                ", active='" + active + '\'' +
                ", account_number='" + account_number + '\'' +
                '}';
    }
}