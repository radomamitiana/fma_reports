package com.fma.wb.integration.reportsmodule.dto.trust;

import java.io.Serializable;

public class TrustBalancingOutputInfinity  implements Serializable {
    private String date = "";
    private String transactionType = "";
    private double amount;
    private int monetaryTransactionId;
    private int reversingId;
    private String debtorId = "";
    private String firstName = "";
    private String lastName = "";
    private String active = "";
    private int organizationId;
    private String clientId = "";
   //private String clientIdentifier = "";
    private String typeName = "";
    private String clientAccountNumber = "";
    //private String description;
    //private String nsf;
    private String infinity;
    private int paymentApplicationId;
    private String busStream = "";
    private String trustAccountNumber = "";
    private String sumAmount = "";
    private String clientIdentifier = "";
    private String type = "";

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getMonetaryTransactionId() {
        return monetaryTransactionId;
    }

    public void setMonetaryTransactionId(int monetaryTransactionId) {
        this.monetaryTransactionId = monetaryTransactionId;
    }

    public int getReversingId() {
        return reversingId;
    }

    public void setReversingId(int reversingId) {
        this.reversingId = reversingId;
    }

    public String getDebtorId() {
        return debtorId;
    }

    public void setDebtorId(String debtorId) {
        this.debtorId = debtorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getClientAccountNumber() {
        return clientAccountNumber;
    }

    public void setClientAccountNumber(String clientAccountNumber) {
        this.clientAccountNumber = clientAccountNumber;
    }

    public String getInfinity() {
        return infinity;
    }

    public void setInfinity(String infinity) {
        this.infinity = infinity;
    }

    public int getPaymentApplicationId() {
        return paymentApplicationId;
    }

    public void setPaymentApplicationId(int paymentApplicationId) {
        this.paymentApplicationId = paymentApplicationId;
    }

    public String getBusStream() {
        return busStream;
    }

    public void setBusStream(String busStream) {
        this.busStream = busStream;
    }

    public String getTrustAccountNumber() {
        return trustAccountNumber;
    }

    public void setTrustAccountNumber(String trustAccountNumber) {
        this.trustAccountNumber = trustAccountNumber;
    }

    public String getSumAmount() {
        return sumAmount;
    }

    public void setSumAmount(String sumAmount) {
        this.sumAmount = sumAmount;
    }

    public String getClientIdentifier() {
        return clientIdentifier;
    }

    public void setClientIdentifier(String clientIdentifier) {
        this.clientIdentifier = clientIdentifier;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public TrustBalancingOutputInfinity() {
    }

    @Override
    public String toString() {
        return "TrustBalancingOutputInfinity{" +
                "date='" + date + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", amount=" + amount +
                ", monetaryTransactionId=" + monetaryTransactionId +
                ", reversingId=" + reversingId +
                ", debtorId='" + debtorId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", active='" + active + '\'' +
                ", organizationId=" + organizationId +
                ", clientId='" + clientId + '\'' +
                ", typeName='" + typeName + '\'' +
                ", clientAccountNumber='" + clientAccountNumber + '\'' +
                ", infinity='" + infinity + '\'' +
                ", paymentApplicationId=" + paymentApplicationId +
                ", busStream='" + busStream + '\'' +
                ", trustAccountNumber='" + trustAccountNumber + '\'' +
                ", sumAmount='" + sumAmount + '\'' +
                ", clientIdentifier='" + clientIdentifier + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}