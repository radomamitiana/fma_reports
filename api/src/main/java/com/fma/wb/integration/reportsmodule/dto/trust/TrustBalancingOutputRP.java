package com.fma.wb.integration.reportsmodule.dto.trust;

import java.io.Serializable;

public class TrustBalancingOutputRP implements Serializable {

    private String amount;
    private String account = "";
    private String pocketGroup = "";
    private String transType = "";
    private String jobKey = "";

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPocketGroup() {
        return pocketGroup;
    }

    public void setPocketGroup(String pocketGroup) {
        this.pocketGroup = pocketGroup;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getJobKey() {
        return jobKey;
    }

    public void setJobKey(String jobKey) {
        this.jobKey = jobKey;
    }

    public TrustBalancingOutputRP() {
    }

    @Override
    public String toString() {
        return "TrustBalancingOutputRP{" +
                "amount=" + amount +
                ", account='" + account + '\'' +
                ", pocketGroup='" + pocketGroup + '\'' +
                ", transType='" + transType + '\'' +
                ", jobKey='" + jobKey + '\'' +
                '}';
    }

    //This is the column RP DATE of the generated output Excel file
    /*private String rpDate = "";
    //This is the column ACH of the generated output Excel file. This represents the ACH payment type
    //Two numbers after comma(Ex: $135.15 or $250.00)
    private float ach=0;
    //This is the column ICL of the generated output Excel file. This represents the ICL payment type
    //Two numbers after comma(Ex: $135.15 or $250.00)
    private float icl=0;

    private float rpSolutions = 0;
    private String officeCode = "";

    public String getRpDate() {
        return rpDate;
    }

    public void setRpDate(String rpDate) {
        this.rpDate = rpDate;
    }

    public float getAch() {
        return ach;
    }

    public void setAch(float ach) {
        this.ach = ach;
    }

    public float getIcl() {
        return icl;
    }

    public void setIcl(float icl) {
        this.icl = icl;
    }

    public float getRpSolutions() {
        return rpSolutions;
    }

    public void setRpSolutions(float rpSolutions) {
        this.rpSolutions = rpSolutions;
    }

    public String getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(String officeCode) {
        this.officeCode = officeCode;
    }

    public TrustBalancingOutputRP() {
    }

    public TrustBalancingOutputRP(String rpDate, float ach, float icl, float rpSolutions, String officeCode) {
        this.rpDate = rpDate;
        this.ach = ach;
        this.icl = icl;
        this.rpSolutions = rpSolutions;
        this.officeCode = officeCode;
    }

    @Override
    public String toString() {
        return "TrustBalancingOutputRP{" +
                "rpDate='" + rpDate + '\'' +
                ", ach=" + ach +
                ", icl=" + icl +
                ", rpSolutions=" + rpSolutions +
                ", officeCode='" + officeCode + '\'' +
                '}';
    }*/
}