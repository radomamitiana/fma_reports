package com.fma.wb.integration.reportsmodule.utils;

public class UtilsTrustBalancing {
    //INFINITY data related
    public static final String INFINITY_DATE_KEY= "infinityDate";
    public static final String DEBTOR_ID_KEY = "debtorId";
    public static final String FIRST_NAME_KEY = "firstName";
    public static final String LAST_NAME_KEY = "lastName";
    public static final String CLIENT_ACCT_NUMBER_KEY = "clientAccountNumber";
    public static final String ORGANIZATION_ID_KEY = "organizationId";
    public static final String PAYMENT_APPLICATION_ID_KEY = "paymentApplicationId";
    public static final String BUS_STREAM = "busStream";
    public static final String UNKNOWN_KEY = "unknown";
    public static final String CC_KEY = "cc";
    public static final String MANUAL_KEY = "manual";
    public static final String NSF_KEY = "nsf";
    public static final String INFINITY_KEY = "infinity";
    public static final String TRUST_ACCOUNT_NUMBER = "trustAccountNumber";
    public static final String SUM_INFINITY_AMOUNT = "sumAmount";

    //RP data related
    public static final String RP_DATE_KEY = "rpDate";
    public static final String ACH_KEY = "ach";
    public static final String ICL_KEY = "icl";
    public static final String RP_SOLUTIONS_KEY = "rpSolutions";
    public static final String MATCH_FOUND = "matchFound";

    public static final String NON_SUFFICIENT_FOND = "RA";
    public static final String ACH_ACCT_NUMBER_PREFIX = "ACH-TA";
    public static final String ICL_ACCT_NUMBER_PREFIX = "ICL-TA";
}
