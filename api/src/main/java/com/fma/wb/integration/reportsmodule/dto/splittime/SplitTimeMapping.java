package com.fma.wb.integration.reportsmodule.dto.splittime;

import java.io.Serializable;
import java.math.BigInteger;

public class SplitTimeMapping implements Serializable {
    private String localUserId = "";
    private String homeDept = "";
    private String fullPt = "";
    //This is the old factor
    private String factor = "";
    private String manager = "";
    private BigInteger durationMin = BigInteger.valueOf(0);
    private String numberOfCalls = "";
    private String fullName = "";

    public String getLocalUserId() {
        return localUserId;
    }

    public void setLocalUserId(String localUserId) {
        this.localUserId = localUserId;
    }

    public String getHomeDept() {
        return homeDept;
    }

    public void setHomeDept(String homeDept) {
        this.homeDept = homeDept;
    }

    public String getFullPt() {
        return fullPt;
    }

    public void setFullPt(String fullPt) {
        this.fullPt = fullPt;
    }

    public String getFactor() {
        return factor;
    }

    public void setFactor(String factor) {
        this.factor = factor;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public BigInteger getDurationMin() {
        return durationMin;
    }

    public void setDurationMin(BigInteger durationMin) {
        this.durationMin = durationMin;
    }

    public String getNumberOfCalls() {
        return numberOfCalls;
    }

    public void setNumberOfCalls(String numberOfCalls) {
        this.numberOfCalls = numberOfCalls;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public SplitTimeMapping() {
    }

    @Override
    public String toString() {
        return "SplitTimeMapping{" +
                "localUserId='" + localUserId + '\'' +
                ", homeDept='" + homeDept + '\'' +
                ", fullPt='" + fullPt + '\'' +
                ", factor='" + factor + '\'' +
                ", manager='" + manager + '\'' +
                ", durationMin=" + durationMin +
                ", numberOfCalls='" + numberOfCalls + '\'' +
                ", fullName='" + fullName + '\'' +
                '}';
    }
}