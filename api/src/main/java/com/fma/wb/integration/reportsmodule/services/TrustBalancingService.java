package com.fma.wb.integration.reportsmodule.services;

import com.fma.wb.integration.common.dao.DbConnexionHelper;
import com.fma.wb.integration.common.dto.User;
import com.fma.wb.integration.common.dto.api.InternalResponse;
import com.fma.wb.integration.common.dto.enums.DBType;
import com.fma.wb.integration.common.utils.ConfigurationUtils;
import com.fma.wb.integration.common.utils.Utils;
import com.fma.wb.integration.reportsmodule.dto.DbUser;
import com.fma.wb.integration.reportsmodule.dto.Option;
import com.fma.wb.integration.reportsmodule.dto.Organization;
import com.fma.wb.integration.reportsmodule.dto.trust.*;
import com.fma.wb.integration.reportsmodule.utils.AvOptions;
import com.fma.wb.integration.reportsmodule.utils.BlowfishEncryptionUtil;
import com.fma.wb.integration.reportsmodule.utils.UtilsTrustBalancing;
import common.Logger;
import common.LoggerTag;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.SQLConnection;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.internal.util.ExceptionUtils;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.fma.wb.integration.common.utils.ConfigurationUtils.*;
import static com.fma.wb.integration.reportsmodule.utils.CommonBackOfficeAndReportingQueries.*;

/**
 * This class is going to be the service that is going to implement all actions of the MainReportVerticle
 *
 * @author Tiana Andriamanalina
 * @version 1.0
 */

public class TrustBalancingService {
    private static TrustBalancingService trustBalancingService;
    private Vertx vertx;
    private static HashMap<String, User> activeSessions = new HashMap<>();
    private HashMap<String, TrustBalancingOutputData> trustHistoryPerSession = new HashMap<>();

    private static DbConnexionHelper dbConnexionHelper;
    private static HashMap<String, String> credentials = new HashMap<>();
    private List<Organization> organizationsListGlobal = new ArrayList<>();
    //Build a map having organization id as key and value contains trust and clients for the given organization id
    private final Map<Integer, TrustOrganizationClientsGrouped> resultMap = new LinkedHashMap<>();
    private String selectedTrustAcctNumber = "";

    public DbConnexionHelper getDbConnexionHelper() {
        return dbConnexionHelper;
    }

    private void setDbConnexionHelper(DbConnexionHelper db) {
        dbConnexionHelper = db;
    }

    public static HashMap<String, User> getActiveSessions() {
        return activeSessions;
    }

    public void setActiveSessions(HashMap<String, User> activeSessionsG) {
        activeSessions = activeSessionsG;
    }

    public HashMap<String, TrustBalancingOutputData> getTrustHistoryPerSession() {
        return trustHistoryPerSession;
    }

    public void setTrustHistoryPerSession(HashMap<String, TrustBalancingOutputData> trustHistoryPerSession) {
        this.trustHistoryPerSession = trustHistoryPerSession;
    }

    /**
     * This constructor accepts no parameters and will initialize vertx properties.
     */
    public TrustBalancingService() {
        this.vertx = Vertx.vertx();
    }

    /**
     * This constructor with parameter
     *
     * @param vertx
     */
    public TrustBalancingService(Vertx vertx) {
        this.vertx = vertx;
        this.setDbConnexionHelper(DbConnexionHelper.getInstance(vertx, credentials));

    }

    /**
     * This method returns an instance of TrustBalancingService
     *
     * @param vertx
     * @return TrustBalancingService instance
     */
    public static TrustBalancingService getInstance(Vertx vertx) {
        if (trustBalancingService == null) {
            trustBalancingService = new TrustBalancingService(vertx);
        }

        return trustBalancingService;
    }

    /**
     * This method allows the user to login to the BO & Report application
     * It allows the user to access to all his/her available options
     * depending on his/her roles
     *
     * @param user
     * @return a long value representing the sessionId
     */
    public void loginBackOfficeAndReportsApp(User user, Handler<User> handler) {

        Logger.getInstance("").log(LoggerTag.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~START: loginBackOfficeAndReportsApp ~~~~~~~~~~~~~~~~~~~~~~", "");
        final User[] returnUser = {null};
        activeSessions.forEach((s, user1) -> {
            if (user1.getUsername().equals(user.getUsername()) && user1.getPassword().equals(user.getPassword()) && user.isConnected()) {
                returnUser[0] = user1;
            }
        });

        if (returnUser[0] != null) {
            handler.handle(returnUser[0]);
        } else if (user.getUsername().toLowerCase().equals("test") && user.getPassword().toLowerCase().equals("test")) {
            String encrypted = BlowfishEncryptionUtil.getInstance().encrypt("root");
            System.out.println("password encrypted -> " + encrypted);
            user.setConnected(true);
            user.addRole(1);
            user.addRole(2);
            user.addRole(3);
            user.addRole(4);

            String currentGeneratedSessionId = UUID.randomUUID().toString();
            user.setSessionId(currentGeneratedSessionId);
            activeSessions.put(currentGeneratedSessionId, user);
            user.setConnected(true);
            handler.handle(user);
        } else {

            try {
                if (dbConnexionHelper.getFmaConnection() == null)
                    connectToDb(DBType.Infinity, event -> {

                        if (event.succeeded()) {
                            dbConnexionHelper.setFmaConnection(event.result());
                            dbConnexionHelper.getFmaConnection().query(String.format(USER_QUERY, user.getUsername().trim()), result -> {

                                if (result.succeeded()) {
                                    if (result.result().getNumRows() > 0) {
                                        result.result().getRows().forEach(entries -> {
                                            final User userGot = DbConnexionHelper.returnQueryObject(entries, User.class);
                                            System.out.println("zz:" + Json.encodePrettily(userGot));
                                            String encrypted = BlowfishEncryptionUtil.getInstance().decrypt(userGot.getPassword()).toLowerCase().trim();
                                            System.out.println("zz2: " + encrypted + " " + user.getPassword().toLowerCase());

                                            if (encrypted.equals(user.getPassword().trim().toLowerCase())) {

                                                dbConnexionHelper.getFmaConnection().query(String.format(USER_ROLES_QUERY, userGot.getUser_id()), rolesRes -> {
                                                    if (rolesRes.succeeded()) {

                                                        List<Integer> roles = new ArrayList<>();

                                                        System.out.println("EE:" + rolesRes.result().getRows().toString());
                                                        rolesRes.result().getRows().forEach(entries1 -> {
                                                            roles.add(entries1.getInteger("role_id"));
                                                        });

                                                        userGot.setRoles(roles);
                                                    } else {
                                                        Logger.getInstance("").log(LoggerTag.WARNING, "Can't get Roles for user: " + user.getUsername() + "\n" + ExceptionUtils.exceptionStackTraceAsString(rolesRes.cause()), "");

                                                    }

                                                    String currentGeneratedSessionId = UUID.randomUUID().toString();
                                                    userGot.setSessionId(currentGeneratedSessionId);
                                                    activeSessions.put(currentGeneratedSessionId, userGot);
                                                    userGot.setConnected(true);
                                                    handler.handle(userGot);

                                                });

                                                Logger.getInstance("").log(LoggerTag.INFORMATION, "User logged in: " + user.getUsername(), "");

                                            } else {
                                                Logger.getInstance("").log(LoggerTag.DEBUG, "Wrong password: " + user.getUsername(), "");
                                                user.setConnected(false);
                                                handler.handle(user);
                                            }
                                        });
                                    } else {
                                        Logger.getInstance("").log(LoggerTag.DEBUG, "No match user found: " + user.getUsername(), "");
                                        user.setConnected(false);
                                        handler.handle(user);
                                    }
                                } else {
                                    Logger.getInstance("").log(LoggerTag.ERROR, "Error getting user from Infinity: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
                                    handler.handle(null);
                                }
                            });

                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, "logging to Infinity: " + ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                            handler.handle(null);
                        }

                    }, dbConnexionHelper);
                else {
                    dbConnexionHelper.getFmaConnection().query(String.format(USER_QUERY, user.getUsername().trim()), result -> {

                        if (result.succeeded()) {
                            if (result.result().getNumRows() > 0) {
                                result.result().getRows().forEach(entries -> {
                                    final User userGot = DbConnexionHelper.returnQueryObject(entries, User.class);
                                    System.out.println("zz:" + Json.encodePrettily(userGot));
                                    String encrypted = BlowfishEncryptionUtil.getInstance().decrypt(userGot.getPassword()).toLowerCase().trim();
                                    System.out.println("zz2: " + encrypted + " " + user.getPassword().toLowerCase());

                                    if (encrypted.equals(user.getPassword().trim().toLowerCase())) {

                                        dbConnexionHelper.getFmaConnection().query(String.format(USER_ROLES_QUERY, userGot.getUser_id()), rolesRes -> {
                                            if (rolesRes.succeeded()) {

                                                List<Integer> roles = new ArrayList<>();

                                                System.out.println("EE:" + rolesRes.result().getRows().toString());
                                                rolesRes.result().getRows().forEach(entries1 -> {
                                                    roles.add(entries1.getInteger("role_id"));
                                                });

                                                userGot.setRoles(roles);
                                            } else {
                                                Logger.getInstance("").log(LoggerTag.WARNING, "Can't get Roles for user: " + user.getUsername() + "\n" + ExceptionUtils.exceptionStackTraceAsString(rolesRes.cause()), "");

                                            }

                                            String currentGeneratedSessionId = UUID.randomUUID().toString();
                                            userGot.setSessionId(currentGeneratedSessionId);
                                            activeSessions.put(currentGeneratedSessionId, userGot);
                                            userGot.setConnected(true);
                                            handler.handle(userGot);

                                        });

                                        Logger.getInstance("").log(LoggerTag.INFORMATION, "User logged in: " + user.getUsername(), "");

                                    } else {
                                        Logger.getInstance("").log(LoggerTag.DEBUG, "Wrong password: " + user.getUsername(), "");
                                        user.setConnected(false);
                                        handler.handle(user);
                                    }
                                });
                            } else {
                                Logger.getInstance("").log(LoggerTag.DEBUG, "No match user found: " + user.getUsername(), "");
                                user.setConnected(false);
                                handler.handle(user);
                            }
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, "Error getting user from Infinity: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
                            handler.handle(null);
                        }
                    });

                }
            } catch (SQLException | ClassNotFoundException e) {
                Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e), "");
                handler.handle(null);
            }
        }

    }

    /**
     * This method allows the user to logout from the BO & Report application
     *
     * @param sessionId
     * @return a boolean value
     */
    public boolean logoutBackOfficeAndReportsApp(String sessionId) {

        Logger.getInstance("").log(LoggerTag.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~START: logoutBackOfficeAndReportsApp ~~~~~~~~~~~~~~~~~~~~~~", "");

        System.out.println("SESS: " + Json.encodePrettily(activeSessions));
        System.out.println("Id: " + sessionId);
        if (activeSessions.containsKey(sessionId)) {
            activeSessions.remove(sessionId);
            if ((this.trustHistoryPerSession.containsKey(sessionId)))
                this.trustHistoryPerSession.remove(sessionId);

            return true;
        } else {
            return false;
        }

    }

    /**
     * This method returns the list of all available options that cab be accessible for a given connected user
     *
     * @param sessionId
     * @return a array of options name
     */
    public void getOptionsList(String sessionId, Handler<ArrayList<Option>> handler) {
        Logger.getInstance("").log(LoggerTag.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~START: getOptionsList ~~~~~~~~~~~~~~~~~~~~~~", "");
        System.out.println("SESS: " + Json.encodePrettily(activeSessions));
        System.out.println("Id: " + sessionId);

        if (!activeSessions.containsKey(sessionId)) {
            handler.handle(null);
        } else {
            AvOptions avOptions = new AvOptions();
            handler.handle(avOptions.getUserOptions(activeSessions.get(sessionId)));
        }


    }

    /**
     * This method returns the list of all organizations that will be displayed and selected
     * from the trust balancing application and retrieved from Infinity database
     *
     * @return a list of organization name
     */
    public void getOrganizationList(String sessionId, Handler<ArrayList<String>> handler) {

        if (!activeSessions.containsKey(sessionId)) {
            handler.handle(null);
        } else {

            ArrayList<String> organizationsList = new ArrayList<>();
            Logger.getInstance("").log(LoggerTag.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~START: getOrganizationList ~~~~~~~~~~~~~~~~~~~~~~", "");
            Map<Integer, String> organizationGotMap = new LinkedHashMap<>();
            try {
                if (dbConnexionHelper.getFmaConnection() == null) {
                    connectToDb(DBType.Infinity, event -> {

                        if (event.succeeded()) {
                            dbConnexionHelper.setFmaConnection(event.result());
                            dbConnexionHelper.getFmaConnection().query(GET_ORGANIZATION_INFO, result -> {
                                System.out.println("RESULT => " + result);
                                if (result.succeeded()) {
                                    result.result().getRows().forEach(entries -> {
                                        final Organization orgGot = DbConnexionHelper.returnQueryObject(entries, Organization.class);
                                        //organizationsList.add(orgGot.getName());
                                        organizationGotMap.put(orgGot.getOrganization_id(), orgGot.getName().toUpperCase());
                                    });
                                    for (Map.Entry<Integer, String> organizationIdAndName : organizationGotMap.entrySet()) {
                                        organizationsList.add(organizationIdAndName.getValue());
                                        Organization tempOrg = new Organization();
                                        tempOrg.setOrganization_id(organizationIdAndName.getKey());
                                        tempOrg.setName(organizationIdAndName.getValue());
                                        organizationsListGlobal.add(tempOrg);
                                    }

                                } else {
                                    System.out.println("Result NOT succeded  " + result.result().getRows());
                                    Logger.getInstance("").log(LoggerTag.ERROR, "Error getting organization list: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
                                    organizationsList.add("ERR");
                                }
                                //This is just for unit testing
                                /*System.out.println("==================================== Connection not already exist. Here is the list of organization ====================================");
                                for (String org : organizationsList) {
                                    System.out.println(org);
                                }*/
                                handler.handle(organizationsList);
                            });

                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, "Error logging to Infinity: " + ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                            handler.handle(null);
                        }

                    }, dbConnexionHelper);
                } else {
                    dbConnexionHelper.getFmaConnection().query(GET_ORGANIZATION_INFO, result -> {
                        //dbConnexionHelper.getFmaConnection().query(ORGANIZATIONS_QUERY, result -> {
                        if (result.succeeded()) {
                            result.result().getRows().forEach(entries -> {
                                final Organization orgGot = DbConnexionHelper.returnQueryObject(entries, Organization.class);
                                //organizationsList.add(orgGot.getName());
                                organizationGotMap.put(orgGot.getOrganization_id(), orgGot.getName().toUpperCase());
                            });
                            for (Map.Entry<Integer, String> organizationIdAndName : organizationGotMap.entrySet()) {
                                organizationsList.add(organizationIdAndName.getValue());
                                Organization tempOrg = new Organization();
                                tempOrg.setOrganization_id(organizationIdAndName.getKey());
                                tempOrg.setName(organizationIdAndName.getValue());
                                organizationsListGlobal.add(tempOrg);
                            }

                        } else {
                            System.out.println("Result NOT succeded  " + result.result().getRows());
                            Logger.getInstance("").log(LoggerTag.ERROR, "Error getting organization list: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
                            organizationsList.add("ERR");
                        }
                        //This is just for unit testing
                                /*System.out.println("==================================== Connection not already exist. Here is the list of organization ====================================");
                                for (String org : organizationsList) {
                                    System.out.println(org);
                                }*/
                        handler.handle(organizationsList);
                    });

                }
            } catch (SQLException | ClassNotFoundException e) {
                Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e), "");
                handler.handle(null);
            }
        }
    }

    /**
     * This methode allows to
     *
     * @param sessionId
     * @param trustBalancingInputData
     * @param handler
     */
    public void getTrustBalancingData(String sessionId, TrustBalancingInputData trustBalancingInputData, Future<InternalResponse> handler) {
        Logger.getInstance("").log(LoggerTag.DEBUG, "~~~~~~~~~~~~~~~~~~~~~~START: getTrustBalancingData ~~~~~~~~~~~~~~~~~~~~~~", "");

        if (!activeSessions.containsKey(sessionId)) {
            InternalResponse resp = new InternalResponse();
            resp.setCause(-1);
            if (!handler.isComplete())
                handler.complete(resp);
        } else {
            //This will contain the final result for the excel file and the web
            ArrayList<TrustBalancingOutputData> trustBalancingOutputDataListResult = new ArrayList<>();
            //This contains all infinity data list
            //ArrayList<InfinityData1> infinityDataList = new ArrayList<>();
            //This contains all rp data list for the entire selected organization and date ranges
            List<TrustBalancingOutputRP> rpDataList = new ArrayList<>();
            //This contains all actual totals rp data list
            ArrayList<DepositBundleSum> actualTotalRpDataList = new ArrayList<>();

            List<Integer> orgIds2 = new ArrayList<>();
            StringBuilder organizationIdsForQuery;

            //Get input dates and format them. Dates comes like this 2019-03-01T06:00:00.000Z from the UI, we need to get something like 20190301
            String datBegin = trustBalancingInputData.getBeginDateRange().substring(0, 10);
            //LocalDate localDateBeginForRPCalculation = (LocalDate.parse(datBegin)).minusDays(5); //we need to substract 5 days from the begin date provided from the  UI for calculation
            //trustBalancingInputData.setBeginDateRange((datBegin.replaceAll("-", "")).trim());
            trustBalancingInputData.setBeginDateRange(datBegin.trim());

            //RP QUERY DATES REQUIREMENTS: The 3 lines below are dates requirements for RP query. In order to query RP the begin date has to be beginDateSelectedByTheUserminus 5 days and the end date has to be endDateSelectedByTheUser plus 6 days
            String datEnd = trustBalancingInputData.getEndDateRange().substring(0, 10);
            LocalDate localDateBeginForCalculationRp = (LocalDate.parse(datBegin)).minusDays(5); //we need to add use begin date - 5 days to the begin date selected by the use for begin date RP query
            LocalDate localDateEndForCalculationRp = (LocalDate.parse(datEnd)).plusDays(6); //we needto add 6 days to the end date provided from the  UI for calculation of the RP date. I add 6 not 5 here because in my query I query where datend < dateend + 6
            //trustBalancingInputData.setEndDateRange((datEnd.replaceAll("-", "")).trim());

            //INFINITY QUERY DATES REQUIREMENTS: we need to add 1 day to the end date because in my query I do endDate stricly inferior to end date selected by the user + 1 day
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(datEnd.trim(), formatter).plusDays(1);
            trustBalancingInputData.setEndDateRange(localDate.format(formatter));

            trustBalancingInputData.getOrganizationList().forEach(s -> organizationsListGlobal.forEach(organization1 -> {
                if (s.toUpperCase().equals(organization1.getName().toUpperCase()) && (!orgIds2.contains(organization1.getOrganization_id()))) {
                    orgIds2.add(organization1.getOrganization_id());
                }
            }));

            organizationIdsForQuery = new StringBuilder(StringUtils.join(orgIds2.toArray(new Integer[orgIds2.size()]), ","));
            System.out.println("organizationIdsForQuery: " + organizationIdsForQuery);

            //resultMap object contains all data and selectedObjectResultMap contains only all objects selected by the user
            //Map<Integer,TrustOrganizationClientsGrouped> selectedObjectResultMap = new HashMap<>();
            try {
                if (dbConnexionHelper.getFmaConnection() == null) {
                    connectToDb(DBType.Infinity, event -> {
                        if (event.succeeded()) {
                            dbConnexionHelper.setFmaConnection(event.result());
                            //getInfinityData(handler,selectedObjectResultMap,trustBalancingOutputDataListResult,rpDataList,trustBalancingInputData,orgIds2,organizationIdsForQuery,localDateBeginForCalculationRp,localDateEndForCalculationRp);
                            getInfinityData(handler, trustBalancingOutputDataListResult, rpDataList, trustBalancingInputData, orgIds2, organizationIdsForQuery, localDateBeginForCalculationRp, localDateEndForCalculationRp);

                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, "getOrganizationTrustClients: " + ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                            //handler.handle(null);

                            InternalResponse resp = new InternalResponse();
                            resp.setCause(0);
                            /*FIXME custom message*/
                            resp.setData("An error happens while trying to connect to infinity database!");
                            if (!handler.isComplete())
                                handler.complete(resp);
                        }
                    }, dbConnexionHelper);
                } else {
                    //getInfinityData(handler,selectedObjectResultMap,trustBalancingOutputDataListResult,rpDataList,trustBalancingInputData,orgIds2,organizationIdsForQuery,localDateBeginForCalculationRp,localDateEndForCalculationRp);
                    getInfinityData(handler, trustBalancingOutputDataListResult, rpDataList, trustBalancingInputData, orgIds2, organizationIdsForQuery, localDateBeginForCalculationRp, localDateEndForCalculationRp);
                }
            } catch (SQLException | ClassNotFoundException e) {
                Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e), "");
                InternalResponse resp = new InternalResponse();
                resp.setCause(0);
                /*FIXME custom message*/
                resp.setData("An error happens while trying to connect to infinity database!");
                if (!handler.isComplete())
                    handler.complete(resp);
            }
        }
    }

    /**
     * This method allows to
     *
     * @param handler
     * @param trustBalancingOutputDataListResult
     * @param rpDataList
     * @param trustBalancingInputData
     * @param orgIds2
     * @param organizationIdsForQuery
     * @param localDateBeginForCalculationRp
     * @param localDateEndForCalculationRp
     */
    /*private void getInfinityData(Future<InternalResponse> handler,
                                 Map<Integer,TrustOrganizationClientsGrouped> selectedObjectResultMap,
                                 ArrayList<TrustBalancingOutputData> trustBalancingOutputDataListResult,
                                 List<TrustBalancingOutputRP> rpDataList,
                                 TrustBalancingInputData trustBalancingInputData,
                                 List<Integer> orgIds2,
                                 StringBuilder organizationIdsForQuery,
                                 LocalDate localDateBeginForCalculationRp,
                                 LocalDate localDateEndForCalculationRp){
                                 */
    private void getInfinityData(Future<InternalResponse> handler,
                                 ArrayList<TrustBalancingOutputData> trustBalancingOutputDataListResult,
                                 List<TrustBalancingOutputRP> rpDataList,
                                 TrustBalancingInputData trustBalancingInputData,
                                 List<Integer> orgIds2,
                                 StringBuilder organizationIdsForQuery,
                                 LocalDate localDateBeginForCalculationRp,
                                 LocalDate localDateEndForCalculationRp) {
        //String trustRequete = TRUST_ORGS_AND_CLIENTS;
        String trustOrganizationClientsQuery = "SELECT trust.trust_id, trust.name as trustName, trust.account_number, " +
                " organization.organization_id, organization.name as orgName," +
                " client.client_id, client.name AS clientName, client.client_identifier" +
                " FROM " + TRUST_TABLE_NAME + " INNER JOIN " + ORGANIZATION_TABLE_NAME + " ON trust.trust_id = organization.trust_id" +
                " INNER JOIN " + CLIENT_TABLE_NAME + " ON organization.organization_id = client.organization_id" +
                " WHERE organization.active <> 'f' " +
                " AND client.active <> 'f' " +
                " AND trust.active <> 'f' " +
                " ORDER BY trust.name, organization.name, client.name";
        System.out.println("********************************      trustOrganizationClientsQuery:   " + trustOrganizationClientsQuery);
        //dbConnexionHelper.getFmaConnection().query(TRUST_ORGS_AND_CLIENTS, result->{
        dbConnexionHelper.getFmaConnection().query(trustOrganizationClientsQuery, result -> {
            if (result.succeeded()) {
                List<JsonObject> res1 = result.result().getRows(true);
                if (res1.size() == 0) {
                    InternalResponse resp = new InternalResponse();
                    resp.setCause(0);
                    /*FIXME custom message*/
                    resp.setData("No organization, trust and client found.");
                    if (!handler.isComplete())
                        handler.complete(resp);
                } else {
                    final List<TrustOrganizationClient> trustOrganizationClientGotStream = DbConnexionHelper.returnQueryList(res1, TrustOrganizationClient.class);
                    trustOrganizationClientGotStream.forEach(trustOrganizationClientGot -> {
                        int currentOrgId = trustOrganizationClientGot.getOrganization_id();
                        if (resultMap.isEmpty()) {
                            //first element of the map
                            TrustOrganizationClientsGrouped curTrustOrganizationClientsGrouped = new TrustOrganizationClientsGrouped();
                            //Set trust
                            //build trust information
                            Trust trust = new Trust();
                            trust.setTrust_id(trustOrganizationClientGot.getTrust_id());
                            trust.setName(trustOrganizationClientGot.getTrustName());
                            trust.setAccount_number(trustOrganizationClientGot.getAccount_number());
                            curTrustOrganizationClientsGrouped.setTrust(trust);
                            //build organization information
                            Organization organization = new Organization();
                            organization.setOrganization_id(trustOrganizationClientGot.getOrganization_id());
                            organization.setName(trustOrganizationClientGot.getOrgName());
                            curTrustOrganizationClientsGrouped.setOrganization(organization);
                            //build client information
                            Client client = new Client();
                            client.setClient_id(trustOrganizationClientGot.getClient_id());
                            client.setName(trustOrganizationClientGot.getClientName());
                            client.setClient_identifier(trustOrganizationClientGot.getClient_identifier());
                            List<Client> clientList = new ArrayList<>();
                            clientList.add(client);
                            curTrustOrganizationClientsGrouped.setClients(clientList);

                            resultMap.put(currentOrgId, curTrustOrganizationClientsGrouped);
                        } else {
                            if (resultMap.containsKey(currentOrgId)) {
                                //same trust, same organization but different client
                                TrustOrganizationClientsGrouped currTrustOrganizationClientsGrouped = resultMap.get(currentOrgId);
                                List<Client> cltList = currTrustOrganizationClientsGrouped.getClients();
                                Client newClient = new Client();
                                newClient.setClient_id(trustOrganizationClientGot.getClient_id());
                                newClient.setName(trustOrganizationClientGot.getClientName());
                                newClient.setClient_identifier(trustOrganizationClientGot.getClient_identifier());
                                currTrustOrganizationClientsGrouped.getClients().add(newClient);
                                currTrustOrganizationClientsGrouped.setClients(cltList);
                            } else {
                                TrustOrganizationClientsGrouped curTrustOrganizationClientsGrouped = new TrustOrganizationClientsGrouped();
                                //Set trust
                                //build trust information
                                Trust trust = new Trust();
                                trust.setTrust_id(trustOrganizationClientGot.getTrust_id());
                                trust.setName(trustOrganizationClientGot.getTrustName());
                                trust.setAccount_number(trustOrganizationClientGot.getAccount_number());
                                curTrustOrganizationClientsGrouped.setTrust(trust);
                                //build organization information
                                Organization organization = new Organization();
                                organization.setOrganization_id(trustOrganizationClientGot.getOrganization_id());
                                organization.setName(trustOrganizationClientGot.getOrgName());
                                curTrustOrganizationClientsGrouped.setOrganization(organization);
                                //build client information
                                Client client = new Client();
                                client.setClient_id(trustOrganizationClientGot.getClient_id());
                                client.setName(trustOrganizationClientGot.getClientName());
                                client.setClient_identifier(trustOrganizationClientGot.getClient_identifier());
                                List<Client> clientList = new ArrayList<>();
                                clientList.add(client);
                                curTrustOrganizationClientsGrouped.setClients(clientList);
                                resultMap.put(currentOrgId, curTrustOrganizationClientsGrouped);
                            }
                        }
                    });

                    //As requested, the end user will always select organizations having the same trust account number. In case multiple organization is seleceted by the user, we need to make sure they all have the same trust account number
                    List<String> trustAcctList = new ArrayList<>(); //this list is supposed to contain only one element, otherwise we throw an exception

                    if (resultMap.isEmpty() || orgIds2.isEmpty()) {
                        Logger.getInstance("").log(LoggerTag.INFORMATION, "No organization, trust and clients were selected. Please select at least one organization ", "");

                        InternalResponse resp = new InternalResponse();
                        resp.setCause(0);
                        /*FIXME custom message*/
                        resp.setData("Error, No organization, trust and clients were selected. Please select at least one organization!");
                        if (!handler.isComplete())
                            handler.complete(resp);
                    } else {
                        //Here we are trying to make sure that the user has selected organization(s) having the same trust account number
                        for (Integer organizationId : orgIds2) {
                            for (Map.Entry<Integer, TrustOrganizationClientsGrouped> resultMapEntry : resultMap.entrySet()) {
                                String currentTrustAcctNumber = resultMapEntry.getValue().getTrust().getAccount_number();
                                if (organizationId.equals(resultMapEntry.getKey()) && (trustAcctList.isEmpty() || !trustAcctList.contains(currentTrustAcctNumber))) {
                                    trustAcctList.add(currentTrustAcctNumber);
                                    break;
                                }
                            }
                        }
                    }

                    //THIS IS JUST FOR TESTING
                    if (trustAcctList == null || (trustAcctList != null && trustAcctList.isEmpty())) {
                        System.out.println("------------->>>>>> trustAcctList IS EMPTY ---------------------");
                    } else {
                        System.out.println("------------->>>>>> trustAcctList IS NOT EMPTY / SIZE IS : " + trustAcctList.size() + " ---------------------");
                    }

                    if (trustAcctList.isEmpty() || trustAcctList.size() > 1) { // this is an error. We throw an exception
                        Logger.getInstance("").log(LoggerTag.INFORMATION, "Error, Multiple organization having different trust account number was selected. Please select organizations having the same trust account number ", "");

                        InternalResponse resp = new InternalResponse();
                        resp.setCause(0);
                        //FIXME custom message
                        resp.setData("Error, Multiple organization having different trust account number was selected. Please select organizations having the same trust account number!");
                        if (!handler.isComplete())
                            handler.complete(resp);
                    } else {
                        selectedTrustAcctNumber = trustAcctList.get(0).toString();
                    }

                    String startingDate = trustBalancingInputData.getBeginDateRange().replaceAll("-", "").trim();
                    String endingDate = trustBalancingInputData.getEndDateRange().replaceAll("-", "").trim();
                    String ordIdQueryString = organizationIdsForQuery.toString();
                    if (!ordIdQueryString.isEmpty() && ordIdQueryString.substring(0, ordIdQueryString.length() - 1).equals(",")) {
                        ordIdQueryString = organizationIdsForQuery.substring(0, organizationIdsForQuery.length() - 1).trim();
                    }

                    /*
                    String infinityQuery = "SELECT format(monetary_transaction.[date],'MM/dd/yyyy') as date , monetary_transaction.debtor_id as debtorId, debtor.first_name as firstName, debtor.last_name as lastName, invoice.client_account_number as clientAccountNumber, "+
                            " CASE WHEN ( client.client_identifier != '' AND len(client.client_identifier) != 0) THEN client.name + '(' + client.client_identifier +')' "+
                            " ELSE client.name END AS busStream, client.client_identifier as clientIdentifier, "+
                            " payment_types.type_name as typeName, monetary_transaction.transaction_type as transactionType, "+
                            " monetary_transaction.amount, monetary_transaction.monetary_transaction_id as monetaryTransactionId, "+
                            " monetary_transaction.active, client.organization_id as organizationId, "+
                            " client.client_id as clientId, monetary_transaction.payment_application_id as paymentApplicationId, trust.account_number as trustAccountNumber, "+
                            " CASE " +
                            " WHEN monetary_transaction.payment_application_id=0 THEN monetary_transaction.amount " +
                            " ELSE (select sum(mt2.amount) from monetary_transaction mt2 where mt2.date=monetary_transaction.date and mt2.payment_application_id=monetary_transaction.payment_application_id and mt2.transaction_type='PA') " +
                            " END as sumAmount "+
                            " FROM monetary_transaction "+
                            " INNER JOIN client ON client.client_id = monetary_transaction.client_id "+
                            " INNER JOIN organization ON organization.organization_id = client.organization_id "+
                            " INNER JOIN trust ON trust.trust_id = organization.trust_id "+
                            " INNER JOIN debtor ON monetary_transaction.debtor_id = debtor.debtor_id "+
                            " INNER JOIN invoice ON monetary_transaction.invoice_id = invoice.invoice_id "+
                            " LEFT OUTER JOIN payment_types ON monetary_transaction.payment_type_id = payment_types.payment_type_id "+
                            " WHERE monetary_transaction.date >= (CONVERT(char(8),'"+ startingDate +"',112)) AND monetary_transaction.date < (CONVERT(char(8),'"+ endingDate  +"',112)) "+
                            " AND client.organization_id in ("+ ordIdQueryString +") AND monetary_transaction.transaction_type in ('PA','RA') AND monetary_transaction.status = 'P' ORDER BY monetary_transaction.date, busStream, trustAccountNumber";
                     */

                    String infinityQuery = "SELECT format(monetary_transaction.[date],'MM/dd/yyyy') as date , monetary_transaction.debtor_id as debtorId, debtor.first_name as firstName, debtor.last_name as lastName, invoice.client_account_number as clientAccountNumber, " +
                            " CASE WHEN ( client.client_identifier != '' AND len(client.client_identifier) != 0) THEN client.name + '(' + client.client_identifier +')' " +
                            " ELSE client.name END AS busStream, client.client_identifier as clientIdentifier, " +
                            " payment_types.type_name as typeName, monetary_transaction.transaction_type as transactionType, " +
                            " monetary_transaction.amount, monetary_transaction.monetary_transaction_id as monetaryTransactionId, " +
                            " monetary_transaction.active, client.organization_id as organizationId, " +
                            " client.client_id as clientId, monetary_transaction.payment_application_id as paymentApplicationId, trust.account_number as trustAccountNumber, " +
                            " CASE " +
                            " WHEN monetary_transaction.payment_application_id=0 THEN monetary_transaction.amount " +
                            " ELSE (select sum(mt2.amount) from monetary_transaction mt2 where mt2.date=monetary_transaction.date and mt2.payment_application_id=monetary_transaction.payment_application_id and mt2.transaction_type='PA') " +
                            " END as sumAmount, payment_application.type " +
                            " FROM monetary_transaction " +
                            " INNER JOIN client ON client.client_id = monetary_transaction.client_id " +
                            " INNER JOIN organization ON organization.organization_id = client.organization_id " +
                            " INNER JOIN trust ON trust.trust_id = organization.trust_id " +
                            " INNER JOIN debtor ON monetary_transaction.debtor_id = debtor.debtor_id " +
                            " INNER JOIN invoice ON monetary_transaction.invoice_id = invoice.invoice_id " +
                            " LEFT OUTER JOIN payment_types ON monetary_transaction.payment_type_id = payment_types.payment_type_id " +
                            " LEFT JOIN payment_application on payment_application.payment_application_id=monetary_transaction.payment_application_id " +
                            " WHERE monetary_transaction.date >= (CONVERT(char(8),'" + startingDate + "',112)) AND monetary_transaction.date < (CONVERT(char(8),'" + endingDate + "',112)) " +
                            " AND client.organization_id in (" + ordIdQueryString + ") AND monetary_transaction.transaction_type in ('PA','RA') AND monetary_transaction.status = 'P' ORDER BY monetary_transaction.date, busStream, trustAccountNumber";

                    System.out.println("INFINITY QUERY infinityQuery:  " + infinityQuery);

                    dbConnexionHelper.getFmaConnection().query(String.format(infinityQuery, startingDate, endingDate, ordIdQueryString), result01 -> {
                        if (result01.succeeded()) {
                            List<JsonObject> resultats01 = result01.result().getRows(true);
                            if (resultats01.size() == 0) {
                                //Logger.getInstance("").log(LoggerTag.DEBUG, "No datafound: ", "");
                                //handler.handle(null);
                                InternalResponse resp = new InternalResponse();
                                resp.setCause(0);
                                /*FIXME custom message*/
                                resp.setData("No transaction found within infinity database for the selected period and the selected organization");
                                if (!handler.isComplete())
                                    handler.complete(resp);
                            } else {
                                final List<TrustBalancingOutputInfinity> trustBalancingOutputInfinityStream = DbConnexionHelper.returnQueryList(resultats01, TrustBalancingOutputInfinity.class);
                                trustBalancingOutputInfinityStream.forEach(trustBalancingOutputInfinityGot4 -> {
                                    TrustBalancingOutputData infinityData = new TrustBalancingOutputData();
                                    infinityData.addTrust(UtilsTrustBalancing.INFINITY_DATE_KEY, (trustBalancingOutputInfinityGot4.getDate()));
                                    if (!trustBalancingOutputInfinityGot4.getDebtorId().isEmpty() && (!trustBalancingOutputInfinityGot4.getDebtorId().startsWith("00"))) {
                                        infinityData.addTrust(UtilsTrustBalancing.DEBTOR_ID_KEY, "00" + trustBalancingOutputInfinityGot4.getDebtorId());
                                    } else {
                                        infinityData.addTrust(UtilsTrustBalancing.DEBTOR_ID_KEY, trustBalancingOutputInfinityGot4.getDebtorId());
                                    }
                                    infinityData.addTrust(UtilsTrustBalancing.FIRST_NAME_KEY, trustBalancingOutputInfinityGot4.getFirstName());
                                    infinityData.addTrust(UtilsTrustBalancing.LAST_NAME_KEY, trustBalancingOutputInfinityGot4.getLastName());
                                    infinityData.addTrust(UtilsTrustBalancing.CLIENT_ACCT_NUMBER_KEY, trustBalancingOutputInfinityGot4.getClientAccountNumber());
                                    infinityData.addTrust(UtilsTrustBalancing.ORGANIZATION_ID_KEY, "" + trustBalancingOutputInfinityGot4.getOrganizationId());
                                    //infinityData.addTrust("officeCode", trustBalancingOutputInfinityGot4.getDescription());
                                    infinityData.addTrust(UtilsTrustBalancing.PAYMENT_APPLICATION_ID_KEY, "" + trustBalancingOutputInfinityGot4.getPaymentApplicationId());
                                    infinityData.addTrust(UtilsTrustBalancing.BUS_STREAM, trustBalancingOutputInfinityGot4.getBusStream());
                                    infinityData.addTrust(UtilsTrustBalancing.SUM_INFINITY_AMOUNT, trustBalancingOutputInfinityGot4.getSumAmount());

                                    //Here we set values for cc, manual, nsf / unknown and infinity
                                    if (trustBalancingOutputInfinityGot4.getTypeName() != null && !trustBalancingOutputInfinityGot4.getTypeName().isEmpty() && !trustBalancingOutputInfinityGot4.getTypeName().equals("NULL")) {
                                        //Set CC amount. This allows to determine if the payment amount was made via credit card or not and set the CC column with the amount
                                        if ((UtilsTrustBalancing.CC_KEY).toUpperCase().equals(trustBalancingOutputInfinityGot4.getTypeName())) {
                                            infinityData.addTrust(UtilsTrustBalancing.CC_KEY, String.valueOf(trustBalancingOutputInfinityGot4.getAmount()));
                                            infinityData.addTrust(UtilsTrustBalancing.MANUAL_KEY, "");
                                            infinityData.addTrust(UtilsTrustBalancing.NSF_KEY, "");
                                            infinityData.addTrust(UtilsTrustBalancing.UNKNOWN_KEY, "");
                                            infinityData.addTrust(UtilsTrustBalancing.INFINITY_KEY, infinityData.getListTrust().get(UtilsTrustBalancing.CC_KEY));
                                        } else if (UtilsTrustBalancing.MANUAL_KEY.toUpperCase().equals(trustBalancingOutputInfinityGot4.getTypeName())) {
                                            //Set MANUAL amount. This allows to determine if the payment amount was manual, if yes set manual column with the amount
                                            infinityData.addTrust(UtilsTrustBalancing.CC_KEY, "");
                                            infinityData.addTrust(UtilsTrustBalancing.MANUAL_KEY, String.valueOf(trustBalancingOutputInfinityGot4.getAmount()));
                                            infinityData.addTrust(UtilsTrustBalancing.NSF_KEY, "");
                                            infinityData.addTrust(UtilsTrustBalancing.UNKNOWN_KEY, "");
                                            infinityData.addTrust(UtilsTrustBalancing.INFINITY_KEY, infinityData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY));
                                        }
                                    } else {
                                        if (trustBalancingOutputInfinityGot4.getTransactionType() != null && UtilsTrustBalancing.NON_SUFFICIENT_FOND.equals(trustBalancingOutputInfinityGot4.getTransactionType())) {
                                            //set nsf
                                            infinityData.addTrust(UtilsTrustBalancing.CC_KEY, "");
                                            infinityData.addTrust(UtilsTrustBalancing.MANUAL_KEY, "");
                                            infinityData.addTrust(UtilsTrustBalancing.NSF_KEY, String.valueOf(trustBalancingOutputInfinityGot4.getAmount()));
                                            infinityData.addTrust(UtilsTrustBalancing.UNKNOWN_KEY, "");
                                            infinityData.addTrust(UtilsTrustBalancing.INFINITY_KEY, "");
                                        } else {
                                            infinityData.addTrust(UtilsTrustBalancing.CC_KEY, "");
                                            infinityData.addTrust(UtilsTrustBalancing.MANUAL_KEY, "");
                                            infinityData.addTrust(UtilsTrustBalancing.NSF_KEY, "");
                                            infinityData.addTrust(UtilsTrustBalancing.UNKNOWN_KEY, String.valueOf(trustBalancingOutputInfinityGot4.getAmount()));
                                            infinityData.addTrust(UtilsTrustBalancing.INFINITY_KEY, String.valueOf(trustBalancingOutputInfinityGot4.getAmount()));
                                        }
                                    }
                                    //add RP data
                                    infinityData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, "");
                                    infinityData.addTrust(UtilsTrustBalancing.ACH_KEY, "");
                                    infinityData.addTrust(UtilsTrustBalancing.ICL_KEY, "");
                                    infinityData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, "");
                                    infinityData.addTrust("clientIdentifier", trustBalancingOutputInfinityGot4.getClientIdentifier());

                                    //Enhancement: we need to display data grouped by client id within the recoverer section of the of output if no recoverer data found
                                    infinityData.addTrust("clientId", trustBalancingOutputInfinityGot4.getClientId());
                                    infinityData.addTrust("typeName", trustBalancingOutputInfinityGot4.getTypeName());
                                    infinityData.addTrust("paymentApplicationId", String.valueOf(trustBalancingOutputInfinityGot4.getPaymentApplicationId()));
                                    infinityData.addTrust("type", trustBalancingOutputInfinityGot4.getType());

                                    //we need this trustAccountNumber for data comparison
                                    if (trustBalancingOutputInfinityGot4.getTrustAccountNumber().charAt(0) == '0') {
                                        infinityData.addTrust(UtilsTrustBalancing.TRUST_ACCOUNT_NUMBER, trustBalancingOutputInfinityGot4.getTrustAccountNumber().substring(1));
                                    } else {
                                        infinityData.addTrust(UtilsTrustBalancing.TRUST_ACCOUNT_NUMBER, trustBalancingOutputInfinityGot4.getTrustAccountNumber());
                                    }

                                    //set RP totals to empty and set excelFileBottomData to null
                                    infinityData.setAchTotal("");
                                    infinityData.setIclTotal("");
                                    infinityData.setRpSolutionsTotal("");
                                    infinityData.setExcelFileBottomData(null);

                                    trustBalancingOutputDataListResult.add(infinityData);
                                });

                                //START RETRIEVING RP DATA
                                if (dbConnexionHelper.getRpConnection() == null) {
                                    processRpDataForNullConn(selectedTrustAcctNumber, handler, trustBalancingOutputDataListResult, rpDataList, trustBalancingInputData, localDateBeginForCalculationRp, localDateEndForCalculationRp);
                                } else {
                                    processRpData(selectedTrustAcctNumber, handler, trustBalancingOutputDataListResult, rpDataList, trustBalancingInputData, localDateBeginForCalculationRp, localDateEndForCalculationRp);

                                } //end else
                                //END RETRIEVING RP DATA
                            } /// end switch

                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, "Error getting trust balancing data from Infinity: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
                            InternalResponse resp = new InternalResponse();
                            resp.setCause(0);
                            /*FIXME custom message*/
                            resp.setData("An error happens while getting transactions from infinity database");
                            if (!handler.isComplete())
                                handler.complete(resp);
                        }
                    });
                }


            } else {
                Logger.getInstance("").log(LoggerTag.ERROR, "Error on getOrganizationTrustClients: " + ExceptionUtils.exceptionStackTraceAsString(result.cause()), "");
                InternalResponse resp = new InternalResponse();
                resp.setCause(0);
                /*FIXME custom message*/
                resp.setData("An error happens while getting organizations, trusts and from infinity database");
                if (!handler.isComplete())
                    handler.complete(resp);
            }
        });
    }

    /**
     * This allows to
     *
     * @param selectedTrustAcctNumber
     * @param handler
     * @param trustBalancingOutputDataListResult
     * @param rpDataList
     * @param trustBalancingInputData
     * @param localDateBeginForCalculationRp
     * @param localDateEndForCalculationRp
     */
    private void processRpData(String selectedTrustAcctNumber,
                               Future<InternalResponse> handler,
                               ArrayList<TrustBalancingOutputData> trustBalancingOutputDataListResult,
                               List<TrustBalancingOutputRP> rpDataList,
                               TrustBalancingInputData trustBalancingInputData,
                               LocalDate localDateBeginForCalculationRp,
                               LocalDate localDateEndForCalculationRp) {

        String pocketGroupCriteriaQueryParam = selectedTrustAcctNumber.trim();

        if (selectedTrustAcctNumber.trim().charAt(0) == '0') {
            pocketGroupCriteriaQueryParam = selectedTrustAcctNumber.trim().substring(1);
        }

        String rpQuery = "SELECT (amount * 0.01) as amount, CASE WHEN len(account) != 10 THEN '00' + account ELSE account END AS account, pocket_group as pocketGroup, trans_type as transType, LEFT(JOB_KEY, 8) as jobKey FROM " + DOC_TABLE_NAME + " WHERE CONVERT(datetime, LEFT(JOB_KEY, 8)) >= '" + localDateBeginForCalculationRp + "' and CONVERT(datetime, LEFT(JOB_KEY, 8)) < '" + localDateEndForCalculationRp + "' AND POCKET_GROUP like '%" + pocketGroupCriteriaQueryParam + "' AND (REJECT is null or  REJECT <> 'R') ORDER BY jobkey desc, DOC.ACCOUNT, DOC.AMOUNT";
        System.out.println("RP QUERY  rpQuery:   " + rpQuery);

        dbConnexionHelper.getRpConnection().query(rpQuery, rpResult -> {
            if (rpResult.succeeded()) {
                List<JsonObject> resultRp = rpResult.result().getRows(true);

                if (resultRp.size() == 0) {
                    Logger.getInstance("").log(LoggerTag.DEBUG, "No matched data found from " + localDateBeginForCalculationRp + " to " + localDateEndForCalculationRp, "");
                    /*FIXME*/
                    InternalResponse resp = new InternalResponse();
                    resp.setData("No matched data found from");
                    resp.setCause(0);
                    if (!handler.isComplete())
                        handler.complete(resp);
                } else {
                    final List<TrustBalancingOutputRP> trustBalancingOutputRPGotStream = DbConnexionHelper.returnQueryList(resultRp, TrustBalancingOutputRP.class);
                    trustBalancingOutputRPGotStream.forEach(trustBalOutputRPGot -> {
                        trustBalOutputRPGot.setAmount(trustBalOutputRPGot.getAmount());
                        //Same amount, date, debtorId, trust
                        String year = String.valueOf(trustBalOutputRPGot.getJobKey()).substring(0, 4).trim();
                        String month = String.valueOf(trustBalOutputRPGot.getJobKey()).substring(4, 6).trim();
                        String day = String.valueOf(trustBalOutputRPGot.getJobKey()).substring(6, 8).trim();
                        trustBalOutputRPGot.setJobKey(month + "/" + day + "/" + year);
                        rpDataList.add(trustBalOutputRPGot);
                    });
                } //end switch


                if (!rpDataList.isEmpty()) {
                    String currentTrustKey = UUID.randomUUID().toString();
                    final int[] i = {trustBalancingOutputDataListResult.size()};

                    for (TrustBalancingOutputData trustBalancingOutputData : trustBalancingOutputDataListResult) {
                        //*************************************** start test tiana
                        if ("0038735227".equals(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.DEBTOR_ID_KEY)) ||
                                "0037167794".equals(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.DEBTOR_ID_KEY)) ||
                                "0037727037".equals(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.DEBTOR_ID_KEY)) ||
                                "0037727038".equals(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.DEBTOR_ID_KEY))
                                ) {
                            System.out.println("debtor IIIIIIDDDDD:   " + trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.DEBTOR_ID_KEY));
                        }
                        //*************************************** end test tiana
                        if (
                            //(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.CC_KEY)!= null && !trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.CC_KEY).isEmpty())
                            //||  (trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.NSF_KEY)!= null && !trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.NSF_KEY).isEmpty())){
                                (Utils.isNotNullAndNotEmpty(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.CC_KEY)))
                                        || (Utils.isNotNullAndNotEmpty(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.NSF_KEY)))) {

                            trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, "");
                            trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, "");
                            trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, "");
                            trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, "");
                            trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "1");
                            trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                        } else {

                            for (TrustBalancingOutputRP rpData : rpDataList) {

                                String paymentType = rpData.getPocketGroup();
                                String currentPocketGroup = "";
                                //if ("68.12".equals(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY)) && "06/25/2019".equals(trustBalancingOutputData.getInfDate()) && "06/25/2019".equals(rpData.getJobKey())) {
                                /*if (    !trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY).isEmpty() &&
                                        "68.12".equals(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY)) &&
                                        "06/25/2019".equals(rpData.getJobKey()) && rpData.getJobKey().equals(trustBalancingOutputData.getInfDate())) {
                                    System.out.println("=============>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   rpData:  "+rpData.toString());
                                }*/
                                if (rpData.getPocketGroup().startsWith(UtilsTrustBalancing.ACH_ACCT_NUMBER_PREFIX)) {
                                    currentPocketGroup = rpData.getPocketGroup().substring(6, rpData.getPocketGroup().length());
                                    paymentType = UtilsTrustBalancing.ACH_KEY;
                                } else if (rpData.getPocketGroup().startsWith(UtilsTrustBalancing.ICL_ACCT_NUMBER_PREFIX)) {
                                    currentPocketGroup = rpData.getPocketGroup().substring(6, rpData.getPocketGroup().length());
                                    paymentType = UtilsTrustBalancing.ICL_KEY;
                                } else {
                                    currentPocketGroup = rpData.getPocketGroup();
                                }
                                String currentDebtorId = rpData.getAccount();
                                ///if (rpData.getAccount() != null && !rpData.getAccount().isEmpty() && (!rpData.getAccount().startsWith("00"))) { //case of misleading 00
                                if (Utils.isNotNullAndNotEmpty(rpData.getAccount()) && (!rpData.getAccount().startsWith("00"))) { //case of misleading 00
                                    currentDebtorId = "00" + rpData.getAccount();
                                }
                                //infinityDataChecking is used to facilitate the data match checking between infinity data and RP
                                if (trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.TRUST_ACCOUNT_NUMBER).trim().equals(currentPocketGroup.trim())
                                        && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.DEBTOR_ID_KEY).trim().equals(currentDebtorId.trim())
                                        && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.INFINITY_DATE_KEY).trim().equals(rpData.getJobKey().trim())
                                        && ((Utils.isNotNullAndNotEmpty(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY)) && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY).trim().equals(rpData.getAmount().trim()))
                                        || (Utils.isNotNullAndNotEmpty(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY)) && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY).trim().equals(rpData.getAmount().trim()))
                                )
                                        ) { // Match found
                                    if (UtilsTrustBalancing.ACH_KEY.equals(paymentType)) { //ACH payment type
                                        if (Utils.isNotNullAndNotEmpty(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY)) && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY).trim().equals(rpData.getAmount().trim())) {
                                            System.out.println("******************************   MANUAL ACH: " + trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY));
                                            trustBalancingOutputData.getListTrust().replace(UtilsTrustBalancing.MANUAL_KEY, "");
                                        }
                                        trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, rpData.getAmount());
                                        trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, "");
                                        trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, rpData.getAmount());
                                        trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, rpData.getJobKey());
                                        trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "1");
                                        trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                                        break;
                                    } else if (UtilsTrustBalancing.ICL_KEY.equals(paymentType)) { //ICL payment type
                                        if (Utils.isNotNullAndNotEmpty(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY)) && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY).trim().equals(rpData.getAmount().trim())) {
                                            System.out.println("******************************   MANUAL ICL: " + trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY));
                                            trustBalancingOutputData.getListTrust().replace(UtilsTrustBalancing.MANUAL_KEY, "");
                                        }
                                        //else {
                                        //if (trustBalancingOutputData.getListTrust().get("typeName") == null && "0".equals(trustBalancingOutputData.getListTrust().get("paymentApplicationId"))
                                        //&& trustBalancingOutputData.getListTrust().get("type") == null) {
                                        //trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, rpData.getAmount());
                                        //trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, "");
                                        //}
                                        //}

                                        trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, rpData.getAmount());
                                        trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, "");
                                        trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, rpData.getAmount());
                                        trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, rpData.getJobKey());
                                        trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "1");
                                        trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                                        break;
                                    } else { //the payment type is not and ACH neither ICL. It may be a check
                                        trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, "");
                                        trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, "");
                                        trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, "");
                                        trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, "");
                                        trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "1");
                                        trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                                        break;
                                    }

                                } else if ((!trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.INFINITY_DATE_KEY).equals(rpData.getJobKey()))
                                        && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.TRUST_ACCOUNT_NUMBER).equals(currentPocketGroup)
                                        && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.DEBTOR_ID_KEY).equals(currentDebtorId)
                                        && ((Utils.isNotNullAndNotEmpty(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY)) && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY).equals(rpData.getAmount()))
                                        || (Utils.isNotNullAndNotEmpty(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY)) && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY).equals(rpData.getAmount()))
                                )
                                        ) {

                                    boolean matchedTempFound = false;
                                    //this case is added to avoid the case where we did not check the whole entire rpDataList
                                    for (TrustBalancingOutputRP rpDataTmp : rpDataList) {
                                        if ((trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.INFINITY_DATE_KEY).equals(rpDataTmp.getJobKey()))
                                                && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.TRUST_ACCOUNT_NUMBER).equals(currentPocketGroup)
                                                && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.DEBTOR_ID_KEY).equals(currentDebtorId)
                                                && ((Utils.isNotNullAndNotEmpty(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY)) && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY).equals(rpDataTmp.getAmount()))
                                                || (Utils.isNotNullAndNotEmpty(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY)) && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY).equals(rpDataTmp.getAmount()))
                                        )
                                                ) {
                                            matchedTempFound = true;

                                            if (UtilsTrustBalancing.ACH_KEY.equals(paymentType)) { //ACH payment type
                                                if (Utils.isNotNullAndNotEmpty(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY)) && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY).trim().equals(rpDataTmp.getAmount().trim())) {
                                                    System.out.println("------------------------------   MANUAL ACH: " + trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY));
                                                    trustBalancingOutputData.getListTrust().replace(UtilsTrustBalancing.MANUAL_KEY, "");
                                                }
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, rpDataTmp.getAmount());
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, rpDataTmp.getAmount());
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, rpDataTmp.getJobKey());
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "1");
                                                trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                                                break;
                                            } else if (UtilsTrustBalancing.ICL_KEY.equals(paymentType)) { //ICL payment type
                                                if (Utils.isNotNullAndNotEmpty(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY)) && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY).trim().equals(rpDataTmp.getAmount().trim())) {
                                                    System.out.println("------------------------------   MANUAL ICL: " + trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.MANUAL_KEY));
                                                    trustBalancingOutputData.getListTrust().replace(UtilsTrustBalancing.MANUAL_KEY, "");
                                                }
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, rpDataTmp.getAmount());
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, rpDataTmp.getAmount());
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, rpDataTmp.getJobKey());
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "1");
                                                trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                                                break;
                                            } else { //the payment type is not and ACH neither ICL. It may be a check
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "1");
                                                trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                                                break;
                                            }
                                        }
                                    }

                                    //Let's make sure there is no other matching payment date within the rpDataList. We check the whole entire list
                                    if (!matchedTempFound) {

                                        //Here we need to build a mini list of rp data having rp date between infinity date 5 days before and 5 days after
                                        DateTimeFormatter myDateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
                                        LocalDate currentInfinityDateMinusFive = LocalDate.parse(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.INFINITY_DATE_KEY), myDateFormatter).minusDays(5);
                                        LocalDate currentInfinityDatePlusFive = LocalDate.parse(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.INFINITY_DATE_KEY), myDateFormatter).plusDays(5);
                                        LocalDate currentRpDateToCheck = LocalDate.parse(rpData.getJobKey(), myDateFormatter);

                                        //First check if the current rp date (jobKey) is between the date range currentInfinityDatMinusFive and currentInfinityDatePlusFive
                                        if (currentRpDateToCheck.isEqual(currentInfinityDateMinusFive) || (currentRpDateToCheck.isAfter(currentInfinityDateMinusFive) && currentRpDateToCheck.isBefore(currentInfinityDatePlusFive))) {
                                            //match found
                                            if (paymentType.equals(UtilsTrustBalancing.ICL_KEY)) { // icl
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, rpData.getAmount());
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, rpData.getAmount());
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, rpData.getJobKey());
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "");
                                                trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                                                break;
                                            } else if (paymentType.equals(UtilsTrustBalancing.ACH_KEY)) { // ach
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, rpData.getAmount());
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, rpData.getAmount());
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, rpData.getJobKey());
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "");
                                                trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                                                break;
                                            } else {
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "");
                                                trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                                                break;
                                            }
                                        }
                                    }
                                } else if (
                                        Utils.isNotNullAndNotEmpty(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY)) && Utils.isNotNullAndNotEmpty(rpData.getAmount())
                                                && !trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY).equals(rpData.getAmount())
                                                && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.TRUST_ACCOUNT_NUMBER).equals(currentPocketGroup)
                                                && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.DEBTOR_ID_KEY).equals(currentDebtorId)
                                                && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.INFINITY_DATE_KEY).equals(rpData.getJobKey())) { //Everything match except the amount
                                    //This case happens if it is a SPLIT TRANSACTION. In this case we need to check if the sum amount within infinity match an rp data within the rpDataList
                                    trustBalancingOutputData.addTrust("split", "1");

                                    for (TrustBalancingOutputRP subRpData : rpDataList) {
                                        String cur2SubPocketGroup = subRpData.getPocketGroup();
                                        String cur2PaymentType = "";
                                        if (subRpData.getPocketGroup().startsWith(UtilsTrustBalancing.ACH_ACCT_NUMBER_PREFIX)) {
                                            cur2SubPocketGroup = subRpData.getPocketGroup().substring(6, subRpData.getPocketGroup().length());
                                            cur2PaymentType = UtilsTrustBalancing.ACH_KEY;
                                        } else if (subRpData.getPocketGroup().startsWith(UtilsTrustBalancing.ICL_ACCT_NUMBER_PREFIX)) {
                                            cur2SubPocketGroup = subRpData.getPocketGroup().substring(6, subRpData.getPocketGroup().length());
                                            cur2PaymentType = UtilsTrustBalancing.ICL_KEY;
                                        }

                                        String currentSubDebtorId2 = subRpData.getAccount();
                                        if (subRpData != null && Utils.isNotNullAndNotEmpty(subRpData.getAccount()) && (!subRpData.getAccount().startsWith("00"))) { //case of misleading 00
                                            currentSubDebtorId2 = "00" + subRpData.getAccount();
                                        }

                                        if (trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.SUM_INFINITY_AMOUNT).equals(subRpData.getAmount())) { //infinity sumAmount match the rp amount, so Match found

                                            if (cur2PaymentType.equals(UtilsTrustBalancing.ICL_KEY)) { //icl payment type
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY));
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY));
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, subRpData.getJobKey());
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "1");
                                                trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                                                break;
                                            } else if (cur2PaymentType.equals(UtilsTrustBalancing.ACH_KEY)) { //ach payment type
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY));
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY));
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, subRpData.getJobKey());
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "1");
                                                trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                                                break;
                                            } else { // payment type check may be
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, "");
                                                trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "1");
                                                trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                                                break;
                                            }
                                        }
                                    }//end for
                                } else if (trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.TRUST_ACCOUNT_NUMBER).equals(currentPocketGroup)
                                        && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.INFINITY_DATE_KEY).equals(rpData.getJobKey())
                                        && !trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.DEBTOR_ID_KEY).equals(currentDebtorId)) { //Everything match except the amount

                                    //We need to iterate to trustBalancingOutputDataListResult222 and calculate the data having the same paymentApplicationId as the current trustBalancingOutPutData
                                    if (Utils.isNotNullAndNotEmpty(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.PAYMENT_APPLICATION_ID_KEY)) && !trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.PAYMENT_APPLICATION_ID_KEY).equals("NULL")) {
                                        float totalAmount = Float.parseFloat("0");

                                        for (TrustBalancingOutputData trustBalancingOutputDataTemp : trustBalancingOutputDataListResult) {
                                            if (trustBalancingOutputDataTemp != null && !trustBalancingOutputDataTemp.getListTrust().get(UtilsTrustBalancing.PAYMENT_APPLICATION_ID_KEY).isEmpty()
                                                    && !trustBalancingOutputDataTemp.getListTrust().get(UtilsTrustBalancing.PAYMENT_APPLICATION_ID_KEY).equals("NULL")
                                                    && trustBalancingOutputDataTemp.getListTrust().get(UtilsTrustBalancing.PAYMENT_APPLICATION_ID_KEY).equals(trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.PAYMENT_APPLICATION_ID_KEY))
                                                    && Utils.isNotNullAndNotEmpty(trustBalancingOutputDataTemp.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY))) {
                                                totalAmount = totalAmount + Float.parseFloat(trustBalancingOutputDataTemp.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY));
                                            }
                                        }

                                        //convert totalAmount to string now
                                        String totalAmountStringFormat = Float.toString(totalAmount);

                                        //we need to loop rpDalaList to make sure we find a match for that amount, trust/pocketGroup, date/jobKey and totalAmountStringFormat/amount
                                        for (TrustBalancingOutputRP subRpData2 : rpDataList) {
                                            String cur3SubPocketGroup = subRpData2.getPocketGroup();
                                            String cur3PaymentType = "";
                                            if (subRpData2.getPocketGroup().startsWith(UtilsTrustBalancing.ACH_ACCT_NUMBER_PREFIX)) {
                                                cur3SubPocketGroup = subRpData2.getPocketGroup().substring(6, subRpData2.getPocketGroup().length());
                                                cur3PaymentType = UtilsTrustBalancing.ACH_KEY;
                                            } else if (subRpData2.getPocketGroup().startsWith(UtilsTrustBalancing.ICL_ACCT_NUMBER_PREFIX)) {
                                                cur3SubPocketGroup = subRpData2.getPocketGroup().substring(6, subRpData2.getPocketGroup().length());
                                                cur3PaymentType = UtilsTrustBalancing.ICL_KEY;
                                            }

                                            String currentSubDebtorId3 = subRpData2.getAccount();
                                            if (subRpData2.getAccount() != null && !subRpData2.getAccount().isEmpty() && !subRpData2.getAccount().startsWith("00")) { //case of misleading 00
                                                currentSubDebtorId3 = "00" + subRpData2.getAccount();
                                            }

                                            if (totalAmountStringFormat.equals(subRpData2.getAmount()) && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.INFINITY_DATE_KEY).equals(subRpData2.getJobKey())
                                                    && trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.TRUST_ACCOUNT_NUMBER).equals(cur3SubPocketGroup)) {

                                                if (cur3PaymentType.equals(UtilsTrustBalancing.ICL_KEY)) { //icl payment type ++++++++++++++++++++++++++++++
                                                    //here we are trying to make sure that even if we have found a rp match, we still want to make sure that the current transaction we are checking is not and ACH
                                                    //Even if we have found a rp match ICL type but the current transaction is a cheque, we classify the current transaction as an ACH
                                                    //if (trustBalancingOutputData.getListTrust().get("typeName") == null && !Utils.isNullOrEmpty(trustBalancingOutputData.getListTrust().get("paymentApplicationId"))
                                                    //&& "0".equals(trustBalancingOutputData.getListTrust().get("paymentApplicationId"))
                                                    //&& trustBalancingOutputData.getListTrust().get("type") == null) {
                                                    //trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY));
                                                    //trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, "");
                                                    //} else {
                                                    trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY));
                                                    trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, "");
                                                    //}
                                                    trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY));
                                                    trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, subRpData2.getJobKey());
                                                    trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "1");
                                                    trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                                                    //dataFound = true;
                                                    break;
                                                } else if (cur3PaymentType.equals(UtilsTrustBalancing.ACH_KEY)) { //ach payment type
                                                    trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY));
                                                    trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, "");
                                                    trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.UNKNOWN_KEY));
                                                    trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, subRpData2.getJobKey());
                                                    trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "1");
                                                    trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                                                    //dataFound = true;
                                                    break;
                                                } else { // payment type check may be
                                                    trustBalancingOutputData.addTrust(UtilsTrustBalancing.ACH_KEY, "");
                                                    trustBalancingOutputData.addTrust(UtilsTrustBalancing.ICL_KEY, "");
                                                    trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_SOLUTIONS_KEY, "");
                                                    trustBalancingOutputData.addTrust(UtilsTrustBalancing.RP_DATE_KEY, "");
                                                    trustBalancingOutputData.addTrust(UtilsTrustBalancing.MATCH_FOUND, "1");
                                                    trustBalancingOutputDataListResult.set(trustBalancingOutputDataListResult.indexOf(trustBalancingOutputData), trustBalancingOutputData);
                                                    //dataFound = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            } //end new for
                        }//end else
                    }//end big for

                    this.calculateRpTotals(selectedTrustAcctNumber, handler, trustBalancingOutputDataListResult, trustBalancingInputData);
                } else {
                    Logger.getInstance("").log(LoggerTag.INFORMATION, "Empty RP data list", "");
                    InternalResponse resp = new InternalResponse();
                    resp.setCause(0);
                    /*FIXME custom message*/
                    resp.setData("Empty RP data list");
                    if (!handler.isComplete())
                        handler.complete(resp);
                }
            } else {
                Logger.getInstance("").log(LoggerTag.ERROR, "Error getting Doc record from RP database: " + ExceptionUtils.exceptionStackTraceAsString(rpResult.cause()), "");
                InternalResponse resp = new InternalResponse();
                resp.setCause(0);
                /*FIXME custom message*/
                resp.setData("Error getting Doc record from RP database");
                if (!handler.isComplete())
                    handler.complete(resp);


            }
        });
    }

    /**
     * This allows to calculate the RP values for each transaction
     *
     * @param selectedTrustAcctNumber
     * @param handler
     * @param trustBalancingOutputDataListResult
     * @param trustBalancingInputData
     */
    private void calculateRpTotals(String selectedTrustAcctNumber,
                                   Future<InternalResponse> handler,
                                   ArrayList<TrustBalancingOutputData> trustBalancingOutputDataListResult,
                                   TrustBalancingInputData trustBalancingInputData) {

        String pocketGroupCriteriaQueryParam = selectedTrustAcctNumber.trim();
        if (selectedTrustAcctNumber.trim().charAt(0) == '0') {
            pocketGroupCriteriaQueryParam = selectedTrustAcctNumber.trim().substring(1);
        }

        //------------------------------------------- RP SOLUTIONS TOTALS CALCULATION -------------------------------------------
        //Here we are adding totals RP to the output list
        String rpTotalsQuery = "SELECT PROC_DATE as procDate, POCKET_GROUP as pocketGroup, sum(NUM_CHECKS) as numChecks, (sum(TOTAL_AMOUNT) * 0.01) as ttlCks FROM " + ConfigurationUtils.DEPOSIT_BUNDLE_SUM_TABLE_NAME + " WHERE CONVERT(datetime, PROC_DATE) >= '" + trustBalancingInputData.getBeginDateRange() + "' and CONVERT(datetime, PROC_DATE) < '" + trustBalancingInputData.getEndDateRange() + "' AND POCKET_GROUP like '%" + pocketGroupCriteriaQueryParam + "' GROUP BY PROC_DATE, POCKET_GROUP ORDER BY PROC_DATE, POCKET_GROUP";
        System.out.println("rpTotalsQuery:   " + rpTotalsQuery);
        dbConnexionHelper.getRpConnection().query(rpTotalsQuery, rpTotalResult -> {
            List<JsonObject> resultRpTotal = rpTotalResult.result().getRows(true);
            if (rpTotalResult.succeeded()) {
                List<DepositBundleSum> depositBundleSumList = new ArrayList<>();
                final List<DepositBundleSum> depositBundleSumGotStream = DbConnexionHelper.returnQueryList(resultRpTotal, DepositBundleSum.class);

                depositBundleSumGotStream.forEach(depositBundleSumGot -> {
                    String year = String.valueOf(depositBundleSumGot.getProcDate()).substring(0, 4).trim();
                    String month = String.valueOf(depositBundleSumGot.getProcDate()).substring(4, 6).trim();
                    String day = String.valueOf(depositBundleSumGot.getProcDate()).substring(6, 8).trim();
                    String currentNewDate = month + "/" + day + "/" + year;
                    depositBundleSumGot.setProcDate(currentNewDate);
                    depositBundleSumList.add(depositBundleSumGot);
                });

                String currentInfinityDate = "";
                List<String> dateAlreadyProcessed = new ArrayList<>();

                for (TrustBalancingOutputData trustBalancingOutputData : trustBalancingOutputDataListResult) {
                    currentInfinityDate = trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.INFINITY_DATE_KEY);
                    String iclPocketGpe = "";
                    String achPocketGpe = "";
                    int iclTotalNumChecks = 0;
                    float iclTotalAmountChecks = 0;
                    int achTotalNumChecks = 0;
                    float achTotalAmountChecks = 0;

                    for (DepositBundleSum depositBundleSumElement : depositBundleSumList) {
                        if (depositBundleSumElement.getProcDate().equals(currentInfinityDate)) {
                            //rp pocketGroup looks like this ACH-TA265478606 or ICL-TA265478606
                            if (depositBundleSumElement.getPocketGroup().startsWith(UtilsTrustBalancing.ICL_ACCT_NUMBER_PREFIX)) {
                                iclPocketGpe = depositBundleSumElement.getPocketGroup().substring(6, depositBundleSumElement.getPocketGroup().length());
                                if (trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.TRUST_ACCOUNT_NUMBER).equals(iclPocketGpe)) {
                                    iclTotalNumChecks = iclTotalNumChecks + depositBundleSumElement.getNumChecks();
                                    iclTotalAmountChecks = iclTotalAmountChecks + depositBundleSumElement.getTtlCks();
                                }
                            } else if (depositBundleSumElement.getPocketGroup().startsWith(UtilsTrustBalancing.ACH_ACCT_NUMBER_PREFIX)) {
                                achPocketGpe = depositBundleSumElement.getPocketGroup().substring(6, depositBundleSumElement.getPocketGroup().length());
                                if (trustBalancingOutputData.getListTrust().get(UtilsTrustBalancing.TRUST_ACCOUNT_NUMBER).equals(achPocketGpe)) {
                                    achTotalNumChecks = achTotalNumChecks + depositBundleSumElement.getNumChecks();
                                    achTotalAmountChecks = achTotalAmountChecks + depositBundleSumElement.getTtlCks();
                                }
                            }
                        }
                    }//end 2nd for

                    //Very first time, we need to add the current date
                    if (dateAlreadyProcessed.isEmpty() || !dateAlreadyProcessed.contains(currentInfinityDate)) {
                        dateAlreadyProcessed.add(currentInfinityDate);
                        String s = String.format("%,.2f", achTotalAmountChecks);
                        trustBalancingOutputData.setAchTotal(achTotalNumChecks + "/$" + String.format("%,.2f", achTotalAmountChecks));
                        trustBalancingOutputData.setIclTotal(iclTotalNumChecks + "/$" + String.format("%,.2f", iclTotalAmountChecks));
                        trustBalancingOutputData.setRpSolutionsTotal(String.valueOf(achTotalNumChecks + iclTotalNumChecks) + "/$" + String.format("%,.2f", achTotalAmountChecks + iclTotalAmountChecks));
                    }
                }//end 1sf for

                InternalResponse resp = new InternalResponse();
                resp.setCause(1);
                resp.setData(Json.encode(trustBalancingOutputDataListResult));
                if (!handler.isComplete())
                    handler.complete(resp);
            } else {
                Logger.getInstance("").log(LoggerTag.ERROR, "Error getting data from [xprpsreports].[dbo].[DEPOSIT_BUNDLE_SUM] table: " + ExceptionUtils.exceptionStackTraceAsString(rpTotalResult.cause()), "");
                InternalResponse resp = new InternalResponse();
                resp.setCause(0);
                /*FIXME custom message*/
                resp.setData("Error getting data from database");
                if (!handler.isComplete())
                    handler.complete(resp);
            }
        });
    }

    /**
     * This allows to
     *
     * @param selectedTrustAcctNumber
     * @param handler
     * @param trustBalancingOutputDataListResult
     * @param rpDataList
     * @param trustBalancingInputData
     * @param localDateBeginForCalculationRp
     * @param localDateEndForCalculationRp
     */
    private void processRpDataForNullConn(String selectedTrustAcctNumber,
                                          Future<InternalResponse> handler,
                                          ArrayList<TrustBalancingOutputData> trustBalancingOutputDataListResult,
                                          List<TrustBalancingOutputRP> rpDataList,
                                          TrustBalancingInputData trustBalancingInputData,
                                          LocalDate localDateBeginForCalculationRp,
                                          LocalDate localDateEndForCalculationRp) {
        try {
            connectToDb(DBType.RP, event44 -> {
                if (event44.succeeded()) {
                    dbConnexionHelper.setRpConnection(event44.result());
                    processRpData(selectedTrustAcctNumber, handler, trustBalancingOutputDataListResult, rpDataList, trustBalancingInputData, localDateBeginForCalculationRp, localDateEndForCalculationRp);
                } else {
                    Logger.getInstance("").log(LoggerTag.ERROR, "logging to RP database: " + ExceptionUtils.exceptionStackTraceAsString(event44.cause()), "");
                    InternalResponse resp = new InternalResponse();
                    resp.setCause(0);
                    /*FIXME custom message*/
                    resp.setData("Error logging to RP database");
                    if (!handler.isComplete())
                        handler.complete(resp);
                }
            }, dbConnexionHelper);
        } catch (SQLException | ClassNotFoundException e) {
            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(e), "");

            InternalResponse resp = new InternalResponse();
            resp.setCause(0);
            /*FIXME custom message*/
            resp.setData("Internal server error");
            if (!handler.isComplete())
                handler.complete(resp);
        }
    }

    /**
     * This allows to
     *
     * @param dbNumber
     * @param handler
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static void connectToDb(DBType dbNumber, Handler<AsyncResult<SQLConnection>> handler, DbConnexionHelper dbConnexionHelper) throws SQLException, ClassNotFoundException {
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~START: connectToDb ~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("~~~~~~~~~ DB: " + dbNumber.name());


        credentials = new HashMap<>();
        switch (dbNumber) {
            case Infinity:
                if (dbConnexionHelper.getClientFma() == null) {
                    credentials.put("host", INF_DB_HOST);
                    credentials.put("user", INF_DB_USER);
                    credentials.put("psswd", INF_DB_PSSWD);
                    credentials.put("db", INFINITY_DB_NAME);
                    dbConnexionHelper.setCredentials(credentials);
                    dbConnexionHelper.setClientFma(dbConnexionHelper.getDbConnection(credentials));
                    dbConnexionHelper.getClientFma().getConnection(event -> {
                        if (event.succeeded()) {
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                        }
                    });
                } else {
                    dbConnexionHelper.getClientFma().getConnection(event -> {
                        if (event.succeeded()) {
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                        }
                    });
                }
                break;


            case Infinity2:
                if (dbConnexionHelper.getClientFma() == null) {
                    credentials.put("host", INF_DB_HOST32);
                    credentials.put("user", INF_DB_USER32);
                    credentials.put("psswd", INF_DB_PSSWD32);
                    credentials.put("db", INFINITY_DB_NAME32);
                    dbConnexionHelper.setCredentials(credentials);
                    dbConnexionHelper.setClientFma(dbConnexionHelper.getDbConnection(credentials));
                    dbConnexionHelper.getClientFma().getConnection(event -> {
                        if (event.succeeded()) {
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                        }
                    });
                } else {
                    dbConnexionHelper.getClientFma().getConnection(event -> {
                        if (event.succeeded()) {
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                        }
                    });
                }
                break;

            case Phone_Sys:
                if (dbConnexionHelper.getClientIC() == null) {
                    credentials.put("host", IC_DB_HOST);
                    credentials.put("user", IC_DB_USER);
                    credentials.put("psswd", IC_DB_PSSWD);
                    credentials.put("db", IC_DB_NAME);
                    dbConnexionHelper.setCredentials(credentials);

                    dbConnexionHelper.setClientIC(dbConnexionHelper.getDbConnection(credentials));
                    dbConnexionHelper.getClientIC().getConnection(event -> {
                        if (event.succeeded()) {
                            System.out.println("Connection succeeded");
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                        }
                    });
                } else {
                    dbConnexionHelper.getClientIC().getConnection(event -> {
                        if (event.succeeded()) {
                            System.out.println("Connection succeeded");
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                        }
                    });
                }

                break;

            case Quick_Book:
                if (dbConnexionHelper.getClientQuickBook() == null) {
                    credentials.put("host", Q_B_DB_HOST);
                    credentials.put("user", Q_B_DB_USER);
                    credentials.put("psswd", Q_B_DB_PSSWD);
                    credentials.put("db", Q_B_DB_NAME);
                    dbConnexionHelper.setCredentials(credentials);
                    dbConnexionHelper.setClientQuickBook(dbConnexionHelper.getDbConnection(credentials));
                    dbConnexionHelper.getClientQuickBook().getConnection(event -> {
                        if (event.succeeded()) {
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                        }
                    });
                } else {
                    dbConnexionHelper.getClientQuickBook().getConnection(event -> {
                        if (event.succeeded()) {
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                        }
                    });
                }
                break;

            default:
                if (dbConnexionHelper.getClientRP() == null) {
                    credentials.put("host", RP_DB_HOST);
                    credentials.put("user", RP_DB_USER);
                    credentials.put("psswd", RP_DB_PSSWD);
                    credentials.put("db", RP_DB_NAME);
                    dbConnexionHelper.setCredentials(credentials);
                    dbConnexionHelper.setClientRP(dbConnexionHelper.getDbConnection(credentials));
                    dbConnexionHelper.getClientRP().getConnection(event -> {
                        if (event.succeeded()) {
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                        }
                    });
                } else {
                    dbConnexionHelper.getClientRP().getConnection(event -> {
                        if (event.succeeded()) {
                            credentials = new HashMap<>();
                            handler.handle(event);
                        } else {
                            Logger.getInstance("").log(LoggerTag.ERROR, ExceptionUtils.exceptionStackTraceAsString(event.cause()), "");
                        }
                    });
                }
                break;
        }
    }
}