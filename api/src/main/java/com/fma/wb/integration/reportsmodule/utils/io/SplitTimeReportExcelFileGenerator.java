package com.fma.wb.integration.reportsmodule.utils.io;

import com.fma.wb.integration.reportsmodule.dto.splittime.SplitTimeDto;
import com.fma.wb.integration.reportsmodule.dto.splittime.SplitTimeReportOutputData;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class SplitTimeReportExcelFileGenerator {
    private SplitTimeReportOutputData data =  new SplitTimeReportOutputData();
    //private Workbook workbook = new XSSFWorkbook();
    //private static String[] columnNames = {"Name", "Manager", "Number of Calls", "Mins Duration", "Split Time", "Calls Short"};
    private static String[] columnNames = {"Name", "Manager", "Number of Calls", "Mins Duration", "Split Time", "Factor"};
    private DataFormat styleNumberCustomFormat;
    private short csStyleNumberCustomFormat;
    private DataFormat styleNumberCustomFormatSpecial;
    private short csStyleNumberCustomFormatSpecial;

    public SplitTimeReportExcelFileGenerator() {
    }

    public SplitTimeReportExcelFileGenerator(SplitTimeReportOutputData data) {
        this.data = data;
    }

    public String getBase64GeneratedFile() {
        String fullTime = "Reg F/T";
        String partTime = "Reg P/T";

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Split Time Calculation Report"+System.currentTimeMillis());

        styleNumberCustomFormat = workbook.createDataFormat();
        csStyleNumberCustomFormat = styleNumberCustomFormat.getFormat("#,##0");

        styleNumberCustomFormatSpecial = workbook.createDataFormat();
        csStyleNumberCustomFormatSpecial = styleNumberCustomFormatSpecial.getFormat("#");

        AtomicInteger atomicInteger = new AtomicInteger();

        //SplitTimeReportOutputData dataListToProcess = this.data;
        String requestDate = this.data.getDateRequest();

        OutputStream fileOut;
        String fileName = Paths.get("assets") +File.separator+"splitTimeReport_test.xlsx";

        Font basicContentFont = workbook.createFont();
        basicContentFont.setFontHeightInPoints((short) 10);
        basicContentFont.setFontName("Arial");
        basicContentFont.setBold(false);

        Font header1Font = workbook.createFont();
        header1Font.setFontHeightInPoints((short) 10);
        header1Font.setFontName("Arial");
        header1Font.setColor(IndexedColors.WHITE.getIndex());
        header1Font.setBold(true);

        //Style header
        CellStyle styleHeader1 = workbook.createCellStyle();
        styleHeader1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleHeader1.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
        styleHeader1.setFillBackgroundColor(IndexedColors.DARK_BLUE.getIndex());
        styleHeader1.setFont(header1Font);
        styleHeader1.setAlignment(HorizontalAlignment.CENTER);

        //Style content sheet aligned left
        CellStyle styleContent1 = workbook.createCellStyle();
        styleContent1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleContent1.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        styleContent1.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        styleContent1.setFont(basicContentFont);
        styleContent1.setAlignment(HorizontalAlignment.LEFT);

        //Style content sheet aligned center
        CellStyle styleContent2 = workbook.createCellStyle();
        styleContent2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleContent2.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        styleContent2.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        styleContent2.setFont(basicContentFont);
        styleContent2.setAlignment(HorizontalAlignment.CENTER);
        styleContent2.setDataFormat(csStyleNumberCustomFormat);

        CellStyle styleContent3 = workbook.createCellStyle();
        styleContent3.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleContent3.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        styleContent3.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        styleContent3.setFont(basicContentFont);
        styleContent3.setAlignment(HorizontalAlignment.CENTER);
        styleContent3.setDataFormat(csStyleNumberCustomFormatSpecial);

        //Style content sheet aligned center with background in green
        CellStyle styleContentGreen = workbook.createCellStyle();
        styleContentGreen.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleContentGreen.setFillForegroundColor(IndexedColors.BRIGHT_GREEN1.getIndex());
        styleContentGreen.setFillBackgroundColor(IndexedColors.BRIGHT_GREEN1.getIndex());
        styleContentGreen.setFont(basicContentFont);
        styleContentGreen.setAlignment(HorizontalAlignment.CENTER);

        //Style content sheet aligned center with background in yellow
        CellStyle styleContentYellowCenter = workbook.createCellStyle();
        styleContentYellowCenter.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleContentYellowCenter.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        styleContentYellowCenter.setFillBackgroundColor(IndexedColors.YELLOW.getIndex());
        styleContentYellowCenter.setFont(basicContentFont);
        styleContentYellowCenter.setAlignment(HorizontalAlignment.CENTER);

        //Style content sheet aligned left with background in yellow
        CellStyle styleContentYellowLeft = workbook.createCellStyle();
        styleContentYellowLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleContentYellowLeft.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        styleContentYellowLeft.setFillBackgroundColor(IndexedColors.YELLOW.getIndex());
        styleContentYellowLeft.setFont(basicContentFont);
        styleContentYellowLeft.setAlignment(HorizontalAlignment.LEFT);

        sheet.createRow(atomicInteger.getAndIncrement());

        Row row1 = sheet.createRow(atomicInteger.getAndIncrement());
        Cell cellRow1 = row1.createCell(0);
        cellRow1.setCellValue("Date Request");
        cellRow1.setCellStyle(styleContent1);

        Cell cellRow2 = row1.createCell(1);
        //cellRow2.setCellValue("7/2/19");
        cellRow2.setCellValue(requestDate);
        cellRow2.setCellStyle(styleContent1);

        Row row3 = sheet.createRow(atomicInteger.getAndIncrement());
        Cell cellRow3 = row3.createCell(0);
        cellRow3.setCellValue("Part Time");
        cellRow3.setCellStyle(styleContentYellowLeft);

        Cell cellRow31 = row3.createCell(1);
        cellRow31.setCellValue("360");
        cellRow31.setCellStyle(styleContent1);


        Row row4 = sheet.createRow(atomicInteger.getAndIncrement());

        Cell cellRow4 = row4.createCell(0);
        cellRow4.setCellValue("Full Time");
        cellRow4.setCellStyle(styleContent1);

        Cell cellRow41 = row4.createCell(1);
        cellRow41.setCellValue("480");
        cellRow41.setCellStyle(styleContent1);

        sheet.createRow(atomicInteger.getAndIncrement());

        // Create a Row for header
        Row headerRow = sheet.createRow(atomicInteger.getAndIncrement());

        this.data.getSplitTimeDtoList().forEach(splitTimeDto -> {
            System.out.println("-+++++++++++++++++++++++++++++++++++++++++++++++++++  splitTimeDto I AM HERE " + splitTimeDto.toString());
        });

        // Create cells
        for (int i = 0; i < columnNames.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columnNames[i]);
            cell.setCellStyle(styleHeader1);
            int columnIndex = cell.getColumnIndex();
            sheet.autoSizeColumn(columnIndex);
        }

        /*List<SplitTimeDto> splitTimeDtoListTemp = new ArrayList<>();

        SplitTimeDto splitTimeReportOutputData1 = new SplitTimeDto();
        splitTimeReportOutputData1.setName("Sandrea Lockett");
        splitTimeReportOutputData1.setManager("Manager 1");
        splitTimeReportOutputData1.setNumberOfCalls(237);
        splitTimeReportOutputData1.setMinsDuration("178");
        splitTimeReportOutputData1.setSplitTime("0.766455696202532");
        splitTimeReportOutputData1.setCallsShort("-146");
        splitTimeReportOutputData1.setFactor("480");

        SplitTimeDto splitTimeReportOutputData2 = new SplitTimeDto();
        splitTimeReportOutputData2.setName("Imanue Perdue");
        splitTimeReportOutputData2.setManager("Manager 2");
        splitTimeReportOutputData2.setNumberOfCalls(208);
        splitTimeReportOutputData2.setMinsDuration("161");
        splitTimeReportOutputData2.setSplitTime("0.956971153846154");
        splitTimeReportOutputData2.setCallsShort("-108");
        splitTimeReportOutputData2.setFactor("360");

        SplitTimeDto splitTimeReportOutputData3 = new SplitTimeDto();
        splitTimeReportOutputData3.setName("Cedric Jefferson");
        splitTimeReportOutputData3.setManager("Manager 3");
        splitTimeReportOutputData3.setNumberOfCalls(187);
        splitTimeReportOutputData3.setMinsDuration("107");
        splitTimeReportOutputData3.setSplitTime("1.99260249554367");
        splitTimeReportOutputData3.setCallsShort("-1");
        splitTimeReportOutputData3.setFactor("480");

        SplitTimeDto splitTimeReportOutputData4 = new SplitTimeDto();
        splitTimeReportOutputData4.setName("Elena Martinez");
        splitTimeReportOutputData4.setManager("Manager 4");
        splitTimeReportOutputData4.setNumberOfCalls(99);
        splitTimeReportOutputData4.setMinsDuration("162");
        splitTimeReportOutputData4.setSplitTime("2.00420875420875");
        splitTimeReportOutputData4.setCallsShort("0");
        splitTimeReportOutputData4.setFactor("360");

        SplitTimeDto splitTimeReportOutputData5 = new SplitTimeDto();
        splitTimeReportOutputData5.setName("Wendy Mc Dermott");
        splitTimeReportOutputData5.setManager("Manager 5");
        splitTimeReportOutputData5.setNumberOfCalls(116);
        splitTimeReportOutputData5.setMinsDuration("127");
        splitTimeReportOutputData5.setSplitTime("2.01063218390805");
        splitTimeReportOutputData5.setCallsShort("1");
        splitTimeReportOutputData5.setFactor("360");

        SplitTimeDto splitTimeReportOutputData66 = new SplitTimeDto();
        splitTimeReportOutputData66.setName("Luna Vargas");
        splitTimeReportOutputData66.setManager("Manager 66");
        splitTimeReportOutputData66.setNumberOfCalls(1);
        splitTimeReportOutputData66.setMinsDuration("0");
        splitTimeReportOutputData66.setSplitTime("479.9");
        splitTimeReportOutputData66.setCallsShort("239");
        splitTimeReportOutputData66.setFactor("480");

        SplitTimeDto splitTimeReportOutputData6 = new SplitTimeDto();
        splitTimeReportOutputData6.setName("Melissa Buchanan");
        splitTimeReportOutputData6.setManager("Manager 6");
        splitTimeReportOutputData6.setNumberOfCalls(1);
        splitTimeReportOutputData6.setMinsDuration("0");
        splitTimeReportOutputData6.setSplitTime("479.933333333333");
        splitTimeReportOutputData6.setCallsShort("239");
        splitTimeReportOutputData6.setFactor("480");

        SplitTimeDto splitTimeReportOutputData7 = new SplitTimeDto();
        splitTimeReportOutputData7.setName("Laura Ovalle");
        splitTimeReportOutputData7.setManager("Manager 7");
        splitTimeReportOutputData7.setNumberOfCalls(1);
        splitTimeReportOutputData7.setMinsDuration("12");
        splitTimeReportOutputData7.setSplitTime("468.2");
        splitTimeReportOutputData7.setCallsShort("233");
        splitTimeReportOutputData7.setFactor("480");

        splitTimeDtoListTemp.add(splitTimeReportOutputData1);
        splitTimeDtoListTemp.add(splitTimeReportOutputData2);
        splitTimeDtoListTemp.add(splitTimeReportOutputData3);
        splitTimeDtoListTemp.add(splitTimeReportOutputData4);
        splitTimeDtoListTemp.add(splitTimeReportOutputData5);
        splitTimeDtoListTemp.add(splitTimeReportOutputData66);
        splitTimeDtoListTemp.add(splitTimeReportOutputData6);
        splitTimeDtoListTemp.add(splitTimeReportOutputData7);*/

        this.data.getSplitTimeDtoList().forEach(splitTimeDto -> {
            //splitTimeDtoListTemp.forEach(splitTimeDto -> {
            Row dataRow = sheet.createRow(atomicInteger.getAndIncrement());

            int i = 0;
            double minSplitTime = 2.00;

            //Name column
            Cell cell0 = dataRow.createCell(0);
            String value0 = "";
            if (splitTimeDto.getName() != null && !splitTimeDto.getName().isEmpty()) {
                value0 = splitTimeDto.getName();
            }
            cell0.setCellValue(value0);

            // For User part time, we highlight the Name cell background in yellow
            /*if (splitTimeDto.getFactor().equals("360")){
                cell0.setCellStyle(styleContentYellowLeft);
            } else {
                cell0.setCellStyle(styleContent1);
            }*/
            if (partTime.equals(splitTimeDto.getFullPt())){
                cell0.setCellStyle(styleContentYellowLeft);
            } else {
                cell0.setCellStyle(styleContent1);
            }

            int columnIndex1 = cell0.getColumnIndex();
            sheet.autoSizeColumn(columnIndex1);

            //Manager column
            Cell cell1 = dataRow.createCell(1);
            String value1 = "";
            if (splitTimeDto.getManager() != null && !splitTimeDto.getManager().isEmpty()) {
                value1 = splitTimeDto.getManager();
            }
            cell1.setCellValue(value1);
            cell1.setCellStyle(styleContent2);
            int columnIndex2 = cell1.getColumnIndex();
            sheet.autoSizeColumn(columnIndex2);

            //Number of Calls column
            Cell cell2 = dataRow.createCell(2);
            int value2 = splitTimeDto.getNumberOfCalls();

            cell2.setCellValue(value2);
            cell2.setCellStyle(styleContent3);
            int columnIndex3 = cell2.getColumnIndex();
            sheet.autoSizeColumn(columnIndex3);

            //Mins Duration column
            Cell cell3 = dataRow.createCell(3);
            int value3 = 0;
            if (splitTimeDto.getMinsDuration() != null && !splitTimeDto.getMinsDuration().isEmpty()) {
                value3 = Integer.valueOf(splitTimeDto.getMinsDuration());
            }
            cell3.setCellValue(value3);
            cell3.setCellStyle(styleContent3);
            int columnIndex4 = cell3.getColumnIndex();
            sheet.autoSizeColumn(columnIndex4);

            // Split time column
            Cell cell4 = dataRow.createCell(4);
            String value4 = "";
            if (splitTimeDto.getSplitTime() != null && !splitTimeDto.getSplitTime().isEmpty()) {
                value4 = splitTimeDto.getSplitTime();
            }

            BigDecimal splitTimeInBigDecimal = new BigDecimal(value4).setScale(2, RoundingMode.HALF_UP);

            if (splitTimeInBigDecimal.doubleValue() < minSplitTime) {
                styleContentGreen.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
                cell4.setCellStyle(styleContentGreen);
            } else {
                styleContent2.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
                cell4.setCellStyle(styleContent2);
            }
            cell4.setCellValue(splitTimeInBigDecimal.doubleValue());

            int columnIndex5 = cell4.getColumnIndex();
            sheet.autoSizeColumn(columnIndex5);

            //Factor
            Cell cell5 = dataRow.createCell(5);
            String value5 = "";
            if (splitTimeDto.getFactor() != null && !splitTimeDto.getFactor().isEmpty()) {
                value5 = splitTimeDto.getFactor();
            }

            BigDecimal factorInBigDecimal = new BigDecimal(value5).setScale(2, RoundingMode.HALF_UP);

            styleContent2.setDataFormat(workbook.createDataFormat().getFormat("0.00"));
            cell5.setCellStyle(styleContent2);

            cell5.setCellValue(factorInBigDecimal.doubleValue());

            int columnIndex6 = cell5.getColumnIndex();
            sheet.autoSizeColumn(columnIndex6);

        });

        try {

            fileOut = new FileOutputStream(fileName);

            workbook.write(fileOut);

            fileOut.close();
            workbook.close();
            System.out.println("END......");

            return encodeFileToBase64Binary(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("END......");

        return "";
    }

    private static String encodeFileToBase64Binary(String fileName) throws IOException {
        File file = new File(fileName);
        byte[] encoded = Base64.encodeBase64(Files.readAllBytes(file.toPath()));
        return new String(encoded, StandardCharsets.US_ASCII);
    }

    public static void main(String[] args) {
        //getBase64GeneratedFile();
    }

}