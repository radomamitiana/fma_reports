import { SplitTimeReportInputData } from './../dto/split';
import { Component, OnInit } from '@angular/core';
import { User } from '../dto/user';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { RestService } from '../rest.service';
import { FileSaverService } from 'ngx-filesaver';

@Component({
  selector: 'app-split-time',
  templateUrl: './split-time.component.html',
  styleUrls: ['./split-time.component.scss']
})
export class SplitTimeComponent implements OnInit {

  splittime: SplitTimeReportInputData = new SplitTimeReportInputData();
  currentUser: User;
  loading = false;


options = new FormGroup({
  startDate: new FormControl('', Validators.required),
  endDate: new FormControl('', Validators.required),
});


constructor(
  public dialog: MatDialog,
  private router: Router,
  private snackBar: MatSnackBar,
  private restService: RestService,
  private ffS: FileSaverService,
  ) { }

  ngOnInit() {

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    }



    run() {
      if (this.options.valid) {
        console.log('OK');
        console.log(this.splittime);
        console.log(JSON.stringify(this.splittime));
        this.loading = true;

        this.restService.getSplit(this.splittime).subscribe((httpResponse: HttpResponse<any>) => {
          console.log('Response: ' + httpResponse);
          const response = httpResponse.body;
          this.loading = false;

          switch ('' + response.responseType) {
            case 'TYPE_REQUEST':
            // localStorage.setItem('currentSplit', response.data);
            // this.router.navigate(['/healthresult']);
            const blob = this.base64ToBlob(response.data, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            this.ffS.save(blob, Date.now() + '_split.xlsx');

            this.snackBar.open('Open the file', 'OK', {
              duration: 4000,
            });

            break;

            default:
            this.snackBar.open('An error occured: ' + response.comment, 'OK', {
              duration: 4000,
            });
            break;

          }

        }, error => {
          this.loading = false;

          console.log(error.json());
          this.snackBar.open('Error sending request', 'OK', {
            duration: 4000,
          });
        });

  /*       setTimeout( () => {
          this.loading = false;
          this.router.navigate(['/trustresult']);

      }, 5000); */
      } else {
        this.snackBar.open('Fill all required fields', 'OK', {
          duration: 4000,
        });
      }
    }



    savaFile(data: string[], format: string) {
      console.log(Date.now());


      const blob = this.base64ToBlob(data[0], 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      this.ffS.save(blob, Date.now() + '_split.xlsx');



/*
      const blob3 = this.base64ToBlob(data[2], 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      this.ffS.save(blob3, Date.now() + '_trust.xlsx');
 */
      this.snackBar.open('Open files', 'OK', {
        duration: 4000,
      });
      // this.location.back();

    }


    public base64ToBlob(b64Data, contentType= '', sliceSize= 512) {
      b64Data = b64Data.replace(/\s/g, ''); // For IE compatibility.
      const byteCharacters = atob(b64Data);
      const byteArrays = [];
      for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          const slice = byteCharacters.slice(offset, offset + sliceSize);

          const byteNumbers = new Array(slice.length);
          for (let i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
          }
          const byteArray = new Uint8Array(byteNumbers);
          byteArrays.push(byteArray);
      }
      return new Blob(byteArrays, {type: contentType});
  }

}
