import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplitTimeComponent } from './split-time.component';

describe('SplitTimeComponent', () => {
  let component: SplitTimeComponent;
  let fixture: ComponentFixture<SplitTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplitTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
