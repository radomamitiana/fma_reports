import { WebsocketService } from './../websocket.service';
import { LoginDialogComponent } from './../login-dialog/login-dialog.component';
import { User } from './../dto/user';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { DialogData } from '../dto/dialog-data';
import { RestService } from '../rest.service';
import { ApiResponse } from '../dto/api-response';
import { WsResponseType } from '../dto/ws-response-type';
import { HttpResponse } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit, OnDestroy {

  currentUser: User;
  data: DialogData = {title: 'Logout', content: 'Comfirm logout?', showLoader: false, valid: false};

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private restService: RestService,
    private snackBar: MatSnackBar,
    private location: Location,
    private wsService: WebsocketService,
    ) {

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.wsService = new WebsocketService();
    this.wsService.subscribe(this.dialog, this.router);

  }

  ngOnInit() {
  }

  ngOnDestroy() {
    // this.wsService.close();
  }


  /*logoutUser() {
    console.log('DECONNECTING...');
    this.wsService.close();
    localStorage.clear();
    this.dialog.closeAll();
    this.router.navigate(['login/']);
  }*/

  logout() {
    const dialogRef = this.dialog.open(LoginDialogComponent, {
      width: '500px',
      data: this.data
    });

    dialogRef.afterClosed().subscribe(result => {
      // cancel the login process...
      console.log('RR: ' + result);
      if (result !== undefined) {

        this.restService.logoutUser().subscribe((httpResponse: HttpResponse<any>) => {

          console.log('Response: ' + httpResponse);
          const response = httpResponse.body;
          console.log(response);

          switch ('' + response.responseType) {
            case 'TYPE_REQUEST' :
                this.wsService.close();
                localStorage.clear();
                this.dialog.closeAll();
                this.router.navigate(['login/']);
                break;

            default:
            this.snackBar.open('Error while logging out \n' + response.comment, 'OK', {
              duration: 4000,
            });
            break;

          }

        }, error => {

          localStorage.clear();
          this.dialog.closeAll();
          this.router.navigate(['login/']);

          /* this.snackBar.open('Error while logging out', 'OK', {
            duration: 4000,
          }); */
        });
      }

    });


  }



  back() {
    this.location.back(); // <-- go back to previous location
  }

  home() {
    this.router.navigate(['home/']); // <-- go back to home
  }

}
