export enum WsResponseType {
    TYPE_REQUEST,
    TYPE_EVENT,
    TYPE_ERROR
}
