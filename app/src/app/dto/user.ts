export class User {
    // tslint:disable-next-line:variable-name
    user_id: number;
    sessionId: string;
    username: string;
    password: string;
    // tslint:disable-next-line:variable-name
    last_name: string;
    // tslint:disable-next-line:variable-name
    first_name: string;
    email: string;
    roles: Array<number> = [];
    isconnected = false;

    addRole(role: number) {
        this.roles.push(role);
    }


}
