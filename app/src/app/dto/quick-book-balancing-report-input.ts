export class QuickBookBalancingReportInputData {
  iWantTo: number = 0;
  startDate: string;
  stopDate: string;
  dataSelectionRange: number = 0;
  organizationId: number = 0;
  allOrganizations: boolean = false;
}
