export class Option {
    text: string;
    icon: string;
    roles: Array<string> = [];
}
