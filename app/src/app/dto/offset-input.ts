export class OffsetInputData {
    offset_id = 0;
    org_id = 0;
    client_id = 0;
    offset_date: string;
    wire_offset_amt: '';
    ar_offset_amt: '';
    created_by: string;
    created_on: string;
    changed_by: '';
    changed_on: string;
    cancel_status: 0;
}
