import { WsResponseType } from './ws-response-type';
export class ApiResponse {
    responseType: WsResponseType;
    data = '';
    comment = '';
    date = '';
    dataSize = 0;
}
