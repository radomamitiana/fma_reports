export class TrustBalancingInputData {
    organizationList: Array<string> = [];
    showClientAccountNums = false;
    matchData = false;
    displayRecovererCodeTotals = false;
    beginDateRange: string;
    endDateRange: string;
    disableMatchPossibleDebtors = false;
    findAmounts = 0;

}

export class TrustBalancingOutputData {
    // TODO: to change

}
