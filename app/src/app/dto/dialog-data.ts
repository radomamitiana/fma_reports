export class DialogData {
    title: string;
    content: string;
    showLoader: boolean;
    valid = false;
  }
