export class HealthCareInputData {
    organizationId: number;
    beginDateRange: Date;
    endDateRange: Date;
    showClients: boolean;
}
