export class Organization {
      // tslint:disable-next-line:variable-name
      organization_id: number;
      name = '';
      // tslint:disable-next-line:variable-name
      segment_id;
      // tslint:disable-next-line:variable-name
      trust_id;
      // tslint:disable-next-line:variable-name
      match_switch = '';
      active = '';
      converted = '';
      status = '';
      // tslint:disable-next-line:variable-name
      credit_card_sweep_category = '';
      // tslint:disable-next-line:variable-name
      is_portfolio_org = '';
      // tslint:disable-next-line:variable-name
      remit_with_organization_id = '';
      // tslint:disable-next-line:variable-name
      pay_principle_first = '';
      // tslint:disable-next-line:variable-name
      match_invoice_category = '';
      // tslint:disable-next-line:variable-name
      match_invoice_subtype = '';
      // tslint:disable-next-line:variable-name
      mask_account_number = '';
      // tslint:disable-next-line:variable-name
      mask_ssn = '';
      allowimc = '';
      // tslint:disable-next-line:variable-name
      imc_phone = '';
      // tslint:disable-next-line:variable-name
      payment_letter: number;
      // tslint:disable-next-line:variable-name
      override_Placement = '';
      // tslint:disable-next-line:variable-name
      email_indicator = '';
      // tslint:disable-next-line:variable-name
      payonline_indicator = '';
      // tslint:disable-next-line:variable-name
      rpt_short_name = '';
}
