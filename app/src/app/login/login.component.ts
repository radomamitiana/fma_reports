import { ApiResponse } from './../dto/api-response';
import { User } from './../dto/user';
import { LoginDialogComponent } from './../login-dialog/login-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { RestService } from '../rest.service';
import { WsResponseType } from '../dto/ws-response-type';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  title = 'Login user';
  content = '';
  showLoader = true;
  loading = false;
  currentUser: User;
  errorMsg = '';

  constructor(public dialog: MatDialog,
              private router: Router,
              private snackBar: MatSnackBar,
              private restService: RestService,
              ) { }

  ngOnInit(): void {

     if (localStorage.getItem('currentUser') == null || localStorage.getItem('currentUser') === undefined) {
      this.currentUser = new User();
    } else {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.router.navigate(['/home']);
    }

  }

  loginUser() {
    this.currentUser.username = this.loginForm.get('username').value;
    this.currentUser.password = this.loginForm.get('password').value;

    console.warn(this.loginForm.value);
    // const bf = new Blowfish('super key', Blowfish.MODE.ECB, Blowfish.PADDING.NULL)
    this.loading = true;

    if (this.loginForm.valid) {

      this.openDialog();

      this.restService.loginUser(this.currentUser.username, this.currentUser.password).subscribe((httpResponse: HttpResponse<any>) => {
        console.log(httpResponse);
        console.log('Body: ');
        console.log(httpResponse.body);

        const response = httpResponse.body;

        this.dialog.closeAll();
        switch ('' + response.responseType) {
          case 'TYPE_REQUEST' :
              this.currentUser = JSON.parse(response.data);
              localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
              const token = httpResponse.headers.get('token');
              console.log(httpResponse.headers);
              console.log('Token:');
              console.log(token);
              localStorage.setItem('currentAuthToken', token);

              this.router.navigate(['/home']);
              break;

          case 'TYPE_EVENT' :
          break;

          default:
          this.snackBar.open(response.comment, 'OK', {
            duration: 4000,
          });
          break;

        }

      }, error => {
        console.log(error);
        this.dialog.closeAll();
        this.snackBar.open('Error login user, check your network connection', 'OK', {
          duration: 4000,
        });
        return;
      });


/*  setTimeout( () => {
      this.dialog.closeAll();
      this.router.navigate(['/home']);

      }, 3000); */

    } else {
      this.snackBar.open('UserName and PassWord are required', 'OK', {
        duration: 4000,
      });
    }

  }

  openDialog(): void {
      const dialogRef = this.dialog.open(LoginDialogComponent, {
        width: '500px',
        data: {title: this.title, content: this.content, showLoader: this.showLoader}
      });

      dialogRef.afterClosed().subscribe(result => {

        if (result === undefined) {
        }
      });



  }
}
