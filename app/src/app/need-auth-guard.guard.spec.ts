import { TestBed, async, inject } from '@angular/core/testing';

import { NeedAuthGuardGuard } from './need-auth-guard.guard';

describe('NeedAuthGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NeedAuthGuardGuard]
    });
  });

  it('should ...', inject([NeedAuthGuardGuard], (guard: NeedAuthGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
