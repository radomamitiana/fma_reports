import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrustbalancingComponent } from './trustbalancing.component';

describe('TrustbalancingComponent', () => {
  let component: TrustbalancingComponent;
  let fixture: ComponentFixture<TrustbalancingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrustbalancingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrustbalancingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
