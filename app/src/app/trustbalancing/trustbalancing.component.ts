import { ApiResponse } from './../dto/api-response';
import { User } from './../dto/user';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';
import { TrustBalancingInputData } from '../dto/trust';
import { RestService } from '../rest.service';
import { WsResponseType } from '../dto/ws-response-type';
import { HttpResponse } from '@angular/common/http';
@Component({
  selector: 'app-trustbalancing',
  templateUrl: './trustbalancing.component.html',
  styleUrls: ['./trustbalancing.component.scss']
})
export class TrustbalancingComponent implements OnInit {

  trustIput: TrustBalancingInputData = new TrustBalancingInputData();

  companiesLoaded = true;
  loading = false;
  loadingComp = true;
  currentUser: User;
  companies = [];

options = new FormGroup({
  comp: new FormControl('', Validators.required),
  totals: new FormControl(''),
  match: new FormControl(''),
  acct: new FormControl(''),
  debtors: new FormControl(''),
  startDate: new FormControl('', Validators.required),
  endDate: new FormControl('', Validators.required),
  amount: new FormControl(''),
});

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private snackBar: MatSnackBar,
    private restService: RestService,
    ) {
      this.restService.getCompList().subscribe((httpResponse: HttpResponse<any>) => {
        console.log('Response: ' + httpResponse);
        const response = httpResponse.body;
        this.loadingComp = false;
        switch ('' + response.responseType) {
          case 'TYPE_REQUEST':
          this.companies = JSON.parse(response.data);
          console.log(this.companies);
          break;

          default:
          this.loadingComp = false;
          this.snackBar.open('Error loading organization list: ' + response.comment, 'OK', {
            duration: 4000,
          });
          break;

        }
      }, error => {
        console.log(error);
        this.dialog.closeAll();
        this.snackBar.open('Error loading organization list, check your network connection', 'OK', {
          duration: 4000,
        });
        this.loadingComp = false;
        return;
      });

    }

ngOnInit() {
/* if (localStorage.getItem('currentUser') == null || localStorage.getItem('currentUser') === undefined) {
this.currentUser = new User();
this.router.navigate(['/login']);
} else {
this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
} */

this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

}

  run() {
    if (this.options.valid) {
      console.log('OK');
      this.loading = true;
      this.restService.getTrust(this.trustIput).subscribe((httpResponse: HttpResponse<any>) => {
        console.log('Response: ' + httpResponse);
        const response = httpResponse.body;
        this.loading = false;

        switch ('' + response.responseType) {
          case 'TYPE_REQUEST':
          localStorage.setItem('currentTrust', response.data);

          this.router.navigate(['/trustresult']);
          break;

          default:
          this.snackBar.open('Error: ' + response.comment, 'OK', {
            duration: 4000,
          });
          break;

        }

      }, error => {
        this.loading = false;
        console.log(error.json());
        this.snackBar.open('Error sending request', 'OK', {
          duration: 4000,
        });
      });

/*       setTimeout( () => {
        this.loading = false;
        this.router.navigate(['/trustresult']);

    }, 5000); */
    } else {
      this.snackBar.open('Fill all required fields', 'OK', {
        duration: 4000,
      });
    }
  }

}
