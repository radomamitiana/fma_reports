import {DataSource, CollectionViewer} from '@angular/cdk/collections';
import {Customer} from 'src/app/dto/customer';
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';
import {Observable} from 'rxjs/internal/Observable';
import {QuickBookBalancingService} from './quick-book-balancing.service';
import {HttpResponse} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CustomerDataSource implements DataSource<Customer> {

    private customerSubject = new BehaviorSubject<Customer[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private quickBookBalancingService: QuickBookBalancingService,
                private snackBar: MatSnackBar) {

    }

    connect(collectionViewer: CollectionViewer): Observable<any> {
        return this.customerSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.customerSubject.complete();
        this.loadingSubject.complete();
    }

    loadCustomer(organizationId) {

        if (organizationId) {
            const customers = [];
            this.quickBookBalancingService.getClientByOrganization(organizationId).subscribe((httpResponse: HttpResponse<any>) => {
                const response = httpResponse.body;
                switch ('' + response.responseType) {
                    case 'TYPE_REQUEST':
                        if (response && response.data && response.dataSize) {
                            const responseDto = JSON.parse(response.data);
                            for (let i = 0; i < response.dataSize; i++) {
                                const customer = new Customer();
                                customer.clientId = responseDto[i].client_id;
                                customer.clientName = responseDto[i].clientName;
                                customer.description = responseDto[i].description;
                                customer.pa = responseDto[i].paid_agency;
                                customer.pd = responseDto[i].paid_direct;
                                customer.processed = responseDto[i].fee_grace_period;
                                customer.ra = responseDto[i].nsf_agency;
                                customer.rd = responseDto[i].nsf_direct;
                                customers[i] = customer;
                            }
                            this.customerSubject.next(customers);
                        }
                        this.loadingSubject.next(false);
                        break;

                    default:
                        this.snackBar.open('Error loading client organization list: ' + response.comment, 'OK', {
                            duration: 4000,
                        });
                        break;

                }
            }, error => {
                console.log(error);
                return;
            });
            this.customerSubject.next(customers);
        }
    }
}
