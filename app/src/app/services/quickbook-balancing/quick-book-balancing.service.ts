import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../../dto/user';
import {CollectionsProcessingInputData} from '../../dto/collections-processing-input';
import {OffsetInputData} from '../../dto/offset-input';
import {QuickBookBalancingReportInputData} from '../../dto/quick-book-balancing-report-input';

@Injectable({
  providedIn: 'root'
})
export class QuickBookBalancingService {

  /* URL MAPPING */
  serverBaseURL = 'http://localhost:9095';
  getOrganizationListUrl = this.serverBaseURL + '/ws/auth/getOrganizationQuickBookList';
  getClientByOrganizationUrl = this.serverBaseURL + '/ws/auth/getClientOrganizationQuick';
  saveCollectionsProcessingByOneClientUrl = this.serverBaseURL + '/ws/auth/saveCollectionsProcessingByOneClient';
  saveCollectionsProcessingByAllClient = this.serverBaseURL + '/ws/auth/saveCollectionsProcessingByAllClient';
  saveOffsetUrl = this.serverBaseURL + '/ws/auth/saveOffset';
  getCollectionsProcessingUrl = this.serverBaseURL + '/ws/auth/getCollectionsProcessing';
  printProcInstrUrl = this.serverBaseURL + '/ws/auth/printProcInstr';
  createReportUrl = this.serverBaseURL + '/ws/auth/quickBookBalancingReportData';

  private headers: HttpHeaders;

  constructor(public http: HttpClient) {
  }

  createAuthorizationHeader(): HttpHeaders {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set('Authorization', 'Bearer ' + this.getUserCurToken())
      .set('Cache-control', 'no-cache')
      .set('Pragma', 'no-cache');
    this.headers = headers;
    return this.headers;
  }

  getCurrentUser(): User {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  getUserCurToken(): string {
    return localStorage.getItem('currentAuthToken') || '';
  }

  getOrganizationList() {
    return this.http.get(this.getOrganizationListUrl + ':' + this.getCurrentUser().sessionId,
      {headers: this.createAuthorizationHeader(), observe: 'response'});
  }

  getClientByOrganization(organizationId) {
    return this.http.post(this.getClientByOrganizationUrl + ':' + this.getCurrentUser().sessionId, {organizationId},
      {headers: this.createAuthorizationHeader(), observe: 'response'});
  }

  saveCollectionsProcessing(collectionsProcessing: CollectionsProcessingInputData, isAll: boolean) {
    const urlWS = (isAll) ? this.saveCollectionsProcessingByAllClient : this.saveCollectionsProcessingByOneClientUrl;
    return this.http.post(urlWS + ':' + this.getCurrentUser().sessionId, JSON.stringify(collectionsProcessing),
      {headers: this.createAuthorizationHeader(), observe: 'response'});
  }

  getCollectionsProcessing(organizationId, clientId) {
    return this.http.post(this.getCollectionsProcessingUrl + ':' + this.getCurrentUser().sessionId, {organizationId, clientId},
      {headers: this.createAuthorizationHeader(), observe: 'response'});
  }

  saveOffset(offset: OffsetInputData) {
    return this.http.post(this.saveOffsetUrl + ':' + this.getCurrentUser().sessionId, JSON.stringify(offset),
      {headers: this.createAuthorizationHeader(), observe: 'response'});
  }

  printProcInstr() {
    return this.http.get(this.printProcInstrUrl + ':' + this.getCurrentUser().sessionId, {
      headers: this.createAuthorizationHeader(),
      observe: 'response'
    });
  }

  createReport(quickBookBalancingReportInputData: QuickBookBalancingReportInputData) {
    return this.http.post(this.createReportUrl + ':' + this.getCurrentUser().sessionId, JSON.stringify(quickBookBalancingReportInputData), {
      headers: this.createAuthorizationHeader(),
      observe: 'response'
    });
  }
}
