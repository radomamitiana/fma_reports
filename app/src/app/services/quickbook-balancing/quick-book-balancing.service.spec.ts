import { TestBed } from '@angular/core/testing';

import { QuickBookBalancingService } from './quick-book-balancing.service';

describe('QuickBookBalancingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QuickBookBalancingService = TestBed.get(QuickBookBalancingService);
    expect(service).toBeTruthy();
  });
});
