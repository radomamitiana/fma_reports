import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogData } from '../dto/dialog-data';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss']
})
export class LoginDialogComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<LoginDialogComponent>,
               @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
  }

   cancel(): void {
    this.dialogRef.close();

  }

}
