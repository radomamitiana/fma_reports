import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './../dto/user';
import { Option } from '../dto/option';
import { RestService } from '../rest.service';
import { ApiResponse } from '../dto/api-response';
import { WsResponseType } from '../dto/ws-response-type';
import { MatSnackBar } from '@angular/material';
import { HttpResponse } from '@angular/common/http';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @Input() searchModel;

  @Output() searchModelChange: EventEmitter<any> = new EventEmitter();

  noOption = 'Loading options from server...';
  optionLoaded = false;
  options: Option[] = [];
  currentUser: User;
  breakpoint = 5;
  constructor(private router: Router, private restService: RestService, private snackBar: MatSnackBar) { }

  ngOnInit() {

    /*if (localStorage.getItem('currentUser') == null || localStorage.getItem('currentUser') === undefined) {
      this.currentUser = new User();
      this.router.navigate(['/login']);*/
    // } else {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));


      this.restService.getUserOptionList().subscribe((httpResponse: HttpResponse<any>) => {
        console.log('Response: ' + httpResponse);
        const response = httpResponse.body;
        switch ('' + response.responseType) {
          case 'TYPE_REQUEST' :
          this.noOption = '';
          this.optionLoaded = true;
          this.options = JSON.parse(response.data);
          break;
          default:
          this.optionLoaded = true;
          // TODO: this line is to be removed later
          // this.options = [{icon: './assets/img/analysis.svg', text: 'Trust Balancing', roles: ['1',  'kareen', 'admin']}];
          this.noOption = 'No available options, please be sure you have sufficients rights or check network connection';
          this.snackBar.open('Error loading options :' + response.comment, 'OK', {
            duration: 4000,
          });
          break;
        }
      }, error => {
        console.log(error);
        this.snackBar.open('Error loading oroption list, check your network connection', 'OK', {
          duration: 4000,
        });
        this.optionLoaded = true;
        // TODO: this line is to be removed later
        this.options = [{icon: './assets/img/analysis.svg', text: 'Trust Balancing', roles: ['1',  'kareen', 'admin']}];

        return;
      });

      // this.breakpoint = (window.innerWidth <= 400) ? 1 : 6;
      if (window.innerWidth <= 600) {
        this.breakpoint = 1;
      } else if (window.innerWidth <= 900) {
        this.breakpoint = 2;
      } else if (window.innerWidth <= 1200) {
        this.breakpoint = 3;
      } else {
        this.breakpoint = 5;
      }
    // }

    }

  openOption(option: string) {
    try {
    console.log('LINK: ' + option.toLowerCase().replace(' ' , ''));
    this.router.navigate([option.toLowerCase().replace(' ' , '')]);
    } catch (error) {
      console.log('NOT IMPLEMENTED');
    }

  }


  updateSearch(value) {
    this.searchModel = value;
    this.searchModelChange.emit(this.searchModel);
  }

  onResize(event) {
    if (event.target.innerWidth <= 600) {
      this.breakpoint = 1;

    } else if (event.target.innerWidth <= 900) {
      this.breakpoint = 2;
    } else if (event.target.innerWidth <= 1200) {
      this.breakpoint = 3;
    } else {
      this.breakpoint = 5;
    }
  }
}
