import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AlertModule } from 'ngx-bootstrap';
import { TrustbalancingComponent } from './trustbalancing/trustbalancing.component';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatPaginator, MatPaginatorModule, MatTabsModule} from '@angular/material';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SearchPipe } from './search.pipe';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialogModule} from '@angular/material/dialog';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatRippleModule, MatNativeDateModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import { HeaderComponent } from './header/header.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { TrustResultComponent } from './trust-result/trust-result.component';
import {MatDividerModule} from '@angular/material/divider';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTableModule} from '@angular/material/table';
import { FileSaverModule } from 'ngx-filesaver';
import { HealthcareComponent } from './healthcare/healthcare.component';
import { HealthResultComponent } from './health-result/health-result.component';
import { NeedAuthGuardGuard } from './need-auth-guard.guard';
import {MatTooltipModule} from '@angular/material/tooltip';
import { SplitTimeComponent } from './split-time/split-time.component';
import {QuickHomeComponent} from './quickbook-balancing/home/quick.home.component';
import { QuickOffsetComponent } from './quickbook-balancing/quick-offset/quick-offset.component';
import { QuickPrintComponent } from './quickbook-balancing/quick-print/quick-print.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    TrustbalancingComponent,
    AdmindashboardComponent,
    SearchPipe,
    LoginDialogComponent,
    HeaderComponent,
    TrustResultComponent,
    HealthcareComponent,
    HealthResultComponent,
    SplitTimeComponent,
    QuickHomeComponent,
    QuickOffsetComponent,
    QuickPrintComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatGridListModule,
    MatCardModule,
    MatInputModule,
    RouterModule,
    FormsModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatIconModule,
    MatRippleModule,
    MatSelectModule,
    MatRadioModule,
    MatDividerModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatTabsModule,
    FileSaverModule,
    AlertModule.forRoot()

  ],

  providers: [NeedAuthGuardGuard],
  exports: [LoginDialogComponent],
  entryComponents: [LoginDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
