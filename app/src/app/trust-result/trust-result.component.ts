import { User } from './../dto/user';
import { TrustBalancingOutputData } from './../dto/trust';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatSnackBar, MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { RestService } from '../rest.service';
import { FileSaverService } from 'ngx-filesaver';
import { Location } from '@angular/common';

@Component({
  selector: 'app-trust-result',
  templateUrl: './trust-result.component.html',
  styleUrls: ['./trust-result.component.scss']
})
export class TrustResultComponent implements OnInit {

  trustResult: TrustBalancingOutputData;
  currentUser: User;
  errorMsg = '';
  /* displayedColumns = ['position', 'name', 'weight', 'symbol'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator;
 */
  constructor(public dialog: MatDialog,
              private router: Router,
              private snackBar: MatSnackBar,
              private restService: RestService,
              private location: Location,
              private ffS: FileSaverService
              ) { }

  ngOnInit() {
    /* if (localStorage.getItem('currentUser') == null || localStorage.getItem('currentUser') === undefined) {
      this.currentUser = new User();
      this.router.navigate(['/login']);
    } else {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    } */

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

  }

  seeTrust() {
    const base64Trust = localStorage.getItem('currentTrust');
    console.log(Date.now());

    const blob = this.base64ToBlob(base64Trust, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    this.ffS.save(blob, Date.now() + '_trust.xlsx');

    this.snackBar.open('Open generated trust file', 'OK', {
      duration: 4000,
    });
    // this.location.back();

  }


  public base64ToBlob(b64Data, contentType= '', sliceSize= 512) {
    b64Data = b64Data.replace(/\s/g, ''); // For IE compatibility.
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);

        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, {type: contentType});
}

}


