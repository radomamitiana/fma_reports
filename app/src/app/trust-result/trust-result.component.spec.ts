import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrustResultComponent } from './trust-result.component';

describe('TrustResultComponent', () => {
  let component: TrustResultComponent;
  let fixture: ComponentFixture<TrustResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrustResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrustResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
