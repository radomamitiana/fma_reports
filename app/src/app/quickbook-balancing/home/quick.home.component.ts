import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialog, MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {HttpResponse} from '@angular/common/http';
import {SelectionModel} from '@angular/cdk/collections';
import {CollectionsProcessingInputData} from '../../dto/collections-processing-input';
import {CollectionsProcessingOutputData} from '../../dto/collections-processing-output';
import {CustomerDataSource} from '../../services/quickbook-balancing/customer-datasource';
import {QuickBookBalancingService} from '../../services/quickbook-balancing/quick-book-balancing.service';
import {User} from '../../dto/user';
import {FileSaverService} from 'ngx-filesaver';

@Component({
    selector: 'app-quick.home',
    templateUrl: './quick.home.component.html',
    styleUrls: ['./quick.home.component.scss']
})
export class QuickHomeComponent implements AfterViewInit, OnInit {

    collectionsProcessing: CollectionsProcessingInputData = new CollectionsProcessingInputData();
    collectionsProcessingOutput: CollectionsProcessingOutputData = new CollectionsProcessingOutputData();

    companiesLoaded = true;
    loading = false;
    loadingComp = true;
    currentUser: User;
    companies = [];
    applyToAllClient = false;

    pa_add_gross = false;
    pa_add_net = false;
    pa_subtract_tax = false;
    pa_ar_add_fees = false;
    pa_ar_add_tax = false;
    ra_add_gross = false;
    ra_add_net = false;
    ra_add_fees = false;
    ra_subtract_gross = false;
    ra_subtract_net = false;
    ra_subtract_fees = false;
    ra_do_nothing = false;
    ra_add_tax = false;
    ra_ar_add_net = false;
    ra_ar_add_gross = false;
    ra_ar_subtract_fee = false;
    ra_ar_subtract_tax = false;
    pd_subtract_fee = false;
    pd_subtract_tax = false;
    pd_ar_add_fee = false;
    pd_ar_add_tax = false;
    rd_add_fee = false;
    rd_add_tax = false;
    rd_ar_subtract_fee = false;
    rd_ar_subtract_tax = false;
    cancel_status = false;

    messageCheckingNsfAgency = 'Note:\n' +
        'You can select only 1 of the following\n' +
        '1) Add Gross\n' +
        '2) Add Net\n' +
        '3) Subtract Fee\n' +
        'When you select any one of the aforementioned it will deselect the others\n';


    options = new FormGroup({
        comp: new FormControl('', Validators.required),
        totals: new FormControl(''),
        match: new FormControl(''),
        acct: new FormControl(''),
        debtors: new FormControl(''),
        startDate: new FormControl('', Validators.required),
        endDate: new FormControl('', Validators.required),
        amount: new FormControl(''),
    });

    displayedColumns = ['clientName', 'clientId', 'processed', 'description', 'pa', 'pd', 'ra', 'rd'];

    /**** Begin PA ****/
    clientWirePA = 'Add Net';
    clientWireTypePA: string[] = ['Add Gross', 'Add Net'];
    accountsReceivablePA: string;
    /**** End PA ****/

    /**** Begin RA ****/
    clientWireRA = 'Do Nothing';
    clientWireTypeRA: string[] = ['Add Gross', 'Add Net', 'Add Fees', 'Substract Gross', 'Substract Net', 'Substract Fees', 'Do Nothing'];
    /**** End RA ****/

    organizationId = 0;
    clientId = 0;

    selection: any;

    selectedRowIndex = -1;

    constructor(
        public dialog: MatDialog,
        private router: Router,
        private snackBar: MatSnackBar,
        private quickBookBalancingService: QuickBookBalancingService,
        private dataSource: CustomerDataSource,
        private ffS: FileSaverService
    ) {
        this.selection = new SelectionModel<CustomerDataSource>(false, null);
        this.quickBookBalancingService.getOrganizationList().subscribe((httpResponse: HttpResponse<any>) => {
            console.log('Response: ' + httpResponse);
            const response = httpResponse.body;
            this.loadingComp = false;
            switch ('' + response.responseType) {
                case 'TYPE_REQUEST':
                    this.companies = JSON.parse(response.data);
                    break;

                default:
                    this.loadingComp = false;
                    this.snackBar.open('Error loading organization list: ' + response.comment, 'OK', {
                        duration: 4000,
                    });
                    break;

            }
        }, error => {
            this.dialog.closeAll();
            this.snackBar.open('Error loading organization list, check your network connection', 'OK', {
                duration: 4000,
            });
            this.loadingComp = false;
            return;
        });
    }

    ngOnInit() {
        if (localStorage.getItem('currentUser') == null || localStorage.getItem('currentUser') === undefined) {
            this.currentUser = new User();
            this.router.navigate(['/login']);
        } else {
            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        }
    }

    ngAfterViewInit(): void {

    }

    loadClientOrganization() {
        this.collectionsProcessing.org_id = this.organizationId;
        this.dataSource.loadCustomer(this.organizationId);
    }

    highlight(row) {
        this.selectedRowIndex = row.clientId;
        this.clientId = row.clientId;
        this.collectionsProcessing.client_id = this.clientId;
        if (this.organizationId) {
            console.log('organizationId => ' + this.organizationId);
            this.quickBookBalancingService.getCollectionsProcessing(this.organizationId, this.clientId).subscribe((httpResponse: HttpResponse<any>) => {
                console.log('Response: ' + httpResponse);
                const response = httpResponse.body;
                this.loadingComp = false;
                switch ('' + response.responseType) {
                    case 'TYPE_REQUEST':
                        this.collectionsProcessingOutput = JSON.parse(response.data);
                        this.mapCollectionProcessingToDataInput();
                        break;

                    default:
                        this.collectionsProcessingOutput = new CollectionsProcessingOutputData();
                        this.mapCollectionProcessingToDataInput();
                        break;
                }
            }, error => {
                console.log(error);
                this.dialog.closeAll();
                this.snackBar.open('Error get collection processing, check your network connection', 'OK', {
                    duration: 4000,
                });
                this.loadingComp = false;
                return;
            });
        }
    }

    applyToAllClientOnChanged() {
        if (this.applyToAllClient) {
            this.selectedRowIndex = -1;
            this.clientId = -1;
            this.collectionsProcessing.client_id = this.clientId;
        }
    }

    storeClientInfo() {
        this.handlePaidAgency();
        this.handleNsfAgency();
        this.collectionsProcessing.org_id = this.organizationId;
        this.collectionsProcessing.client_id = this.clientId;
        this.collectionsProcessing.cancel_status = 0;
        this.conmapDataInputToCollectionProcessing();
        console.log(JSON.stringify(this.collectionsProcessing));
        if (this.collectionsProcessing.org_id > 0) {
            this.quickBookBalancingService.saveCollectionsProcessing(this.collectionsProcessing, this.applyToAllClient).subscribe((httpResponse: HttpResponse<any>) => {
                const response = httpResponse.body;
                this.loadingComp = false;
                switch ('' + response.responseType) {
                    case 'TYPE_REQUEST':
                        this.snackBar.open('Collection processing save successfull: ', 'OK', {
                            duration: 4000,
                        });
                        break;

                    default:
                        this.loadingComp = false;
                        this.snackBar.open('Error save collections processing: ' + response.comment, 'OK', {
                            duration: 4000,
                        });
                        break;

                }
            }, error => {
                this.dialog.closeAll();
                this.snackBar.open('Error save collections processing, check your network connection', 'OK', {
                    duration: 4000,
                });
                this.loadingComp = false;
                return;
            });
        } else {
            this.snackBar.open('Error organization Id is empty or client Id is empty', 'OK', {
                duration: 4000,
            });
        }
    }

    handlePaidAgency() {
        switch (this.clientWirePA) {
            case 'Add Net':
                this.collectionsProcessing.pa_add_net = 1;
                this.collectionsProcessing.pa_add_gross = 0;
                break;
            case 'Add Gross':
                this.collectionsProcessing.pa_add_net = 0;
                this.collectionsProcessing.pa_add_gross = 1;
                break;
        }
        return;
    }

    handleNsfAgency() {
        switch (this.clientWireRA) {
            case 'Add Net':
                this.collectionsProcessing.ra_add_net = 1;
                this.collectionsProcessing.ra_add_gross = 0;
                this.collectionsProcessing.ra_add_fees = 0;
                this.collectionsProcessing.ra_subtract_gross = 0;
                this.collectionsProcessing.ra_subtract_net = 0;
                this.collectionsProcessing.ra_ar_subtract_fee = 0;
                this.collectionsProcessing.ra_do_nothing = 0;
                break;
            case 'Add Gross':
                this.collectionsProcessing.ra_add_net = 0;
                this.collectionsProcessing.ra_add_gross = 1;
                this.collectionsProcessing.ra_add_fees = 0;
                this.collectionsProcessing.ra_subtract_gross = 0;
                this.collectionsProcessing.ra_subtract_net = 0;
                this.collectionsProcessing.ra_ar_subtract_fee = 0;
                this.collectionsProcessing.ra_do_nothing = 0;
                break;
            case 'Add Fees':
                this.collectionsProcessing.ra_add_net = 0;
                this.collectionsProcessing.ra_add_gross = 0;
                this.collectionsProcessing.ra_add_fees = 1;
                this.collectionsProcessing.ra_subtract_gross = 0;
                this.collectionsProcessing.ra_subtract_net = 0;
                this.collectionsProcessing.ra_ar_subtract_fee = 0;
                this.collectionsProcessing.ra_do_nothing = 0;
                break;
            case 'Substract Gross':
                this.collectionsProcessing.ra_add_net = 0;
                this.collectionsProcessing.ra_add_gross = 0;
                this.collectionsProcessing.ra_add_fees = 0;
                this.collectionsProcessing.ra_subtract_gross = 1;
                this.collectionsProcessing.ra_subtract_net = 0;
                this.collectionsProcessing.ra_ar_subtract_fee = 0;
                this.collectionsProcessing.ra_do_nothing = 0;
                break;
            case 'Substract Net':
                this.collectionsProcessing.ra_add_net = 0;
                this.collectionsProcessing.ra_add_gross = 0;
                this.collectionsProcessing.ra_add_fees = 0;
                this.collectionsProcessing.ra_subtract_gross = 0;
                this.collectionsProcessing.ra_subtract_net = 1;
                this.collectionsProcessing.ra_ar_subtract_fee = 0;
                this.collectionsProcessing.ra_do_nothing = 0;
                break;
            case 'Substract Fees':
                this.collectionsProcessing.ra_add_net = 0;
                this.collectionsProcessing.ra_add_gross = 0;
                this.collectionsProcessing.ra_add_fees = 0;
                this.collectionsProcessing.ra_subtract_gross = 0;
                this.collectionsProcessing.ra_subtract_net = 0;
                this.collectionsProcessing.ra_ar_subtract_fee = 1;
                this.collectionsProcessing.ra_do_nothing = 0;
                break;
            case 'Do Nothing':
                this.collectionsProcessing.ra_add_net = 0;
                this.collectionsProcessing.ra_add_gross = 0;
                this.collectionsProcessing.ra_add_fees = 0;
                this.collectionsProcessing.ra_subtract_gross = 0;
                this.collectionsProcessing.ra_subtract_net = 0;
                this.collectionsProcessing.ra_ar_subtract_fee = 0;
                this.collectionsProcessing.ra_do_nothing = 1;
                break;
        }
        return;
    }

    checkRaArAddGross() {
        if (this.ra_ar_add_net || this.ra_ar_subtract_fee) {
            this.ra_ar_add_net = false;
            this.ra_ar_subtract_fee = false;
            this.snackBar.open(this.messageCheckingNsfAgency, 'OK', {
                duration: 4000,
            });
        }
    }

    checkRaArAddNet() {
        if (this.ra_ar_add_gross || this.ra_ar_subtract_fee) {
            this.ra_ar_add_gross = false;
            this.ra_ar_subtract_fee = false;
            this.snackBar.open(this.messageCheckingNsfAgency, 'OK', {
                duration: 4000,
            });
        }
    }


    checkRaArSubtractFee() {
        if (this.ra_ar_add_net || this.ra_ar_add_gross) {
            this.ra_ar_add_net = false;
            this.ra_ar_add_gross = false;
            this.snackBar.open(this.messageCheckingNsfAgency, 'OK', {
                duration: 4000,
            });
        }
    }

    conmapDataInputToCollectionProcessing() {
        this.collectionsProcessing.pa_add_gross = this.pa_add_gross ? 1 : 0;
        this.collectionsProcessing.pa_add_net = this.pa_add_net ? 1 : 0;
        this.collectionsProcessing.pa_subtract_tax = this.pa_subtract_tax ? 1 : 0;
        this.collectionsProcessing.pa_ar_add_fees = this.pa_ar_add_fees ? 1 : 0;
        this.collectionsProcessing.pa_ar_add_tax = this.pa_ar_add_tax ? 1 : 0;
        this.collectionsProcessing.ra_add_gross = this.ra_add_gross ? 1 : 0;
        this.collectionsProcessing.ra_add_net = this.ra_add_net ? 1 : 0;
        this.collectionsProcessing.ra_add_fees = this.ra_add_fees ? 1 : 0;
        this.collectionsProcessing.ra_subtract_gross = this.ra_subtract_gross ? 1 : 0;
        this.collectionsProcessing.ra_subtract_net = this.ra_subtract_net ? 1 : 0;
        this.collectionsProcessing.ra_subtract_fees = this.ra_subtract_fees ? 1 : 0;
        this.collectionsProcessing.ra_do_nothing = this.ra_do_nothing ? 1 : 0;
        this.collectionsProcessing.ra_add_tax = this.ra_add_tax ? 1 : 0;
        this.collectionsProcessing.ra_ar_add_net = this.ra_ar_add_net ? 1 : 0;
        this.collectionsProcessing.ra_ar_add_gross = this.ra_ar_add_gross ? 1 : 0;
        this.collectionsProcessing.ra_ar_subtract_fee = this.ra_ar_subtract_fee ? 1 : 0;
        this.collectionsProcessing.ra_ar_subtract_tax = this.ra_ar_subtract_tax ? 1 : 0;
        this.collectionsProcessing.pd_subtract_fee = this.pd_subtract_fee ? 1 : 0;
        this.collectionsProcessing.pd_subtract_tax = this.pd_subtract_tax ? 1 : 0;
        this.collectionsProcessing.pd_ar_add_fee = this.pd_ar_add_fee ? 1 : 0;
        this.collectionsProcessing.pd_ar_add_tax = this.pd_ar_add_tax ? 1 : 0;
        this.collectionsProcessing.rd_add_fee = this.rd_add_fee ? 1 : 0;
        this.collectionsProcessing.rd_add_tax = this.rd_add_tax ? 1 : 0;
        this.collectionsProcessing.rd_ar_subtract_fee = this.rd_ar_subtract_fee ? 1 : 0;
        this.collectionsProcessing.rd_ar_subtract_tax = this.rd_ar_subtract_tax ? 1 : 0;
        this.collectionsProcessing.cancel_status = this.cancel_status ? 1 : 0;
    }

    mapCollectionProcessingToDataInput() {
        this.pa_add_gross = this.collectionsProcessingOutput.pa_add_gross;
        this.pa_add_net = this.collectionsProcessingOutput.pa_add_net;
        this.pa_subtract_tax = this.collectionsProcessingOutput.pa_subtract_tax;
        this.pa_ar_add_fees = this.collectionsProcessingOutput.pa_ar_add_fees;
        this.pa_ar_add_tax = this.collectionsProcessingOutput.pa_ar_add_tax;
        this.ra_add_gross = this.collectionsProcessingOutput.ra_add_gross;
        this.ra_add_net = this.collectionsProcessingOutput.ra_add_net;
        this.ra_add_fees = this.collectionsProcessingOutput.ra_add_fees;
        this.ra_subtract_gross = this.collectionsProcessingOutput.ra_subtract_gross;
        this.ra_subtract_net = this.collectionsProcessingOutput.ra_subtract_net;
        this.ra_subtract_fees = this.collectionsProcessingOutput.ra_subtract_fees;
        this.ra_do_nothing = this.collectionsProcessingOutput.ra_do_nothing;
        this.ra_add_tax = this.collectionsProcessingOutput.ra_add_tax;
        this.ra_ar_add_net = this.collectionsProcessingOutput.ra_ar_add_net;
        this.ra_ar_add_gross = this.collectionsProcessingOutput.ra_ar_add_gross;
        this.ra_ar_subtract_fee = this.collectionsProcessingOutput.ra_ar_subtract_fee;
        this.ra_ar_subtract_tax = this.collectionsProcessingOutput.ra_ar_subtract_tax;
        this.pd_subtract_fee = this.collectionsProcessingOutput.pd_subtract_fee;
        this.pd_subtract_tax = this.collectionsProcessingOutput.pd_subtract_tax;
        this.pd_ar_add_fee = this.collectionsProcessingOutput.pd_ar_add_fee;
        this.pd_ar_add_tax = this.collectionsProcessingOutput.pd_ar_add_tax;
        this.rd_add_fee = this.collectionsProcessingOutput.rd_add_fee;
        this.rd_add_tax = this.collectionsProcessingOutput.rd_add_tax;
        this.rd_ar_subtract_fee = this.collectionsProcessingOutput.rd_ar_subtract_fee;
        this.rd_ar_subtract_tax = this.collectionsProcessingOutput.rd_ar_subtract_tax;
        this.cancel_status = this.collectionsProcessingOutput.cancel_status;
    }

    goToCreateReport() {
        this.router.navigate(['/quickbookbalacing-print']);
    }

    printProcInstr() {
        this.quickBookBalancingService.printProcInstr().subscribe((httpResponse: HttpResponse<any>) => {
            console.log('Response: ' + httpResponse);
            const response = httpResponse.body;
            this.loading = false;

            switch ('' + response.responseType) {
                case 'TYPE_REQUEST':
                    const blob = this.base64ToBlob(response.data, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    this.ffS.save(blob, Date.now() + '_split.xlsx');

                    this.snackBar.open('Open the file', 'OK', {
                        duration: 4000,
                    });

                    break;

                default:
                    this.snackBar.open('An error occured: ' + response.comment, 'OK', {
                        duration: 4000,
                    });
                    break;

            }

        }, error => {
            this.loading = false;

            console.log(error.json());
            this.snackBar.open('Error sending request', 'OK', {
                duration: 4000,
            });
        });
    }

    public base64ToBlob(b64Data, contentType= '', sliceSize= 512) {
        b64Data = b64Data.replace(/\s/g, ''); // For IE compatibility.
        const byteCharacters = atob(b64Data);
        const byteArrays = [];
        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            const slice = byteCharacters.slice(offset, offset + sliceSize);

            const byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            const byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        return new Blob(byteArrays, {type: contentType});
    }
}
