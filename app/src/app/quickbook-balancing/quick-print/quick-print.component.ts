import {Component, OnInit} from '@angular/core';
import {User} from '../../dto/user';
import {QuickBookBalancingService} from '../../services/quickbook-balancing/quick-book-balancing.service';
import {HttpResponse} from '@angular/common/http';
import {MatDialog, MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {CheckBoxModel} from '../../dto/checkbox-model';
import {QuickBookBalancingReportInputData} from '../../dto/quick-book-balancing-report-input';
import {BlobUtils} from '../../utils/blob-utils';
import {FileSaverService} from 'ngx-filesaver';

@Component({
  selector: 'app-quick-print',
  templateUrl: './quick-print.component.html',
  styleUrls: ['./quick-print.component.scss']
})
export class QuickPrintComponent implements OnInit {

  companiesLoaded = true;
  loading = false;
  loadingComp = true;
  currentUser: User;
  companies = [];
  iWantToType = [];
  dataSelectionRangeType = [];
  allOrganizations = false;
  organizationId = 0;

  startDate: string;
  stopDate: string;
  quickBookBalancingReportInputData: QuickBookBalancingReportInputData = new QuickBookBalancingReportInputData;

  constructor(public dialog: MatDialog,
              private router: Router,
              private snackBar: MatSnackBar,
              private quickBookBalancingService: QuickBookBalancingService,
              private ffS: FileSaverService) {
    this.initCheckBoxModel();
    this.quickBookBalancingService.getOrganizationList().subscribe((httpResponse: HttpResponse<any>) => {
      console.log('Response: ' + httpResponse);
      const response = httpResponse.body;
      this.loadingComp = false;
      switch ('' + response.responseType) {
        case 'TYPE_REQUEST':
          this.companies = JSON.parse(response.data);
          break;

        default:
          this.loadingComp = false;
          this.snackBar.open('Error loading organization list: ' + response.comment, 'OK', {
            duration: 4000,
          });
          break;

      }
    }, error => {
      this.dialog.closeAll();
      this.snackBar.open('Error loading organization list, check your network connection', 'OK', {
        duration: 4000,
      });
      this.loadingComp = false;
      return;
    });
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser') == null || localStorage.getItem('currentUser') === undefined) {
      this.currentUser = new User();
      this.router.navigate(['/login']);
    } else {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }
  }

  initCheckBoxModel() {
    const printAllCheck = new CheckBoxModel();
    printAllCheck.index = 1;
    printAllCheck.value = 'Print all collections between dates';

    const dateSelectionRangeCheck = new CheckBoxModel();
    dateSelectionRangeCheck.index = 2;
    dateSelectionRangeCheck.value = 'Select client statements created between dates';

    const printDifferenceCheck = new CheckBoxModel();
    printDifferenceCheck.index = 3;
    printDifferenceCheck.value = 'Print Differences';

    this.iWantToType[0] = printAllCheck;
    this.iWantToType[1] = dateSelectionRangeCheck;
    this.iWantToType[2] = printDifferenceCheck;

    const showSummaryCheck = new CheckBoxModel();
    showSummaryCheck.index = 1;
    showSummaryCheck.value = 'Show Sumary';

    const showDetailCheck = new CheckBoxModel();
    showDetailCheck.index = 2;
    showDetailCheck.value = 'Show Detail';

    this.dataSelectionRangeType[0] = showSummaryCheck;
    this.dataSelectionRangeType[1] = showDetailCheck;
  }

  difference() {
  }

  loadClientOrganization() {
    if (this.quickBookBalancingReportInputData.allOrganizations && this.quickBookBalancingReportInputData.organizationId > 0) {
      this.quickBookBalancingReportInputData.allOrganizations = false;
    }
  }

  allOrganizationsChanged() {
    if (this.quickBookBalancingReportInputData.allOrganizations) {
      this.quickBookBalancingReportInputData.organizationId = 0;
    }
  }

  createReport() {
    if (this.missingData()) {
      this.snackBar.open('Please supply the missing information', 'OK', {
        duration: 4000,
      });
    } else if (this.reportDatesOutOfSequence()) {
      this.snackBar.open('The Start date cannot be greater than the stop date', 'OK', {
        duration: 4000,
      });
    } else {
      this.quickBookBalancingService.createReport(this.quickBookBalancingReportInputData).subscribe((httpResponse: HttpResponse<any>) => {
        console.log('Response: ' + httpResponse);
        const response = httpResponse.body;
        this.loading = false;

        switch ('' + response.responseType) {
          case 'TYPE_REQUEST':
            const blob = BlobUtils.base64ToBlob(response.data, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            this.ffS.save(blob, Date.now() + '_split.xlsx');

            this.snackBar.open('Open the file', 'OK', {
              duration: 4000,
            });

            break;

          default:
            this.snackBar.open('An error occured: ' + response.comment, 'OK', {
              duration: 4000,
            });
            break;

        }

      }, error => {
        this.dialog.closeAll();
        this.snackBar.open('Error sending request', 'OK', {
          duration: 4000,
        });
        return;
      });
    }
  }

  goToCreateOffset() {
    this.router.navigate(['/quickbookbalacing-offset']);
  }

  missingData() {
    if (this.quickBookBalancingReportInputData.iWantTo !== 1 && this.quickBookBalancingReportInputData.iWantTo !== 2) {
      return true;
    }

    if (this.quickBookBalancingReportInputData.dataSelectionRange !== 1 && this.quickBookBalancingReportInputData.dataSelectionRange !== 2) {
      return true;
    }
    return !this.quickBookBalancingReportInputData.allOrganizations && this.quickBookBalancingReportInputData.organizationId === 0;
  }

  reportDatesOutOfSequence() {
    return this.startDate > this.stopDate;
  }
}
