import {Component, OnInit} from '@angular/core';
import {User} from '../../dto/user';
import {MatDialog, MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {QuickBookBalancingService} from '../../services/quickbook-balancing/quick-book-balancing.service';
import {HttpResponse} from '@angular/common/http';
import {OffsetInputData} from '../../dto/offset-input';

@Component({
    selector: 'app-quick-offset',
    templateUrl: './quick-offset.component.html',
    styleUrls: ['./quick-offset.component.scss']
})
export class QuickOffsetComponent implements OnInit {

    offset: OffsetInputData = new OffsetInputData();

    companiesLoaded = true;
    loading = false;
    loadingComp = true;
    currentUser: User;
    organizations = [];
    clients = [];

    organizationId = 0;
    clientId = 0;
    selectedRowIndex = -1;

    displayedColumns = ['client', 'date', 'wireOffset', 'arOffset', 'creator'];

    constructor(public dialog: MatDialog,
                private router: Router,
                private snackBar: MatSnackBar,
                private quickBookBalancingService: QuickBookBalancingService) {
        this.quickBookBalancingService.getOrganizationList().subscribe((httpResponse: HttpResponse<any>) => {
            console.log('Response: ' + httpResponse);
            const response = httpResponse.body;
            this.loadingComp = false;
            switch ('' + response.responseType) {
                case 'TYPE_REQUEST':
                    this.organizations = JSON.parse(response.data);
                    break;

                default:
                    this.loadingComp = false;
                    this.snackBar.open('Error loading organization list: ' + response.comment, 'OK', {
                        duration: 4000,
                    });
                    break;

            }
        }, error => {
            this.dialog.closeAll();
            this.snackBar.open('Error loading organization list, check your network connection', 'OK', {
                duration: 4000,
            });
            this.loadingComp = false;
            return;
        });
    }

    ngOnInit() {
        if (localStorage.getItem('currentUser') == null || localStorage.getItem('currentUser') === undefined) {
            this.currentUser = new User();
            this.router.navigate(['/login']);
        } else {
            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        }
    }

    loadClientOrganization() {
        this.quickBookBalancingService.getClientByOrganization(this.organizationId).subscribe((httpResponse: HttpResponse<any>) => {
            console.log('Response: ' + httpResponse);
            const response = httpResponse.body;
            this.loadingComp = false;
            switch ('' + response.responseType) {
                case 'TYPE_REQUEST':
                    this.clients = JSON.parse(response.data);
                    break;

                default:
                    this.loadingComp = false;
                    this.snackBar.open('Error loading client list: ' + response.comment, 'OK', {
                        duration: 4000,
                    });
                    break;

            }
        }, error => {
            this.dialog.closeAll();
            this.snackBar.open('Error loading client list, check your network connection', 'OK', {
                duration: 4000,
            });
            this.loadingComp = false;
            return;
        });
    }

    saveOffset() {
        if (this.verifyDataOffset()) {
            this.offset.org_id = this.organizationId;
            this.offset.client_id = this.clientId;
            this.offset.created_by = this.currentUser && this.currentUser.username;
            this.offset.cancel_status = 0;
            console.log(JSON.stringify(this.offset));
            this.quickBookBalancingService.saveOffset(this.offset).subscribe((httpResponse: HttpResponse<any>) => {
                const response = httpResponse.body;
                this.loadingComp = false;
                switch ('' + response.responseType) {
                    case 'TYPE_REQUEST':
                        this.snackBar.open('Offset save successfull: ', 'OK', {
                            duration: 4000,
                        });
                        break;

                    default:
                        this.loadingComp = false;
                        this.snackBar.open('Error save offset: ' + response.comment, 'OK', {
                            duration: 4000,
                        });
                        break;

                }
            }, error => {
                this.dialog.closeAll();
                this.snackBar.open('Error save offset, check your network connection', 'OK', {
                    duration: 4000,
                });
                this.loadingComp = false;
                return;
            });
        }
    }

    verifyDataOffset() {
        let isOk = true;
        console.log('this.clientId => ' + this.clientId);
        console.log('this.organizationId => ' + this.organizationId);
        console.log('this.offset.wire_offset_amt => ' + this.offset.wire_offset_amt);
        console.log('this.offset.ar_offset_amt => ' + this.offset.ar_offset_amt);
        if (this.clientId < 1 || this.organizationId < 1) {
            isOk = false;
            this.snackBar.open('Please select an organization and client', 'OK', {
                duration: 4000,
            });
        } else if (this.offset.wire_offset_amt === ''
            || this.conditionMoney(this.offset.wire_offset_amt) === '' || isNaN(Number(this.conditionMoney(this.offset.wire_offset_amt)))
            || this.offset.ar_offset_amt === ''
            || this.conditionMoney(this.offset.ar_offset_amt) === '' || isNaN(Number(this.conditionMoney(this.offset.ar_offset_amt)))) {
            isOk = false;
            this.snackBar.open('This value must be a number', 'OK', {
                duration: 4000,
            });
        }
        return isOk;
    }

    conditionMoney(amt) {
        console.log('amt => ' + amt);
        let negative = false;
        if (amt.trim().startsWith('-')) {
            negative = true;
        }
        let temp = '';

        for (let i = 0; i < amt.length; i++) {
            if (!isNaN(Number(amt.substring(i, 1))) || amt.substring(i, 1) === '.') {
                temp += amt.substring(i, 1);
            }
        }

        temp = temp.replace('..', '.');
        while (!temp.indexOf('..')) {
            temp = temp.replace('..', '.');
        }

        if (negative) {
            temp = '-' + temp;
        }
        return temp;
    }

}
