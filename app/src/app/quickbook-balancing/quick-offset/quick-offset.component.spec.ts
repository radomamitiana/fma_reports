import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickOffsetComponent } from './quick-offset.component';

describe('QuickOffsetComponent', () => {
  let component: QuickOffsetComponent;
  let fixture: ComponentFixture<QuickOffsetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickOffsetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickOffsetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
