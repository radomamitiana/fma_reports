import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { RestService } from '../rest.service';
import { FileSaverService } from 'ngx-filesaver';
import { User } from '../dto/user';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-health-result',
  templateUrl: './health-result.component.html',
  styleUrls: ['./health-result.component.scss']
})
export class HealthResultComponent implements OnInit {

  currentUser: User;
  mustComment = false;
  loading = false;


  options = new FormGroup({
    comm1: new FormControl('', Validators.required),
    comm2: new FormControl('', Validators.required),
    comm3: new FormControl('', Validators.required),
    comm4: new FormControl('', Validators.required),
  });

  constructor(public dialog: MatDialog,
              private router: Router,
              private snackBar: MatSnackBar,
              private restService: RestService,
              private ffS: FileSaverService
    ) { }

ngOnInit() {

this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
}

seeFiles() {
const base64Trust = JSON.parse(localStorage.getItem('currentHealth')) as string[];
this.mustComment = true;
// const blob1 = this.base64ToBlob(base64Trust[1], 'application/vnd.openxmlformats-officedocument.presentationml.presentation');
const blob2 = this.base64ToBlob(base64Trust[0], 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
// this.ffS.save(blob1, Date.now() + '_health.pptx');
this.ffS.save(blob2, Date.now() + 'health.xlsx');

this.snackBar.open('Open the files', 'OK', {
duration: 4000,
});
// this.location.back();

}

comm() {
  if (this.options.valid) {
    console.log('OK');
    this.loading = true;
    const comms = [
                    this.options.get('comm1').value,
                    this.options.get('comm2').value,
                    this.options.get('comm3').value,
                    this.options.get('comm4').value,

                  ];
    this.restService.getCommentedHealth(comms).subscribe((httpResponse: HttpResponse<any>) => {
      console.log('Response: ' + httpResponse);
      const response = httpResponse.body;
      this.loading = false;
      switch ('' + response.responseType) {
        case 'TYPE_REQUEST':
        const blob = this.base64ToBlob(response.data, 'application/vnd.openxmlformats-officedocument.presentationml.presentation');
        this.ffS.save(blob, Date.now() + 'commented_health.pptx');
        this.snackBar.open('Open the file', 'OK', {
          duration: 4000,
          });
        break;

        default:
        this.snackBar.open('An error occured: ' + response.comment, 'OK', {
          duration: 4000,
        });
        break;

      }

    }, error => {
      this.loading = false;

      console.log(error.json());
      this.snackBar.open('Error sending request', 'OK', {
        duration: 4000,
      });
    });

/*       setTimeout( () => {
      this.loading = false;
      this.router.navigate(['/trustresult']);

  }, 5000); */
  } else {
    this.snackBar.open('Fill all required fields', 'OK', {
      duration: 4000,
    });
  }

}


public base64ToBlob(b64Data, contentType= '', sliceSize= 512) {
b64Data = b64Data.replace(/\s/g, ''); // For IE compatibility.
const byteCharacters = atob(b64Data);
const byteArrays = [];
for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
const slice = byteCharacters.slice(offset, offset + sliceSize);

const byteNumbers = new Array(slice.length);
for (let i = 0; i < slice.length; i++) {
  byteNumbers[i] = slice.charCodeAt(i);
}
const byteArray = new Uint8Array(byteNumbers);
byteArrays.push(byteArray);
}
return new Blob(byteArrays, {type: contentType});
}

}
