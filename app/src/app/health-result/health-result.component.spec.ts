import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthResultComponent } from './health-result.component';

describe('HealthResultComponent', () => {
  let component: HealthResultComponent;
  let fixture: ComponentFixture<HealthResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
