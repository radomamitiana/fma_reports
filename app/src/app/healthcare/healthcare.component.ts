import { Component, OnInit } from '@angular/core';
import { User } from '../dto/user';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { RestService } from '../rest.service';
import { HttpResponse } from '@angular/common/http';
import { HealthCareInputData } from '../dto/healthcare';
import { FileSaverService } from 'ngx-filesaver';
import { Organization } from '../dto/organization';

@Component({
  selector: 'app-healthcare',
  templateUrl: './healthcare.component.html',
  styleUrls: ['./healthcare.component.scss']
})
export class HealthcareComponent implements OnInit {

  healthInput: HealthCareInputData = new HealthCareInputData();
  companiesLoaded = true;
  loading = false;
  loadingComp = true;
  currentUser: User;
  companies: Organization[] = [];

options = new FormGroup({
  comp: new FormControl('', Validators.required),
  startDate: new FormControl('', Validators.required),
  endDate: new FormControl('', Validators.required),
  showClients: new FormControl(),
});

constructor(
  public dialog: MatDialog,
  private router: Router,
  private snackBar: MatSnackBar,
  private restService: RestService,
  private ffS: FileSaverService,
  ) {
    this.restService.getCompListHealth().subscribe((httpResponse: HttpResponse<any>) => {
      console.log('Response: ' + httpResponse);
      const response = httpResponse.body;
      this.loadingComp = false;
      switch ('' + response.responseType) {
        case 'TYPE_REQUEST':
        this.companies = JSON.parse(response.data);
        console.log(this.companies);
        break;

        default:
        this.loadingComp = false;
        this.snackBar.open('Error loading organization list: ' + response.comment, 'OK', {
          duration: 4000,
        });
        break;

      }
    }, error => {
      console.log(error);
      this.dialog.closeAll();
      this.snackBar.open('Error loading organization list, check your network connection', 'OK', {
        duration: 4000,
      });
      this.loadingComp = false;
      return;
    });

  }


  ngOnInit() {
    /* if (localStorage.getItem('currentUser') == null || localStorage.getItem('currentUser') === undefined) {
    this.currentUser = new User();
    this.router.navigate(['/login']);
    } else {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    } */
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    }



    run() {
      if (this.options.valid) {
        console.log('OK');
        console.log(this.healthInput);
        console.log(JSON.stringify(this.healthInput));
        this.loading = true;
        this.restService.getHealth(this.healthInput).subscribe((httpResponse: HttpResponse<any>) => {
          console.log('Response: ' + httpResponse);
          const response = httpResponse.body;
          this.loading = false;
          switch ('' + response.responseType) {
            case 'TYPE_REQUEST':
            localStorage.setItem('currentHealth', response.data);
            this.router.navigate(['/healthresult']);

            break;

            default:
            this.snackBar.open('An error occured: ' + response.comment, 'OK', {
              duration: 4000,
            });
            break;

          }

        }, error => {
          this.loading = false;

          console.log(error.json());
          this.snackBar.open('Error sending request', 'OK', {
            duration: 4000,
          });
        });

  /*       setTimeout( () => {
          this.loading = false;
          this.router.navigate(['/trustresult']);

      }, 5000); */
      } else {
        this.snackBar.open('Fill all required fields', 'OK', {
          duration: 4000,
        });
      }
    }



}
