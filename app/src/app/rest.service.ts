import { SplitTimeReportInputData } from './dto/split';
import { HealthCareInputData } from './dto/healthcare';
import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders} from '@angular/common/http';
import { ApiResponse } from './dto/api-response';
import { WsResponseType } from './dto/ws-response-type';
import { User } from './dto/user';
import { TrustBalancingInputData } from './dto/trust';
import { isNull } from 'util';


@Injectable({
  providedIn: 'root'
})
export class RestService {


  /* URL MAPPING */
  serverBaseURL = 'http://localhost:9095';
  loginUserUrl = this.serverBaseURL + '/ws/loginBackOfficeAndReportsApp';
  logoutUserUrl = this.serverBaseURL + '/ws/auth/logoutBackOfficeAndReportsApp';
  getTrustUrl = this.serverBaseURL + '/ws/auth/getTrustBalancingData';
  getCompaniesUrl = this.serverBaseURL + '/ws/auth/getOrganizationList';
  getHealthCompaniesUrl = this.serverBaseURL + '/ws/auth/getHealthOrganizationList';
  getOptionsUrl = this.serverBaseURL + '/ws/auth/getOptionsList';
  getHealthUrl = this.serverBaseURL + '/ws/auth/getHealthcareData';
  getSplitUrl = this.serverBaseURL + '/ws/auth/getSplitTimeReportData';
  getCommHealthUrl = this.serverBaseURL + '/ws/auth/getCommentedHealthData';
 // newRepport2Link = this.serverBaseURL + '/ws/auth/getCommentedHealthData';
  user: User;
  apiResponse: ApiResponse;
  private headers: HttpHeaders;
  constructor(public http: HttpClient) {
      this.apiResponse = new ApiResponse();
   }

  getCurrentUser(): User {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  getUserCurToken(): string {
    return localStorage.getItem('currentAuthToken')  || '';
  }


  createAuthorizationHeader(): HttpHeaders {
    // if (isNull(this.headers)) {
     const headers = new HttpHeaders()
        .set('Content-Type', 'application/json; charset=utf-8')
        .set('Authorization', 'Bearer ' + this.getUserCurToken())
        .set('Cache-control', 'no-cache')
        .set('Pragma', 'no-cache');
     this.headers = headers;
    // }

     return this.headers;
}



createAuthorizationHeaderM(): HttpHeaders {
 // if (isNull(this.headers)) {
  const headers = new HttpHeaders()
     .set('Content-Type', 'multipart/form-data; charset=utf-8')
     .set('Authorization', 'Bearer ' + this.getUserCurToken())
     .set('Cache-control', 'no-cache')
     .set('Pragma', 'no-cache');

  this.headers = headers;
 // }
  return this.headers;
}




getCompList() {
  return this.http.get(this.getCompaniesUrl + ':' + this.getCurrentUser().sessionId,
   {headers: this.createAuthorizationHeader(), observe: 'response'});
}


getCompListHealth() {
  return this.http.get(this.getHealthCompaniesUrl + ':' + this.getCurrentUser().sessionId,
   {headers: this.createAuthorizationHeader(), observe: 'response'});
}



/* getNRepport2() {
  return this.http.get(this.newRepport2Link + ':' + this.getCurrentUser().sessionId,
   {headers: this.createAuthorizationHeader(), observe: 'response'});
}
 */

  getUserOptionList() {
    return this.http.get(this.getOptionsUrl + ':' + this.getCurrentUser().sessionId
    , {headers: this.createAuthorizationHeader(), observe: 'response'});
  }


 /*  .subscribe((response: ApiResponse) => {
    console.log('Response: ' + response);
    this.apiResponse = response;
    switch (this.apiResponse.responseType) {
      case WsResponseType.TYPE_REQUEST:
      return this.apiResponse.data;
      case WsResponseType.TYPE_EVENT:
      return;

      default:
      return false;

    }
  }); */

  getTrust(trust: TrustBalancingInputData) {

    return this.http.post(this.getTrustUrl + ':' + this.getCurrentUser().sessionId,
    JSON.stringify(trust), {headers: this.createAuthorizationHeader(), observe: 'response'});
 }


 getHealth(health: HealthCareInputData) {

  return this.http.post(this.getHealthUrl + ':' + this.getCurrentUser().sessionId,
  JSON.stringify(health), {headers: this.createAuthorizationHeader(), observe: 'response'});

}


getSplit(splittime: SplitTimeReportInputData) {
  return this.http.post(this.getSplitUrl + ':' + this.getCurrentUser().sessionId,
  JSON.stringify(splittime), {headers: this.createAuthorizationHeader(), observe: 'response'});

}

getCommentedHealth(data: string[]) {

  return this.http.post(this.getCommHealthUrl + ':' + this.getCurrentUser().sessionId,
  JSON.stringify(data), {headers: this.createAuthorizationHeader(), observe: 'response'});

}


  loginUser(username: string, password: string) {
      this.user = new User();
      this.user.username = username;
      this.user.password = password;
      return this.http.post(this.loginUserUrl, JSON.stringify(this.user), {headers: this.createAuthorizationHeader(), observe: 'response'});
   }


   logoutUser() {
    this.user = this.getCurrentUser();
    return this.http.delete(this.logoutUserUrl + ':' + this.getCurrentUser().sessionId,
    {headers: this.createAuthorizationHeader(), observe: 'response'});

 }
}
