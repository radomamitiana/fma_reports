import { SplitTimeComponent } from './split-time/split-time.component';
import { HealthResultComponent } from './health-result/health-result.component';
import { HealthcareComponent } from './healthcare/healthcare.component';
import { LoginComponent } from './login/login.component';
import { TrustbalancingComponent } from './trustbalancing/trustbalancing.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TrustResultComponent } from './trust-result/trust-result.component';
import { NeedAuthGuardGuard } from './need-auth-guard.guard';
import {QuickHomeComponent} from './quickbook-balancing/home/quick.home.component';
import {QuickPrintComponent} from './quickbook-balancing/quick-print/quick-print.component';
import {QuickOffsetComponent} from './quickbook-balancing/quick-offset/quick-offset.component';
const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent,  canActivate: [NeedAuthGuardGuard] },
  { path: 'trustbalancing', component: TrustbalancingComponent, canActivate: [NeedAuthGuardGuard] },
  { path: 'quickbookbalancing', component: QuickHomeComponent, canActivate: [NeedAuthGuardGuard] },
  { path: 'quickbookbalacing-print', component: QuickPrintComponent, canActivate: [NeedAuthGuardGuard] },
  { path: 'quickbookbalacing-offset', component: QuickOffsetComponent, canActivate: [NeedAuthGuardGuard] },
  { path: 'trustresult', component: TrustResultComponent, canActivate: [NeedAuthGuardGuard]},
  { path: 'healthcare', component: HealthcareComponent, canActivate: [NeedAuthGuardGuard] },
  { path: 'healthresult', component: HealthResultComponent, canActivate: [NeedAuthGuardGuard] },
  { path: 'splittime', component: SplitTimeComponent, canActivate: [NeedAuthGuardGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
