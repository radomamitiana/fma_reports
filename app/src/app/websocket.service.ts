import { Injectable } from '@angular/core';
import { webSocket } from 'rxjs/webSocket';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  serverBaseURL = 'localhost:9095';

  subject = webSocket('ws://' + this.serverBaseURL + '/report_msg/');

  constructor() {

   }

   subscribe(dialog: MatDialog, router: Router ) {
    this.subject.subscribe(
      msg => {
        console.log('Msg: ' + msg);
      },
      err => {
        console.log(err);
      },
      () => {
        console.log('DECONNECTING...');
        localStorage.clear();
        dialog.closeAll();
        close();
        router.navigate(['login/']);
      }
    );

  }

  sendMessage(msg: string) {
    this.subject.next(msg);
  }

  close() {
    this.subject.unsubscribe();
    this.subject.complete();
  }
}
